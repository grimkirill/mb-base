<?php

class groups extends View_Page_Controller {
	public $title = 'Управление группами пользователей';
	public $category = 'admin';
	
	function index()
	{
		$this->roll();
	}
	
	function roll()
	{
		if (!ACL::has_perm(ACL::acl_admin_group_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'add'), 
											'edit_link' => CLASSPATH.'edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('name', 'Название', FALSE, '130px');
		$table->AddHead('count', 'Пользователей');
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT * FROM  `users_groups`')->get_rows_array())
		{
			foreach ($res as $key => $item)
			{
				$res[$key]['count'] = $db->Query('SELECT COUNT(*) as count FROM  `users` where gid='.$item['id'])->get_next_row_object()->count;
			}
			$table->AddData($res);
		}
		$this->AddContent($table);
	}
	
	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_admin_group_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `users_groups` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$form = new View_Form(array('caption' => 'Редактировать пользователя', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название группы', 'items' => array(
							new View_Form_Text('name', $data->name, array('caption' => 'Название группы', 'validators'=>array( View_Form_Element::validator_require => 1)))
							))
			) ));
			
			$acl_data = $db->Query('SELECT acl, value FROM users_acls WHERE gid = '.$db->EscapeValue($id))->get_rows_array_id('acl', 'value');
			
			foreach (ACL::get_list() as $title => $acls)
			{
				$items = array();
				foreach ($acls as $name => $caption)
				{
					$on_off = FALSE;
					if (isset($acl_data[$name]))
					{
						$on_off = $acl_data[$name];
					}
					$items[] = new View_Form_Checkbox($name, $on_off, array('caption' => $caption));
				}
				$form->add($title, $items);
			}
			
			if ($form->get_values())
			{
				$save = FALSE;
				
				if ($db->UpdateData('users_groups', array('name' => $form->return_values['name']), array('id' => $id)))
				{
					if ($db->GetAffectedRows() > 0)
					{
						$save = TRUE;
					}
				}
				
				foreach (ACL::get_list() as $title => $acls)
				{
					foreach ($acls as $name => $caption)
					{
						if (isset($acl_data[$name]))
						{
							$db->UpdateData('users_acls', array('value' => $form->return_values[$name]), array('gid'=>$id, 'acl' => $name));
						}
						else
						{
							$db->InsertData('users_acls', array('gid'=>$id, 'acl' => $name, 'value' => $form->return_values[$name]));
						}
						
						if ($db->GetAffectedRows() > 0)
						{
							$save = TRUE;
						}
					}

				}
				
				if ($save)
				{
					$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
				}
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
	function add()
	{
		if (!ACL::has_perm(ACL::acl_admin_group_add)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('caption' => 'Редактировать пользователя', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название группы', 'items' => array(
							new View_Form_Text('name', '', array('caption' => 'Название группы', 'validators'=>array( View_Form_Element::validator_require => 1)))
							))
			) ));
		if ($form->get_values())
		{
			if ($db->InsertData('users_groups', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Группа добавлена.') );
				}
			}
			$this->roll();
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
}
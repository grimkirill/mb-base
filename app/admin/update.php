<?php

class update extends View_Page_Controller {
	public $title = 'Обновление системы';
	public $category = 'admin';
	public $host_url = 'http://support.noadmin.ru/';
	
	function index()
	{
		if (!ACL::has_perm(ACL::acl_admin_update)) { $this->AddContent(ACL::deny()); return ; }
		$server_ver = (int)file_get_contents($this->host_url.'ver.txt');
		if ($server_ver > VERSION)
		{
			$this->AddContent(View_Element::PanelNotice('Доступная новая версия: '.$server_ver.' текущая версия: '.VERSION.' рекмондуется обновить до последней. '.View_Element::Button('Обновить', CLASSPATH.'do_update')));
		}
		else
		{
			$this->AddContent(View_Element::PanelInfo('У вас установлена последняя версия'));
		}
		
	}
	
	function do_update()
	{
		if (!ACL::has_perm(ACL::acl_admin_update)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if (!file_put_contents(BASEPATH.'base.tar', file_get_contents($this->host_url.'base.tar')))
		{
			$this->AddContent(View_Element::PanelError('Не удалось скачать обновление'));
			return;
		}
		else {
			chmod(BASEPATH.'base.tar', 0666);
		}
		$sql = file_get_contents($this->host_url.'get_sql.php?ver='.VERSION);
		$sql_array = explode("\n",$sql);
		foreach ($sql_array as $item)
		{
			if ($item)
			{
				$db->SimpleQuery($item);
			}
		}
		
		$this->AddContent(exec("tar -xzf '".BASEPATH.'base.tar'."' -C '".BASEPATH."'"));
		unlink(BASEPATH.'base.tar');
		
		$this->AddContent(View_Element::PanelSuccess('Успешно обновлено.'));
		
	}
	
}
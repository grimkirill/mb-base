<?php

class users extends View_Page_Controller {
	public $title = 'Управление пользователями';
	public $category = 'admin';
	
	function index()
	{
		$this->roll();
	}
	
	function roll()
	{
		if (!ACL::has_perm(ACL::acl_admin_user_roll)) { $this->AddContent(ACL::deny()); return ; }
		
		global $db;	
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'add'), 
											'edit_link' => CLASSPATH.'edit/'));
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('username', 'Имя', FALSE, '100px');
		$table->AddHead('name', 'Группа', FALSE, '130px');
		$table->AddHead('fio', 'ФИО' );
		$table->AddHead('date', 'Вход' );
		$table->AddHead('block', 'Блокировка', View_Table::column_type_bool_inverse);
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT `users`.*, users_groups.name as name FROM `users`, users_groups WHERE users.gid = users_groups.id ORDER BY users.id'))
		{
			$data = $res->get_rows_array();
			foreach ($data as $key => $item)
			{
				$data[$key]['username'] = View_Element::Link($item['username'],CLASSPATH.'rolluser/'.$item['id'] );
			}
			$table->AddData($data);
		}
		
		
		$this->AddContent($table);
	}
	
	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_admin_user_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `users` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$form = new View_Form(array('caption' => 'Редактировать пользователя', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'ФИО', 'items' => array(
							new View_Form_Text('fio', $data->fio, array('caption' => 'Фамилия', 'validators'=>array( View_Form_Element::validator_require => 1)))
							)),
							array('caption' => 'Логин', 'items' => array(
								new View_Form_Text('username', $data->username, array('caption' => 'Логин', 'validators'=>array( View_Form_Element::validator_require => 1))),
								' Пароль: ',new View_Form_Password('password', '',array('caption' => 'Пароль')), ' укажите новый пароль для изменения.'							 
							)),
							array('caption' => 'Группа', 'items' => array(
								new view_form_select_usergroup('gid', $data->gid)							 
							)),
							array('caption' => 'Доступ', 'items' => array(
								new View_Form_Text('allowip', $data->allowip, array('caption' => 'разрешенный ip', 'validators'=>array( View_Form_Element::validator_require => 1))),
								' "/.*/" - с любого ip'							 
							)),
							array('caption' => '', 'items' => array(
								new View_Form_Checkbox('block', $data->block, array('caption' => 'Доступ запрещен'))
							))		
			) ));
			

			if ($form->get_values())
			{
				if (trim($form->return_values['password']) != '')
				{
					if ($db->UpdateData('users', array('password' => md5(trim($form->return_values['password']))), array('id' => $id)))
					{
						$this->AddContent(View_Element::PanelSuccess('Пароль изменен.') );
					}	
				}
				
				unset($form->return_values['password']);
				
				if ($db->UpdateData('users', $form->return_values, array('id' => $id)))
				{
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
					}
				}
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
	function rolluser($id)
	{
		if (!ACL::has_perm(ACL::acl_admin_user_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `users` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$table = new View_Table(array('caption' => View_Element::Link($data->fio, CLASSPATH.'roll')));
			$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
			$table->AddHead('ip', 'IP', FALSE, '200px');
			$table->AddHead('dt', 'Дата');
			
			if ($res = $db->Query('SELECT *  FROM `users_logon` WHERE `user` = '.$db->EscapeValue($id).' ORDER BY `id` DESC LIMIT 100'))
			{
				$table->AddData($res->get_rows_array());
			}
			$this->AddContent($table);
		}
	}
	
	
	function add()
	{
		if (!ACL::has_perm(ACL::acl_admin_user_add)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('caption' => 'Редактировать пользователя', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'ФИО', 'items' => array(
							new View_Form_Text('fio', '', array('caption' => 'Фамилия', 'validators'=>array( View_Form_Element::validator_require => 1)))
							)),
							array('caption' => 'Логин', 'items' => array(
								new View_Form_Text('username', '', array('caption' => 'Логин', 'validators'=>array( View_Form_Element::validator_require => 1))),
								' Пароль: ',new View_Form_Password('password', '',array('caption' => 'Пароль', 'validators'=>array( View_Form_Element::validator_require => 1))), ' укажите новый пароль для изменения.'							 
							)),
							array('caption' => 'Группа', 'items' => array(
								new view_form_select_usergroup('gid', '')							 
							)),
							array('caption' => 'Доступ', 'items' => array(
								new View_Form_Text('allowip', '/.*/', array('caption' => 'разрешенный ip', 'validators'=>array( View_Form_Element::validator_require => 1))),
								' "/.*/" - с любого ip'							 
							)),
							array('caption' => '', 'items' => array(
								new View_Form_Checkbox('block', '', array('caption' => 'Доступ запрещен'))
							))		
		) ));
		if ($form->get_values())
		{
			$form->return_values['password'] = md5(trim($form->return_values['password']));
			if ($db->InsertData('users', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Пользователь добавлен.') );
				}
			}
			$this->roll();
		}
		else
		{
			$this->AddContent($form);
		}
		
	}
	
}
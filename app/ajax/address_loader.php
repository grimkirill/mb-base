<?php

class address_loader {
	
	function street_list($gid)
	{
		global $db;
		$data = $db->Query('SELECT * FROM address_street WHERE gid = '.$db->EscapeValue($gid).' ORDER BY `street` ASC')->get_rows_array_id('id', 'street');
		$str = '';
		if (count($data) > 0)
		{
			foreach ($data as $key => $item)
			{
				$str .= '<option value="'.$key.'" >'.$item.'</option>';
			}
		}
		echo $str;
	}
	
	function home_list($gid)
	{
		global $db;
		$data = $db->Query('SELECT * FROM address_home WHERE gid = '.$db->EscapeValue($gid).' ORDER BY home+0')->get_rows_array_id('id', 'home');
		$str = '';
		if (count($data) > 0)
		{
			foreach ($data as $key => $item)
			{
				$str .= '<option value="'.$key.'" >'.$item.'</option>';
			}
		}
		echo $str;
	}
	
	function street_list_filter($gid)
	{
		
		global $db;
		$data = array();
		if ($gid != 0)
		{
			$data = $db->Query('SELECT * FROM address_street WHERE gid = '.$db->EscapeValue($gid).' ORDER BY `street` ASC')->get_rows_array_id('id', 'street');
		}
		$str = '<option value="0" >--=--</option>';
		if (count($data) > 0)
		{
			foreach ($data as $key => $item)
			{
				$str .= '<option value="'.$key.'" >'.$item.'</option>';
			}
		}
		echo $str;
		
	
	}
	
	function home_list_filter($gid)
	{
		global $db;
		$data = $db->Query('SELECT * FROM address_home WHERE gid = '.$db->EscapeValue((int)$gid).' ORDER BY home+0')->get_rows_array_id('id', 'home');
		$str = '<option value="0" >--=--</option>';
		if (count($data) > 0)
		{
			foreach ($data as $key => $item)
			{
				$str .= '<option value="'.$key.'" >'.$item.'</option>';
			}
		}
		echo $str;
	}
	
	
}
<?php

class device_manage {
	
	function exec($id, $function)
	{
		if (!ACL::has_perm(ACL::acl_network_device_portadmin)) { echo ACL::deny(); return ; }
		$dev = new device($id);
		echo $dev->driver->$function();
	}
	
	function port_exec($id, $function, $port)
	{
		if (!ACL::has_perm(ACL::acl_network_device_portadmin)) { echo ACL::deny(); return ; }
		//if($id == 91 && $port == 22 && $function == 'port_diag') {
//$str = <<<EOD
//cable_diag ports 22
//
//Command: cable_diag ports 22
//
//
//
// Perform Cable Diagnostics ....
//
//
//
// Port   Type    Link Status            Test Result           Cable Length (M)
//
// ----  ------  -------------  -----------------------------  ----------------
//
//  22    FE      Link Down      Pair1 Open      at 81  M       -
//
//                               Pair2 Open      at 82  M
//EOD;
//			echo $str;
//			exit;
//		}
		$dev = new device($id);
		echo $dev->driver->$function($port);
	}
	
	function delete_tag($id, $tag)
	{
		global $db;
		$db->DeleteData('devices_tags', array('gid'=>(int)$id, 'tag'=>(int)$tag));
	}
	
	function command($id, $commandId)
	{
		$dev = new device($id);
		echo $dev->call_command($commandId);
	}
	
	function resetcounter_ping($id)
	{
		$dev = new device($id);
		$dev->resetcounter_ping();
	}
	
	function resetcounter_ports($id)
	{
		$dev = new device($id);
		$dev->resetcounter_ports();
	}
	
	function add_tag($id, $tag)
	{
		global $db;
		$db->InsertData('devices_tags', array('gid'=>(int)$id, 'tag'=>(int)$tag));
	}
	
	function block_port($id)
	{
		if (!ACL::has_perm(ACL::acl_network_device_portadmin)) { echo ACL::deny(); return ; }
		global $db;
		$admin_port = $db->Query('SELECT * FROM devices_ports WHERE id ='.$id)->get_next_row_object();
		$admin = $admin_port->admin;
		$dev = new device($admin_port->gid);
		if ($dev->driver->block_port($admin_port->port, $admin) )
		{
			$new_admin = 1;
			if ($admin)
			{
				$new_admin = 0;
			}
			
			$db->UpdateData('devices_ports', array('admin' => $new_admin), array('id' => $id));
			$db->InsertData('devices_port_admin', array('gid'=> (int)$id, 'state' => $new_admin));
			//$db->Query('INSERT INTO `devices_port_admin` ( `gid` , `state` ) VALUES ( '.$id.' , '.$new_admin.')');
			if ($new_admin)
			{
				echo View_Element::image_active();
			}
			else
			{
				echo View_Element::image_stop();
			}
		}
	}
	
	function setxy($id, $x, $y)
	{
		global $db;
		$db->UpdateData('devices', array('mapx' => $x, 'mapy' => $y), array('id' => (int)$id));
		if ($db->GetAffectedRows() > 0)
		{
			echo View_Element::PanelSuccess('Положение сохранено');
		}
		//echo $x.' = '.$y;
		$this->loadinfo($id);
	}
	
	function loadinfo($id)
	{
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { echo ACL::deny(); return ; }
		$dev = new device($id);
		echo View_Element::Link($dev->get_address(), 'network/looking/show/'.$dev->id).'<hr>';
		if ($dev->get_address_disposition() != '')
		{
			echo $dev->get_address_disposition().'<hr>';
		}
		echo $dev->get_ip_link();
	}
	
}

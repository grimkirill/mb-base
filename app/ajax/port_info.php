<?php

class port_info {
	
	function info($id)
	{
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { echo ACL::deny(); return ; }
		global $db;
		
		$data = $db->Query('SELECT * FROM `devices_ports` WHERE `id` = '.$db->EscapeValue($id))->get_next_row_object();
		$dev = new device($data->gid);
		
		if ($abonent = $db->Query('SELECT * FROM `devices_abonents` WHERE `gid`='.$data->gid.' AND port = '.$data->port)->get_next_row_object())
		{
			echo 'Подключено: <b>'.$abonent->flat.'</b>'.View_Element::Space(3).View_Element::Link('edit', 'network/edituser/edit/'.$abonent->id);
			if ($abonent->comment)
			{
				echo '<br>'.$abonent->comment;
			}
			echo '<hr>';
			if ($abonent->uid)
			{
				$billing_info = new nibs_abonent($abonent->uid);
				echo $billing_info->get_address().View_Element::Space(2).$billing_info->get_fio().'<br>';
				echo $billing_info->get_packet().View_Element::Space(2).$billing_info->get_deposit();
				if ($billing_info->get_block())
				{
					echo View_Element::Space(2).View_Element::image_stop();
				}
				echo View_Element::Space(2).View_Element::Link('edit', 'billing/looking/show/'.$abonent->uid);
				//echo ' <a target="_blank" href="'.get_default_value::get('option.nibs.link').$abonent->uid.'">edit</a>';
				echo '<hr>';
				$status = $billing_info->get_status();
				if($status == nibs_abonent::STATUS_USER_BLOCKED) {
                                        $status = "<font color=red>блокирован</font>";
                                } else if($status == nibs_abonent::STATUS_USER_DELETED) {
                                        $status = "<font color=red>удален</font>";
                                } else if($status == nibs_abonent::STATUS_USER_FROZEN) {
                                        $status = "<font color=DarkOrange>заморожен</font>";
                                } else {
                                        $status = "<font color=blue>активный</font>";
                                }

				echo 'СТАТУС:<b> '.$status.'</b>';
				echo '<hr>';
			}
			
			
		}
		if ($data->comment)
		{
			echo $data->comment.'<hr>';
		} 
		
		
		echo '<hr>'.View_Element::submit_action('Reset порт', 'Будет сброшена история изменений блокировок и активности на порту, а таже удалены все обнаруженные MAC адреса. Продолжить?', "$('#info_container').load(WEBPATH+'ajax/port_info/resetport/$id');");
		echo View_Element::Space().View_Element::submit_action('Reset MAC', 'Будут удалены все обнаруженные MAC адреса. Продолжить?', "$('#info_container').load(WEBPATH+'ajax/port_info/resetmac/$id');");
		
		
		if (is_array($dev->driver->list_function_port()))
			{
				echo '<div   >';
				$list_a = array();
				foreach ($dev->driver->list_function_port() as $name => $caption)
				{
					$list_a[] = '<a href="#" onclick="device_exec_port('.$dev->id.',\''.$name.'\', '.$data->port.');">'.$caption.'</a>';
				}
				echo implode(' | ', $list_a);
				
				echo '</div>  <hr>';
				
		}
		
		if ($dev->get_dev_dlink())
		{
			echo View_Element::Link('<img style="margin-left: -12px;" alt="" src="'.WEBPATH.'network/graph_port/small/'.$id.'/'.microtime(true).'">', 'network/port_traff/show/'.$id);
		}
		
		$admin_list = $db->Query('SELECT `state`, `dt` FROM `devices_port_admin` WHERE `gid` = '.$db->EscapeValue($id).' ORDER BY `dt` DESC LIMIT 6')->get_rows_array();
		if (is_array($admin_list) )
		{
			$table_admin = new View_Table();
			$table_admin->AddHead('dt', 'Блокировка' );
			$table_admin->AddHead('state', ' ', View_Table::column_type_bool );
			$table_admin->AddData($admin_list);
			echo $table_admin->out();
		}
		
		$active_list = $db->Query('SELECT `state`, `dt` FROM `devices_port_active` WHERE `gid` = '.$db->EscapeValue($id).' ORDER BY `dt` DESC LIMIT 10')->get_rows_array();
		
		if (is_array($active_list))
		{
			$table_active = new View_Table();
			$table_active->AddHead('dt', 'Активность' );
			$table_active->AddHead('state', ' ', View_Table::column_type_bool );
			$table_active->AddData($active_list);
			echo $table_active->out();
		}
		
		//echo 'load from '.$id;
		$mac_list = $db->Query('SELECT `mac`, `dt` FROM `devices_port_mac` WHERE `gid` = '.$db->EscapeValue($id).' LIMIT 30')->get_rows_array();
		if (is_array($mac_list))
		{
			$table_mac = new View_Table();
			$table_mac->AddHead('num', '', View_Table::column_type_autonum );
			$table_mac->AddHead('mac', 'MAC' );
			$table_mac->AddHead('dt', 'Время' );
			$table_mac->AddData($mac_list);
			echo $table_mac->out();
		}

	}
	
	function addflat($gid, $port, $value)
	{
		if (!ACL::has_perm(ACL::acl_network_device_useradmin)) { echo ACL::deny(); return ; }
		$value = urldecode($value);
		global $db;
		$home_data = $db->Query('SELECT `billing_prefix` FROM `address_home` WHERE id = (SELECT address_home FROM `devices` WHERE id = '.$gid.')')->get_next_row_object()->billing_prefix;
		$uid = nibs_abonent::find_uid($home_data.$value);
		$db->InsertData('devices_abonents', array('gid' => $gid, 'port' => $port, 'flat' => $value, 'uid' => $uid));
		if ($data = $db->Query('SELECT id FROM `devices_ports` WHERE gid ='.$gid.' AND port ='.$port)->get_next_row_object())
		{
			$this->info($data->id);
		}
	}
	
	function resetmac($id)
	{
		if (!ACL::has_perm(ACL::acl_network_device_useradmin)) { echo ACL::deny(); return ; }
		global $db;
		
		$data = $db->Query('SELECT * FROM `devices_ports` WHERE `id` = '.$db->EscapeValue($id))->get_next_row_object();
		$dev = new device($data->gid);
		$dev->resetmac_port($id);
		$this->info($id);
	}
	
	function resetport($id)
	{
		if (!ACL::has_perm(ACL::acl_network_device_useradmin)) { echo ACL::deny(); return ; }
		global $db;
		
		$data = $db->Query('SELECT * FROM `devices_ports` WHERE `id` = '.$db->EscapeValue($id))->get_next_row_object();
		$dev = new device($data->gid);
		$dev->resetcounter_port($id);
		$this->info($id);
	}
	
	function addcomment($id, $comment)
	{
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { echo ACL::deny(); return ; }
		$comment = urldecode($comment);
		global $db;
		$db->UpdateData('devices_ports', array('comment'=> $comment), array('id'=>$id));
		$this->info($id);
	}
	
	function addflatuid($gid, $port, $uid)
	{
		if (!ACL::has_perm(ACL::acl_network_device_useradmin)) { echo ACL::deny(); return ; }
		global $db;
		$home_data = $db->Query('SELECT `billing_prefix` FROM `address_home` WHERE id = (SELECT address_home FROM `devices` WHERE id = '.$gid.')')->get_next_row_object()->billing_prefix;
		$abon = new nibs_abonent($uid);
		$flat = $abon->get_address();
		$flat = substr($flat, strlen($home_data), strlen($flat) - strlen($home_data) );
		if ($flat == '')
		{
			$flat = $abon->get_address();
		}
		$db->UpdateData('devices_abonents', array('port' => 0), array('gid' => $gid, 'port' => $port));
		$db->InsertData('devices_abonents', array('gid' => $gid, 'port' => $port, 'flat' => $flat, 'uid' => $uid));
		echo 'Добавлена кв.: '.$flat;
	}
	
}

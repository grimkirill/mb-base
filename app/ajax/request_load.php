<?php

class request_load {
	
	function set_done($id)
	{
		global $db;
		$id = (int)$id;
		
		$tmp_req = new request($id);
		$tmp_req->set_next_done();
		
		echo $tmp_req->get_done_print();
		

	}
	
	function get_list($servicer, $date)
	{
		global $db;
		$date = substr($date, 6,4).'-'.substr($date, 3,2).'-'.substr($date, 0,2);
		
		$requests = $db->Query('SELECT *  FROM `requests` WHERE `date` = \''.$date.'\' AND `servicer` = '.$servicer.' ORDER BY  `time_start` ASC ');
		$data = '';
		$num = 0;
		while($request = $requests->get_next_row_object())
		{
			$num++;
			$tmp_req = new request($request->id);
			$abon = new nibs_abonent($request->gid);
			$data[] = $tmp_req->get_data_array();
		}
		
		$table = new View_Table();
		$table->AddHead('num', '', View_Table::column_type_autonum);
		$table->AddHead('gid_link', 'Абонент');
		$table->AddHead('time', 'Время');
		$table->AddHead('type', 'Тип');
		$table->AddData($data);
		if ($num)
		{
			echo '<h4>Другие заявки на этот день</h4>';
			echo $table->out();
		}
		else
		{
			echo '<h4>Заявок на этот день нет</h4>';
		}
	}
}
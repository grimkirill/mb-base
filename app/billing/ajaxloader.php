<?php

class ajaxloader {
	
	function get_form_sms($id, $value)
	{
		global $db;
		if ( !preg_match('/\(\d{3}\) \d{3}-\d{4}/', $value, $results))
		{
			$value = '';
		}
		
		$db->UpdateData('billing_users', array('phone_sms'=>$value), array('id' => $id));
		echo View_Element::SpanEdit($value, 'billing/ajaxloader/get_form_sms/'.$id, '(999) 999-9999');
		
	}
	
}
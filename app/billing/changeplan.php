<?php

class changeplan extends View_Page_Controller {
	public $title = 'Переходы';
	public $category = 'billing';
	
	
	function addmenu()
	{
		$this->AddContent(View_Element::Link('Эквалайзер', CLASSPATH.'equalizer').' | '.
			View_Element::Link('Список переходов', CLASSPATH.'listfull').' | '.
			View_Element::Link('Переходы на тариф', CLASSPATH).
			'<hr>');
	}
	
	function listfull()
	{
		global $db;
		$this->addmenu();
		$form = new View_Form(array('method' => 'get', 'oneline' => TRUE, 'submit' => 'Просмотр'));
		
		$form->add('', array( 
			'c ', 
			new View_Form_Date('date_start', date('Y-m-d')), 
			' по ',
			new View_Form_Date('date_stop', date('Y-m-d'))
			
		));
		
		if ($form->get_values())
		{
			$this->AddContent($form);
			
			$table = new View_Table();	
			$table_data = array();
			
			$sql = "SELECT * FROM `billing_user_gid` WHERE `dt` >= '".$form->return_values['date_start']." 00:00:00' AND  `dt` <= '".$form->return_values['date_stop']." 23:59:59' ";
			
			$pack_list = $db->Query('SELECT * FROM `billing_packets`')->get_rows_array_id('id', 'packet');
			
			$res = $db->Query($sql);
						
			while ($data = $res->get_next_row_object())
			{
				$abon = new nibs_abonent($data->uid);
				$table_data[] = array('uid' => $abon->get_address_link(),'fgid' => $pack_list[$data->fgid], 'gid' => $pack_list[$data->gid], 'dt' => $data->dt );
				
			}
			
			$table->AddData($table_data);
			$table->AddHead('uid', 'Абонент');
			$table->AddHead('fgid', 'С тарифа');
			$table->AddHead('gid', 'На тариф');
			$table->AddHead('dt', 'Дата');
			
			
			$this->AddContent($table);
			
			
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function index()
	{
		global $db;
		$this->addmenu();
		$form = new View_Form(array('method' => 'get', 'oneline' => TRUE, 'submit' => 'Просмотр'));
		
		$form->add('', array( 
			'c ', 
			new View_Form_Date('date_start', date('Y-m-d', mktime(0, 0, 0, date("m") - 1, date("d"), date("Y")))), 
			' по ',
			new View_Form_Date('date_stop', date('Y-m-d')),
			View_Element::Space(2),
			' тариф: ',
			new view_form_select_tarif('tarif', FALSE)
			
		));
		
		if ($form->get_values())
		{
			$this->AddContent($form);
			
			$table = new View_Table();	
			$table_data = array();
			
			$sql = "SELECT *, COUNT(*) as count  FROM `billing_user_gid` WHERE `dt` >= '".$form->return_values['date_start']." 00:00:00' AND  `dt` <= '".$form->return_values['date_stop']." 23:59:59' ";
			$sql .= 'AND `gid` = '.$form->return_values['tarif'].' GROUP BY `fgid`';
			
			$res = $db->Query($sql);
						
			while ($data = $res->get_next_row_object())
			{
				$gid = $data->fgid;
				
				$table_data[$gid]['name'] = $db->Query('SELECT packet  FROM `billing_packets` WHERE `id` = '.$gid)->get_next_row_object()->packet;
				$table_data[$gid]['in'] = $data->count;
				if (!isset($table_data[$gid]['out']))
				{
					$table_data[$gid]['out'] = '';
				}

			}
			
			
			$sql = "SELECT *, COUNT(*) as count  FROM `billing_user_gid` WHERE `dt` >= '".$form->return_values['date_start']." 00:00:00' AND  `dt` <= '".$form->return_values['date_stop']." 23:59:59' ";
			$sql .= 'AND `fgid` = '.$form->return_values['tarif'].' GROUP BY `gid`';
			
			$res = $db->Query($sql);
						
			while ($data = $res->get_next_row_object())
			{
				$gid = $data->gid;
				
				$table_data[$gid]['name'] = $db->Query('SELECT packet  FROM `billing_packets` WHERE `id` = '.$gid)->get_next_row_object()->packet;
				$table_data[$gid]['out'] = $data->count;
				if (!isset($table_data[$gid]['in']))
				{
					$table_data[$gid]['in'] = '';
				}
			}
			
			$table->AddData($table_data);
			$table->AddHead('name', 'Тариф');
			$table->AddHead('in', 'Пришло c тарифа');
			$table->AddHead('out', 'Ушло на тариф');
			
			$this->AddContent($table);
			
			
		}
		else
		{
			$this->AddContent($form);
		}
		
	}
	
	
	function equalizer()
	{
		$this->out_left_menu = FALSE;
		$this->addmenu();
		global $db;
		$form = new View_Form(array('method' => 'get'));
		$form->add('', array( 
			'c ', 
			new View_Form_Date('date_start', date('Y-m-d', mktime(0, 0, 0, date("m") - 1, date("d"), date("Y")))), 
			' по ',
			new View_Form_Date('date_stop', date('Y-m-d')),
			' Коэффицент отображения ',
			new View_Form_Text('factor', '8'),
			
		));
		$list = $db->Query('SELECT * FROM billing_packets')->get_rows_array_id('id', 'packet');
		foreach ($list as $key => $packet)
		{
			$form->add('', array( 
				$packet, 
				new View_Form_Checkbox('use'.$key, '', array('caption' => 'Анализировать')),
				new View_Form_Color('color'.$key, '')
				));
		} 
		if ($form->get_values())
		{
			$table = new View_Table();	
			$table_data = array();
			$pack_list = array();
			$pack_color = array();
			foreach ($list as $key => $packet)
			{
				if ($form->return_values['use'.$key])
				{
					$table_data[$key]['caption'] = $packet;
					$table_data[$key]['color'] = View_Element::ColorPanel($form->return_values['color'.$key]);
					$table_data[$key]['count'] = $db->Query('SELECT COUNT(*) as count FROM `billing_users` WHERE gid = '.$key)->get_next_row_object()->count;
					$table_data[$key]['data_in'] = '';
					$table_data[$key]['data_out'] = '';
					$pack_list[] = $key;
					$pack_color[$key] = $form->return_values['color'.$key];
				}
			}
			
			foreach ($pack_list as $gid)
			{
				$sql = "SELECT *, COUNT(*) as count  FROM `billing_user_gid` WHERE `dt` >= '".$form->return_values['date_start']." 00:00:00' AND  `dt` <= '".$form->return_values['date_stop']." 23:59:59' AND fgid IN (".implode(', ',$pack_list).") ";
				$sql .= 'AND `gid` = '.$gid.' GROUP BY `fgid`';
				$res = $db->Query($sql);
				while ($data = $res->get_next_row_object())
				{
					if ($data->count > 0)
					{
						if ($data->count > 10)
						{
							$table_data[$gid]['data_in'] .= '<span style="height: 18px; width: '.$data->count * $form->return_values['factor'].'px; background-color: '.$pack_color[$data->fgid].'; display: block; float: left; text-align: center; ">'.$data->count.'</span>';
						}
						else
						{
							$table_data[$gid]['data_in'] .= '<span style="height: 18px; width: '.$data->count * $form->return_values['factor'].'px; background-color: '.$pack_color[$data->fgid].'; display: block; float: left; ">&nbsp;</span>';
						}
					}
				}

				$sql = "SELECT *, COUNT(*) as count  FROM `billing_user_gid` WHERE `dt` >= '".$form->return_values['date_start']." 00:00:00' AND  `dt` <= '".$form->return_values['date_stop']." 23:59:59' AND gid IN (".implode(', ',$pack_list).")";
				$sql .= 'AND `fgid` = '.$gid.' GROUP BY `gid`';
				
				$res = $db->Query($sql);
				while ($data = $res->get_next_row_object())
				{
					if ($data->count > 0)
					{
						if ($data->count > 10)
						{
							$table_data[$gid]['data_out'] .= '<span style="height: 18px; width: '.$data->count * $form->return_values['factor'].'px; background-color: '.$pack_color[$data->gid].'; display: block; float: left; text-align: center; ">'.$data->count.'</span>';
						}
						else
						{
							$table_data[$gid]['data_out'] .= '<span style="height: 18px; width: '.$data->count * $form->return_values['factor'].'px; background-color: '.$pack_color[$data->gid].'; display: block; float: left; ">&nbsp;</span>';
						}
					}
				}
			}
			
			
			$this->AddContent(View_Element::PanelCollapse('Настройки', $form, TRUE));
			
			$table->AddHead('caption', 'Тариф');
			$table->AddHead('count', 'Всего');
			$table->AddHead('color', 'Цвет');
			$table->AddHead('data_in', 'Пришло c тарифа');
			$table->AddHead('data_out', 'Ушло на тариф');
			
			$table->AddData($table_data);
			$this->AddContent($table);
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function fulllist()
	{
		
	}
	
}
<?php

class find extends View_Page_Controller {
	public $title = 'Поиск абонента';
	public $category = 'billing';
	
	function index()
	{
		$this->show();
	}

	function show()
	{
		if (!ACL::has_perm(ACL::acl_billing_request_roll )) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('method' => 'get', 'submit' => 'Поиск'));
		
		$form->add('Адрес', array( 
			new View_Form_Text('address', '', array('width' => '20em')), ' UID: ',
			new View_Form_Text('uid','',	array('width' => '6em', 'mask' => '?99999') )
		));
		$form->add('IP адрес', array('ip: ' , new View_Form_Text('ip',''), ' Тариф: ',
				new view_form_select_tarif('tarif', FALSE, array('as_filter' => TRUE)) 
				));
				
		$sql = FALSE;
		if ($form->get_values())
		{
			$this->AddContent($form);
			
			$form->return_values['ip'] = str_replace('*', '%', $form->return_values['ip']);
			$form->return_values['address'] = str_replace('*', '%', $form->return_values['address']);
			
			
			if ($list = nibs_abonent::get_list_found(
							$form->return_values['address'],
							$form->return_values['uid'],
							$form->return_values['ip'],
							$form->return_values['tarif']))
			{
				$this->show_list($list);
			}
			
		}
		else
		{
			$this->AddContent($form);
		}
		
		
	}
	
	
	function show_list($list)
	{
		global $db;
		$user_list = '';
		if (is_array($list))
		{
			if (count($list) == 1)
			{
				header('Location: '.WEBPATH.'billing/looking/show/'.array_pop($list));
				return;
			}
			
			foreach ($list as $uid)
			{
				$abonent = new nibs_abonent($uid);
				$user_list[$uid]['uid'] = $uid;
				$user_list[$uid]['address'] = View_Element::Link($abonent->get_address(), 'billing/looking/show/'.$uid);
				$user_list[$uid]['fio'] = View_Element::Link($abonent->get_fio(), 'billing/looking/show/'.$uid);
				$user_list[$uid]['tarif'] = $abonent->get_packet();
				$user_list[$uid]['deposit'] = $abonent->get_deposit();
                		$status = $abonent->get_status();
				if($status == nibs_abonent::STATUS_USER_BLOCKED) {
                        		$status = "<font color=red>блокирован</font>";
				} else if($status == nibs_abonent::STATUS_USER_DELETED) {
					$status = "<font color=red>удален</font>";
                		} else if($status == nibs_abonent::STATUS_USER_FROZEN) {
					$status = "<font color=DarkOrange>заморожен</font>";
				} else {
                        		$status = "<font color=blue>активный</font>";
                		}
				$user_list[$uid]['status'] = $status;
			}
			$table = new View_Table(array('edit_link' => 'network/editdevices/edit/'));

			$table->AddHead('num', '', View_Table::column_type_autonum );
			
			$table->AddHead('address', 'Адрес' );
			$table->AddHead('fio', 'ФИО');
			$table->AddHead('tarif', 'Тариф');
			$table->AddHead('deposit', 'Счет', View_Table::column_type_number  );
			$table->AddHead('status', 'Статус');
			$table->AddHead('uid', 'UID' );
			//$table->AddHead('id', '', View_Table::column_type_edit);
			
			$table->AddData($user_list);
			
			$this->AddContent($table);
		}
	}
}

<?php
header("Content-type: image/png");

class graph {
	
	function show($id)
	{
		$this->out($id);
	}
	
	function out($id, $title = '', $start = '-86100', $stop = 'now')
	{
			$rrd = BASEPATH.'rrd/abonent_'.$id.'.rrd';
			$cmd = 'rrdtool graph - --imgformat=PNG --start='.$start.' --end='.$stop.' ';
			if ($title)
			{
				$cmd .= ' --title="'.$title.'" ';
			}
			$cmd .= ' --base=1024 --height=200 --width=600 --alt-autoscale-max --lower-limit=0 --vertical-label="" --slope-mode --font TITLE:12: --font AXIS:8: --font LEGEND:10: --font UNIT:8: '.
			'DEF:c="'.$rrd.'":input:AVERAGE DEF:a="'.$rrd.'":output:AVERAGE CDEF:cdefa=a,8,* CDEF:cdefe=c,8,* '.
			'AREA:cdefa#00CF00FF:"Inbound"  GPRINT:cdefa:LAST:" Current\:%8.2lf %s"  GPRINT:cdefa:AVERAGE:"Average\:%8.2lf %s"  GPRINT:cdefa:MAX:"Maximum\:%8.2lf %s\n"  LINE1:cdefe#002A97FF:"Outbound"  GPRINT:cdefe:LAST:"Current\:%8.2lf %s"  GPRINT:cdefe:AVERAGE:"Average\:%8.2lf %s"  GPRINT:cdefe:MAX:"Maximum\:%8.2lf %s"';

			$fp = popen($cmd, "r");
			
			if (isset($fp)) {
				fpassthru($fp);
			}
		
	}
	
	function last_week($id)
	{
		$this->out($id, 'Последняя неделя', '-1w');
	}
	
	function last_2week($id)
	{
		$this->out($id, 'Предпоследняя неделя', '-2w', '-1w');
	}
	
	function last_month($id)
	{
		$this->out($id, 'Последний месяц', '-1m');
	}
	
	function last_day($id)
	{
		$this->out($id, 'Последние 24 часа');
	}
	
	
	function out_tarif($id, $title = '', $start = '-86100', $stop = 'now' )
	{
		
			$rrd = BASEPATH.'rrd/tarif_'.$id.'.rrd';
			$cmd = 'rrdtool graph - --imgformat=PNG --start='.$start.' --end='.$stop.' ';
			if ($title)
			{
				$cmd .= ' --title="'.$title.'" ';
			}
			$cmd .= ' --base=1024 --height=200 --width=700 --alt-autoscale-max --lower-limit=0 --vertical-label="" --slope-mode --font TITLE:12: --font AXIS:8: --font LEGEND:10: --font UNIT:8: '.
			'DEF:c="'.$rrd.'":input:AVERAGE DEF:a="'.$rrd.'":output:AVERAGE CDEF:cdefa=a,8,* CDEF:cdefe=c,8,* '.
			'AREA:cdefa#00CF00FF:"Inbound"  GPRINT:cdefa:LAST:" Current\:%8.2lf %s"  GPRINT:cdefa:AVERAGE:"Average\:%8.2lf %s"  GPRINT:cdefa:MAX:"Maximum\:%8.2lf %s\n"  LINE1:cdefe#002A97FF:"Outbound"  GPRINT:cdefe:LAST:"Current\:%8.2lf %s"  GPRINT:cdefe:AVERAGE:"Average\:%8.2lf %s"  GPRINT:cdefe:MAX:"Maximum\:%8.2lf %s"';

			$fp = popen($cmd, "r");
			
			if (isset($fp)) {
				fpassthru($fp);
			}
	}
	
	function last_tarif_dashboard($id)
	{
		global $db;
		$title = $db->Query('SELECT `packet`  FROM `billing_packets` WHERE `id` = '.$id)->get_next_row_object()->packet;
		$count = $db->Query('SELECT COUNT(*) as count  FROM `billing_users` WHERE `gid` = '.$id)->get_next_row_object()->count;
		$title .= ' [ '.$count.' ]';
		$start = '-86100';
		$stop = 'now';
			$rrd = BASEPATH.'rrd/tarif_'.$id.'.rrd';
			$cmd = 'rrdtool graph - --imgformat=PNG --start='.$start.' --end='.$stop.' ';
			if ($title)
			{
				$cmd .= ' --title="'.$title.'" ';
			}
			$cmd .= ' --base=1024 --height=150 --width=380 --alt-autoscale-max --lower-limit=0 --vertical-label="" --slope-mode --font TITLE:10: --font AXIS:8: --font LEGEND:10: --font UNIT:8: '.
			'DEF:c="'.$rrd.'":input:AVERAGE DEF:a="'.$rrd.'":output:AVERAGE CDEF:cdefa=a,8,* CDEF:cdefe=c,8,* '.
			'AREA:cdefa#00CF00FF:""  LINE1:cdefe#002A97FF:"" ';	

			$fp = popen($cmd, "r");
			
			if (isset($fp)) {
				fpassthru($fp);
			}
	}
	
	function last_tarif_day($id)
	{
		global $db;
		$caption = $db->Query('SELECT `packet`  FROM `billing_packets` WHERE `id` = '.$id)->get_next_row_object()->packet;
		
		$this->out_tarif($id, $caption, '-86100');
	}
	
	
	function last_tarif_week($id)
	{
		global $db;
		$caption = $db->Query('SELECT `packet`  FROM `billing_packets` WHERE `id` = '.$id)->get_next_row_object()->packet;

		$this->out_tarif($id, $caption, '-1w');
	}
	
	function last_tarif_2week($id)
	{
		global $db;
		$caption = $db->Query('SELECT `packet`  FROM `billing_packets` WHERE `id` = '.$id)->get_next_row_object()->packet;
		$this->out_tarif($id, $caption, '-2w', '-1w');
	}
	
	function last_tarif_month($id)
	{
		global $db;
		$caption = $db->Query('SELECT `packet`  FROM `billing_packets` WHERE `id` = '.$id)->get_next_row_object()->packet;
		$this->out_tarif($id, $caption, '-1m');
	}
}
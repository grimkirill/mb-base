<?php
class index extends View_Page_Controller {
	public $title = 'Биллинг';
	public $category = 'billing';
	public $first_call = FALSE;
	
	function index()
	{
		if ($this->first_call == FALSE)
		{
			$this->first_call = TRUE;
			
			header('Location: '.WEBPATH.'billing/find/');
			
		}
	}
}
<?php

error_reporting(E_ALL);

class looking extends View_Page_Controller {
	public $title = 'Просмотр абонента';
	public $category = 'billing';
	public $form_request = FALSE;
	
	
	function prepare_form_request($id)
	{
		$this->form_request = new View_Form(array('action' => WEBPATH.CLASSPATH.'addrequest/'.$id));
		$this->form_request->add('', array(
			new view_form_select_request_servicer('servicer', ''),View_Element::Space(2),
			new view_form_select_request_type('type', ''),View_Element::Space(2),
			new View_Form_Date('date', date('Y-m-d')), ' C ',
			new View_Form_Select('time_start', '', array('options' => request_data::timeList()) ), ' продолжительность: ',
			new View_Form_Select('time_count', '', array('options' => request_data::timeCount()) )
			));
		$this->form_request->add('Описание', array(
			new View_Form_Textarea('comment', '')
			));
		$this->form_request->add('', array(
			'Телефон: ',
			new View_Form_Text('phone', '', array('width' => '18em'))
			));
	}
	
	
	function setblock($id)
	{
		if (!ACL::has_perm(ACL::acl_billing_request_roll )) { $this->AddContent(ACL::deny()); return ; }
		$abonent = new nibs_abonent($id);
		
		$new_block = 1;		
		if ($abonent->get_block())
		{
			$new_block = 0;
		}		
		$abonent->set_block($new_block);
		$this->show($id);
	}
		
	function show($id)
	{
		if (!ACL::has_perm(ACL::acl_billing_request_roll )) { $this->AddContent(ACL::deny()); return ; }
		$id = (int)$id;
		global $db;
		$abonent = new nibs_abonent($id);
		$status = $abonent->get_status();
                if($status == nibs_abonent::STATUS_USER_BLOCKED) {
                	$status = "<font color=red>блокирован</font>";
                } else if($status == nibs_abonent::STATUS_USER_DELETED) {
                	$status = "<font color=red>удален</font>";
		} else if($status == nibs_abonent::STATUS_USER_FROZEN) {
			$status = "<font color=DarkOrange>заморожен</font>";
                } else {
                	$status = "<font color=blue>активный</font>";
                }

		$caption = $abonent->get_address().View_Element::Space(2).$abonent->get_fio().View_Element::Space(2).
				$abonent->get_packet().View_Element::Space(2).$abonent->get_deposit().View_Element::Space(2).$status;
		$new_block = 1;		
		if ($abonent->get_block())
		{
			$new_block = 0;
			$caption .= View_Element::Space(2).View_Element::image_stop();
		}
		$this->prepare_form_request($id);
		if ($abonent->is_online())
		{
				$caption .= View_Element::Space(1).View_Element::image_online().View_Element::Space(1);
		}
		
		$caption .= View_Element::Space(2).' <a target="_blank" href="'.get_default_value::get('option.nibs.link').$abonent->uid.'">nibs</a>';
		
		
		
		$this->AddContent('<h4>'.$caption.'</h4>');

		$request_data = '';   // заявки
		$changegid_data = ''; // смена пакета
		$block_data = View_Element::SpanClick('Сменить блокировку', "billing_set_block('$abonent->uid','$new_block')").'<hr>';;
		
		
		//--------------------------------------------------------
		$ajax_requests = '<div id="ajax_request_list"></div><script type="text/javascript">bind_request_list_load(); request_list_load();</script>';
		$request_data .= $this->get_reguest_list($id);
		$request_data .= View_Element::PanelCollapse('Добавить заявку', $this->form_request->out().$ajax_requests, TRUE);
		
		
		if ($res = $db->Query('SELECT *  FROM `billing_user_gid` WHERE `uid` = '.$id.' ORDER BY `dt` DESC')->get_rows_array())
		{
			foreach ($res as $key => $item)
			{
				$res[$key]['fgid'] = $db->Query('SELECT `packet` FROM `billing_packets` WHERE `id` = '.$res[$key]['fgid'])->get_next_row_object()->packet;
				$res[$key]['gid'] = $db->Query('SELECT `packet` FROM `billing_packets` WHERE `id` = '.$res[$key]['gid'])->get_next_row_object()->packet;
				
			}
			
			$table = new View_Table();
			$table->AddHead('fgid', 'С');
			$table->AddHead('gid', 'На');
			$table->AddHead('dt', 'Дата');
			$table->AddData($res);
			
			$changegid_data = $table->out();
		}
		
		
		if ($res = $db->Query('SELECT *  FROM `billing_user_blocked` WHERE `uid` = '.$id.' ORDER BY `dt` DESC')->get_rows_array())
		{
			$table = new View_Table();
			$table->AddHead('dt', 'Дата');
			$table->AddHead('blocked', 'Блокировка', View_Table::column_type_bool_inverse );
			$table->AddData($res);
			$block_data .= $table->out();
		}
		
		$graph = '<img alt="" src="'.WEBPATH.'billing/graph/last_day/'.$id.'">'.
				'<img alt="" src="'.WEBPATH.'billing/graph/last_week/'.$id.'">'.
				'<img alt="" src="'.WEBPATH.'billing/graph/last_2week/'.$id.'">'.
				'<img alt="" src="'.WEBPATH.'billing/graph/last_month/'.$id.'">';

		
		$table_inet = new View_Table();
		$table_inet->AddHead('start_time', 'Подключился');
		$table_inet->AddHead('stop_time', 'Отключился');
		$table_inet->AddHead('in_bytes', 'Отправлено');
		$table_inet->AddHead('out_bytes', 'Принято');
		$table_inet->AddHead('ip', 'IP');
		$table_inet->AddHead('server', 'NAS');
		$table_inet->AddHead('call_from', 'MAC');
		$data_inet = $abonent->get_use_list();
		if (is_array($data_inet))
		{
			foreach ($data_inet as $key => $item)
			{
				if ($data_inet[$key]['stop_time'] == '0000-00-00 00:00:00')
				{
					$data_inet[$key]['stop_time'] = 'В интернете';
				}
				$data_inet[$key]['in_bytes'] = (int)($data_inet[$key]['in_bytes'] / 1024)  / 1000;
				$data_inet[$key]['out_bytes'] = (int)($data_inet[$key]['out_bytes'] / 1024)  / 1000;
			}
		}
		$table_inet->AddData($data_inet);
		
		$inet_use = $table_inet->out();
		
		
		$table_pay =  new View_Table();
		$table_pay->AddHead('start_time', 'Дата');
		$table_pay->AddHead('call_to', 'Кем');
		$table_pay->AddHead('call_from', 'Как');
		$table_pay->AddHead('comment', 'Назаначение');
		$table_pay->AddHead('before_billing', 'До', View_Table::column_type_number);
		$table_pay->AddHead('billing_minus', 'Сумма', View_Table::column_type_number);	
		$table_pay->AddData($abonent->get_pay_list());
		$pay_out = $table_pay->out();


		// False - don't delete colon from mac
		$mac = strtolower($abonent->get_mac(False));

		
		$tmp_data = $abonent->get_print_array();
		$user_info = new View_Properties();
		$user_info->AddTitle('Персональные данные');
		$status = $tmp_data['u_status'];
                if($status == nibs_abonent::STATUS_USER_BLOCKED) {
                	$status = "<font color=red>блокирован</font>";
		} else if($status == nibs_abonent::STATUS_USER_FROZEN) {
                        $status = "<font color=DarkOrange>заморожен</font>";
                } else if($status == nibs_abonent::STATUS_USER_DELETED) {
                	$status = "<font color=red>удален</font>";
                } else {
                	$status = "<font color=blue>активный</font>";
                }

		$user_info->AddProperty('<b>Статус</b>', $status);
		$user_info->AddProperty('Ф.И.О.', $tmp_data['u_fio']);
		$user_info->AddProperty('Адрес', $tmp_data['u_address']);
		$user_info->AddProperty('Телефон', $tmp_data['u_phone']);
		$user_info->AddProperty('Примечание', $tmp_data['u_prim']);
		$user_info->AddProperty('Дата заключения договора', $tmp_data['u_add_date']);
//freeze_date
		if($tmp_data['u_status'] == nibs_abonent::STATUS_USER_DELETED) {
			$user_info->AddProperty('Дата расторжения договора', $tmp_data['u_del_date']);
		}
		if($tmp_data['u_status'] == nibs_abonent::STATUS_USER_BLOCKED) {
			$user_info->AddProperty('Дата блокировки (отключения)', $tmp_data['u_block_date']);
		}
		if($tmp_data['u_status'] == nibs_abonent::STATUS_USER_FROZEN) {
                        $user_info->AddProperty('Дата заморозки', $tmp_data['u_freeze_date']);
                }

		$user_info->AddProperty('Телефон SMS', View_Element::SpanEdit($db->Query('SELECT * FROM billing_users WHERE id = '.$abonent->uid)->get_next_row_object()->phone_sms, 'billing/ajaxloader/get_form_sms/'.$abonent->uid, '(999) 999-9999'));
		$user_info->AddProperty('Промо код', substr($tmp_data['u_user'], 0, 3).$abonent->uid);
		$user_info->AddTitle('Параметры Доступа');
		$user_info->AddProperty('Имя пользователя (логин)', $tmp_data['u_user']);
		$user_info->AddProperty('Пароль', $tmp_data['u_password']);
		$user_info->AddProperty('IP адрес', $tmp_data['u_framed_ip']);
		$user_info->AddTitle('Финансы');
		$user_info->AddProperty('Тариф', $tmp_data['p_packet']);
		$user_info->AddProperty('Баланс', $tmp_data['u_deposit']);
		$user_info->AddProperty('Кредит', $tmp_data['u_credit']);
                //$user_info->AddTitle('Фильтр на установку новой Интернет сессии');
		//$user_info->AddProperty('Описание', 'Принудительно удаляет фильтр на установку подключения к Интернету. При 651 (Vista и выше) и 678 (XP) ошибках');
		//$user_info->AddProperty('Последний MAC-адрес', $mac);
                //$user_info->AddProperty('Фильтр', '<a target="_blank" href="http://31.134.16.252/allowpadi?mac='.$mac.'">Снять фильтр</a>');

		//$user_info->AddProperty('Фильтр', View_Element::SpanClick('Удалить фильтр', "unset_filter_padi('$abonent->uid', '$new_block')"));
		
		$services_b_out = new View_Properties();
		$services_b_out->AddTitle('Базовые услуги');
		$services_b_data = $abonent->get_b_services();
		$services_table = new View_Table();
                $services_table->AddHead('servicename', 'Имя услуги');
                $services_table->AddHead('description', 'Описание');
                $services_table->AddHead('amount', 'Стоимость');
		$services_table->AddData($services_b_data);
                $stotal = $abonent->get_bserv_total();
                $services_b_out->AddProperty('Стоимость итого:', $stotal);
		$services_b_out .= $services_table->out();

                $services_i_out = new View_Properties();
                $services_i_out->AddTitle('Индивидуальные услуги');
                $services_i_data = $abonent->get_i_services();
                $services_table = new View_Table();
                $services_table->AddHead('servicename', 'Имя услуги');
                $services_table->AddHead('description', 'Описание');
                $services_table->AddHead('amount', 'Стоимость');
                $services_table->AddData($services_i_data);
                $stotal = $abonent->get_iserv_total();
                $services_i_out->AddProperty('Стоимость итого:', $stotal);
                $services_i_out .= $services_table->out();

		$services_out = $services_b_out.$services_i_out;
		
		$this->AddContent(View_Element::Tabs(array(
			'Информация' =>View_Element::PanelClear($user_info),
			'Заявки' => $request_data,
			'Тарифы' => $changegid_data,
			'Блокировки' => $block_data,
			'Сеть' => $this->get_device_info($id),
			'Печать' => $this->getprintlist($id),
			//'Траффик' => $graph,
			'Интернет' => $inet_use,
			'Платежи' => $pay_out,
			'Услуги' => $services_out,
			//'Фильтрация MAC' => $unset_mac
			)));
		
	}
	
	
	function get_device_info($id)
	{
		global $db;
		$sql = 'SELECT *  FROM `devices_abonents` WHERE `uid` = '.$id;
		$results = $db->Query($sql)->get_rows_array();
		if (is_array($results))
		{
			foreach ($results as $key => $item)
			{
				$dev = new device($item['gid']);
				$results[$key]['dev'] = View_Element::Link($dev->get_address(), 'network/looking/show/'.$dev->id);	
				$results[$key]['portadmin'] = '';
				if ($dev->get_port_admin())
				{
					if ($port = $db->Query('SELECT *  FROM `devices_ports` WHERE `gid` = '.$dev->id.' AND port = '.$item['port'])->get_next_row_object() )
					{
						if($port->port == '22' && $port->gid == '91') {
                                        		$port->admin = '0';
						}
						if ($port->admin){
						$results[$key]['portadmin'] = '<span style="display: block; width: 100%; height: 19px; cursor: pointer;" ondblclick="block_port('.$port->id.')" id="port_block_'.$port->id.'">'. 
															View_Element::image_active().'</span>';
						}
						else
						{
							$results[$key]['portadmin'] = '<span style="display: block; width: 100%; height: 19px; cursor: pointer;" ondblclick="block_port('.$port->id.')" id="port_block_'.$port->id.'">'.
																View_Element::image_stop().'</span>';
						}
						if($port->port == '22' && $port->gid == '91') {
                                        		$port->active = '0';
                                		}
						if ($port->active)
						{
							$results[$key]['portadmin'] .= View_Element::image_connect();
						}
						
						
					}
				}
			}
			
			$table = new View_Table();
			$table->AddHead('dev', 'Устройство');
			$table->AddHead('port', 'порт');
			$table->AddHead('flat', 'Квартира');
			$table->AddHead('comment', 'комментарий');
			$table->AddHead('portadmin', 'Состояние');
			
			$table->AddData($results);
			return $table->out();
		}
		return '';
	}
	
	function get_reguest_list($id)
	{
		global $db, $auth_user;
		$list = $db->Query('SELECT *  FROM `requests` WHERE `gid` = '.$id.' ORDER BY `date` DESC')->get_rows_array();
		if (is_array($list))
		{
			foreach ($list as $key => $item)
			{
				$list[$key]['date'] = '<span title="'.$item['dt'].View_Element::Space(1).User_Auth::get_loginid($item['user']).'" >'.$item['date'].'</span>';
				$list[$key]['type'] = $db->Query('SELECT `name` FROM `request_type` WHERE `id` = '.$item['type'])->get_next_row_object()->name;
				$list[$key]['time'] = request_data::getPrintTime($item['time_start'], $item['time_count']);	
				$list[$key]['done'] = request_data::reguestDone($item['id'], $item['done']);
				if ($item['dt_done'] != '0000-00-00 00:00:00')
				{
					$list[$key]['done'] .= View_Element::Space(1).$item['dt_done'];
				}
				
				$list[$key]['servicer'] = $db->Query('SELECT `name` FROM `request_servicer` WHERE `id` = '.$item['servicer'])->get_next_row_object()->name;
			}
			$table = new View_Table(array('caption' => '', 
											'edit_link' => CONTROLLER_PATH.'requests/edit/'));
			$table->AddHead('date', 'дата');
			$table->AddHead('time', 'время');
			$table->AddHead('servicer', 'Исполнитель');
			$table->AddHead('type', 'Тип');
			$table->AddHead('comment', 'Информация');
			$table->AddHead('done', 'Выполнено');
			$table->AddHead('id', '', View_Table::column_type_edit);
			$table->AddData($list);
			return $table->out();
		}
		else
		{
			return '';
		}
	}
	
	
	function getprintlist($id)
	{
		global $db;
		$out = '';
		$list = $db->Query('SELECT id, name FROM `billing_print`');
		while ($data = $list->get_next_row_object())
		{
			$out .= View_Element::Link($data->name, 'billing/printform/show/'.$data->id.'/'.$id).'<br>';
		}
		return $out;
	}
	
	function addrequest($id)
	{
		if (!ACL::has_perm(ACL::acl_billing_request_add )) { $this->AddContent(ACL::deny()); return ; }
		global $db, $auth_user;
		$this->prepare_form_request($id);
		if ($this->form_request->get_values())
		{
			$db->InsertData('requests', $this->form_request->return_values+array('gid' => $id, 'user' => $auth_user->id));
			if ($db->GetAffectedRows() > 0)
			{
				$this->AddContent(View_Element::PanelSuccess('Заявка добавлена.') );
			}
			if ($this->form_request->return_values['date'] == date('Y-m-d'))
			{
				$tmp_req = new request($db->GetInsertId());
				$tmp_req->do_trigger();
			}
		}
		$this->show($id);
	}
	
}

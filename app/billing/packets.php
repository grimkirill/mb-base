<?php

class packets extends View_Page_Controller {
	public $title = 'Управление тарифами';
	public $category = 'billing';
	
	function index()
	{
		$this->roll();
	} 
	
	function roll()
	{
		global $db;	
		$table = new View_Table(array('caption' => '', 
											'edit_link' => CLASSPATH.'edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('packet', 'Название NIBS');
		$table->AddHead('count', 'Абонентов');
		$table->AddHead('type', 'Проверять', View_Table::column_type_bool_inverse);
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT * FROM  `billing_packets`')->get_rows_array())
		{
			foreach ($res as $key => $item)
			{
				$res[$key]['count'] = $db->Query('SELECT COUNT(*) as count FROM `billing_users` WHERE gid = '.$item['id'])->get_next_row_object()->count;
			}
			$table->AddData($res);
		}
		$this->AddContent(View_Element::Link('Графики', CLASSPATH.'graph'));
		$this->AddContent($table);
	}
	
	function graph()
	{
		global $db;
		$this->out_left_menu = FALSE;
		$this->AddContent(View_Element::Link('Список', CLASSPATH.'roll').'<hr>');
		
		$res = $db->Query('SELECT * FROM  `billing_packets`');
		while ($item = $res->get_next_row_object())
		{
			$this->AddContent(View_Element::Link('<img alt="" src="'.WEBPATH.'billing/graph/last_tarif_dashboard/'.$item->id.'">', CLASSPATH.'show_graph/'.$item->id));
		}
		
	}
	
	
	function show_graph($id)
	{
		global $db;
		$this->AddContent(View_Element::Link('Список', CLASSPATH.'roll').'<hr>');
		
		$this->AddContent('<img alt="" src="'.WEBPATH.'billing/graph/last_tarif_day/'.$id.'">');
		$this->AddContent('<img alt="" src="'.WEBPATH.'billing/graph/last_tarif_week/'.$id.'">');
		
	}
	
	function edit($id)
	{
		global $db;
		if ($data = $db->Query('SELECT * FROM  `billing_packets` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$form = new View_Form(array('caption' => 'Изменение '.$data->packet, 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название', 'items' => array(
							new View_Form_Text('packet', $data->packet, array('caption' => 'Название', 'width' => '50em' ,'validators'=>array( View_Form_Element::validator_require => 1)))
							)),
							array('caption' => '', 'items' => array(
							new View_Form_Checkbox('type', $data->type, array('caption' => 'Не проверять на отключения и расторжения' ))
							))
			) ));
			
			if ($form->get_values())
			{
				if ($db->UpdateData('billing_packets', $form->return_values, array('id' => $id)))
				{
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
					}
				}
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
}
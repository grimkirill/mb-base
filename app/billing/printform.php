<?php
function generate_user($length = 8){
  $chars = 'abcdefghjkmnopqrstwuyxz23456789';
  $numChars = strlen($chars);
  $string = '';
  for ($i = 0; $i < $length; $i++) {
    $string .= substr($chars, rand(1, $numChars) - 1, 1);
  }
  return $string;
}

function generate_password($length = 10){
  $chars = 'abcdefghjkmnopqrstwuyxzABCDEFGHIKLMNOPQRSTVXYZ23456789';
  $numChars = strlen($chars);
  $string = '';
  for ($i = 0; $i < $length; $i++) {
    $string .= substr($chars, rand(1, $numChars) - 1, 1);
  }
  return $string;
}


class printform {
	
	function show($id, $uid)
	{
		if (!ACL::has_perm(ACL::acl_billing_request_roll )) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		global $l;
				
		$abonent = new nibs_abonent($uid);
		$replace = $abonent->get_print_array();

		$dev_info = $abonent->get_device_info();
		if($dev_info) {
			$replace += $dev_info;

		}

		$fio = str_replace('  ', ' ',ucwords(trim($abonent->get_fio()))); 
		$fio_array = explode(' ',$fio);
		$fio = $fio_array[0].' '.substr($fio_array[1],0, 2).'. '.substr($fio_array[2],0,2).'.';
		$replace['u_fio_s'] = $fio;
		$replace['date'] = date('d-m-Y');
		$replace['add_date'] = substr($replace['u_add_date'], 8,2).'-'.substr($replace['u_add_date'], 5,2).'-'.substr($replace['u_add_date'], 0,4);
		$month = substr($replace['u_add_date'], 5,2);
		$month_format = array('1' => 'января',
					'2' => 'февраля',
					'3' => 'марта',
					'4' => 'апреля',
					'5' => 'мая',
					'6' => 'июня',
					'7' => 'июля',
					'8' => 'августа',
					'9' => 'сентября',
					'10' => 'октября',
					'11' => 'ноября',
					'12' => 'декабря');
		$replace['month_format_only'] = $month_format[(int)$month];
		$replace['year_only'] = substr($replace['u_add_date'], 0,4);
		$replace['date_only'] = substr($replace['u_add_date'], 8,2);
		$replace['date_format'] = '"'.substr($replace['u_add_date'], 8,2).'" '.$month_format[$month]." ".substr($replace['u_add_date'], 0,4);
		$current_month = (int)substr($replace['date'], 3,2);
		$replace['current_month'] = $current_month;
		$replace['current_month_name'] = $month_format[$current_month];
		$replace['current_day'] = substr($replace['date'], 0,2);
		$replace['current_year'] = substr($replace['date'], 6,4);

		for ($i = 6; $i < 11; $i++)
		{
			$replace['new_username_'.$i] = generate_user($i);
			$replace['new_password_'.$i] = generate_password($i);
		}
		
		$print_data = $db->Query('SELECT * FROM `billing_print` WHERE id ='.$id)->get_next_row_object(); 
		$content = $print_data->text;
		$caption = $print_data->name;
		
		$font_list = view_form_select_pdf_font::get_option_list();
		$font_name = $font_list[$print_data->font]; 
		
		foreach ($replace as $key => $item)
		{
			$content = str_replace('{'.$key.'}', $item, $content);
		}
		
		$content = str_replace('<font size="5">', '<font size="16">', $content);
		$content = str_replace('<font size="4">', '<font size="12">', $content);
		$content = str_replace('<font size="3">', '<font size="8">', $content);
		$content = str_replace('<font size="2">', '<font size="5">', $content);
		
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Task by kirill@telewest.ru');
		$pdf->SetTitle($caption.' uid '.$uid);
		$pdf->SetSubject($caption.' uid '.$uid);
		//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		
		$pdf->SetMargins(10, 11, 10);
		$pdf->SetAutoPageBreak(TRUE, 15);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->setLanguageArray($l);
		
		$pdf->SetFont($font_name, '', 8);
		// add a page
		$pdf->AddPage();
		
		$pdf->writeHTML($content);

		$pdf->Output('docprint'.$uid, 'I');
		
	}
	
}

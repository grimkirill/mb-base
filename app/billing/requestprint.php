<?php

class requestprint {
	private $Header = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
	"http://www.w3.org/TR/html4/strict.dtd">
	<html lang="en">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Печать</title>
	<!-- Framework CSS -->
	<link rel="stylesheet" href="BASEPATHweb/css/screen.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="BASEPATHweb/css/print.css" type="text/css" media="print">
	<!--[if lt IE 8]><link rel="stylesheet" href="BASEPATHweb/css/ie.css" type="text/css" media="screen, projection"><![endif]-->
	
	<link rel="stylesheet" href="BASEPATHweb/css/jdpicker.css" type="text/css">
	<link rel="stylesheet" href="BASEPATHweb/css/jquery.cleditor.css" type="text/css">

	<div class="container">';
	
	private $Footer = '</div> 
	</body> </html>	';
	
	function printhtml($servicer, $date)
	{
		if (!ACL::has_perm(ACL::acl_billing_request_roll )) { $this->AddContent(ACL::deny()); return ; }
		$this->Header = str_replace('BASEPATH', WEBPATH, $this->Header);
		echo $this->Header.$this->generate_text($servicer, $date).$this->Footer;
	}
	
	function printpdf($servicer, $date)
	{
		if (!ACL::has_perm(ACL::acl_billing_request_roll )) { $this->AddContent(ACL::deny()); return ; }
		
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Task by kirill@telewest.ru');

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		
		$pdf->SetMargins(10, 11, 10);
		$pdf->SetAutoPageBreak(TRUE, 15);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->setLanguageArray($l);
		
		$pdf->SetFont('dejavusans', '', 8);
		// add a page
		$pdf->AddPage();
		
		$pdf->writeHTML($this->generate_text($servicer, $date));

		$pdf->Output('taskprint'.$date, 'I');
	}
	
	function generate_text($servicer, $date)
	{
		global $db;
		$head_array['servicer'] = $db->Query('SELECT `full_name` FROM `request_servicer` WHERE `id` = '.$servicer)->get_next_row_object()->full_name;
		$head_array['date'] = $date;
		$head_out = get_default_value::get('reguest.print.head');
		$foot_out = get_default_value::get('reguest.print.foot');
		
		foreach ($head_array as $key => $item)
		{
			$head_out = str_replace('{'.$key.'}', $item, $head_out);
			$foot_out = str_replace('{'.$key.'}', $item, $foot_out);
		}
		$out = $head_out;
		
		$requests = $db->Query('SELECT *  FROM `requests` WHERE `date` = \''.$date.'\' AND `servicer` = '.$servicer.' ORDER BY  `time_start` ASC ');
		$num = 0;
		while($request = $requests->get_next_row_object()) {
			$tmp_req = new request($request->id);
			$row_array = $tmp_req->get_data_array();
			
			
			$num++;
			$row_array['num'] = $num;
			
						
			$row_out = get_default_value::get('reguest.print.row');
			foreach ($row_array as $key => $item)
			{
				$row_out = str_replace('{'.$key.'}', $item, $row_out);
			}
			$out .= $row_out;
		}
		
		$out .= $foot_out;
		return $out;
		
	}
	
}
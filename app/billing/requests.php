<?php

class requests extends View_Page_Controller {
	public $title = 'Просмотр абонента';
	public $category = 'billing';
	
	function index()
	{
		if (!ACL::has_perm(ACL::acl_billing_request_roll )) { $this->AddContent(ACL::deny()); return ; }
		
		$this->AddContent(View_Element::Link('Добавить заявку', CLASSPATH.'addnew'));
		$this->AddContent(View_Element::Space(3).'|'.View_Element::Space(3));


		$form = new View_Form(array('method' => 'get'));
		$form->add('Поиск', array(
			new view_form_select_request_servicer('servicer', '', array('as_filter' => TRUE)),View_Element::Space(2),
			new view_form_select_request_type('type', '', array('as_filter' => TRUE)),View_Element::Space(2),'C ',
			new View_Form_Date('date_start', date('Y-m-d')), ' ПО ',
			new View_Form_Date('date_stop', date('Y-m-d')),View_Element::Space(2),
			new View_Form_Select('done', '', array('options' => array(0 => '--=--', 1 => 'Выполнено', 2 => 'Не выполнено', 3 => 'Отменена')) ),
			new View_Form_Checkbox('num','', array('caption' => '№'))
			));
		if ($form->get_values())
		{

			
			$this->AddContent(View_Element::Link('Сегодня', CLASSPATH.'?servicer='.$form->return_values['servicer'].'&type=0&date_start='.date('d-m-Y').'&date_stop='.date('d-m-Y').'&done='.$form->return_values['done'].'&form_submit=Принять'));
			$this->AddContent(View_Element::Space(2).'|'.View_Element::Space(2));
			$tommorow = date('d-m-Y', mktime(0,0,0,date("m"),date("d")+1,date("Y")));
			$this->AddContent(View_Element::Link('Завтра', CLASSPATH.'?servicer='.$form->return_values['servicer'].'&type=0&date_start='.$tommorow.'&date_stop='.$tommorow.'&done='.$form->return_values['done'].'&form_submit=Принять'));
			
			$this->AddContent($form);
			
			
			$show_date = TRUE;
			$show_servicer = TRUE;

			
			$sql = "SELECT * FROM `requests` WHERE `date` >= '".$form->return_values['date_start']."' AND `date` <= '".$form->return_values['date_stop']."' ";
			
			if ($form->return_values['date_start'] == $form->return_values['date_stop'])
			{
				$show_date = FALSE;
			} 
			
			if ($form->return_values['servicer'])
			{
				$sql .= ' AND servicer ='.$form->return_values['servicer'];
				$show_servicer = FALSE;
			}
			
			if ($form->return_values['type'])
			{
				$sql .= ' AND type ='.$form->return_values['type'];
			}
			
			if ($form->return_values['done'])
			{
				if ($form->return_values['done'] == 1)
				{
					$sql .= ' AND done = 1';
				}
				
				if ($form->return_values['done'] == 2)
				{
					$sql .= ' AND done = 0';
				}
				
				if ($form->return_values['done'] == 3)
				{
					$sql .= ' AND done = 2';
				}
			}
			
			if (!$show_date && !$show_servicer)
			{
				$this->AddContent(View_Element::Link('HTML', CONTROLLER_PATH.'requestprint/printhtml/'.$form->return_values['servicer'].'/'.$form->return_values['date_start'], TRUE ));
				$this->AddContent(View_Element::Space(2) );
				$this->AddContent(View_Element::Link('PDF', CONTROLLER_PATH.'requestprint/printpdf/'.$form->return_values['servicer'].'/'.$form->return_values['date_start'], TRUE ));
			}
			$this->showlist($sql, $show_date, $show_servicer, $form->return_values['num']);
		}	
		else
		{
			$tommorow = date('d-m-Y', mktime(0,0,0,date("m"),date("d")+1,date("Y")));
			$this->AddContent(View_Element::Link('Завтра', CLASSPATH.'?servicer=0&type=0&date_start='.$tommorow.'&date_stop='.$tommorow.'&done=0&form_submit=Принять'));
			
			$this->AddContent($form);
			$sql = "SELECT * FROM `requests` WHERE `date` >= '".date('Y-m-d')."' AND `date` <= '".date('Y-m-d')."' ";
			$this->showlist($sql);
		}
	}
	
	
	function showlist($sql, $show_date = TRUE, $show_servicer = TRUE, $num = FALSE)
	{
		global $db;
		
		
		$tmp_res = $db->Query($sql.' ORDER BY `date` DESC');
		
		$tb_data = array();
		while ($tmp_res->get_next_row_object())
		{
			$tmp_req = new request($tmp_res->last_row->id);
			$tb_data[] = $tmp_req->get_data_array();
		}
		
		
			$table = new View_Table(array('caption' => '', 
											'edit_link' => CLASSPATH.'edit/'));
			if ($num)
			{
				$table->AddHead('num', '', View_Table::column_type_autonum);
			}
			$table->AddHead('gid_link', 'Абонент');
			$table->AddHead('time', 'Время', FALSE, '90px;');
			$table->AddHead('type', 'Тип');
			if ($show_servicer)
			{
				$table->AddHead('servicer', 'Исполнитель');
			}
			if ($show_date)
			{
				$table->AddHead('date', 'Дата');
			}
			
			$table->AddHead('comment', 'комментарий');
			$table->AddHead('done_print', 'Выполнено');
			$table->AddHead('id', '', View_Table::column_type_edit);
			
			$table->AddData($tb_data);
			
			$this->AddContent($table);
		
	}
	
	
	function addnew()
	{
		global $db, $auth_user;
		
		$this->title = 'Добавление заявки';
		$form_request = new View_Form(array('cancel' => CLASSPATH));
		$form_request->add('', array(
			'Адрес: ',
			new View_Form_Text('gid_data', '', array('width' => '40em', 'caption' => 'Адрес', 'validators'=>array( View_Form_Element::validator_require => 1)))
			));
		$form_request->add('', array(
			new view_form_select_request_servicer('servicer', ''),View_Element::Space(2),
			new view_form_select_request_type('type', ''),View_Element::Space(2),
			new View_Form_Date('date', date('Y-m-d')), ' C ',
			new View_Form_Select('time_start', '', array('options' => request_data::timeList()) ), ' продолжительность: ',
			new View_Form_Select('time_count', '', array('options' => request_data::timeCount()) )
			));
		$form_request->add('Описание', array(
			new View_Form_Textarea('comment', '')
			));
		$form_request->add('', array(
			'Телефон: ',
			new View_Form_Text('phone', '', array('width' => '18em'))
			));

		if ($form_request->get_values())
		{	
			$db->InsertData('requests', $form_request->return_values+array('gid' => 0, 'user' => $auth_user->id));
			
			if ($form_request->return_values['date'] == date('Y-m-d'))
			{
				$tmp_req = new request($db->GetInsertId());
				$tmp_req->do_trigger();
			}
			header('Location: '.WEBPATH.'billing/requests');
		}
		else 
		{	
			$this->AddContent($form_request);
			$this->AddContent('<div id="ajax_request_list"></div><script type="text/javascript">bind_request_list_load(); request_list_load();</script>');
		}	
	}
	
	
	function edit($id)
	{
		global $db, $auth_user;
		if ($data = $db->Query('SELECT * FROM `requests` WHERE id ='.$id)->get_next_row_object())
		{
			$request = new request($id);
			
			
			$properties = new View_Properties();
			$properties->AddTitle('Параметры заявки');
			$properties->AddProperty('Адрес', $request->get_gid_link());
			$properties->AddProperty('Дата', $request->data->date);
			$properties->AddProperty('Время', $request->get_time_print());
			$properties->AddProperty('Тип', $request->get_type());
			$properties->AddProperty('Исполнитель', $request->get_serviser());
			$properties->AddProperty('Комментарий', $request->data->comment);
			$properties->AddProperty('Взята', $request->data->dt.' | '.User_Auth::get_loginid($request->data->user));
			
			//$this->AddContent($properties);
			
			$this->AddContent(View_Element::PanelStandart($properties.View_Element::Button('Редактировать', CLASSPATH.'realedit/'.$id)));
			//$this->AddContent();
			
			$form_comment = new View_Form(array('caption' => 'Добавить заметку', 'border' => TRUE));
			$form_comment->add('', array(
				new View_Form_Textarea('comment', '', array('caption' => 'Адрес', 'validators'=>array( View_Form_Element::validator_require => 1) ))
				));
				
			if ($form_comment->get_values())
			{
				if ($form_comment->return_values['comment'])
				{
					$db->InsertData('request_comments', array('text'=>$form_comment->return_values['comment'], 'gid' => $id, 'user' => $auth_user->id));
				}
			}
				
			$res = $db->Query('SELECT *  FROM request_comments WHERE gid ='.$id);
			while ($item = $res->get_next_row_object())
			{
				$this->AddContent(View_Element::PanelInfo($item->text.' | '.$item->dt.' | '.User_Auth::get_loginid($item->user)));
			}
			
			
			$this->AddContent($form_comment);
			
		}
		
	}
	
	function realedit($id)
	{
		if (!ACL::has_perm(ACL::acl_billing_request_edit )) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM `requests` WHERE id ='.$id)->get_next_row_object())
		{
			$form = new View_Form(array('cancel' => CLASSPATH.'edit/'.$id));
			if ($data->gid == 0)
			{
				$form->add('', array(
					'Адрес: ',
					new View_Form_Text('gid_data', $data->gid_data, array('width' => '40em', 'caption' => 'Адрес', 'validators'=>array( View_Form_Element::validator_require => 1) ) )
				));
			}
			
			$form->add('', array(
				new view_form_select_request_servicer('servicer', $data->servicer),View_Element::Space(2),
				new view_form_select_request_type('type', $data->type),View_Element::Space(2),
				new View_Form_Date('date', $data->date), ' C ',
				new View_Form_Select('time_start', $data->time_start, array('options' => request_data::timeList()) ), ' продолжительность: ',
				new View_Form_Select('time_count', $data->time_count, array('options' => request_data::timeCount()) )
				));
			$form->add('Описание', array(
				new View_Form_Textarea('comment', $data->comment)
				));
			$form->add('', array(
				'Телефон: ',
				new View_Form_Text('phone', $data->phone, array('width' => '18em'))
				));
			$form->add('', array(
				new View_Form_Checkbox('delete', '', array('caption' => 'Удалить'))
				));
			if ($form->get_values())
			{
				if ($form->return_values['delete'])
				{
					$db->DeleteData('requests', array('id' => $id));
				}
				else
				{
					unset($form->return_values['delete']);
					$db->UpdateData('requests', $form->return_values, array('id' => $id));
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
						
					}
				}
				if ($data->gid)
				{
					header('Location: '.WEBPATH.'billing/looking/show/'.$data->gid);
				}
				else
				{
					header('Location: '.WEBPATH.'billing/requests');
				}
				//$this->index();
			}
			else
			{
				$this->AddContent($form);
			}	
		}	
	}
}
<?php

class requeststat extends View_Page_Controller {
	public $title = 'Статистика заявок';
	public $category = 'billing';
	
	function index()
	{
		global $db;
		if (!ACL::has_perm(ACL::acl_billing_request_roll )) { $this->AddContent(ACL::deny()); return ; }
		$form = new View_Form(array('method' => 'get', 'oneline' => TRUE));
		$form->add('Фильтр', array(
			new View_Form_Date('date_start', date('Y-m-d')), ' ПО ',
			new View_Form_Date('date_stop', date('Y-m-d')),View_Element::Space(2),
			new View_Form_Select('done', '', array('options' => array(0 => '--=--', 1 => 'Выполнено', 2 => 'Не выполнено')) )
			));
		if ($form->get_values())
		{
			$this->AddContent($form);
			$sql_done = '';
			
			if ($form->return_values['done'])
			{
				if ($form->return_values['done'] == 1)
				{
					$sql_done = ' AND done = 1';
				}
				
				if ($form->return_values['done'] == 2)
				{
					$sql_done = ' AND done = 0';
				}
			}
			
			$data = array();
			$foot_data = array();
			$table = new View_Table();

			$table->AddHead('name', 'Тип');
			foreach (view_form_select_request_servicer::get_list() as $servicer_id => $servicer_name )
			{
				$foot_data[$servicer_id] = 0;
				$table->AddHead($servicer_id, $servicer_name, View_Table::column_type_no_zero);
			}
			$table->AddHead('total', 'итого');
			
			foreach (view_form_select_request_type::get_list() as $type_id => $type_name)
			{
				$count_row = 0;
				foreach (view_form_select_request_servicer::get_list() as $servicer_id => $servicer_name )
				{
					$sql = 'SELECT COUNT(`id`) as count 
							FROM  `requests` 
							WHERE  `date` >=  \''.$form->return_values['date_start'].'\'
							AND  `date` <=  \''.$form->return_values['date_stop'].'\'
							AND  `type` = '.$type_id.'
							AND  `servicer` ='.$servicer_id.$sql_done;
					$count = $db->Query($sql)->get_next_row_object()->count;
					$data[$type_id][$servicer_id] = $count;
					$foot_data[$servicer_id] += $count;
					$count_row += $count;
				}
				
				$data[$type_id]['total'] = $count_row;
				$data[$type_id]['name'] = $type_name;
			}
			
			
			$table->AddData($data);
			
			
			$table->addFoot('Итого:');
			foreach (view_form_select_request_servicer::get_list() as $servicer_id => $servicer_name )
			{
				$table->addFoot($foot_data[$servicer_id]);
			}
			$table->addFoot(array_sum($foot_data));
			
			$this->AddContent($table);
			
		}
		else
		{
			$this->AddContent($form);
		}
		
	}
	
	
	
}
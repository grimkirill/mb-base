<?php

class analysismac extends View_Page_Controller {
	
	public $title = 'Анализ данных оборудования';
	public $category = 'network';
	
	function show($id)
	{
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$dev = new device($id);
		if ($dev->id)
		{

			$port_headers['port'] = 185;
			$port_headers['comment'] = 130;
			if ($dev->get_port_admin())
			{
				$port_headers['admin'] = 25;
			}
			if ($dev->get_port_active())
			{
				$port_headers['active'] = 22;
			}
			$port_headers['online'] = 22;
			
			
			$port_list = array_fill(1, $dev->get_port_count(), array('port' => FALSE, 'comment' => ''));
			// описываем входящий кабель 
			if ($dev->data->port_in)
			{
				if ($dev->data->gid)
				{
					$dev_parent = new device($dev->data->gid);
					
					$port_list[$dev->data->port_in]['port'] = View_Element::image_exclamation().View_Element::Space(1).'<b>'.View_Element::Link($dev_parent->get_diff_address($dev->data->address_home), 'network/looking/show/'.$dev->data->gid ).'</b>';
				}
				else
				{
					$port_list[$dev->data->port_in]['port'] = View_Element::image_exclamation().View_Element::Space(1).'<b>Входящий</b> ';
				}
			}
			
			//описываем исходящий кабель
			$dev_out_list = $db->Query('SELECT `id` FROM `devices` WHERE `gid` = '.$dev->id);
			while ($dev_out_row = $dev_out_list->get_next_row_object())
			{
				$dev_out = new device($dev_out_row->id);
				
				if ( ($dev_out->data->port_out > 0) AND ($dev_out->data->port_out <= $dev->get_port_count()) AND (!$port_list[$dev_out->data->port_out]['port'])){
					$port_list[$dev_out->data->port_out]['port'] = View_Element::Link($dev_out->get_diff_address($dev->data->address_home), 'network/looking/show/'.$dev_out->id ).View_Element::Space(1).View_Element::image_arrow_right();
				}
			}
			
			
			
			
			$abonents_off_list = array();
			
			$abonents_list = $db->Query('SELECT * FROM `devices_abonents` WHERE `gid` = '.$dev->id.' ');
			while ($abonent_row = $abonents_list->get_next_row_object())
			{
				$added = FALSE;
				if ($abonent_row->port != 0)
				{
					if(!$port_list[$abonent_row->port]['port']){
						$prefix_length = strlen($abonent_row->flat);
						$str = '<b>'.$abonent_row->flat.'</b> ';
						
						$str .= ' <font color="#666">'.$abonent_row->comment.'</font>';
						
						$port_list[$abonent_row->port]['uid'] = $abonent_row->uid;
						
						if ($abonent_row->uid)
						{
							$abonent = new nibs_abonent($abonent_row->uid);
							//$str .= ' '.$abonent->get_address().$abonent->get_fio();
							if ($abonent->get_block())
							{
								$str .= View_Element::Space(1).View_Element::image_lock();
							}
						}
						
						$port_list[$abonent_row->port]['port'] = $str;
						
					}
					
				}
				
			}
			
			
			$port_state_list = $db->Query('SELECT *  FROM `devices_ports` WHERE `gid` = '.$dev->id);
			while ($port = $port_state_list->get_next_row_object())
			{
				
				if ($port->comment)
				{
					$port_list[$port->port]['port'] .= ' [ '.$port->comment.' ]';
				}
				
				$port_list[$port->port]['id'] = $port->id;
				if ($port->admin){
					$port_list[$port->port]['admin'] = '<span style="display: block; width: 100%; height: 19px; cursor: pointer;" ondblclick="block_port('.$port->id.')" id="port_block_'.$port->id.'">'. 
														View_Element::image_active().'</span>';
				}
				else
				{
					$port_list[$port->port]['admin'] = '<span style="display: block; width: 100%; height: 19px; cursor: pointer;" ondblclick="block_port('.$port->id.')" id="port_block_'.$port->id.'">'.
														View_Element::image_stop().'</span>';
				}
				
				if ($port->active)
				{
					$port_list[$port->port]['active'] = View_Element::image_connect();
				}
				else
				{
					$port_list[$port->port]['active'] = View_Element::Space(1);
				}
				
				
			}
			
			
			// analysis
			
				
			foreach ($port_list as $key => $row)
			{
				$port_list[$key]['mac'] = $db->Query('SELECT COUNT(*) as count FROM `devices_port_mac` WHERE `gid` = '.$row['id'])->get_next_row_object()->count;
				$port_list[$key]['abonlist'] = '';
				if ( ($port_list[$key]['mac'] < 500) AND ($port_list[$key]['mac'] > 0) )
				{
					$mac_list = $db->Query('SELECT * FROM `devices_port_mac` WHERE `gid` = '.$row['id']);
					while ($data_mac = $mac_list->get_next_row_object())
					{
						$abonents = $db->Query("SELECT id  FROM `billing_users` WHERE `mac` = '$data_mac->mac'");
						while ($abon_data = $abonents->get_next_row_object())
						{
							$billing_info = new nibs_abonent($abon_data->id);
							if (isset($port_list[$key]['uid'])  AND $port_list[$key]['uid'] == $abon_data->id)
							{
								$port_list[$key]['abonlist'] .= View_Element::image_marker();
							}
							$port_list[$key]['abonlist'] .= View_Element::Link($billing_info->get_address(), 'billing/looking/show/'.$abon_data->id);
							
							if ( ((isset($port_list[$key]['uid'])  AND $port_list[$key]['uid'] != $abon_data->id)) OR (!isset($port_list[$key]['uid'])))
							{
								$port_list[$key]['abonlist'] .= '<span>'.View_Element::SpanClick(' '.View_Element::image_add(), "$(this).parent().load(WEBPATH+'ajax/port_info/addflatuid/$dev->id/$key/$abon_data->id')").'</span>';
							}
							
							$port_list[$key]['abonlist'] .= View_Element::Space(2);
						}
					}
				}
			}
			
			$vlans = $db->Query('SELECT *  FROM `devices_vlans` WHERE `gid` = '.$dev->id.' AND `untagged` = 1');
			while ($port_vlan = $vlans->get_next_row_object())
			{
				$port_list[$port_vlan->port]['vlan'] = $port_vlan->vlan; 
			}
			
			//$this->AddContent($table);
			
			
			$this->AddContent('<h4 >'.$dev->get_address_link().View_Element::Space(5).$dev->get_ip_link().' <font size="-1">[ '.$dev->get_type_name().' ] '.View_Element::Link('edit', 'network/editdevices/edit/'.$dev->id).'</font> </h4>' );
	
			

			$table_port = new View_Table();
			$table_port->AddHead('num', '№', View_Table::column_type_autonum);
			$table_port->AddHead('port', 'Подкл' , 0, '250px');
			$table_port->AddHead('admin', '' );
			$table_port->AddHead('active', '' );
			$table_port->AddHead('vlan', 'vlan' );
			$table_port->AddHead('mac', 'mac' );
			$table_port->AddHead('abonlist', 'Возможные абоненты', 0, '400px;');
			$table_port->AddData($port_list);
			
			
		
			
			$this->AddContent($table_port );
			
		}
	}
	
}

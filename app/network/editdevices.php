<?php

class editdevices extends View_Page_Controller {
	public $title = 'Управление сетью';
	public $category = 'network';
	
	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_network_device_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$id = (int)$id;
		if ($data = $db->Query('SELECT * FROM devices WHERE id = '.$db->EscapeValue($id))->get_next_row_object() )
		{
			$dev = new device($id);
			$form = new View_Form(array('caption' => 'Редактировать устройство', 'method' => 'post', 'border' => FALSE,
										'cancel' => 'network/looking/show/'.$id ));
			$form->add('IP адрес', array( new View_Form_Text('ip', $data->ip)));
			$form->add('Адрес размещения', array( 
				new view_form_select_home('address_home', $data->address_home), ' Подъезд: ',
				new View_Form_Text('address_dorway', $data->address_dorway,	array(
							'caption' => 'Подъезд', 
							'validators'=>array( View_Form_Element::validator_require => 1), 
							'width' => '6em') ),
				'<br>',
				new View_Form_WYSIWYG('address_disposition', $data->address_disposition, array('min' => TRUE))
			));
			
			$form->add('Модель оборудования', array(
				 new view_form_select_device_type('type', $data->type)
			));
			
			$form->add('Параметры доступа', array(
				 'Имя пользователя: ', new View_Form_Text('access_username', $data->access_username),
				 ' Пароль: ', new View_Form_Text('access_password', $data->access_password)			 
			));
			
			$form->add('', array(
				 'SNMP Community read: ', new View_Form_Text('access_snmp_read', $data->access_snmp_read),
				 ' write: ',new View_Form_Text('access_snmp_write', $data->access_snmp_write)
			));
			
			$port_list = array(0 => '--=--');
			$port_list_out = array(0 => '--=--');
			for ($i = 1; $i <= $dev->get_port_count(); $i++) { $port_list[$i] = $i; }
			if ($data->gid)
			{
				$dev_out = new device($data->gid);
				for ($i = 1; $i <= $dev_out->get_port_count(); $i++) { $port_list_out[$i] = $i; }
			}
			
			$gid_list = array(0 => '--=--');
			$devices_list = $db->Query('SELECT d.id, d.address_home, d.address_dorway, d.ip, h.home, s.street FROM devices as d LEFT JOIN address_home h ON h.id = d.address_home LEFT JOIN address_street s ON s.id = h.gid WHERE d.id != '.$data->id.' ORDER BY s.street ASC, h.home+0')->get_rows_array();
			foreach ($devices_list as $key => $item)
			{
				$gid_list[$item['id']] = view_form_select_home::get_home_print($item['address_home']).', п '.$item['address_dorway'].' ('.$item['ip'].')';
			}
			
			$form->add('Подключение', array(
				'Подключен к: ', new View_Form_Select('gid', $data->gid, array('options' => $gid_list)),
				'<br>Входящий порт: ', new View_Form_Select('port_in', $data->port_in, array('options' => $port_list)),
				' Исходящий порт: ', new View_Form_Select('port_out', $data->port_out, array('options' => $port_list_out))
			));
			
			if ($db->Query('SELECT COUNT(*) as count  FROM `devices` WHERE `gid` = '.$id)->get_next_row_object()->count == 0)
			{
				$form->add('', array(
					new View_Form_Checkbox('delete', '', array('caption' => 'Удалить')) 
				));
			}
			
			if ($form->get_values())
			{
				if ($form->return_values['delete'])
				{
					//FIXME: make delete dev
					$this->_delete_all($id);
				}
				else
				{
					unset($form->return_values['delete']);
					$db->UpdateData('devices', $form->return_values, array('id' => $data->id));
					if ($db->GetAffectedRows() > 0)
					{
						$dev = new device($data->id);
						$dev->update();
						$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
						
					}
					header('Location: '.WEBPATH.'network/looking/show/'.$data->id);
				}
			}
			else
			{
				$this->AddContent($form);
			}
			
			$this->AddContent(View_Element::Link('Отметить на карте', CLASSPATH.'reset_on_map/'.$id));
			if (($data->mapx != 0 ) OR ($data->mapy != 0 ))
			{
				$this->AddContent(View_Element::Space(3).View_Element::Link('Убрать с карты', CLASSPATH.'delete_on_map/'.$id));
			}
		}
		
		
	}
	
	function _delete_all($id)
	{
		global $db;
		$ports = $db->Query('SELECT *  FROM `devices_ports` WHERE `gid` = '.$id);
		while ($port = $ports->get_next_row_object())
		{
			$db->DeleteData('devices_port_active', array('gid' => $port->id));
			$db->DeleteData('devices_port_admin', array('gid' => $port->id));
			$db->DeleteData('devices_port_mac', array('gid' => $port->id));
			$db->DeleteData('devices_ports', array('id' => $port->id));
			
			
		}
		$db->DeleteData('devices_tags', array('gid' => $id));
		$db->DeleteData('devices_vlans', array('gid' => $id));
		$db->DeleteData('devices_vlan_history', array('gid' => $id));
		$db->DeleteData('devices_ping', array('gid' => $id));
		$db->DeleteData('devices_abonents', array('gid' => $id));
		$db->DeleteData('devices', array('id' => $id));
		
		header('Location: '.WEBPATH.'network');
	}
	
	function reset_on_map($id)
	{
		if (!ACL::has_perm(ACL::acl_network_device_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$db->UpdateData('devices', array('mapx' => 10, 'mapy' => 10), array('id' => (int)$id));
		header('Location: '.WEBPATH.'network/map');
	}
	
	function delete_on_map($id)
	{
		if (!ACL::has_perm(ACL::acl_network_device_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$db->UpdateData('devices', array('mapx' => 0, 'mapy' => 0), array('id' => (int)$id));
	}
}
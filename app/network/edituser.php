<?php

class edituser extends View_Page_Controller {
	public $title = 'Управление сетью';
	public $category = 'network';
	
	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_network_device_useradmin)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM devices_abonents WHERE id = '.$db->EscapeValue($id))->get_next_row_object() )
		{
			$dev = new device($data->gid);
			$form = new View_Form(array('caption' => 'Редактировать пользователя', 'method' => 'post', 'border' => FALSE,
					'cancel' => 'network/looking/show/'.$data->gid ));
			$form->add('Квартира', array( new View_Form_Text('flat', $data->flat)));
			$form->add('uid по биллингу', array( new View_Form_Text('uid', $data->uid)));
			$port_list = array(0 => '--=--');
			for ($i = 1; $i <= $dev->get_port_count(); $i++) { $port_list[$i] = $i; }
			$form->add('№ Порта', array( new View_Form_Select('port', $data->port, array('options' => $port_list))));
			$form->add('Комментарий', array( new View_Form_Textarea('comment', $data->comment)));
			$form->add('', array( new View_Form_Checkbox('delete','', array('caption' => 'удалить'))));
			if ($form->get_values())
			{
				if (!$form->return_values['delete'])
				{
					unset($form->return_values['delete']);
					$db->UpdateData('devices_abonents', $form->return_values, array('id' => $data->id));
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
					}
				}
				else
				{
					$db->DeleteData('devices_abonents', array('id' => $data->id));
				}
				
				header('Location: '.WEBPATH.'network/looking/show/'.$data->gid);
			}
			else
			{
				$this->AddContent($form);
			}
		}	
	}
	
}
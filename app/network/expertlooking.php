<?php

class expertlooking extends View_Page_Controller {
	public $title = 'Управление сетью';
	public $category = 'network';
	public $out_left_menu = FALSE;

	
	function show($id)
	{
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$dev = new device($id);
		if ($dev->id)
		{

			$port_headers['port'] = 185;
			$port_headers['comment'] = 130;
			if ($dev->get_port_admin())
			{
				$port_headers['admin'] = 25;
			}
			if ($dev->get_port_active())
			{
				$port_headers['active'] = 22;
			}
			$port_headers['online'] = 22;
			
			
			$port_list = array_fill(1, $dev->get_port_count(), array('port' => FALSE, 'comment' => ''));
			// описываем входящий кабель 
			if ($dev->data->port_in)
			{
				if ($dev->data->gid)
				{
					$dev_parent = new device($dev->data->gid);
					
					$port_list[$dev->data->port_in]['port'] = View_Element::image_exclamation().View_Element::Space(1).'<b>'.View_Element::Link($dev_parent->get_diff_address($dev->data->address_home), 'network/looking/show/'.$dev->data->gid ).'</b>';
				}
				else
				{
					$port_list[$dev->data->port_in]['port'] = View_Element::image_exclamation().View_Element::Space(1).'<b>Входящий</b> ';
				}
			}
			
			//описываем исходящий кабель
			$dev_out_list = $db->Query('SELECT `id` FROM `devices` WHERE `gid` = '.$dev->id);
			while ($dev_out_row = $dev_out_list->get_next_row_object())
			{
				$dev_out = new device($dev_out_row->id);
				
				if ( ($dev_out->data->port_out > 0) AND ($dev_out->data->port_out <= $dev->get_port_count()) AND (!$port_list[$dev_out->data->port_out]['port'])){
					$port_list[$dev_out->data->port_out]['port'] = View_Element::Link($dev_out->get_diff_address($dev->data->address_home), 'network/looking/show/'.$dev_out->id ).View_Element::Space(1).View_Element::image_arrow_right();
				}
			}
			
			$port_state_list = $db->Query('SELECT *  FROM `devices_ports` WHERE `gid` = '.$dev->id);
			while ($port = $port_state_list->get_next_row_object())
			{
				//$port_list[$port->port]['comment'] = $port->comment;
				
				$port_list[$port->port]['comment'] = '<span style="display: block; width: 100%; height: 19px;" ondblclick="make_new_comment('.$port->id.')" id="port_comment_'.$port->id.'">'.$port->comment.'</span>';
				
				$port_list[$port->port]['id'] = $port->id;
				if ($port->admin){
					$port_list[$port->port]['admin'] = '<span style="display: block; width: 100%; height: 19px; cursor: pointer;" ondblclick="block_port('.$port->id.')" id="port_block_'.$port->id.'">'. 
														View_Element::image_active().'</span>';
				}
				else
				{
					$port_list[$port->port]['admin'] = '<span style="display: block; width: 100%; height: 19px; cursor: pointer;" ondblclick="block_port('.$port->id.')" id="port_block_'.$port->id.'">'.
														View_Element::image_stop().'</span>';
				}
				
				if ($port->active)
				{
					$port_list[$port->port]['active'] = View_Element::image_connect();
				}
				else
				{
					$port_list[$port->port]['active'] = View_Element::Space(1);
				}
				
				$port_list[$port->port]['online'] = View_Element::Space(1);
				
			}
			
			
			$abonents_off_list = array();
			
			$abonents_list = $db->Query('SELECT * FROM `devices_abonents` WHERE `gid` = '.$dev->id.' ');
			while ($abonent_row = $abonents_list->get_next_row_object())
			{
				$added = FALSE;
				if ($abonent_row->port != 0)
				{
					if(!$port_list[$abonent_row->port]['port']){
						$prefix_length = strlen($abonent_row->flat);
						$str = '<b>'.$abonent_row->flat.'</b> ';
						
						$str .= ' <font color="#666">'.$abonent_row->comment.'</font>';
						
						
						if ($abonent_row->uid)
						{
							$abonent = new nibs_abonent($abonent_row->uid);
							//$str .= ' '.$abonent->get_address().$abonent->get_fio();
							if ($abonent->is_online())
							{
								$port_list[$abonent_row->port]['online'] =  View_Element::image_online();
							}
							if ($abonent->get_block())
							{
								$str .= View_Element::Space(1).View_Element::image_lock();
							}
						}
						
						$port_list[$abonent_row->port]['port'] = $str;
						
					}
					
				}
				
			}
			
			
			foreach ($port_list as $key => $row)
			{
				$port_list[$key]['mac'] = $db->Query('SELECT COUNT(*) as count FROM `devices_port_mac` WHERE `gid` = '.$row['id'])->get_next_row_object()->count;
				$port_list[$key]['broadcast'] = 0;
				if ($tmp_data = $db->Query('SELECT *  FROM devices_port_broadcast WHERE port = '.$row['id'])->get_next_row_object()) 
				{
					$port_list[$key]['broadcast'] = (int)($tmp_data->diff/300);
				}
			}
			
			$vlans = $db->Query('SELECT *  FROM `devices_vlans` WHERE `gid` = '.$dev->id.' AND `untagged` = 1');
			while ($port_vlan = $vlans->get_next_row_object())
			{
				$port_list[$port_vlan->port]['vlan'] = $port_vlan->vlan; 
			}
			
			//$this->AddContent($table);
			
			
			$this->AddContent('<h4 >'.$dev->get_address_link().View_Element::Space(5).$dev->get_ip_link().' <font size="-1">[ '.$dev->get_type_name().' ] '.View_Element::Link('edit', 'network/editdevices/edit/'.$dev->id).'</font> </h4>' );
	
			

			$table_port = new View_Table();
			$table_port->AddHead('num', '№', View_Table::column_type_autonum);
			$table_port->AddHead('port', 'Подкл' );
			$table_port->AddHead('comment', 'Comment' );
			$table_port->AddHead('admin', '' );
			$table_port->AddHead('active', '' );
			$table_port->AddHead('online', '' );
			$table_port->AddHead('vlan', 'vlan' );
			$table_port->AddHead('mac', 'mac' );
			$table_port->AddHead('broadcast', 'Broadcast/sec');
			$table_port->AddData($port_list);
			
			
			
			$this->AddContent($table_port );
			
		}
	}
	
}

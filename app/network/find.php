<?php
class find extends View_Page_Controller {
	public $title = 'Управление сетью';
	public $category = 'network';
	
	function index()
	{
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('method' => 'get'));
		
		$form->add('Адрес размещения', array( 
			new view_form_select_home('address_home', '', array('as_filter' => TRUE)), ' Подъезд: ',
			new View_Form_Text('address_dorway','',	array('width' => '6em') )
		));
		$form->add('IP адрес', array('ip: ' , new View_Form_Text('ip',''), ' Модель: ',
				new view_form_select_device_type('type', FALSE, array('as_filter' => TRUE)),
				' Тэг: ' , new View_Form_Select('tag', '', array('options' => device_tags::get_list_null() )) 
				));
				
		$sql = FALSE;
		$dev_home_list = FALSE;
		if ($form->get_values())
		{
			//$this->AddContent(View_Element::PanelSuccess(print_r($form->return_values, TRUE)));
			if (count($form->return_values['address_home']) > 0)
			{
				$sql = 'SELECT *, (availability_active / availability_total) as availability FROM `devices` WHERE address_home IN ('.implode(', ',$form->return_values['address_home']).') ';
			}
			
			if ($form->return_values['address_dorway'] != '')
			{
				$sql .= 'AND address_dorway LIKE '.$db->EscapeValue($form->return_values['address_dorway']);
			}
				
			if ($form->return_values['ip'] != '')
			{
				$form->return_values['ip'] = str_replace('*', '%', $form->return_values['ip']);
				$sql .= 'AND ip LIKE '.$db->EscapeValue($form->return_values['ip']);
			}
			
			if ($form->return_values['type'] != 0)
			{
				$sql .= 'AND type ='.$db->EscapeValue((int)$form->return_values['type']);
			}
			
			if ($form->return_values['tag'] != 0)
			{
				$sql .= 'AND id IN (SELECT `gid` FROM `devices_tags` WHERE `tag` = '.$db->EscapeValue((int)$form->return_values['tag']).') ';
			}
			
			if ((count($form->return_values['address_home']) == 1) && ($form->return_values['address_home']))
			{
				if ($home = array_pop($form->return_values['address_home']))
				{
					$dev_home_list = $db->Query('SELECT id FROM `devices` WHERE address_home = '.$home)->get_rows_array_id('id', 'id');
				}
			}
		}
		$this->AddContent(View_Element::Link('Добавить устройство', 'network/newdevices'));
		$this->AddContent($form);
		
		if ($sql)
		{
			$this->show_list($sql);
		}
		
		if ($dev_home_list)
		{
			$this->show_abonent_list($dev_home_list);
		}
		
	}
	
	
	function show_abonent_list($adev_list)
	{
		if (!is_array($adev_list))
		{
			return;
		}
		global $db;
		$abon_list = $db->Query('SELECT *  FROM `devices_abonents` WHERE `gid` IN ( '.implode(', ', $adev_list).' ) ORDER BY `flat` +0');
		//$abon_list = $db->Query('SELECT *  FROM `devices_abonents` WHERE `gid` IN ( '.implode(', ', $adev_list).' ) GROUP BY `flat` +0');
		
		$data = array();
		while ($abonent = $abon_list->get_next_row_object())
		{
			$abon_data['flat'] = View_Element::Link($abonent->flat, 'network/looking/show/'.$abonent->gid.'/'.$abonent->port);
			$abon_data['comment'] = $abonent->comment;
			$abon_data['port'] = View_Element::Link($abonent->port, 'network/looking/show/'.$abonent->gid.'/'.$abonent->port);
			if ($abonent->uid)
			{
				$abon = new nibs_abonent($abonent->uid);
				
				
				$abon_data['packet'] = $abon->get_packet();
				if ($abon->is_online())
				{
					$abon_data['addr'] = View_Element::Link($abon->get_address(), 'billing/looking/show/'.$abonent->uid).View_Element::Space(2).View_Element::image_online();
				}
				else
				{
					$abon_data['addr'] = View_Element::Link($abon->get_address(), 'billing/looking/show/'.$abonent->uid);
				}
				$abon_data['deposit'] = $abon->get_deposit();

				$status = $abon->get_status();
                		if($status == nibs_abonent::STATUS_USER_BLOCKED) {
                        		$abon_data['status'] = "<font color=red>блокирован</font>";
                		} else if($status == nibs_abonent::STATUS_USER_DELETED) {
                        		$abon_data['status'] = "<font color=red>удален</font>";
                		} else if($status == nibs_abonent::STATUS_USER_FROZEN) {
					$abon_data['status'] = "<font color=DarkOrange>заморожен</font>";
				} else {
                        		$abon_data['status'] = "<font color=blue>активный</font>";
                		}

			}
			if ($abonent->port > 0)
			{
				$dev = new device($abonent->gid);
				if ($dev->get_port_admin())
				{
					if ($port = $db->Query('SELECT *  FROM `devices_ports` WHERE `gid` = '.$dev->id.' AND port = '.$abonent->port)->get_next_row_object() )
						{
							if ($port->admin){
							$abon_data['state'] = View_Element::image_active();
							}
							else
							{
								$abon_data['state'] = View_Element::image_stop();
							}
							
							if ($port->active)
							{
								$abon_data['state'] .= View_Element::Space(2).View_Element::image_connect();
							}
						}
				}
				
			}
			else
			{
				$abon_data['state'] = '';
			}
			
			array_push($data, $abon_data);
		}
		
		$table = new View_Table();
		$table->AddHead('num', '', View_Table::column_type_autonum );
		$table->AddHead('flat', 'Квартира' );
		$table->AddHead('comment', 'Коммент');
		$table->AddHead('port', '№ порта');
		$table->AddHead('addr', 'Адрес');
		$table->AddHead('state', '');
		$table->AddHead('packet', 'Тариф' );
		$table->AddHead('deposit', 'Баланс', View_Table::column_type_number);
		$table->AddHead('status', 'Статус');
			
		$table->AddData($data);
			
		$this->AddContent($table);
	}
	
	
	function show_list($sql)
	{
		global $db;
		if ($devices_list = $db->Query($sql)->get_rows_array())
		{
		
			if (count($devices_list) == 1)
			{
				header('Location: '.WEBPATH.'network/looking/show/'.$devices_list[0]['id']);
			}
			
			if (count($devices_list) > 0)
			{
				foreach ($devices_list as $key => $item)
				{
					$home = view_form_select_home::get_home_print($item['address_home']).', п '.$item['address_dorway'];
					$devices_list[$key]['home'] = View_Element::Link($home, 'network/looking/show/'.$item['id']);
					$devices_list[$key]['ip'] = '<a href="http://'.$item['ip'].'" target="blank">'.$item['ip'].'</a>';
					$devices_list[$key]['type'] = device_types::get_type_name($item['type']);
				}
			} 
			
			$table = new View_Table(array('edit_link' => 'network/editdevices/edit/'));

			$table->AddHead('num', '', View_Table::column_type_autonum );
			$table->AddHead('home', 'Адрес', FALSE, '250px' );
			$table->AddHead('ip', 'IP', FALSE, '100px');
			$table->AddHead('type', 'Тип');
			$table->AddHead('ping', 'ping', View_Table::column_type_bool, '60px' );
			$table->AddHead('availability', 'Доступность');
			$table->AddHead('id', '', View_Table::column_type_edit);
			
			$table->AddData($devices_list);
			
			$this->AddContent($table);
			
			
		}
		else
		{
			$this->AddContent(View_Element::PanelNotice('Ничего не найдено.'));
		}
	}
	
}

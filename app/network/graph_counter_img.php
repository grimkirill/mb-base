<?php

header("Content-type: image/png");

class graph_counter_img {
	
	function base($id)
	{
		global $db;
		
		if ($data = $db->Query('SELECT * FROM  `graph_counter` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$this->out($data->id, $data->name);
		}
	}
	
	
	function out($id, $title = '', $start = '-85800', $stop = '-1')
	{
			$rrd_sub_name = BASEPATH.'rrd/graph_counter_'.$id.'.rrd';
			$cmd = 'rrdtool graph - --imgformat=PNG --start='.$start.' --end='.$stop.' ';
			if ($title)
			{
				$cmd .= ' --title="'.$title.'" ';
			}
			$cmd .= ' --base=1000 --height=200 --width=700 --alt-autoscale-max --lower-limit=0 --vertical-label="" --slope-mode --font TITLE:12: --font AXIS:8: --font LEGEND:10: --font UNIT:8: DEF:a="'.$rrd_sub_name.'":value:AVERAGE AREA:a#4444FFFF:""  GPRINT:a:LAST:"Current\:%8.0lf"  GPRINT:a:AVERAGE:"Average\:%8.2lf"  GPRINT:a:MAX:"Maximum\:%8.0lf"';
			$fp = popen($cmd, "r");
			
			if (isset($fp)) {
				fpassthru($fp);
			}
		
	}
	
	
	function lastday($id)
	{
		global $db;
		
		if ($data = $db->Query('SELECT * FROM  `graph_counter` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$this->out($data->id, '');
		}
	}
	
	function lasthalfday($id)
	{
		global $db;
		
		if ($data = $db->Query('SELECT * FROM  `graph_counter` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$this->out($data->id, '', -42600);
		}
	}
	
	function lastweek($id)
	{
		global $db;
		
		if ($data = $db->Query('SELECT * FROM  `graph_counter` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$this->out($data->id, '', '-1w');
		}
	}
	
	function lastmonth($id)
	{
		global $db;
		
		if ($data = $db->Query('SELECT * FROM  `graph_counter` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$this->out($data->id, '', '-1m');
		}
	}
	
	function lastyear($id)
	{
		global $db;
		
		if ($data = $db->Query('SELECT * FROM  `graph_counter` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$this->out($data->id, '', '-1y');
		}
	}
	
	
}

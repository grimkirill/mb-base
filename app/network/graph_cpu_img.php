<?php

header("Content-type: image/png");

class graph_cpu_img {
	
	function base($id)
	{

			$this->out($id);
	}
	
	
	function out($id, $title = '', $start = '-86400', $stop = 'now')
	{
			$rrd_sub_name = BASEPATH.'rrd/dlink_cpu_'.$id.'.rrd';
			$cmd = 'rrdtool graph - --imgformat=PNG --start='.$start.' --end='.$stop.' ';
			if ($title)
			{
				$cmd .= ' --title="'.$title.'" ';
			}
			$cmd .= ' --base=1000 --height=200 --width=600 --alt-autoscale-max --lower-limit=0 --vertical-label="" --slope-mode --font TITLE:12: --font AXIS:8: --font LEGEND:10: --font UNIT:8: DEF:a="'.$rrd_sub_name.'":value:AVERAGE AREA:a#4444FFFF:""  GPRINT:a:LAST:"Current\:%8.0lf"  GPRINT:a:AVERAGE:"Average\:%8.2lf"  GPRINT:a:MAX:"Maximum\:%8.0lf"';
			$fp = popen($cmd, "r");
			
			if (isset($fp)) {
				fpassthru($fp);
			}
		
	}
	
	
	function lastday($id)
	{

			$this->out($id, '');
		
	}
	
	function lasthalfday($id)
	{

			$this->out($id, '', -43200);
		
	}
	
	function lastweek($id)
	{

			$this->out($id, '', '-1w');
		
	}
	
	function lastmonth($id)
	{

			$this->out($id, '', '-1m');
		
	}
	
	
}
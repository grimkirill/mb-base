<?php
header("Content-type: image/png");

class graph_port {
	
	function show($id)
	{
		$this->out($id);
	}
	
	function small($id)
	{
		$start = '-86100';
		$stop = 'now';
		
		$rrd = BASEPATH.'rrd/ports/port_'.$id.'.rrd';
			$cmd = 'rrdtool graph - --imgformat=PNG --start='.$start.' --end='.$stop.' ';
			$cmd .= ' --base=1024 --height=110 --width=223 --alt-autoscale-max --lower-limit=0 --vertical-label="" --slope-mode --font TITLE:12: --font AXIS:8: --font LEGEND:10: --font UNIT:8: '.
			'DEF:c="'.$rrd.'":input:AVERAGE DEF:a="'.$rrd.'":output:AVERAGE CDEF:cdefa=a,8,* CDEF:cdefe=c,8,* '.
			'AREA:cdefe#00CF00FF:""  LINE1:cdefa#002A97FF:"" ';

			$fp = popen($cmd, "r");
			
			if (isset($fp)) {
				fpassthru($fp);
			}
	}
	
	function out($id,  $start = '-86100', $stop = '-300')
	{
			$rrd = BASEPATH.'rrd/ports/port_'.$id.'.rrd';
			$cmd = 'rrdtool graph - --imgformat=PNG --start='.$start.' --end='.$stop.' ';

			$cmd .= ' --base=1024 --height=200 --width=700 --alt-autoscale-max --lower-limit=0 --vertical-label="" --slope-mode --font TITLE:12: --font AXIS:8: --font LEGEND:10: --font UNIT:8: '.
			'DEF:c="'.$rrd.'":output:AVERAGE DEF:a="'.$rrd.'":input:AVERAGE CDEF:cdefa=a,8,* CDEF:cdefe=c,8,* '.
			'AREA:cdefa#00CF00FF:"Inbound"  GPRINT:cdefa:LAST:" Current\:%8.2lf %s"  GPRINT:cdefa:AVERAGE:"Average\:%8.2lf %s"  GPRINT:cdefa:MAX:"Maximum\:%8.2lf %s\n"  LINE1:cdefe#002A97FF:"Outbound"  GPRINT:cdefe:LAST:"Current\:%8.2lf %s"  GPRINT:cdefe:AVERAGE:"Average\:%8.2lf %s"  GPRINT:cdefe:MAX:"Maximum\:%8.2lf %s"';

			$fp = popen($cmd, "r");
			
			if (isset($fp)) {
				fpassthru($fp);
			}
		
	}
	
	function lastmonth($id)
	{
		
		$this->out($id, '-1m');
		
	}
	
	function last_week($id)
	{
		$this->out($id, '-1w');
	}
	
	function last_day($id)
	{
		$this->out($id);
	}
	
	function last_year($id)
	{
		$this->out($id, '-1y');
	}
	
}
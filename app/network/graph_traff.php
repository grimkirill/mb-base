<?php
header("Content-type: image/png");

class graph_traff {
	
	function show()
	{
		$this->out();
	}
	

	function out( $start = '-86100', $stop = '-300')
	{
			$rrd = BASEPATH.'rrd/traff_all.rrd';
			$cmd = 'rrdtool graph - --imgformat=PNG --start='.$start.' --end='.$stop.' ';

			$cmd .= ' --base=1024 --height=200 --width=700 --alt-autoscale-max --lower-limit=0 --vertical-label="" --slope-mode --font TITLE:12: --font AXIS:8: --font LEGEND:10: --font UNIT:8: '.
			'DEF:c="'.$rrd.'":output:AVERAGE DEF:a="'.$rrd.'":input:AVERAGE CDEF:cdefa=a,8,* CDEF:cdefe=c,8,* '.
			'AREA:cdefa#00CF00FF:"Inbound"  GPRINT:cdefa:LAST:" Current\:%8.2lf %s"  GPRINT:cdefa:AVERAGE:"Average\:%8.2lf %s"  GPRINT:cdefa:MAX:"Maximum\:%8.2lf %s\n"  LINE1:cdefe#002A97FF:"Outbound"  GPRINT:cdefe:LAST:"Current\:%8.2lf %s"  GPRINT:cdefe:AVERAGE:"Average\:%8.2lf %s"  GPRINT:cdefe:MAX:"Maximum\:%8.2lf %s"';

			$fp = popen($cmd, "r");
			
			if (isset($fp)) {
				fpassthru($fp);
			}
		
	}
	
	function lastmonth()
	{
		
		$this->out( '-1m');
		
	}
	
	function last_week()
	{
		$this->out( '-1w');
	}
	
	function last_day()
	{
		$this->out();
	}
	
	function last_year()
	{
		$this->out( '-1y');
	}
	
}
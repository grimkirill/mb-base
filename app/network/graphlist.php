<?php

class graphlist extends View_Page_Controller {
	public $title = 'Управление сетью простые графики';
	public $category = 'network';
	
	function index()
	{
		global $db;
		$list = $db->Query('SELECT *  FROM `graph_counter` WHERE `use` = 1');
		while ($item = $list->get_next_row_object())
		{
			$img = '<img alt="" src="'.WEBPATH.'network/graph_counter_img/base/'.$item->id.'">';
			$this->AddContent(View_Element::Link($img, CLASSPATH.'show/'.$item->id));
			
		}
	}
	
	function show($id)
	{
		global $db;
		
		if ($data = $db->Query('SELECT * FROM  `graph_counter` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$this->AddContent('<h4>'.$data->name.'</h4>');
			$this->AddContent('<img alt="" src="'.WEBPATH.'network/graph_counter_img/lasthalfday/'.$data->id.'">');
			$this->AddContent('<img alt="" src="'.WEBPATH.'network/graph_counter_img/lastday/'.$data->id.'">');
			$this->AddContent('<img alt="" src="'.WEBPATH.'network/graph_counter_img/lastweek/'.$data->id.'">');
			$this->AddContent('<img alt="" src="'.WEBPATH.'network/graph_counter_img/lastmonth/'.$data->id.'">');
			$this->AddContent('<img alt="" src="'.WEBPATH.'network/graph_counter_img/lastyear/'.$data->id.'">');
			
			
		}
	}
	
}
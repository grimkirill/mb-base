<?php

class index extends View_Page_Controller {
	public $title = 'Управление сетью';
	public $category = 'network';
	public $first_call = FALSE;
	
	function index()
	{
		if ($this->first_call == FALSE)
		{
			$this->first_call = TRUE;
			if (!ACL::has_perm(ACL::acl_network_device_roll)) { $this->AddContent(ACL::deny()); return ; }
			global $db;
			$devices_list = $db->Query('SELECT * FROM `devices`  WHERE `ping` = 0 AND `ip` != \'\'')->get_rows_array();
			
			if (is_array($devices_list))foreach ($devices_list as $key => $item)
			{
				$home = view_form_select_home::get_home_print($item['address_home']).', п '.$item['address_dorway'];
				$devices_list[$key]['home'] = View_Element::Link($home, 'network/looking/show/'.$item['id']);
				$devices_list[$key]['ip'] = '<a href="http://'.$item['ip'].'" target="blank">'.$item['ip'].'</a>';
				$devices_list[$key]['type'] = device_types::get_type_name($item['type']);
				$devices_list[$key]['date'] = @$db->Query('SELECT *  FROM `devices_ping` WHERE `gid` = '.$item['id'].' ORDER BY `dt` DESC LIMIT 1')->get_next_row_object()->dt;
			} 
			
			$table = new View_Table(array('edit_link' => 'network/editdevices/edit/'));
			
			$table->AddHead('num', '', View_Table::column_type_autonum );
			$table->AddHead('home', 'Адрес', FALSE, '250px' );
			$table->AddHead('ip', 'IP', FALSE, '100px');
			$table->AddHead('type', 'Тип');
			//$table->AddHead('ping', 'ping', View_Table::column_type_bool, '60px' );
			$table->AddHead('date', 'Время' );
			$table->AddHead('id', '', View_Table::column_type_edit);
			
			$table->AddData($devices_list);
			$this->AddContent('<h4>Оборудование в режиме OFFLine</h4>');
			$this->AddContent($table);
		}
		
	}
	
	
}
<?php
error_reporting(E_ALL);

class looking extends View_Page_Controller {
	public $title = 'Управление сетью';
	public $category = 'network';
	
	/*
	function import($id)
	{
		global $db;
		$db_import = new DB_MySql_Driver(array(
			'hostname' => '87.226.247.250',
			'username' => 'network',
			'password' => 'CSNdYfceb5y7XqZX',
			'database' => 'network',
			'char_set' => '',
			'dbcollat' => ''
		));
		
		$dev = new device($id);
		$sql = 'SELECT *  FROM `switch` WHERE `ip` LIKE \''.$dev->data->ip.'\''; 
	//	echo $sql;
		$db_import->Query($sql)->get_next_row_object();
		if ($parent = $db_import->Query($sql)->get_next_row_object())
		{
		//	print_r($parent); echo '<hr>';
			$abonents = $db_import->Query('SELECT *  FROM `abonents` WHERE useruid != 0 AND `switchid` = '.$parent->id);
			while ($abonent = $abonents->get_next_row_object())
			{
			//	print_r($abonent); echo '<hr>';
				if ($db->Query('SELECT COUNT(*) as count FROM devices_abonents WHERE uid ='.$abonent->useruid)->get_next_row_object()->count == 0)
				{
					$db->InsertData('devices_abonents', array(
						'gid' => $dev->id,
						'uid' => $abonent->useruid,
						'port' => $abonent->switchport,
						'flat' => $abonent->flat,
						'comment' => $abonent->comment
					));
				}
			}
			
		}
		
		
		$this->show($id);
	}
	*/
	
	function update_data($id)
	{
		$id = (int)$id;
		$dev = new device($id);
		$dev->read_port_data();
		$this->show($id);
		
	}
	
	function show($id, $select_port = FALSE)
	{
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$dev = new device($id);
		if ($dev->id)
		{

			$port_headers['port'] = 185;
			$port_headers['comment'] = 130;
                        //$port_headers['what']=3;
			if ($dev->get_port_admin())
			{
				$port_headers['admin'] = 25;
			}
			if ($dev->get_port_active())
			{
				$port_headers['active'] = 22;
			}
			$port_headers['online'] = 22;
			
			$port_list = array_fill(1, $dev->get_port_count(), array('port' => FALSE, 'comment' => ''));
			// описываем входящий кабель 
			if ($dev->data->port_in)
			{
				if ($dev->data->gid)
				{
					$dev_parent = new device($dev->data->gid);
					
					$port_list[$dev->data->port_in]['port'] = View_Element::image_exclamation().View_Element::Space(1).'<b>'.View_Element::Link($dev_parent->get_diff_address($dev->data->address_home), 'network/looking/show/'.$dev->data->gid ).'</b>';
				}
				else
				{
					$port_list[$dev->data->port_in]['port'] = View_Element::image_exclamation().View_Element::Space(1).'<b>Входящий</b> ';
				}
			}
			
			//описываем исходящий кабель
			$dev_out_list = $db->Query('SELECT `id` FROM `devices` WHERE `gid` = '.$dev->id);
			while ($dev_out_row = $dev_out_list->get_next_row_object())
			{
				$dev_out = new device($dev_out_row->id);
				
				if ( ($dev_out->data->port_out > 0) AND ($dev_out->data->port_out <= $dev->get_port_count()) AND (!$port_list[$dev_out->data->port_out]['port'])){
					$port_list[$dev_out->data->port_out]['port'] = View_Element::Link($dev_out->get_diff_address($dev->data->address_home), 'network/looking/show/'.$dev_out->id ).View_Element::Space(1).View_Element::image_arrow_right();
				}
			}
			
			$port_state_list = $db->Query('SELECT *  FROM `devices_ports` WHERE `gid` = '.$dev->id);
			while ($port = $port_state_list->get_next_row_object())
			{
				//$port_list[$port->port]['comment'] = $port->comment;
				
				$port_list[$port->port]['comment'] = '<span style="display: block; width: 100%; height: 19px;" ondblclick="make_new_comment('.$port->id.')" id="port_comment_'.$port->id.'">'.$port->comment.'</span>';
				
				$port_list[$port->port]['id'] = $port->id;
                                //if($port->port == '4' && $port->gid == '98') {
                                //        $port->admin = '0';
                                //}
				if ($port->admin){
					$port_list[$port->port]['admin'] = '<span style="display: block; width: 100%; height: 19px; cursor: pointer;" ondblclick="block_port('.$port->id.')" id="port_block_'.$port->id.'">'. 
														View_Element::image_active().'</span>';
				}
				else
				{
					$port_list[$port->port]['admin'] = '<span style="display: block; width: 100%; height: 19px; cursor: pointer;" ondblclick="block_port('.$port->id.')" id="port_block_'.$port->id.'">'.
														View_Element::image_stop().'</span>';
				}

///////////////////////////////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                if ($port->admin)
                                {
                                        $port_list[$port->port]['what'] = View_Element::image_connect();
                                }
                                else
                                {
                                        $port_list[$port->port]['what'] = View_Element::Space(1);
                                }

				
				if($port->port == '23' && $port->gid == '536') {
					$port->active = '0';
				}
				//if($port->port == '26' && $port->gid == '172') {
				//	$port->active = '0';
				//}
				if ($port->active)
				{
					$port_list[$port->port]['active'] = View_Element::image_connect();
				}
				else
				{
					$port_list[$port->port]['active'] = View_Element::Space(1);
				}

				$port_list[$port->port]['online'] = View_Element::Space(1);
				
			}
			
			
			$abonents_off_list = array();
			
			$abonents_list = $db->Query('SELECT * FROM `devices_abonents` WHERE `gid` = '.$dev->id.' ');
			while ($abonent_row = $abonents_list->get_next_row_object())
			{
				$added = FALSE;
				if ($abonent_row->port != 0)
				{
					if(!$port_list[$abonent_row->port]['port']){
						$prefix_length = strlen($abonent_row->flat);
						$str = '<b>'.$abonent_row->flat.'</b> ';
						if ((strlen($abonent_row->comment) + $prefix_length)< 30)
						{
							$str .= ' <font color="#666">'.$abonent_row->comment.'</font>';
						}
						else
						{
							$str .= ' <font color="#666">'.substr($abonent_row->comment, 0, 30-$prefix_length).'...</font>';
						}
						
						if ($abonent_row->uid)
						{
							$abonent = new nibs_abonent($abonent_row->uid);
							//$str .= ' '.$abonent->get_address().$abonent->get_fio();
							if ($abonent->is_online())
							{
								$port_list[$abonent_row->port]['online'] =  View_Element::image_online();
							}
							if ($abonent->get_block())
							{
								$str .= View_Element::Space(1).View_Element::image_lock();
							}
						}
						
						$port_list[$abonent_row->port]['port'] = $str;
						$added = TRUE;
					}
					else
					{
						// значит порт уже занят 
						$db->UpdateData('devices_abonents', array('port' => 0), array('id'=>$abonent_row->id));
					}
				}
				if (!$added)
				{
					$str = '';
					if ($abonent_row->uid)
						{
							$abonent = new nibs_abonent($abonent_row->uid);
							$str .= $abonent->get_address().' '.$abonent->get_packet().' ['.$abonent->get_deposit().'] ';
							
							if ($abonent->get_block())
							{
								$str .= View_Element::Space(1).View_Element::image_lock();
							}
					}
						
					$abonents_off_list[] = array('id' => $abonent_row->id, 'port' => $abonent_row->flat, 'comment' => $abonent_row->comment, 'info' => $str);
				}				
			}
			
			
			foreach ($port_list as $key => $row)
			{
				if(!$port_list[$key]['port']){
					$port_list[$key]['port'] = '<span style="display: block; width: 100%; height: 19px;" ondblclick="make_new_flat('.$dev->id.', '.$key.')" id="port_flat_'.$key.'"> </span>';
				}
			}
			
			
			$ping_list = $db->Query('SELECT `state`, `dt` FROM `devices_ping` WHERE `gid` = '.$dev->id.' ORDER BY `dt` DESC LIMIT 20')->get_rows_array();
			$table_ping = new View_Table();
			$table_ping->AddHead('dt', 'Пинг' );
			$table_ping->AddHead('state', ' ', View_Table::column_type_bool );
			$table_ping->AddData($ping_list);
			
			//$this->AddContent($table);
			
			$out = '<div class="span-19 last" id="ports"> <h4 >'.
			$dev->get_address().View_Element::Space(5).$dev->get_ip_link().' <font size="-1">[ '.$dev->get_type_name().' ] '.View_Element::Link('edit', 'network/editdevices/edit/'.$dev->id).'</font> </h4>';
			//hidden values
			//$out .= $dev->get_address_disposition();
			

		//	$actions_str = View_Element::Link('Импорт', CLASSPATH.'import/'.$dev->id).'<hr>';
			$actions_str = '';
			
			// list commands from config
			$comm_list = $dev->get_list_commands();
			if (is_array($comm_list))
			{
				$actions_str .= '<hr>';
				$comm_list_link = array();
				foreach ($comm_list as $command_id => $command_name)
				{
					$comm_list_link[] = View_Element::SpanClick($command_name, 'load_data(\'ajax/device_manage/command/'.$dev->id.'/'.$command_id.'\')');
				}
				$actions_str .= implode(' | ', $comm_list_link);
			}
			
/*
			if (is_array($dev->list_function_device()))
			{
				
				$list_a = array();
				foreach ($dev->list_function_device() as $name => $caption)
				{
					$list_a[] = '<a href="#" onclick="device_exec('.$dev->id.',\''.$name.'\');">'.$caption.'</a>';
				}
				$actions_str .= implode(' | ', $list_a);
				$actions_str .= '<hr>'.View_Element::Link('Расширенный вид', 'network/expertlooking/show/'.$dev->id).
						' | '.View_Element::Link('Анализ MAC', 'network/analysismac/show/'.$dev->id);
				
			}
*/			
			
			
			$actions_str .= '<hr>';
			$actions_str .= View_Element::submit_link('Сбросить Ping', 'Будут сброшена история ping и обнулен счетчтик доступности. Продолжить?', 'ajax/device_manage/resetcounter_ping/'.$dev->id);
			$actions_str .= View_Element::Space(2);
			$actions_str .= View_Element::submit_link('Сбросить состояния портов', 'Будет сброшена история изменений блокировок и активности всех портов, а таже удалены все обнаруженные MAC адреса. Продолжить?', 'ajax/device_manage/resetcounter_ports/'.$dev->id);
			
			
			
			//$actions_str .= View_Element::ButtonLoad('Сбросить Ping', 'ajax/device_manage/resetcounter_ping/'.$dev->id);
			//$actions_str .= View_Element::ButtonLoad('Сбросить состояния портов', 'ajax/device_manage/resetcounter_ports/'.$dev->id);
			$out_js = '';
			
			$out .= '</div>'; //end header span
			
			$out .= '<div class="span-11" id="ports"> '; // start port span box
			foreach ($port_list as $num => $row)
			{
				//$color = '#fff';
			//	if ($num % 2)
			//	{
			//		$color = '#ddd';
			//	}
				$out .= '<div style="border-bottom: 1px solid #ddd; height: 21px; " onclick="select_port('.$row['id'].', \''.WEBPATH.'\');" id="port_'.$row['id'].'"> ';
				$out .= '<span style="width: 40px; height: 19px; display: block; float: left; color: #666; " >'.$num.'</span>';
				$i = 1;
				if ($select_port) {
					if ($select_port == $num)
					{
						$out_js = '<script type="text/javascript">
									select_port('.$row['id'].', \''.WEBPATH.'\');
									</script>';
					}
				}
				foreach ($port_headers as $header => $width)
				{
					if ($row[$header] == '')
					{
						$row[$header] = View_Element::Space(1);
					}
					$out .= '<span style="width: '.$width.'px; height: 19px; display: block; ';
					
					$out .= ' float: left;';
				
					
					
					$out .= '">'.$row[$header].'</span>';
					$i++;
				}
				$out .= '</div>';
			}
			
			$out .= '</div>'; // end span port box
			
			//start left conteiner
			$out .= '<div class="span-8 last" id="left_container" >';

			$out .= '<div id="subinfo_container" style="padding:0.8em; border:2px solid #c6d880; margin-bottom: 1em;"> '; // start sub info panel
			$out .= $dev->get_address_disposition().'<hr>';
			$out .= 'Доступность: '.$dev->get_availability().'<br>';
			$out .= device_tags::get_device_info($dev->id);
			$out .= '<hr>';
			$triggers = $db->Query('SELECT *  FROM `trigger_devices` WHERE `gid` = '.$dev->id);
			while ($trigger_data = $triggers->get_next_row_object())
			{
				$out .= View_Element::Link($db->Query('SELECT name FROM `trigger_action` WHERE id ='.$trigger_data->action)->get_next_row_object()->name, 'network/triggerdevices/edit/'.$trigger_data->id).'<br>';				
			}
			if ($dev->get_port_admin())
			{
				$out .= View_Element::Link(View_Element::image_refresh().' Обновить', CLASSPATH.'update_data/'.$dev->id).'<br>';
			}
			
			$out .= View_Element::Link('Добавить событие', 'network/triggerdevices/add/'.$dev->id);
			
			$out .= '<hr>'.View_Element::Link('Расширенный вид', 'network/expertlooking/show/'.$dev->id).
						' | '.View_Element::Link('Анализ MAC', 'network/analysismac/show/'.$dev->id);
			$out .= '</div>';

			
			$out .= '<div id="info_container" style="padding:0.8em; border:2px solid #ddd;"> '; // start info_container span box
			$out .= $table_ping->out();
			$out .= '</div>'; //end info_container  span box
			//end left conteiner
			$out .= '</div>'; 
			
		
			
			if (count($abonents_off_list) > 0)
			{
				$table_off = new View_Table(array('edit_link' => 'network/edituser/edit/'));
				$table_off->AddHead('num', '', View_Table::column_type_autonum );
				$table_off->AddHead('port', 'Квартира', FALSE, '100px' );
				$table_off->AddHead('comment', 'Комментарий' );
				$table_off->AddHead('info', 'Иноформация' );
				$table_off->AddHead('id', '', View_Table::column_type_edit);
				
				$table_off->AddData($abonents_off_list);
				
				$out .= '<div class="span-19 last"><br><h4>Не подключенные абоненты:</h4>'.$table_off.'</div>';
				//$this->AddContent('<div class="span-19 last"><br><h4>Не подключенные абоненты:</h4>'.$table_off.'</div>');
			}
			
			//$this->AddContent($out);
			
			$ping_list = $db->Query('SELECT `state`, `dt` FROM `devices_ping` WHERE `gid` = '.$dev->id.' ORDER BY `dt` DESC LIMIT 30')->get_rows_array();
			$table_ping = new View_Table();
			$table_ping->AddHead('dt', 'Пинг' );
			$table_ping->AddHead('state', ' ', View_Table::column_type_bool );
			$table_ping->AddData($ping_list);
			
			// SELECT * FROM `devices_port_admin` WHERE gid IN (SELECT `id` FROM `devices_ports` WHERE `gid` = 1) ORDER BY dt DESC LIMIT 20
			
			$admin_list = $db->Query('SELECT * FROM `devices_port_admin` WHERE gid IN (SELECT `id` FROM `devices_ports` WHERE `gid` = '.$dev->id.') ORDER BY dt DESC LIMIT 30')->get_rows_array();
			if (is_array($admin_list))
			{
				foreach ($admin_list as $key => $item)
				{
					$admin_list[$key]['port'] = $db->Query('SELECT `port` FROM `devices_ports` WHERE `id` = '.$item['gid'])->get_next_row_object()->port;
				}
			}
			$table_admin = new View_Table();
			$table_admin->AddHead('port', 'Порт' );
			$table_admin->AddHead('dt', 'Блокировка' );
			$table_admin->AddHead('state', ' ', View_Table::column_type_bool );
			$table_admin->AddData($admin_list);
		
			$vlan = '';
			$cpu_load = '';
			
			if ($dev->get_dev_dlink() )
			{
				$res = $db->Query('SELECT *  FROM `devices_vlans` WHERE `gid` = '.$dev->id.' ORDER BY `vlan` ASC, `port` ASC');
				$table_vlan_data = '';
				while ($vlan_data = $res->get_next_row_object())
				{
					$table_vlan_data[$vlan_data->vlan]['vlan'] = $vlan_data->vlan;
					if ($vlan_data->untagged)
					{
						$table_vlan_data[$vlan_data->vlan]['untag'][] = $vlan_data->port;
					}
					else
					{
						$table_vlan_data[$vlan_data->vlan]['tag'][] = $vlan_data->port;
					}
				}
				
				if (is_array($table_vlan_data)) foreach ($table_vlan_data as $key => $item)
				{
					if (isset($table_vlan_data[$key]['untag']))
					{
						$table_vlan_data[$key]['untag'] = implode(', ', $table_vlan_data[$key]['untag']);
					}
					else
					{
						$table_vlan_data[$key]['untag'] = '';
					}
					if (isset($table_vlan_data[$key]['tag']))
					{
						$table_vlan_data[$key]['tag'] = implode(', ', $table_vlan_data[$key]['tag']);
					}
					else
					{
						$table_vlan_data[$key]['tag'] = '';
					}
				}
				$table_vlan = new View_Table();
				//$table_vlan->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
				$table_vlan->AddHead('vlan' , 'Vlan');
				$table_vlan->AddHead('untag', 'Untagged');
				$table_vlan->AddHead('tag', 'Tagged');
				$table_vlan->AddData($table_vlan_data);
				$vlan = $table_vlan->out();
				
				$cpu_load = '<img alt="" src="'.WEBPATH.'network/graph_cpu_img/base/'.$dev->id.'">';
			}
			
			$config_str = '';
			if ($dev->data->config)
			{
				$config_str = 'Текущая конфигурация: <br><textarea style="width: 100%; height: 300px;">'.$dev->data->config.'</textarea>';
			}
			
			$this->AddContent(View_Element::Tabs(array(
                'Общее' => $out,
                'Пинг' => $table_ping->out(),
                'Блокировки' => $table_admin->out(),
			    'Действия' => $actions_str,
                'VLan' => $vlan,
                'Конфигурация' => $config_str,
                'CPU' => $cpu_load,
            )));
			$this->AddContent($out_js);
		}
	}

    public function getLog(device $dev)
    {
        //TODO вывод лога сообщений для свича
    }
}

<?php

class macaddr extends View_Page_Controller {
	public $title = 'Поиск MAC';
	public $category = 'network';
	
	function index()
	{
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form();
		
		$form->add('MAC адрес:', array( 
			new View_Form_Text('mac','',	array('width' => '12em') )
		));
		
		if ($form->get_values())
		{
			$data = array();
			$this->AddContent($form);
			$form->return_values['mac'] = str_replace(':', '', $form->return_values['mac']);
			$form->return_values['mac'] = str_replace('-', '', $form->return_values['mac']);
			$form->return_values['mac'] = strtoupper($form->return_values['mac']);
			
			$sql = 'SELECT *  FROM `devices_port_mac` WHERE `mac` LIKE \''.$form->return_values['mac'].'\'';
			$tmp = $db->Query($sql);
			while ($dev_port = $tmp->get_next_row_object())
			{
				$port = $db->Query('SELECT *  FROM `devices_ports` WHERE `id` = '.$dev_port->gid)->get_next_row_object();
				$dev = new device($port->gid);
				
				$data[] = array('dev' => $dev->get_address_link(), 'port' => $port->port, 'date' => $dev_port->dt);
			}
			$table = new View_Table();
			
			$table->AddData($data);
			$table->AddHead('dev', 'Устройство');
			$table->AddHead('port', 'Порт');
			$table->AddHead('date', 'Дата появления');
			$this->AddContent($table);
			
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
}
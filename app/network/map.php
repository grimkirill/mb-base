<?php

class map {
	
	private $Header = '
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Карта</title>
<link rel="stylesheet" href="WEBPATHweb/css/screen.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="WEBPATHweb/css/print.css" type="text/css" media="print">
	<!--[if lt IE 8]><link rel="stylesheet" href="WEBPATHweb/css/ie.css" type="text/css" media="screen, projection"><![endif]-->
	
	<script type="text/javascript" src="WEBPATHweb/js/jquery.js"></script>
	<script type="text/javascript" src="WEBPATHweb/js/functions.js"></script>
	<script type="text/javascript" src="WEBPATHweb/js/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" src="WEBPATHweb/js/ui/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="WEBPATHweb/js/ui/jquery.ui.mouse.js"></script>
	<script type="text/javascript" src="WEBPATHweb/js/ui/jquery.ui.draggable.js"></script>
	
	<style type="text/css">
		.device {
			width: 14px;
			height: 14px;
			display: block;
			position: absolute;
			-webkit-border-radius: 8px;
			-moz-border-radius: 8px;
			border-radius: 8px;
			opacity: 0.8;
			border: 1px solid #666;
			margin-left: -1px;
			margin-top: -1px;
		}
		
		.enabled {
			background-color: green;
		}
		
		.disabled {
			background-color: red;
		}
		
		.none {
			background-color: yellow;
		}
		
		.draggable {
			cursor: move;
		}
		
		.mark {
			border: 4px solid #fff;
			margin-left: -4px;
			margin-top: -4px;		
		}
		
	</style>
	<script type="text/javascript">
	$(document).ready(function () {
		$(\'#parent_box > div\').click(function() {
			if ((selected == 0) || (selected.attr(\'id\') != $(this).attr(\'id\')))
			{
			  $(\'#box_show_info\').show();
			  $(\'#box_show_info_content\').text(\'Loading...\');
			  $(\'#box_show_info_content\').load(WEBJS+\'ajax/device_manage/loadinfo/\'+$(this).attr(\'id\'));
			  selected = $(this);
			  selected.addClass(\'mark\');
			 }
		});
	});
	var selected = 0;
	var WEBJS = \'WEBPATH\';
	</script>
</head>
<body style="">
	<div style="" id="parent_box">
		<img alt="" src="WEBPATHweb/css/map.jpeg">
		<a href="WEBPATHnetwork" style="position: fixed; top: 8px; left: 30px;" >Назад</a>
		<span style="text-decoration: underline; cursor: pointer; position: fixed; top: 8px; left: 80px;" 
			onclick="$(\'#parent_box > div\').addClass(\'draggable\');
				 $(this).hide(); 
				 $(\'#parent_box > div\').draggable({ 
					 start: function() {
					 	selected = $(this);
						selected.addClass(\'mark\');
					 }, 
				 
					stop: function() {
					 var position = $(this).position();
					 var uid = $(this).attr(\'id\');
					  $(\'#box_show_info_content\').load(WEBJS+\'ajax/device_manage/setxy/\'+uid+\'/\'+position.left+\'/\'+position.top);
					  $(\'#box_show_info\').show();	
					}
					}); ">Разблокировать</span>';
	
	private $Footer = '</div>
	
	
	<div style="position: fixed; height:100%; width:100%; left: 0; top: 40px;" id="box_show_info" class="hide" >
	<div style="display:table; margin:0 auto; border: 2px solid #ddd; padding: 10px; background-color: #eee; width: 300px; height: 200px; opacity: 0.8; " >
	<center><span style="text-decoration: underline; cursor: pointer;"  onclick="selected.removeClass(\'mark\'); $(\'#box_show_info\').hide(); selected =0;">Закрыть</span></center><hr>
		<div  id="box_show_info_content"> </div> 
	</div>
	</div> </body> </html>';
	
	function index()
	{
		
		global $db;
		echo str_replace('WEBPATH', WEBPATH, $this->Header);
		if (ACL::has_perm(ACL::acl_network_device_map)) 
		{
			$list = $db->Query('SELECT devices.`id`, devices.`ip`, devices.`mapx`, devices.`mapy`, devices.`ping`, devices_config.color_on, devices_config.color_off FROM  devices_config, `devices`  WHERE (devices.`mapx` != 0 OR devices.`mapy` != 0) AND devices_config.type = devices.`type` ');
			while ($item = $list->get_next_row_object())
			{
				
				$on = '';
				$color = $item->color_on;
				if ($item->ping == 0)
				{
					$color = $item->color_off;
				}
				if ($item->ip == '')
				{
					$on = 'none';
					$color = 'yellow';
				}
				
				echo '<div class="device '.$on.'" style=" background-color: '.$color.'; top: '.$item->mapy.'px; left: '.$item->mapx.'px;" id="'.$item->id.'"> </div>';
			}
		}
		else
		{
			echo ACL::deny();
		}
		
		echo $this->Footer;
	}
}
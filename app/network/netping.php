<?php

/**
 * управление нетпингами
 * @author kirill
 *
 */
class netping extends View_Page_Controller {
	public $title = 'Управление NetPing';
	public $category = 'network';
	
	function index()
	{
		$this->roll();
	} 
	
	function roll()
	{
		if (!ACL::has_perm(ACL::acl_netping_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		$this->AddContent(View_Element::Link('Иcтория', CLASSPATH.'history'));
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'add'), 
											'edit_link' => CLASSPATH.'edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('name', 'Название', FALSE, '400px');
		$table->AddHead('action', 'Действия', FALSE, '150px');
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT * FROM  `netping`')->get_rows_array())
		{
			foreach ($res as $key => $item)
			{
				$res[$key]['action'] = View_Element::Link('Reboot', CLASSPATH.'reboot/'.$item['id']).
					View_Element::Space(2).View_Element::Link('Off', CLASSPATH.'makeoff/'.$item['id']).
					View_Element::Space(2).View_Element::Link('On', CLASSPATH.'makeon/'.$item['id']); 
			}
			$table->AddData($res);
		}
		$this->AddContent($table);
	}
	
	function reboot($id)
	{
		if (!ACL::has_perm(ACL::acl_netping_actions)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		global $auth_user;
		$fw_oid = "1.3.6.1.2.1.1.1";
		if ($data = $db->Query('SELECT * FROM  `netping` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			// проверяем версию прошивки Netping. Если ответа не получили, то
			// возможно прошивка новая. В этом случае oid для перезагрузки другой.
			$fw = snmpget($data->ip, $data->community, $fw_oid);
			$reboot_oid = "1.3.6.1.4.1.25728.52.1.5801.".$data->channel.".1";
			if (!$fw)
			{
				$reboot_oid = "1.3.6.1.4.1.25728.5800.3.1.2.".$data->channel;
			}
			if (snmpset($data->ip, $data->community, $reboot_oid, "i", 1))
			{
				$this->AddContent(View_Element::PanelSuccess('Перезагружено '.$data->name));
				$db->InsertData('netping_history', array('gid' => (int)$id, 'user' => $auth_user->id, 'action' => 'reboot'));
			}
			else
			{
				$this->AddContent(View_Element::PanelError('Ошибка связи с '.$data->name));
			}
		}
		$this->roll();
	}
	
	function makeoff($id)
	{
		if (!ACL::has_perm(ACL::acl_netping_actions)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		global $auth_user;
		if ($data = $db->Query('SELECT * FROM  `netping` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			if (snmpset($data->ip, $data->community, "1.3.6.1.4.1.25728.52.1.5801.".$data->channel.".2","i",0))
			{
				$this->AddContent(View_Element::PanelSuccess('Выключено '.$data->name));
				$db->InsertData('netping_history', array('gid' => (int)$id, 'user' => $auth_user->id, 'action' => 'off'));
			}
			else
			{
				$this->AddContent(View_Element::PanelError('Ошибка связи с '.$data->name));
			}
		}
		$this->roll();
	}
	
	function makeon($id)
	{
		if (!ACL::has_perm(ACL::acl_netping_actions)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		global $auth_user;
		if ($data = $db->Query('SELECT * FROM  `netping` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			if (snmpset($data->ip, $data->community, "1.3.6.1.4.1.25728.52.1.5801.".$data->channel.".2","i",1))
			{
				$this->AddContent(View_Element::PanelSuccess('Включено '.$data->name));
				$db->InsertData('netping_history', array('gid' => (int)$id, 'user' => $auth_user->id, 'action' => 'on'));
			}
			else
			{
				$this->AddContent(View_Element::PanelError('Ошибка связи с '.$data->name));
			}
		}
		$this->roll();
	}
	
	function add()
	{
		if (!ACL::has_perm(ACL::acl_netping_add)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('caption' => 'Добавить', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название', 'items' => array(
							new View_Form_Text('name', '', array('caption' => 'Название', 'validators'=>array( View_Form_Element::validator_require => 1))),
							)),
							array('caption' => 'Конфигурация', 'items' => array(
							'IP: ',
							new View_Form_Text('ip', '', array('caption' => 'Название', 'validators'=>array( View_Form_Element::validator_require => 1))),
							' SNMP: ',
							new View_Form_Text('community', 'SWITCH', array('caption' => 'Название участка', 'validators'=>array( View_Form_Element::validator_require => 1))),
							' Канал: ',
							new View_Form_Select('channel', 'channel', array('options' => array(1 => 'Первый', 2 => 'Второй') ))							
							))
			) ));
		if ($form->get_values())
		{
			if ($db->InsertData('netping', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Тег добавлен.') );
				}
			}
			$this->roll();
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_netping_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `netping` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$form = new View_Form(array('caption' => 'Редактирование', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array(array('caption' => 'Название', 'items' => array(
							new View_Form_Text('name', $data->name, array('caption' => 'Название', 'validators'=>array( View_Form_Element::validator_require => 1))),
							)),
							array('caption' => 'Конфигурация', 'items' => array(
							'IP: ',
							new View_Form_Text('ip', $data->ip, array('caption' => 'Название', 'validators'=>array( View_Form_Element::validator_require => 1))),
							' SNMP: ',
							new View_Form_Text('community', $data->community, array('caption' => 'Название участка', 'validators'=>array( View_Form_Element::validator_require => 1))),
							' Канал: ',
							new View_Form_Select('channel', $data->channel, array('options' => array(1 => 'Первый', 2 => 'Второй') ))							
							)),
							array('caption' => '', 'items' => array(
								new View_Form_Checkbox('delete', '', array('caption' => 'Удалить') )
								)							
							)
			) ));
					
			if ($form->get_values())
			{
				if ($form->return_values['delete'])
				{
					$db->DeleteData('netping', array('id' => $id));
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelNotice('NetPing удален.') );
					}
				}
				else
				{
					unset($form->return_values['delete']);
					if ($db->UpdateData('netping', $form->return_values, array('id' => $id)))
					{
						if ($db->GetAffectedRows() > 0)
						{
							$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
						}
					}
				}
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
	
	function history()
	{
		if (!ACL::has_perm(ACL::acl_netping_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$this->AddContent(View_Element::Link('Обзор NetPing', CLASSPATH));

		$form = new View_Form();
		$form->add('Время', array( 
			'c ', new View_Form_Date('date_start', date('Y-m-d')), ' по ',
			new View_Form_Date('date_stop', date('Y-m-d'))
		));
		$date_start = date('Y-m-d');
		$date_stop = date('Y-m-d');
		$group = '';
		if ($form->get_values())
		{
			$date_start = $form->return_values['date_start'];
			$date_stop = $form->return_values['date_stop'];
			
		}
		$this->AddContent($form);
		
		$sql = 'SELECT *  FROM `netping_history`  WHERE dt >= \''.$date_start.' 00:00:00\' AND dt <= \''.$date_stop.' 23:59:59\' ORDER BY `id` DESC';
		$data = $db->Query($sql)->get_rows_array();
		
		if (is_array($data)) foreach ($data as $key => $item)
		{
			$data[$key]['user'] = User_Auth::get_loginid($item['user']);
			$data[$key]['dev'] = $db->Query('SELECT name FROM  `netping` WHERE id = '.$item['gid'])->get_next_row_object()->name;
		}

		$table = new View_Table();
		$table->AddHead('dev' , 'Устройство', FALSE, '250px');
		$table->AddHead('user', 'Пользователь');
		$table->AddHead('action', 'Действие');
		$table->AddHead('dt', 'Время');
		
		$table->AddData($data);
		$this->AddContent($table);
	}
	
}

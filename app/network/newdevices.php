<?php

class newdevices extends View_Page_Controller {
	public $title = 'Управление сетью';
	public $category = 'network';
	
	function index()
	{
		$this->add();
	}
	
	function add()
	{
		if (!ACL::has_perm(ACL::acl_network_device_add)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('caption' => 'Добавить устройство', 'method' => 'post', 'border' => FALSE ));
		$form->add('IP адрес', array( new View_Form_Text('ip','')));
		$form->add('Адрес размещения', array( 
			new view_form_select_home('address_home'), ' Подъезд: ',
			new View_Form_Text('address_dorway','',	array(
						'caption' => 'Подъезд', 
						'validators'=>array( View_Form_Element::validator_require => 1), 
						'width' => '6em') ),
			'<br>',
			new View_Form_WYSIWYG('address_disposition', '', array('min' => TRUE))
		));
		
		$form->add('Модель оборудования', array(
				 new view_form_select_device_type('type')
			));
			
		$form->add('Параметры доступа', array(
			 'Имя пользователя: ', new View_Form_Text('access_username', get_default_value::get('default.access.username')),
			 ' Пароль: ', new View_Form_Text('access_password', get_default_value::get('default.access.password'))			 
		));
		
		$form->add('', array(
			 'SNMP Community read: ', new View_Form_Text('access_snmp_read', get_default_value::get('default.snmp.read')),
			 ' write: ',new View_Form_Text('access_snmp_write', get_default_value::get('default.snmp.write'))
		));
		
		if ($form->get_values())
		{
			$db->InsertData('devices', $form->return_values);
			$dev = new device($db->GetInsertId());
			$dev->update();
			header('Location: '.WEBPATH.'network/looking/show/'.$dev->id);
		}
		else
		{
			$this->AddContent($form);
		}
	}
}
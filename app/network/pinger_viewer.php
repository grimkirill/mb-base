<?php

class pinger_viewer extends View_Page_Controller {
	public $title = 'Управление сетью';
	public $category = 'network';
	
	function index()
	{
		$this->show();
	}
	
	function show()
	{
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('oneline' => TRUE, 'submit' => 'Поиск'));
		
		$form->add('', array( 
			'c ', new View_Form_Date('date_start', date('Y-m-d')), ' по ',
			new View_Form_Date('date_stop', date('Y-m-d')), ' ',
			new View_Form_Checkbox('group','', array('caption' => 'группировать'))
		));
		
		$date_start = date('Y-m-d');
		$date_stop = date('Y-m-d');
		$group = '';
		if ($form->get_values())
		{
			$date_start = $form->return_values['date_start'];
			$date_stop = $form->return_values['date_stop'];
			
		}
		$this->AddContent($form);
		$sql = 'SELECT * FROM `devices_ping` WHERE dt >= \''.$date_start.' 00:00:00\' AND dt <= \''.$date_stop.' 23:59:59\'  ORDER BY `dt` DESC';
		
		if ($form->return_values['group'])
		{
			$sql = ' SELECT *, COUNT(*) as count FROM `devices_ping` WHERE dt >= \''.$date_start.' 00:00:00\' AND dt <= \''.$date_stop.' 23:59:59\' GROUP BY gid ORDER BY `dt` DESC';
		}
		$admin_list = $db->Query($sql)->get_rows_array();
		if (is_array($admin_list)) foreach ($admin_list as $key => $item)
		{
			$dev = new device($item['gid']);
			$admin_list[$key]['dev'] = View_Element::Link($dev->get_address(), 'network/looking/show/'.$dev->id);
			$admin_list[$key]['type'] = $dev->get_type_name();			
		}
		
		if (is_array($admin_list) )
		{
			$table_admin = new View_Table();
			$table_admin->AddHead('dev', 'Устройство' );
			$table_admin->AddHead('type', 'Тип' );
			$table_admin->AddHead('dt', 'Время' );
			if ($form->return_values['group'])
			{
				$table_admin->AddHead('count', 'Количество' );
			}
			else
			{
				$table_admin->AddHead('state', ' ', View_Table::column_type_bool );
			}
			$table_admin->AddData($admin_list);
			$this->AddContent($table_admin);
		}
		
	}	
	
}
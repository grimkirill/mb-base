<?php

class port_traff extends View_Page_Controller {
	public $title = 'Управление сетью';
	public $category = 'network';
	
	
	function show($id)
	{
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { echo ACL::deny(); return ; }
		global $db;
		
		$data = $db->Query('SELECT * FROM `devices_ports` WHERE `id` = '.$db->EscapeValue($id))->get_next_row_object();
		$dev = new device($data->gid);
		
		$this->AddContent($dev->get_address_link());
		$this->AddContent(' Порт: <b>'.$data->port.'</b>');
		
		$this->AddContent('<img alt="" src="'.WEBPATH.'network/graph_port/last_day/'.$id.'/'.microtime(true).'">');
		$this->AddContent('<img alt="" src="'.WEBPATH.'network/graph_port/last_week/'.$id.'/'.microtime(true).'">');
		$this->AddContent('<img alt="" src="'.WEBPATH.'network/graph_port/lastmonth/'.$id.'/'.microtime(true).'">');
		$this->AddContent('<img alt="" src="'.WEBPATH.'network/graph_port/last_year/'.$id.'/'.microtime(true).'">');
	}
	
}
<?php

class report extends View_Page_Controller {
	
	public $title = 'Отчеты данных оборудования';
	public $category = 'network';
	
	function index()
	{
		$this->AddContent(View_Element::Link('Абоненты без привязок', CLASSPATH.'listnouid'));
		$this->AddContent(' | ');
		$this->AddContent(View_Element::Link('Блокировки', 'network/report_block/show'));
		$this->AddContent(' | ');
		$this->AddContent(View_Element::Link('Статистика PING', 'network/pinger_viewer'));
		$this->AddContent(' | ');
		$this->AddContent(View_Element::Link('Все свичи', 'network/review'));
		$this->AddContent(' | ');
		$this->AddContent(View_Element::Link('Количество свичей', CLASSPATH.'devcount'));
		$this->AddContent(' | ');
		$this->AddContent(View_Element::Link('Домовая ведомость', CLASSPATH.'homestat'));
		$this->AddContent(' | ');
		$this->AddContent(View_Element::Link('Топ Broadcast', CLASSPATH.'topbroadcast'));
		$this->AddContent('<hr>');
	}
	
	function devcount()
	{
		global $db;
		$this->index();
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { $this->AddContent(ACL::deny()); return ; }
		$data = $db->Query('SELECT type, COUNT(*) as count FROM `devices` GROUP BY type')->get_rows_array();
		$total = 0;
		foreach ($data as $key => $item)
		{
			$data[$key]['type'] = device_types::get_type_name($item['type']);
			$total += $item['count'];
		}
		$table = new View_Table();
		$table->AddHead('type', 'Модель');
		$table->AddHead('count', 'Количество');
		$table->addFoot('Итого');
		$table->addFoot($total);
		
		$table->AddData($data);
		$this->AddContent($table);
		
	}
	
	function listnouid()
	{
		global $db;
		$this->index();
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { $this->AddContent(ACL::deny()); return ; }
		$connetions = $db->Query('SELECT *  FROM devices_abonents WHERE `uid` = 0');
		$data = array();
		while ($conn = $connetions->get_next_row_object())
		{
			$dev = new device($conn->gid);
			$port_comment = '';
			if ($conn->port)
			{
				$port_comment = $db->Query("SELECT comment FROM `devices_ports` WHERE `gid` = $conn->gid AND `port` = ".$conn->port)->get_next_row_object()->comment;
			}
			$data[] = array(
				'dev' => $dev->get_address_link($conn->port),
				'port' => $conn->port,
				'flat' => $conn->flat,
				'comment' => $conn->comment.' '.$port_comment
			);
		}
		$table = new View_Table();
		$table->AddHead('num', '', View_Table::column_type_autonum );
		$table->AddHead('dev', 'Устройство');
		$table->AddHead('port', '№ Порта');
		$table->AddHead('flat', 'Квартра');
		$table->AddHead('comment', 'Комментарий');
		$table->AddData($data);
		$this->AddContent($table);
	}
	
	function topbroadcast()
	{
		global $db;
		$this->index();
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { $this->AddContent(ACL::deny()); return ; }
		$list = $db->Query('SELECT p.id, p.port, p.gid, br.diff FROM `devices_port_broadcast` AS br LEFT JOIN devices_ports p ON p.id = br.port ORDER BY br.`diff`  DESC LIMIT 100');
		$data = array();
		while ($item = $list->get_next_row_object())
		{
			$dev = new device($item->gid);
			$data[] = array(
				'gid' => $dev->get_address_link($item->port),
				'port' => $item->port,
				'broadcast' => (int)($item->diff/300),
				'ip' => $dev->get_ip_link()
			);
		}
		
		$table_admin = new View_Table();
		$table_admin->AddHead('gid', 'Свич');
		$table_admin->AddHead('ip', 'IP');
		$table_admin->AddHead('port', 'Порт');
		$table_admin->AddHead('broadcast', 'Broadcast / sec');
		
		$table_admin->AddData($data);
		$this->AddContent($table_admin);
		
	}
	
	
	function homestat()
	{
		global $db;
		$this->index();
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { $this->AddContent(ACL::deny()); return ; }
		$list = $db->Query('SELECT h.id, h.home, s.street,s.id as streetid, c.city FROM address_home as h LEFT JOIN address_street s ON s.id = h.gid LEFT JOIN address_city c ON c.id = s.gid ORDER BY c.city ASC, s.street ASC, h.home+0');
		
		$table = new View_Table();
		$table->AddHead('home', 'Дом');
		$table->AddHead('dev_count', 'Свичей');
		$table->AddHead('conn_count', 'Подкл.');
		$table->AddHead('abon_count', 'Абон');
		$table->AddHead('abon_on', 'Вкл Аб');
		$table->AddHead('abon_off', 'Откл Аб');
		$table->AddHead('q', 'Вкл/Абон %');
		
		$street_summ = array();
		$street_summ['dev_count'] = 0;
		$street_summ['conn_count'] = 0;
		$street_summ['abon_count'] = 0;
		$street_summ['abon_on'] = 0;
		$street_summ['abon_off'] = 0;
		
		$street_id = 0;
		$street_text = '';
		while ($home = $list->get_next_row_object())
		{
			$dev_count = $db->Query('SELECT COUNT(*) AS count  FROM `devices` WHERE `address_home` = '.$home->id)->get_next_row_object()->count;
			if ($dev_count > 0)
			{
				$conn_count = $db->Query('SELECT COUNT(*) AS count FROM `devices_abonents` AS a  LEFT JOIN devices d ON d.id = a.gid WHERE d.address_home = '.$home->id)->get_next_row_object()->count;		
				$abon_count = $db->Query('SELECT COUNT(*) AS count FROM `devices_abonents` AS a  LEFT JOIN devices d ON d.id = a.gid WHERE a.uid != 0 AND d.address_home = '.$home->id)->get_next_row_object()->count;
				$abon_on = $db->Query('SELECT COUNT(*) AS count FROM `devices_abonents` AS a  LEFT JOIN devices d ON d.id = a.gid LEFT JOIN billing_users b ON a.uid = b.id WHERE a.uid != 0 AND b.blocked = 0 AND d.address_home = '.$home->id)->get_next_row_object()->count;
				
				if ($home->streetid != $street_id)
				{
					if ($street_id != 0)
					{
						$table->AddRow(array(
							'home' => '<b>'.$street_text.'</b>',
							'dev_count' => '<b>'.$street_summ['dev_count'].'</b>',
							'conn_count' => '<b>'.$street_summ['conn_count'].'</b>',
							'abon_count' => '<b>'.$street_summ['abon_count'].'</b>',
							'abon_on' => '<b>'.$street_summ['abon_on'].'</b>',
							'abon_off'=> '<b>'.$street_summ['abon_off'].'</b>',
							'q' => '<b>'.intval($street_summ['abon_on']*100/$street_summ['abon_count']).'</b>'
						));
					}
					$street_id = $home->streetid;
					$street_summ['dev_count'] = 0;
					$street_summ['conn_count'] = 0;
					$street_summ['abon_count'] = 0;
					$street_summ['abon_on'] = 0;
					$street_summ['abon_off'] = 0;
				}
				
				$street_text = 'Итого '.$home->city.', '.$home->street;
				$street_summ['dev_count'] += $dev_count;
				$street_summ['conn_count'] += $conn_count;
				$street_summ['abon_count'] += $abon_count;
				$street_summ['abon_on'] += $abon_on;
				$street_summ['abon_off'] += ($abon_count-$abon_on);
				
				$table->AddRow(array(
					'home' => $home->city.', '.$home->street.', '.$home->home,
					'dev_count' => $dev_count,
					'conn_count' => $conn_count,
					'abon_count' => $abon_count,
					'abon_on' => $abon_on,
					'abon_off'=> ($abon_count-$abon_on),
					'q' => intval($abon_on*100/$abon_count)
				));
			}
		}
		
		$table->AddRow(array(
			'home' => '<b>'.$street_text.'</b>',
			'dev_count' => '<b>'.$street_summ['dev_count'].'</b>',
			'conn_count' => '<b>'.$street_summ['conn_count'].'</b>',
			'abon_count' => '<b>'.$street_summ['abon_count'].'</b>',
			'abon_on' => '<b>'.$street_summ['abon_on'].'</b>',
			'abon_off'=> '<b>'.$street_summ['abon_off'].'</b>',
			'q' => '<b>'.intval($street_summ['abon_on']*100/$street_summ['abon_count']).'</b>'
		));
		
		$this->AddContent($table);
	
	}
	
	
}
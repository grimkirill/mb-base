<?php

class report_block extends View_Page_Controller {
	public $title = 'Отчет о блокировках';
	public $category = 'network';
	
	function show()
	{
		if (!ACL::has_perm(ACL::acl_network_device_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('oneline' => TRUE, 'submit' => 'Просмотр'));
		
		$form->add('Время', array( 
			'c ', new View_Form_Date('date_start', date('Y-m-d')), ' по ',
			new View_Form_Date('date_stop', date('Y-m-d'))
		));
		
		$date_start = date('Y-m-d');
		$date_stop = date('Y-m-d');
		if ($form->get_values())
		{
			$date_start = $form->return_values['date_start'];
			$date_stop = $form->return_values['date_stop'];
		}
		$this->AddContent($form);
		
		$admin_list = $db->Query('SELECT * FROM `devices_port_admin` WHERE dt >= \''.$date_start.' 00:00:00\' AND dt <= \''.$date_stop.' 23:59:59\' ORDER BY `dt` DESC')->get_rows_array();
		if (is_array($admin_list)) foreach ($admin_list as $key => $port_admin)
		{
			$port = $db->Query('SELECT * FROM `devices_ports` WHERE id = '.$port_admin['gid'])->get_next_row_object();
			$dev = new device($port->gid);
			$flat = '';
			if ($abonent = $db->Query('SELECT * FROM `devices_abonents` WHERE gid = '.$port->gid.' AND port ='.$port->port)->get_next_row_object())
			{
				$flat = '<b>'.$abonent->flat.'</b>';
			}
			$admin_list[$key]['port'] = View_Element::Link($dev->get_address().' [ '.$port->port.' ] ', 'network/looking/show/'.$dev->id).' '.$flat;			
		}
		
		if (is_array($admin_list) )
		{
			$table_admin = new View_Table();
			$table_admin->AddHead('port', 'Порт' );
			$table_admin->AddHead('dt', 'Время' );
			$table_admin->AddHead('state', ' ', View_Table::column_type_bool );
			$table_admin->AddData($admin_list);
			$this->AddContent($table_admin);
		}
		
	}	
	
}
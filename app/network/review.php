<?php

class review extends View_Page_Controller {
	public $title = 'Отчет о блокировках';
	public $category = 'network';
	
	function index()
	{
		global $db;
		$devices_list = $db->Query('SELECT d.id, d.address_home, d.address_dorway, d.ip, h.home, s.street, d.type, d.address_disposition FROM devices as d LEFT JOIN address_home h ON h.id = d.address_home LEFT JOIN address_street s ON s.id = h.gid LEFT JOIN address_city c ON c.id = s.gid ORDER BY c.city, s.street ASC, h.home+0')->get_rows_array();
		//$devices_list = $db->Query('SELECT * FROM `devices` ')->get_rows_array();
		
		foreach ($devices_list as $key => $item)
		{
			$home = view_form_select_home::get_home_print($item['address_home']).', п '.$item['address_dorway'];
			$devices_list[$key]['home'] = View_Element::Link($home, 'network/looking/show/'.$item['id']);
			$devices_list[$key]['ip'] = '<a href="http://'.$item['ip'].'" target="blank">'.$item['ip'].'</a>';
			$devices_list[$key]['type'] = device_types::get_type_name($item['type']);
		} 
		
		$table = new View_Table();
		
		$table->AddHead('num', '', View_Table::column_type_autonum );
		$table->AddHead('home', 'Адрес', FALSE, '200px' );
		$table->AddHead('ip', 'IP', FALSE, '100px');
		$table->AddHead('type', 'Тип');
		$table->AddHead('address_disposition', 'Расположение');

		
		$table->AddData($devices_list);
		
		$this->AddContent($table);
	}
	
	
}
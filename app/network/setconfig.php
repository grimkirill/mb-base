<?php

class setconfig extends View_Page_Controller {
	public $title = 'Управление конфигурацией свичей';
	public $category = 'network';
	
	function get_dev_list()
	{
		$list = device_types::get_list();
		foreach ($list as $key => $item)
		{
			if (!device_types::get_dlink($key))
			{
				unset($list[$key]);
			}
		}
		return $list;
		
	}
	
	function index()
	{
	
		global $db;
		$this->show_help();
		if (!ACL::has_perm(ACL::acl_network_device_configuring )) { $this->AddContent(ACL::deny()); return ; }
		$form = new View_Form();
		
		$form->add('Адрес размещения', array( 
			new view_form_select_home('address_home', '', array('as_filter' => TRUE)), ' Подъезд: ',
			new View_Form_Text('address_dorway','',	array('width' => '6em') )
		));
		$form->add('IP адрес', array('ip: ' , new View_Form_Text('ip',''), ' Модель: ',
				new View_Form_Select('type', '', array('options' => $this->get_dev_list())),
				' Тэг: ' , new View_Form_Select('tag', '', array('options' => device_tags::get_list_null() )) 
				));
		$form->add('Комманда', array(
				new View_Form_Textarea('command','',array('width' => '100%')) 
				));
				
		$sql = FALSE;
		$command = FALSE;
		if ($form->get_values())
		{
			//$this->AddContent(View_Element::PanelSuccess(print_r($form->return_values, TRUE)));
			if (count($form->return_values['address_home']) > 0)
			{
				$sql = 'SELECT * FROM `devices` WHERE ping = 1 AND address_home IN ('.implode(', ',$form->return_values['address_home']).') ';
			}
			
			if ($form->return_values['address_dorway'] != '')
			{
				$sql .= ' AND address_dorway LIKE '.$db->EscapeValue($form->return_values['address_dorway']);
			}
				
			if ($form->return_values['ip'] != '')
			{
				$form->return_values['ip'] = str_replace('*', '%', $form->return_values['ip']);
				$sql .= ' AND ip LIKE '.$db->EscapeValue($form->return_values['ip']);
			}
			
			if ($form->return_values['type'] != 0)
			{
				$sql .= ' AND type ='.$db->EscapeValue((int)$form->return_values['type']);
			}
			
			if ($form->return_values['tag'] != 0)
			{
				$sql .= ' AND id IN (SELECT `gid` FROM `devices_tags` WHERE `tag` = '.$db->EscapeValue((int)$form->return_values['tag']).') ';
			}
			
			if ($form->return_values['command'] != '')
			{
				$command = $form->return_values['command'];
			}
		}
		
		$this->AddContent($form);
		
		if ($sql)
		{
			$this->show_list($sql, $command);
		}
	}
	
	function prepare_command($dev, $command)
	{
		$cmd_list = explode("\n", $command);
		$out = array();
		foreach ($cmd_list as $cmd)
		{
			$ip_list = explode('.', $dev->data->ip);
			$cmd = str_replace('{ip1}', $ip_list[0], $cmd);
			$cmd = str_replace('{ip2}', $ip_list[1], $cmd);
			$cmd = str_replace('{ip3}', $ip_list[2], $cmd);
			$cmd = str_replace('{ip4}', $ip_list[3], $cmd);
			
			if ($dev->data->port_in != 0)
			{
				$cmd = str_replace('{port_in}', $dev->data->port_in, $cmd);
			}
			
			if (strpos($cmd, '{port_not_in}'))
			{
				if ($dev->data->port_in != 0)
				{
					for ($i = 1; $i <= $dev->get_port_count() ; $i++)
					{
						if ($i != $dev->data->port_in)
						{
							$out[] = str_replace('{port_not_in}', $i, $cmd);
						}
					}
				}
			}
			else
			{
				$out[] = $cmd;
			}

			
		}
		return 	$out;
	}
	
	
	function show_list($sql, $command = FALSE)
	{
		global $db;
		if ($devices_list = $db->Query($sql)->get_rows_array())
		{
			if (count($devices_list) > 0)
			{
				foreach ($devices_list as $key => $item)
				{
					$home = view_form_select_home::get_home_print($item['address_home']).', п '.$item['address_dorway'];
					$devices_list[$key]['home'] = View_Element::Link($home, 'network/looking/show/'.$item['id']);
					$devices_list[$key]['ip'] = '<a href="http://'.$item['ip'].'" target="blank">'.$item['ip'].'</a>';
					$devices_list[$key]['type'] = device_types::get_type_name($item['type']);
					if ($command)
					{
						$dev = new device($item['id']);
						if (method_exists($dev->driver, 'set_config'))
						{
							$cmd_list = $this->prepare_command($dev, $command);
							
							$devices_list[$key]['cmd'] = '<textarea style="width: 400px; height: 200px;">';
							if (is_array($cmd_list))
							{
								foreach ($cmd_list as $cmd) 
								{
									$devices_list[$key]['cmd'] .= $dev->driver->set_config($cmd)."\n------------------------------------------------\n";
								}
							}
							$devices_list[$key]['cmd'] .= '</textarea>';
												
							
						}
						else  // driver has not function set_config
						{
							$devices_list[$key]['cmd'] = 'Error Driver';
						}
					}
				}
			
			
				$table = new View_Table(array('edit_link' => 'network/editdevices/edit/'));
	
				$table->AddHead('num', '', View_Table::column_type_autonum );
				$table->AddHead('home', 'Адрес' );
				$table->AddHead('ip', 'IP', FALSE, '100px');
				$table->AddHead('type', 'Тип');
				if ($command)
				{
					$table->AddHead('cmd', 'Выполнение');
				}
				//$table->AddHead('ping', 'ping', View_Table::column_type_bool, '60px' );
				//$table->AddHead('id', '', View_Table::column_type_edit);
				
				$table->AddData($devices_list);
				
				$this->AddContent($table);
			} 
		}
		else
		{
			$this->AddContent(View_Element::PanelNotice('Ничего не найдено.'));
		}
	}
	
	
	function show_help()
	{
		$this->AddContent(View_Element::PanelCollapse('Help', '
		Назначение данного инструмента - массовая установка параметров для оборудования или выполнение других операций со свичами Dlink через Telnet<hr>
		Шаг первый: ищем список свичей<br>
		Шаг второй: вводим комманду и смотрим на результат.<hr>
		Инструкции по дополительным опциям:<br>
		{port_in} - входящий порт<br>
		{port_not_in} - все порты кроме входящих<br>
		{ip1}.{ip2}.{ip3}.{ip4} - ip адрес свича<br>
		', TRUE));
	}
	
}
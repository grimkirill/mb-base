<?php
/**
 * Created by JetBrains PhpStorm.
 * User: k.Skatov
 * Date: 03.07.12
 * Time: 17:22
 * To change this template use File | Settings | File Templates.
 */

class traff extends View_Page_Controller {
    public $title = 'Полный траффик';
    public $category = 'network';

	function index()
	{
        $this->AddContent('<img alt="" src="'.WEBPATH.'network/graph_traff/last_day/'.microtime(true).'">');
        $this->AddContent('<img alt="" src="'.WEBPATH.'network/graph_traff/last_week/'.microtime(true).'">');
        $this->AddContent('<img alt="" src="'.WEBPATH.'network/graph_traff/lastmonth/'.microtime(true).'">');
        $this->AddContent('<img alt="" src="'.WEBPATH.'network/graph_traff/last_year/'.microtime(true).'">');
    }

}
<?php

class triggerdevices extends View_Page_Controller {
	public $title = 'Управление триггреами';
	public $category = 'network';
	
	function add($gid)
	{
		if (!ACL::has_perm(ACL::acl_network_device_edit)) { $this->AddContent(ACL::deny()); return ; }
		$gid = (int)$gid;
		global $db;
		$dev = new device($gid);
		$form = new View_Form(array('caption' => 'Добавить триггер для '.$dev->get_address_link(), 'method' => 'post', 'border' => FALSE, 'cancel' => 'network/looking/show/'.$gid, 
			'rows' => array(array('caption' => 'Действие', 'items' => array(
								new View_Form_Select('action', '', array('options' => $db->Query('SELECT id, name FROM `trigger_action`')->get_rows_array_id('id', 'name'))),
							)),
							array('caption' => 'Сообщение', 'items' => array(
								new View_Form_Text('text', '', array('caption' => 'Сообщение', 'width' => '90%' , 'validators'=>array( View_Form_Element::validator_require => 1)))
							)),
							array('caption' => '', 'items' => array(
								new View_Form_Checkbox('single', '', array('caption' => 'Одноразовый'))
							))
			) ));
		if ($form->get_values())
		{
			$form->return_values['gid'] = $gid;
			$db->InsertData('trigger_devices', $form->return_values);
			
			header('Location: '.WEBPATH.'network/looking/show/'.$gid);
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_network_device_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data =  $db->Query('SELECT * FROM  `trigger_devices` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$dev = new device($data->gid);
			$form = new View_Form(array('caption' => 'Изменить триггер для '.$dev->get_address_link(), 'method' => 'post', 'border' => FALSE, 'cancel' => 'network/looking/show/'.$data->gid, 
				'rows' => array(array('caption' => 'Действие', 'items' => array(
									new View_Form_Select('action', $data->action, array('options' => $db->Query('SELECT id, name FROM `trigger_action`')->get_rows_array_id('id', 'name'))),
								)),
								array('caption' => 'Сообщение', 'items' => array(
									new View_Form_Text('text', $data->text, array('caption' => 'Сообщение', 'width' => '90%' , 'validators'=>array( View_Form_Element::validator_require => 1)))
								)),
								array('caption' => '', 'items' => array(
									new View_Form_Checkbox('single', $data->single, array('caption' => 'Одноразовый')),
									new View_Form_Checkbox('delete', FALSE, array('caption' => 'Удалить')),
								))
				) ));
			if ($form->get_values())
			{
				if ($form->return_values['delete'])
				{
					$db->DeleteData('trigger_devices', array('id' => $data->id));
				}
				else
				{
					unset($form->return_values['delete']);
					$db->UpdateData('trigger_devices', $form->return_values, array('id' => $data->id));
				}
				
				
				header('Location: '.WEBPATH.'network/looking/show/'.$data->gid);
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
	
}
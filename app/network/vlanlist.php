<?php

class vlanlist extends View_Page_Controller {
	public $title = 'Управление сетью';
	public $category = 'network';
	
	function index()
	{
		$this->roll();
	} 
	
	function roll()
	{
		if (!ACL::has_perm(ACL::acl_network_vlan_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		$this->AddContent(View_Element::Link('Изменения', CLASSPATH.'history'));
		$table = new View_Table(array('caption' => '', 
											'edit_link' => CLASSPATH.'edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('link', 'VID', FALSE, '100px');
		$table->AddHead('count', 'Устройств');
		$table->AddHead('name', 'Название', FALSE, '400px');
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT *  FROM `devices_vlan_list` ORDER BY `id` ASC')->get_rows_array())
		{
			foreach ($res as $key => $item)
			{
				$res[$key]['link'] = View_Element::Link($item['id'], CLASSPATH.'rollvlan/'.$item['id']);
				$res_count = $db->Query('SELECT gid  FROM `devices_vlans` WHERE `vlan` = '.$item['id'].' GROUP BY `gid`');
				$res[$key]['count'] =  $res_count->get_rows_count();
			}
			$table->AddData($res);
		}
		$this->AddContent($table);
	}
	
	function rollvlan($id)
	{
		if (!ACL::has_perm(ACL::acl_network_vlan_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		$this->AddContent(View_Element::Link('VLan <b>'.$id.'</b>', CLASSPATH));
		$table = new View_Table(array('edit_link' => CLASSPATH.'edit/'));
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('gid' , '', FALSE, '250px');
		$table->AddHead('untag', 'Untagged');
		$table->AddHead('tag', 'Tagged');
		
		
		$res = $db->Query('SELECT *  FROM `devices_vlans` WHERE `vlan` = '.$id.' ORDER BY `gid` ASC, `port` ASC');
		$data = array();
		
		while ($vals = $res->get_next_row_object())
		{
			if (!isset($data[$vals->gid]['gid']))
			{
				$dev = new device($vals->gid);
				$data[$vals->gid]['gid'] = View_Element::Link($dev->get_address(), 'network/looking/show/'.$vals->gid);
				$data[$vals->gid]['untag'] = '';
				$data[$vals->gid]['tag'] = '';
			}
			if ($vals->untagged)
			{
				if ($data[$vals->gid]['untag'] != '')
				{
					$data[$vals->gid]['untag'] .= ', ';
				}
				$data[$vals->gid]['untag'] .= $vals->port;
			}
			else
			{
				if ($data[$vals->gid]['tag'] != '')
				{
					$data[$vals->gid]['tag'] .= ', ';
				}
				$data[$vals->gid]['tag'] .= $vals->port;
			}
			
		}
		
		$table->AddData($data);
		$this->AddContent($table);
	}
	
	
	function history()
	{
		if (!ACL::has_perm(ACL::acl_network_vlan_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$this->AddContent(View_Element::Link('Обзор VLan', CLASSPATH));

		$form = new View_Form();
		$form->add('Время', array( 
			'c ', new View_Form_Date('date_start', date('Y-m-d')), ' по ',
			new View_Form_Date('date_stop', date('Y-m-d')),' vlan: ',
			new View_Form_Text('vlan', '', array('width' => '5em'))
		));
		$date_start = date('Y-m-d');
		$date_stop = date('Y-m-d');
		$group = '';
		$where_vlan = '';
		if ($form->get_values())
		{
			$date_start = $form->return_values['date_start'];
			$date_stop = $form->return_values['date_stop'];
			if ($form->return_values['vlan'])
			{
				$where_vlan = ' AND vlan ='.$form->return_values['vlan'];
			}
		}
		$this->AddContent($form);
		
		$sql = 'SELECT *  FROM `devices_vlan_history`  WHERE dt >= \''.$date_start.' 00:00:00\' AND dt <= \''.$date_stop.' 23:59:59\' '.$where_vlan.' ORDER BY `id` DESC';
		$data = $db->Query($sql)->get_rows_array();
		
		if (is_array($data)) foreach ($data as $key => $item)
		{
			$dev = new device($item['gid']);
			$data[$key]['dev'] = View_Element::Link($dev->get_address(), 'network/looking/show/'.$dev->id);
			$data[$key]['vlan'] = View_Element::Link($item['vlan'], CLASSPATH.'rollvlan/'.$item['vlan']); 			
		}

		$table = new View_Table();
		$table->AddHead('dev' , '', FALSE, '250px');
		$table->AddHead('action', 'Действие');
		$table->AddHead('vlan', 'VLan');
		$table->AddHead('port', 'Порт');
		$table->AddHead('untagged', 'Untagged', View_Table::column_type_bool);
		$table->AddHead('dt', 'Время');
		
		
		$table->AddData($data);
		$this->AddContent($table);
	}
	
	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_network_vlan_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `devices_vlan_list` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{

			$form = new View_Form(array('caption' => 'Редактировать VLan '.$data->id, 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название', 'items' => array(
							new View_Form_Text('name', $data->name, array('caption' => 'Название', 'validators'=>array( View_Form_Element::validator_require => 1))),
							))
			) ));
			

			
			if ($form->get_values())
			{
				if ($db->UpdateData('devices_vlan_list', $form->return_values, array('id' => $id)))
				{
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
					}
				}
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
}
<?php

class addr extends View_Page_Controller {
	public $title = 'Управление адресным классификатором';
	public $category = 'option';
	
	function index()
	{
		$this->city_roll();
	}
	 
	function city_roll()
	{
		if (!ACL::has_perm(ACL::acl_option_address_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'city_add'), 
											'edit_link' => CLASSPATH.'city_edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('city', 'Название', FALSE, '200px');
		$table->AddHead('street', '', FALSE, '100px');
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		
		if ($res = $db->Query('SELECT * FROM  `address_city`')->get_rows_array())
		{
			foreach ($res as $key => $item)
			{
				$res[$key]['street'] = View_Element::Link('улицы', CLASSPATH.'street_roll/'.$item['id']);
			}
			$table->AddData($res);
		}
		$this->AddContent($table);
		
	}
	
	function city_add()
	{
		if (!ACL::has_perm(ACL::acl_option_address_add )) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('caption' => 'Добавить город', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'city_roll', 
			'rows' => array( array('caption' => 'Наименование', 'items' => array(
							new View_Form_Text('city', '', array('caption' => 'Наименование', 'validators'=>array( View_Form_Element::validator_require => 1)))
							))
		) ));
		if ($form->get_values())
		{
			if ($db->InsertData('address_city', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Город добавлен.') );
				}
			}
			$this->city_roll();
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function city_edit($id)
	{
		if (!ACL::has_perm(ACL::acl_option_address_edit )) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `address_city` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$form = new View_Form(array('caption' => 'Изменить город', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'city_roll', 
			'rows' => array( array('caption' => 'Наименование', 'items' => array(
							new View_Form_Text('city', $data->city, array('caption' => 'Наименование', 'validators'=>array( View_Form_Element::validator_require => 1)))
							))
			) ));
			
			if ($form->get_values())
			{
				if ($db->UpdateData('address_city', $form->return_values, array('id' => $id)))
				{
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
					}
				}
				$this->city_roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
	function street_roll($gid)
	{
		if (!ACL::has_perm(ACL::acl_option_address_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		if ($city_name = $db->Query('SELECT * FROM  `address_city` WHERE id = '.$db->EscapeValue($gid))->get_next_row_object()->city)
		{
			$this->AddContent('<p>'.View_Element::Link('Адресный кслассификатор', CLASSPATH).' ›› '.View_Element::Link($city_name, CLASSPATH.'street_roll/'.$gid).' ›› улицы</p>');
		}
		else {  return; }
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'street_add/'.$gid), 
											'edit_link' => CLASSPATH.'street_edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('street', 'Название', FALSE, '200px');
		$table->AddHead('home', '', FALSE, '100px');
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		
		if ($res = $db->Query('SELECT * FROM  `address_street` WHERE gid='.$db->EscapeValue($gid).' ORDER BY `street` ASC')->get_rows_array())
		{
			foreach ($res as $key => $item)
			{
				$res[$key]['home'] = View_Element::Link('дома', CLASSPATH.'home_roll/'.$item['id']);
			}
			$table->AddData($res);
		}
		$this->AddContent($table);
		
	}
	
	function street_add($gid)
	{
		if (!ACL::has_perm(ACL::acl_option_address_add )) { $this->AddContent(ACL::deny()); return ; }
		global $db;
				
		$form = new View_Form(array('caption' => 'Добавить улицу', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'street_roll/'.$gid, 
			'rows' => array( array('caption' => 'Наименование', 'items' => array(
							new View_Form_Text('street', '', array('caption' => 'Наименование', 'validators'=>array( View_Form_Element::validator_require => 1)))
							))
		) ));
		if ($form->get_values())
		{
			$form->return_values['gid'] = $gid;
			if ($db->InsertData('address_street', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Улица добавлена.') );
				}
			}
			$this->street_roll($gid);
		}
		else
		{
			if ($city_name = $db->Query('SELECT * FROM  `address_city` WHERE id = '.$db->EscapeValue($gid))->get_next_row_object()->city)
			{
				$this->AddContent('<p>'.View_Element::Link('Адресный кслассификатор', CLASSPATH).' ›› '.View_Element::Link($city_name, CLASSPATH.'street_roll/'.$gid).' ›› улицы</p>');
			}
			$this->AddContent($form);
		}
	}
	
	function street_edit($id)
	{
		if (!ACL::has_perm(ACL::acl_option_address_edit )) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `address_street` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$gid = $data->gid;
			
			$form = new View_Form(array('caption' => 'Изменить город', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'street_roll/'.$gid, 
			'rows' => array( array('caption' => 'Наименование', 'items' => array(
							new View_Form_Text('street', $data->street, array('caption' => 'Наименование', 'validators'=>array( View_Form_Element::validator_require => 1)))
							))
			) ));
			
			if ($form->get_values())
			{
				
				if ($db->UpdateData('address_street', $form->return_values, array('id' => $id)))
				{
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
					}
				}
				$this->street_roll($gid);
			}
			else
			{
				if ($city_name = $db->Query('SELECT * FROM  `address_city` WHERE id = '.$db->EscapeValue($gid))->get_next_row_object()->city)
				{
					$this->AddContent('<p>'.View_Element::Link('Адресный кслассификатор', CLASSPATH).' ›› '.View_Element::Link($city_name, CLASSPATH.'street_roll/'.$gid).' ›› улицы</p>');
				}
				$this->AddContent($form);
			}
		}
	}
	
	function home_roll($gid)
	{
		if (!ACL::has_perm(ACL::acl_option_address_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$street_data = $db->Query('SELECT * FROM  `address_street` WHERE id = '.$db->EscapeValue($gid))->get_next_row_object();
		$street_name = $street_data->street;
		$city_name = $db->Query('SELECT * FROM  `address_city` WHERE id = '.$db->EscapeValue($street_data->gid))->get_next_row_object()->city;
		
		$this->AddContent('<p>'.View_Element::Link('Адресный кслассификатор', CLASSPATH).' ›› '.
			View_Element::Link($city_name, CLASSPATH.'street_roll/'.$street_data->gid).' ›› '.
			View_Element::Link($street_name, CLASSPATH.'home_roll/'.$street_data->id).' ›› дома</p>');
		
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'home_add/'.$gid), 
											'edit_link' => CLASSPATH.'home_edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('home', 'Дом', FALSE, '200px');
		$table->AddHead('area', 'участок', FALSE, '100px');
		$table->AddHead('billing_prefix', 'Префикс', FALSE, '100px');
		$table->AddHead('entrance', 'Кол-во подъездов');
		$table->AddHead('floor', 'Кол-во этажей');
		$table->AddHead('appartment', 'Кол-во квартир');
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		
		if ($res = $db->Query('SELECT address_home.*, address_area.area FROM `address_home`, address_area WHERE address_area.id = address_home.area_id AND gid='.$db->EscapeValue($gid).' ORDER BY home+0')->get_rows_array())
		{
			foreach ($res as $key => $item)
			{
				//$res[$key]['home'] = View_Element::Link('дома', CLASSPATH.'street_roll/'.$item['id']);
			}
			$table->AddData($res);
		}
		$this->AddContent($table);
	}
	
	function home_add($gid)
	{
		if (!ACL::has_perm(ACL::acl_option_address_add )) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$street_data = $db->Query('SELECT * FROM  `address_street` WHERE id = '.$db->EscapeValue($gid))->get_next_row_object();
		$street_name = $street_data->street;
		$city_name = $db->Query('SELECT * FROM  `address_city` WHERE id = '.$db->EscapeValue($street_data->gid))->get_next_row_object()->city;
		
		
		$form = new View_Form(array('caption' => 'Добавить дом', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'home_roll/'.$gid, 
			'rows' => array( array('caption' => 'Наименование', 'items' => array(
							new View_Form_Text('home', '', array('caption' => 'Наименование', 'validators'=>array( View_Form_Element::validator_require => 1))),
							' Участок ', new view_form_select_address_area('area_id')
							)),
							array('caption' => 'Биллинг префикс', 'items' => array(
							    new View_Form_Text('billing_prefix', '')
							)),
							array('caption' => 'Характеристики', 'items' => array(
							    'подъездов ',
							    new View_Form_Text('entrance', ''),
							    ' этажей ',
							    new View_Form_Text('floor', ''),
							    ' квартир ',
							    new View_Form_Text('appartment', ''),
							)),
		) ));
		if ($form->get_values())
		{
			$form->return_values['gid'] = $gid;
			if ($db->InsertData('address_home', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Улица добавлена.') );
				}
			}
			$this->home_roll($gid);
		}
		else
		{
			$this->AddContent('<p>'.View_Element::Link('Адресный кслассификатор', CLASSPATH).' ›› '.
			View_Element::Link($city_name, CLASSPATH.'street_roll/'.$street_data->gid).' ›› '.
			View_Element::Link($street_name, CLASSPATH.'home_roll/'.$street_data->id).' ›› добавить дом</p>');
			$this->AddContent($form);
		}
		
	}
	
	function home_edit($id)
	{
		if (!ACL::has_perm(ACL::acl_option_address_edit )) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `address_home` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$gid = $data->gid;
			$street_data = $db->Query('SELECT * FROM  `address_street` WHERE id = '.$db->EscapeValue($gid))->get_next_row_object();
			$street_name = $street_data->street;
			$city_name = $db->Query('SELECT * FROM  `address_city` WHERE id = '.$db->EscapeValue($street_data->gid))->get_next_row_object()->city;
			
			$form = new View_Form(array('caption' => 'Добавить дом', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'home_roll/'.$gid, 
				'rows' => array( array('caption' => 'Наименование', 'items' => array(
								new View_Form_Text('home', $data->home, array('caption' => 'Наименование', 'validators'=>array( View_Form_Element::validator_require => 1))),
								' Участок ', new view_form_select_address_area('area_id',$data->area_id)
								)),
							array('caption' => 'Биллинг префикс', 'items' => array(
							    new View_Form_Text('billing_prefix', $data->billing_prefix)
							)),
							array('caption' => 'Характеристики', 'items' => array(
							    'подъездов ',
							    new View_Form_Text('entrance', $data->entrance),
							    ' этажей ',
							    new View_Form_Text('floor', $data->floor),
							    ' квартир ',
							    new View_Form_Text('appartment', $data->appartment),
							)),
			) ));
			if ($form->get_values())
			{

				
				if ($db->UpdateData('address_home', $form->return_values, array('id' => $id)))
				{
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
					}
				}
				$this->home_roll($gid);
			}
			else
			{
				$this->AddContent('<p>'.View_Element::Link('Адресный кслассификатор', CLASSPATH).' ›› '.
				View_Element::Link($city_name, CLASSPATH.'street_roll/'.$street_data->gid).' ›› '.
				View_Element::Link($street_name, CLASSPATH.'home_roll/'.$street_data->id).' ›› редактировать дом</p>');
				$this->AddContent($form);
			}
			
		}
	}
}
<?php

class area extends View_Page_Controller {
	public $title = 'Управление участками';
	public $category = 'option';
	
	function index()
	{
		$this->roll();
	} 
	
	function roll()
	{
		if (!ACL::has_perm(ACL::acl_option_address_area_roll )) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'add'), 
											'edit_link' => CLASSPATH.'edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('area', 'Название', FALSE, '200px');
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT * FROM  `address_area`')->get_rows_array())
		{
			$table->AddData($res);
		}
		$this->AddContent($table);
	}
	
	function add()
	{
		if (!ACL::has_perm(ACL::acl_option_address_area_add )) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('caption' => 'Добавить участок', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название участка', 'items' => array(
							new View_Form_Text('area', '', array('caption' => 'Название участка', 'validators'=>array( View_Form_Element::validator_require => 1)))
							))
			) ));
		if ($form->get_values())
		{
			if ($db->InsertData('address_area', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Участок добавлен.') );
				}
			}
			$this->roll();
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_option_address_area_edit )) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `address_area` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$form = new View_Form(array('caption' => 'Добавить участок', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название участка', 'items' => array(
							new View_Form_Text('area', $data->area, array('caption' => 'Название участка', 'validators'=>array( View_Form_Element::validator_require => 1)))
							))
			) ));
			
			if ($form->get_values())
			{
				if ($db->UpdateData('address_area', $form->return_values, array('id' => $id)))
				{
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
					}
				}
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
}
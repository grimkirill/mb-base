<?php

class billprint extends View_Page_Controller {
	public $title = 'Управление печатными формами';
	public $category = 'option';
	
	function index()
	{
		$this->roll();
	} 
	
	function roll()
	{
		if (!ACL::has_perm(ACL::acl_option_billprint_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'add'), 
											'edit_link' => CLASSPATH.'edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('name', 'Название', FALSE, '200px');
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT id, name FROM  `billing_print`')->get_rows_array())
		{
			$table->AddData($res);
		}
		$this->AddContent($table);
	}
	
	function add()
	{
		if (!ACL::has_perm(ACL::acl_option_billprint_add)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('caption' => 'Добавить ', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название', 'items' => array(
							new View_Form_Text('name', '', array('caption' => 'Название', 'validators'=>array( View_Form_Element::validator_require => 1))),
							' Шрифт: ',
							new view_form_select_pdf_font('font', 1)							
							)),
							array('caption' => 'Описание', 'items' => array(
								new View_Form_WYSIWYG('text', '' )
								)							
							)
			) ));
		if ($form->get_values())
		{
			if ($db->InsertData('billing_print', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Интерфейс добавлен.') );
				}
			}
			$this->roll();
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_option_billprint_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `billing_print` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{

			$form = new View_Form(array('caption' => 'Добавить интерфейс', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название', 'items' => array(
							new View_Form_Text('name', $data->name, array('caption' => 'Название', 'validators'=>array( View_Form_Element::validator_require => 1))),
							' Шрифт: ',
							new view_form_select_pdf_font('font', $data->font)							
							)),
							array('caption' => 'Описание', 'items' => array(
								new View_Form_WYSIWYG('text', $data->text )
								)							
							),
							array('caption' => '', 'items' => array(
								new View_Form_Checkbox('delete', '', array('caption' => 'Удалить') )
								)							
							)
			) ));
			

			
			if ($form->get_values())
			{
				if ($form->return_values['delete'])
				{
					$db->DeleteData('billing_print', array('id' => $id));
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelNotice('Интерфейс удален.') );
					}
				}
				else
				{
					unset($form->return_values['delete']);
					if ($db->UpdateData('billing_print', $form->return_values, array('id' => $id)))
					{
						if ($db->GetAffectedRows() > 0)
						{
							$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
						}
					}
				}
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
}
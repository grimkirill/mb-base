<?php

class deviceconfig extends View_Page_Controller {
	public $title = 'Управление настройками оборудования';
	public $category = 'option';
	
	function index()
	{
		$this->roll();
	} 
	
	function roll()
	{
		if (!ACL::has_perm(ACL::acl_option_devices_conf_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		$table = new View_Table(array('edit_link' => CLASSPATH.'edit/', 'caption' => View_Element::Button('Добавить', CLASSPATH.'add'), ));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('name', 'Название', FALSE, '200px');
		$table->AddHead('color', 'Цвет On/Off', FALSE, '200px');
		$table->AddHead('id', '', View_Table::column_type_edit);
		$table->AddHead('command', '');
		$table->AddHead('port_count', 'Портов');

		$data = array();
		
		foreach (device_types::get_list() as $type => $item)
		{
			$data[$type]['name'] = $item;
			$data[$type]['id'] = $type;
			$config_data = $db->Query('SELECT *  FROM `devices_config` WHERE `type` = '.$type)->get_next_row_object(); 
			$data[$type]['color'] = View_Element::ColorPanel($config_data->color_on).View_Element::Space(2).View_Element::ColorPanel($config_data->color_off);
			$data[$type]['command'] = '';
			$data[$type]['port_count'] = $config_data->port_count;
			if ( device_types::get_dlink($type) )
			{
				$data[$type]['command'] = View_Element::Link('Config', CLASSPATH.'rollCommands/'.$type);
			}
			
		}	

		$table->AddData($data);
		
		$this->AddContent($table);
	}
	
	
	function rollCommands($id)
	{
		if (!ACL::has_perm(ACL::acl_option_devices_conf_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'addCommand/'.$id).
									View_Element::Space(2).View_Element::Link('Все свичи', CLASSPATH.'roll'), 
											'edit_link' => CLASSPATH.'editCommand/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('name', 'Название', 0, '300px');
		$table->AddHead('id', '', View_Table::column_type_edit);
		$table->AddData($db->Query('SELECT * from devices_commands WHERE gid ='.$id)->get_rows_array() );
		$this->AddContent($table);
	}
	
	function addCommand($id)
	{
		if (!ACL::has_perm(ACL::acl_option_devices_conf_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array( 'cancel' => CLASSPATH.'rollCommands/'.$id));
		$form->add('Название', array(
				new View_Form_Text('name') 
				));
		$form->add('Комманда', array(
				new View_Form_Textarea('command','',array('width' => '100%')) 
				));
				
				
		if ($form->get_values())
		{		
			$db->InsertData('devices_commands', $form->return_values + array('gid' => $id));
			$this->rollCommands($id);
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function editCommand($id)
	{
		if (!ACL::has_perm(ACL::acl_option_devices_conf_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT *  FROM `devices_commands` WHERE `id` = '.$id)->get_next_row_object() )
		{
			$form = new View_Form(array( 'cancel' => CLASSPATH.'rollCommands/'.$data->gid));
			$form->add('Название', array(
					new View_Form_Text('name', $data->name) 
					));
			$form->add('Комманда', array(
					new View_Form_Textarea('command',$data->command,array('width' => '100%')) 
					));
			$form->add('', array(
					new View_Form_Checkbox('delete','',array('caption' => 'Удалить')) 
					));
			if ($form->get_values())
			{	
				if ($form->return_values['delete'])
				{
					$db->DeleteData('devices_commands', array('id' => $id));
				}
				else
				{				
					unset($form->return_values['delete']);
					$db->UpdateData('devices_commands', $form->return_values, array('id' => $id));
				}
				$this->rollCommands($data->gid);
			}
			else
			{
				$this->AddContent($form);
			}				
		}		
	}


    function add()
    {
        if (!ACL::has_perm(ACL::acl_option_devices_conf_edit)) { $this->AddContent(ACL::deny()); return ; }
        global $db;
        $data = new stdClass();

        $form = new View_Form(array('caption' => '', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll',
            'rows' => array(
                array('caption' => 'Параметры', 'items' => array(
                    'Назвние ',
                    new View_Form_Text('name', $data->name),
                    ' портов ',
                    new View_Form_Text('port_count', $data->port_count),
                    ' драйвер ',
                    new View_Form_Select('driver', $data->driver, array('options' => array(
                        'device_driver_unmanage'=> 'Неуправляемый',
                        'device_driver_SNMP'=> 'SNMP',
                        'device_driver_dlink_3028'=> 'Dlink',
                    ))),
                )),
                array('caption' => 'Конфигурация', 'items' => array(
                    'Цвет On: ',
                    new View_Form_Color('color_on', $data->color_on, array('caption' => 'Цвет', 'validators'=>array( View_Form_Element::validator_require => 1))),
                    ' Цвет Off: ',
                    new View_Form_Color('color_off', $data->color_off, array('caption' => 'Цвет', 'validators'=>array( View_Form_Element::validator_require => 1)))
                )
                ),
            )
        ));
        $form->add('Настройки', array(
            new View_Form_Checkbox('port_admin', $data->port_admin, array('caption' => 'Управление портами')),
            new View_Form_Checkbox('port_active', $data->port_active, array('caption' => 'Мониторинг портов')),
            new View_Form_Checkbox('use_snmp', $data->use_snmp, array('caption' => 'Использование SNMP')),
            new View_Form_Checkbox('snmp_dlink_vlan', $data->snmp_dlink_vlan, array('caption' => 'Dlink telnet')),
        ));
        if ($form->get_values())
        {
            $db->InsertData('devices_config', $form->return_values);
            $this->roll();
        }
        else
        {
            $this->AddContent($form);
        }
    }

	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_option_devices_conf_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$data = $db->Query('SELECT *  FROM `devices_config` WHERE `type` = '.$id)->get_next_row_object(); 
		
		$form = new View_Form(array('caption' => '', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array(
                array('caption' => 'Параметры', 'items' => array(
                    'Назвние ',
                    new View_Form_Text('name', $data->name),
                    ' портов ',
                    new View_Form_Text('port_count', $data->port_count),
                    ' драйвер ',
                    new View_Form_Select('driver', $data->driver, array('options' => array(
                        'device_driver_unmanage'=> 'Неуправляемый',
                        'device_driver_eten'=> 'Eten',
                        'device_driver_SNMP'=> 'SNMP',
                        'device_driver_dlink_3028'=> 'Dlink',
			'device_driver_snr_s2940_8g'=>'SNR_S2940',
                    ))),
                )),
                array('caption' => 'Конфигурация', 'items' => array(
                    'Цвет On: ',
                    new View_Form_Color('color_on', $data->color_on, array('caption' => 'Цвет', 'validators'=>array( View_Form_Element::validator_require => 1))),
                    ' Цвет Off: ',
                    new View_Form_Color('color_off', $data->color_off, array('caption' => 'Цвет', 'validators'=>array( View_Form_Element::validator_require => 1)))
                    )
                ),
			)
        ));
        $form->add('Настройки', array(
            new View_Form_Checkbox('port_admin', $data->port_admin, array('caption' => 'Управление портами')),
            new View_Form_Checkbox('port_active', $data->port_active, array('caption' => 'Мониторинг портов')),
            new View_Form_Checkbox('use_snmp', $data->use_snmp, array('caption' => 'Использование SNMP')),
            new View_Form_Checkbox('snmp_dlink_vlan', $data->snmp_dlink_vlan, array('caption' => 'Dlink telnet')),
        ));
		if ($form->get_values())
		{
			$db->UpdateData('devices_config', $form->return_values, array('type' => $id));
			$this->roll();
		}
		else
		{
			$this->AddContent($form);
		}	
		
	}
}

<?php

class graphcounter extends View_Page_Controller {
	public $title = 'Управление Графиками';
	public $category = 'option';
	
	function index()
	{
		$this->roll();
	} 
	
	function roll()
	{
		if (!ACL::has_perm(ACL::acl_option_triggers_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'add'), 
											'edit_link' => CLASSPATH.'edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('name', 'Название');
		$table->AddHead('cmd', 'Комманда');
		$table->AddHead('use', 'Используется', View_Table::column_type_bool);
		$table->AddHead('test', '');
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT * FROM  `graph_counter`')->get_rows_array())
		{ 
			if (is_array($res)) foreach ($res as $key => $item)
			{
				$res[$key]['test'] = View_Element::Link('test', CLASSPATH.'test/'.$item['id']);
			}
			$table->AddData($res);
		}
		$this->AddContent($table);
	}
	
	function add()
	{
		if (!ACL::has_perm(ACL::acl_option_triggers_add)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('caption' => 'Добавить', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название', 'items' => array(
							new View_Form_Text('name', '', array('caption' => 'Название', 'width' => '90%', 'validators'=>array( View_Form_Element::validator_require => 1)))
							)),
							array('caption' => 'Комманда', 'items' => array(
								new View_Form_Text('cmd', '', array('caption' => 'Комманда','width' => '90%', 'validators'=>array( View_Form_Element::validator_require => 1)))
								
								)							
							)
			) ));
		if ($form->get_values())
		{
			if ($db->InsertData('graph_counter', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Добавлено.') );
					$this->create_rrd($db->GetInsertId());
				}
			}
			$this->roll();
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function edit($id)
	{
		$id = (int)$id;
		if (!ACL::has_perm(ACL::acl_option_triggers_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `graph_counter` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{

			$form = new View_Form(array('caption' => 'Изменить триггер', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название', 'items' => array(
							new View_Form_Text('name', $data->name, array('caption' => 'Название', 'width' => '90%', 'validators'=>array( View_Form_Element::validator_require => 1))),
							View_Element::Space(3)							
							)),
							array('caption' => 'Комманда', 'items' => array(
								new View_Form_Text('cmd', $data->cmd, array('caption' => 'Комманда','width' => '90%', 'validators'=>array( View_Form_Element::validator_require => 1)))
								
								)							
							),
							array('caption' => 'Действие', 'items' => array(
								new View_Form_Select('trigger', $data->trigger,  array('options' => array_merge(array(0=> '--=--'), $db->Query('SELECT id, name FROM `trigger_action`')->get_rows_array_id('id', 'name')))),
								' Min: ',
								new View_Form_Text('value_min', $data->value_min),
								' Max: ',
								new View_Form_Text('value_max', $data->value_max),
							))//,
							//array('caption' => 'Действие', 'items' => array(
                                                        //        new View_Form_Select('trigger', $data->trigger,  array('options' => array_merge(array(0=> '--=--'), $db->Query('SELECT id, name FROM `trigger_action`')->get_rows_array_id('id', 'name')))),
                                                        //        ' Min: ',
                                                        //        new View_Form_Text('value_min', $data->value_min),
                                                        //        ' Max: ',
                                                        //        new View_Form_Text('value_max', $data->value_max),
                                                        //))

			) ));
			

			
			if ($form->get_values())
			{
					if ($db->UpdateData('graph_counter', $form->return_values, array('id' => $id)))
					{
						if ($db->GetAffectedRows() > 0)
						{
							$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
						}
					}
				
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
	function test($id)
	{
		global $db;
		if ($data = $db->Query('SELECT * FROM  `graph_counter` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$path = BASEPATH.'script/';
			$cmd = str_replace('{path}', $path, $data->cmd);
			$msg = exec($cmd);
			$this->AddContent(View_Element::PanelSuccess('Проверка. Call: '.$cmd.' Return: '.$msg) );
		}
		$this->roll();
	}
	
	function create_rrd($id)
	{
		$rrd_path = BASEPATH.'rrd/';
		$rrd_sub_name = $rrd_path.'graph_counter_';
		$cmd = '/usr/bin/rrdtool create '.$rrd_sub_name.$id.'.rrd -s 300 DS:value:GAUGE:600:0:U RRA:AVERAGE:0.5:1:86400 RRA:MIN:0.5:1440:1 RRA:MAX:0.5:1440:1';
		$msg = exec($cmd);
		exec('chmod 666 '.$rrd_sub_name.$id.'.rrd');
		$this->AddContent(View_Element::PanelSuccess('Создано RRD. Call: '.$cmd.' Return: '.$msg) );
	}
	
	
}

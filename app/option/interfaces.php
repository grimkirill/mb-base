<?php

class interfaces extends View_Page_Controller {
	public $title = 'Управление интерфейсами';
	public $category = 'option';
	
	function index()
	{
		$this->roll();
	} 
	
	function roll()
	{
		if (!ACL::has_perm(ACL::acl_option_interfaces_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'add'), 
											'edit_link' => CLASSPATH.'edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('interface', 'Название', FALSE, '200px');
		$table->AddHead('ip', 'IP', FALSE, '200px');
		$table->AddHead('description', 'Описание');
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT * FROM  `devices_interface`')->get_rows_array())
		{
			$table->AddData($res);
		}
		$this->AddContent($table);
	}
	
	function add()
	{
		if (!ACL::has_perm(ACL::acl_option_interfaces_add)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('caption' => 'Добавить интерфейс', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Конфигурация', 'items' => array(
							'Название: ',
							new View_Form_Text('interface', '', array('caption' => 'Название участка', 'validators'=>array( View_Form_Element::validator_require => 1))),
							' IP: ',
							new View_Form_Text('ip', '', array('caption' => 'Название участка', 'validators'=>array( View_Form_Element::validator_require => 1)))							
							)),
							array('caption' => 'Описание', 'items' => array(
								new View_Form_Textarea('description', '' )
								)							
							)
			) ));
		if ($form->get_values())
		{
			if ($db->InsertData('devices_interface', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Интерфейс добавлен.') );
				}
			}
			$this->roll();
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_option_interfaces_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `devices_interface` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{

			$form = new View_Form(array('caption' => 'Добавить интерфейс', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Конфигурация', 'items' => array(
							'Название: ',
							new View_Form_Text('interface', $data->interface, array('caption' => 'Название участка', 'validators'=>array( View_Form_Element::validator_require => 1))),
							' IP: ',
							new View_Form_Text('ip', $data->ip, array('caption' => 'Название участка', 'validators'=>array( View_Form_Element::validator_require => 1)))							
							)),
							array('caption' => 'Описание', 'items' => array(
								new View_Form_Textarea('description', $data->description )
								)							
							),
							array('caption' => '', 'items' => array(
								new View_Form_Checkbox('delete', '', array('caption' => 'Удалить') )
								)							
							)
			) ));
			

			
			if ($form->get_values())
			{
				if ($form->return_values['delete'])
				{
					$db->DeleteData('devices_interface', array('id' => $id));
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelNotice('Интерфейс удален.') );
					}
				}
				else
				{
					unset($form->return_values['delete']);
					if ($db->UpdateData('devices_interface', $form->return_values, array('id' => $id)))
					{
						if ($db->GetAffectedRows() > 0)
						{
							$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
						}
					}
				}
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
}
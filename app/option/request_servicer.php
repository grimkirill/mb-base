<?php

class request_servicer extends View_Page_Controller {
	public $title = 'Управление исполнителями заявок';
	public $category = 'option';
	
	function index()
	{
		$this->roll();
	} 
	
	function roll()
	{
		if (!ACL::has_perm(ACL::acl_option_request_servicer_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'add'), 
											'edit_link' => CLASSPATH.'edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('name', 'Название', FALSE, '100px');
		$table->AddHead('full_name', 'Полное имя', FALSE, '300px');
		$table->AddHead('use', 'Активен', View_Table::column_type_bool);
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT * FROM  `request_servicer`')->get_rows_array())
		{
			$table->AddData($res);
		}
		$this->AddContent($table);
	}
	
	function add()
	{
		if (!ACL::has_perm(ACL::acl_option_request_servicer_add)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('caption' => 'Добавить ', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название', 'items' => array(
							new View_Form_Text('name', '', array('caption' => 'Название', 'validators'=>array( View_Form_Element::validator_require => 1))),
							' Полное имя: ',
							new View_Form_Text('full_name', '', array('caption' => 'Полное Название', 'validators'=>array( View_Form_Element::validator_require => 1))),
							))
			) ));
		if ($form->get_values())
		{
			if ($db->InsertData('request_servicer', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Добавлено.') );
				}
			}
			$this->roll();
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_option_request_servicer_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `request_servicer` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$form = new View_Form(array('caption' => 'Изменение', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название', 'items' => array(
							new View_Form_Text('name', $data->name, array('caption' => 'Название', 'validators'=>array( View_Form_Element::validator_require => 1))),
							' Полное имя: ',
							new View_Form_Text('full_name', $data->full_name, array('caption' => 'Полное Название', 'validators'=>array( View_Form_Element::validator_require => 1))),
							new View_Form_Checkbox('use', $data->use, array('caption' => 'Активен'))
							)),
							array('caption' => 'Действие', 'items' => array(
								new View_Form_Select('trigger', $data->trigger,  array('options' => array_merge(array(0=> '--=--'), $db->Query('SELECT id, name FROM `trigger_action`')->get_rows_array_id('id', 'name')))),
								' Сообщение: ',
								new View_Form_Text('trigger_text', $data->trigger_text)
							))
			) ));
			
			if ($form->get_values())
			{
				if ($db->UpdateData('request_servicer', $form->return_values, array('id' => $id)))
				{
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
					}
				}
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
}
<?php

class request_type extends View_Page_Controller {
	public $title = 'Управление типами заявок';
	public $category = 'option';
	
	function index()
	{
		$this->roll();
	} 
	
	function roll()
	{
		if (!ACL::has_perm(ACL::acl_option_request_type_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'add'), 
											'edit_link' => CLASSPATH.'edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('name', 'Название', FALSE, '300px');
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT * FROM  `request_type`')->get_rows_array())
		{
			$table->AddData($res);
		}
		$this->AddContent($table);
	}
	
	function add()
	{
		if (!ACL::has_perm(ACL::acl_option_request_type_add)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('caption' => 'Добавить ', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название', 'items' => array(
							new View_Form_Text('name', '', array('caption' => 'Название', 'validators'=>array( View_Form_Element::validator_require => 1)))
							))
			) ));
		if ($form->get_values())
		{
			if ($db->InsertData('request_type', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Добавлено.') );
				}
			}
			$this->roll();
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_option_request_type_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `request_type` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$form = new View_Form(array('caption' => 'Изменение', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название', 'items' => array(
							new View_Form_Text('name', $data->name, array('caption' => 'Название', 'validators'=>array( View_Form_Element::validator_require => 1)))
							))
			) ));
			
			if ($form->get_values())
			{
				if ($db->UpdateData('request_type', $form->return_values, array('id' => $id)))
				{
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
					}
				}
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
}
<?php

class taglists extends View_Page_Controller {
	public $title = 'Управление тегами оборудования';
	public $category = 'option';
	
	function index()
	{
		$this->roll();
	} 
	
	function roll()
	{
		if (!ACL::has_perm(ACL::acl_option_devices_tag_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'add'), 
											'edit_link' => CLASSPATH.'edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('name', 'Название', FALSE, '200px');
		$table->AddHead('color', 'Цвет', FALSE, '200px');
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT * FROM  `devices_tag_list`')->get_rows_array())
		{
			$table->AddData($res);
		}
		$this->AddContent($table);
	}
	
	function add()
	{
		if (!ACL::has_perm(ACL::acl_option_devices_tag_add)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('caption' => 'Добавить', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Конфигурация', 'items' => array(
							'Название: ',
							new View_Form_Text('name', '', array('caption' => 'Название участка', 'validators'=>array( View_Form_Element::validator_require => 1))),
							' Цвет: ',
							new View_Form_Text('color', 'black', array('caption' => 'Название участка', 'validators'=>array( View_Form_Element::validator_require => 1)))							
							))
			) ));
		if ($form->get_values())
		{
			if ($db->InsertData('devices_tag_list', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Тег добавлен.') );
				}
			}
			$this->roll();
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_option_devices_tag_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `devices_tag_list` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{

			$form = new View_Form(array('caption' => 'Добавить интерфейс', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Конфигурация', 'items' => array(
							'Название: ',
							new View_Form_Text('name', $data->name, array('caption' => 'Название', 'validators'=>array( View_Form_Element::validator_require => 1))),
							' Цвет: ',
							new View_Form_Color('color', $data->color, array('caption' => 'Цвет', 'validators'=>array( View_Form_Element::validator_require => 1)))							
							)),
							array('caption' => '', 'items' => array(
								new View_Form_Checkbox('delete', '', array('caption' => 'Удалить') )
								)							
							)
			) ));
			

			
			if ($form->get_values())
			{
				if ($form->return_values['delete'])
				{
					$db->DeleteData('devices_tag_list', array('id' => $id));
					if ($db->GetAffectedRows() > 0)
					{
						$this->AddContent(View_Element::PanelNotice('Тэг удален.') );
					}
				}
				else
				{
					unset($form->return_values['delete']);
					if ($db->UpdateData('devices_tag_list', $form->return_values, array('id' => $id)))
					{
						if ($db->GetAffectedRows() > 0)
						{
							$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
						}
					}
				}
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
}
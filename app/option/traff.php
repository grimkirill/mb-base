<?php
/**
 * Created by JetBrains PhpStorm.
 * User: k.Skatov
 * Date: 03.07.12
 * Time: 16:34
 * To change this template use File | Settings | File Templates.
 */
class traff  extends View_Page_Controller {
    public $title = 'Общий траффик';
    public $category = 'option';

    function index()
    {
        $this->roll();
    }

    function roll()
    {
        if (!ACL::has_perm(ACL::acl_option_config_roll)) { $this->AddContent(ACL::deny()); return ; }
        global $db;
        $table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'add'),
            'edit_link' => CLASSPATH.'edit/'));

        $table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
        $table->AddHead('ip', 'IP', FALSE, '200px');
        $table->AddHead('port', 'Порт', FALSE, '200px');
        $table->AddHead('snmp_read', 'SNMP', FALSE, '200px');
        $table->AddHead('block', 'Блокировка', View_Table::column_type_bool_inverse, '100px');
        $table->AddHead('id', '', View_Table::column_type_edit);

        if ($res = $db->Query('SELECT * FROM  `graph_traff`')->get_rows_array())
        {
            $table->AddData($res);
        }
        $this->AddContent($table);
    }

    function add()
    {
        if (!ACL::has_perm(ACL::acl_option_config_edit)) { $this->AddContent(ACL::deny()); return ; }
        global $db;
        $form = new View_Form(array('caption' => 'Добавить', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll',
            'rows' => array( array('caption' => 'Конфигурация', 'items' => array(
                'IP: ',
                new View_Form_Text('ip', '', array('caption' => 'IP', 'validators'=>array( View_Form_Element::validator_require => 1))),
                ' Порт: ',
                new View_Form_Text('port', '1', array('caption' => 'Порт', 'validators'=>array( View_Form_Element::validator_require => 1))),
                ' SNMP: ',
                new View_Form_Text('snmp_read', 'public', array('caption' => 'SNMP', 'validators'=>array( View_Form_Element::validator_require => 1))),
                ' Использование: ',
                new View_Form_Checkbox('block', '', array('caption' => 'Использование')),
            ))
            ) ));
        if ($form->get_values())
        {
            if ($db->InsertData('graph_traff', $form->return_values))
            {
                if ($db->GetAffectedRows() > 0)
                {
                    $this->AddContent(View_Element::PanelSuccess('Тег добавлен.') );
                }
            }
            $this->roll();
        }
        else
        {
            $this->AddContent($form);
        }
    }

    function edit($id)
    {
        if (!ACL::has_perm(ACL::acl_option_config_edit)) { $this->AddContent(ACL::deny()); return ; }
        global $db;
        if ($data = $db->Query('SELECT * FROM  `graph_traff` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
        {
            $form = new View_Form(array('caption' => 'Добавить', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll',
                'rows' => array(
                    array('caption' => 'Конфигурация', 'items' => array(
                        'IP: ',
                        new View_Form_Text('ip', $data->ip, array('caption' => 'IP', 'validators' => array(View_Form_Element::validator_require => 1))),
                        ' Порт: ',
                        new View_Form_Text('port', $data->port, array('caption' => 'Порт', 'validators' => array(View_Form_Element::validator_require => 1))),
                        ' SNMP: ',
                        new View_Form_Text('snmp_read', $data->snmp_read, array('caption' => 'SNMP', 'validators' => array(View_Form_Element::validator_require => 1))),
                        ' Использование: ',
                        new View_Form_Checkbox('block', $data->block, array('caption' => 'Использование')),
                    )),
                    array('caption' => '', 'items' => array(
                        new View_Form_Checkbox('delete', '', array('caption' => 'Удалить') )
                    ))
                )));

            if ($form->get_values())
            {
                if ($form->return_values['delete'])
                {
                    $db->DeleteData('graph_traff', array('id' => $id));
                    if ($db->GetAffectedRows() > 0)
                    {
                        $this->AddContent(View_Element::PanelNotice('Тэг удален.') );
                    }
                }
                else
                {
                    unset($form->return_values['delete']);
                    if ($db->UpdateData('graph_traff', $form->return_values, array('id' => $id)))
                    {
                        if ($db->GetAffectedRows() > 0)
                        {
                            $this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
                        }
                    }
                }
                $this->roll();
            }
            else
            {
                $this->AddContent($form);
            }
        }
    }
}

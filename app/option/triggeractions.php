<?php

class triggeractions extends View_Page_Controller {
	public $title = 'Управление триггерами';
	public $category = 'option';
	
	function index()
	{
		$this->roll();
	} 
	
	function roll()
	{
		if (!ACL::has_perm(ACL::acl_option_triggers_roll)) { $this->AddContent(ACL::deny()); return ; }
		global $db;	
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'add'), 
											'edit_link' => CLASSPATH.'edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('name', 'Название');
		$table->AddHead('cmd', 'Комманда');
		$table->AddHead('test', '');
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT * FROM  `trigger_action`')->get_rows_array())
		{ 
			if (is_array($res)) foreach ($res as $key => $item)
			{
				$res[$key]['test'] = View_Element::Link('test', CLASSPATH.'test/'.$item['id']);
			}
			$table->AddData($res);
		}
		$this->AddContent(View_Element::Link('История', CLASSPATH.'history'));
		$this->AddContent($table);
	}
	
	function add()
	{
		if (!ACL::has_perm(ACL::acl_option_triggers_add)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		$form = new View_Form(array('caption' => 'Добавить триггер', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название', 'items' => array(
							new View_Form_Text('name', '', array('caption' => 'Название', 'validators'=>array( View_Form_Element::validator_require => 1)))
							)),
							array('caption' => 'Комманда', 'items' => array(
								new View_Form_Text('cmd', '', array('caption' => 'Комманда','width' => '90%', 'validators'=>array( View_Form_Element::validator_require => 1)))
								
								)							
							)
			) ));
		if ($form->get_values())
		{
			if ($db->InsertData('trigger_action', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Триггер добавлен.') );
				}
			}
			$this->roll();
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function edit($id)
	{
		if (!ACL::has_perm(ACL::acl_option_triggers_edit)) { $this->AddContent(ACL::deny()); return ; }
		global $db;
		if ($data = $db->Query('SELECT * FROM  `trigger_action` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{

			$form = new View_Form(array('caption' => 'Изменить триггер', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => 'Название', 'items' => array(
							new View_Form_Text('name', $data->name, array('caption' => 'Название', 'validators'=>array( View_Form_Element::validator_require => 1)))
							)),
								array('caption' => 'Время действия', 'items' => array(
								'С ',
								new View_Form_Time('time_start', $data->time_start),
								' По',
								new View_Form_Time('time_stop', $data->time_stop)
							)),
							array('caption' => 'Комманда', 'items' => array(
								new View_Form_Text('cmd', $data->cmd, array('caption' => 'Комманда','width' => '90%', 'validators'=>array( View_Form_Element::validator_require => 1)))
								
								)							
							)
			) ));
			

			
			if ($form->get_values())
			{
					if ($db->UpdateData('trigger_action', $form->return_values, array('id' => $id)))
					{
						if ($db->GetAffectedRows() > 0)
						{
							$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
						}
					}
				
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
	
	function history()
	{
		
		global $db;
		$this->AddContent(View_Element::Link('Список', CLASSPATH));
		$form = new View_Form();
		$form->add('Время', array( 
			'c ', new View_Form_Date('date_start', date('Y-m-d')), ' по ',
			new View_Form_Date('date_stop', date('Y-m-d'))
		));
		
		$date_start = date('Y-m-d');
		$date_stop = date('Y-m-d');

		if ($form->get_values())
		{
			$date_start = $form->return_values['date_start'];
			$date_stop = $form->return_values['date_stop'];
			
		}
		$this->AddContent($form);
		
		$sql = ' SELECT trigger_log.dt, trigger_log.value, trigger_action.name FROM `trigger_log`, trigger_action WHERE trigger_action.id = trigger_log.gid  AND trigger_log.dt >= \''.$date_start.' 00:00:00\' AND trigger_log.dt <= \''.$date_stop.' 23:59:59\'  ORDER BY trigger_log.dt DESC';
		$data = $db->Query($sql)->get_rows_array();
		$table = new View_Table();
		$table->AddHead('dt', 'Время' );
		$table->AddHead('name', 'Тип' );
		$table->AddHead('value', 'Значение' );
		$table->AddData($data);
		$this->AddContent($table);	
		
	}
	
	function test($id)
	{
		$tg = new trigger($id, 'Test');
		$this->AddContent(View_Element::PanelSuccess('Триггер активирован. Call: '.$tg->cmd.' Return: '.$tg->msg) );
		$this->roll();
	}
	
}

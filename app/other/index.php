<?php

class index extends View_Page_Controller {
	public $title = 'Другое';
	public $category = 'other';
	public $first_call = FALSE;
	
	function index()
	{
		if ($this->first_call == FALSE)
		{
			$this->first_call = TRUE;
			
			header('Location: '.WEBPATH.'other/mytask/');
			
		}
	}
}
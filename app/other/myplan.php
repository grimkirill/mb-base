<?php

class myplan extends View_Page_Controller {
	public $title = 'Мой план';
	public $category = 'other';
	
function index()
	{
		$this->roll();
	}
	
	function roll()
	{
		global $db, $auth_user;	
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'add'), 
											'edit_link' => CLASSPATH.'edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('date', 'Дата');
		$table->AddHead('time', 'Время');
		$table->AddHead('value', 'Содежание');
		
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT * FROM  `other_plan` WHERE user = '.$auth_user->id.' ORDER BY `date` DESC, `time` DESC')->get_rows_array())
		{
			$table->AddData($res);
		}
		$this->AddContent($table);
	}
	
	function add()
	{
		global $db, $auth_user;
			$form = new View_Form(array('caption' => 'Добавить', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => '', 'items' => array(
							new view_form_select_user('user', $auth_user->id),
							' действие: ',
							new View_Form_Select('action', '',  array('options' => $db->Query('SELECT id, name FROM `trigger_action`')->get_rows_array_id('id', 'name'))),
							' дата: ',
							new View_Form_Date('date', ''),
							' время: ',
							new View_Form_Time('time',''),
							new View_Form_WYSIWYG('value','')
							
							))
			) ));
		if ($form->get_values())
		{
			if ($db->InsertData('other_plan', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Добавлено.') );
				}
			}
			$this->roll();
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function edit($id)
	{
		global $db, $auth_user;
		if ($data = $db->Query('SELECT * FROM  `other_plan` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$form = new View_Form(array('caption' => 'Изменить', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => '', 'items' => array(
							new view_form_select_user('user', $data->user),
							' действие: ',
							new View_Form_Select('action', $data->action,  array('options' => $db->Query('SELECT id, name FROM `trigger_action`')->get_rows_array_id('id', 'name'))),
							' дата: ',
							new View_Form_Date('date', $data->date),
							' время: ',
							new View_Form_Time('time', $data->time),
							new View_Form_WYSIWYG('value', $data->value),
							new View_Form_Checkbox('delete','', array('caption' => 'Удалить'))
							))
			) ));
			
			if ($form->get_values())
			{
				if ($form->return_values['delete'])
				{
					$db->DeleteData('other_plan', array('id' => $id));
				}
				else
				{
					unset($form->return_values['delete']);
					if ($db->UpdateData('other_plan', $form->return_values, array('id' => $id)))
					{
						if ($db->GetAffectedRows() > 0)
						{
							$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
						}
					}
				}
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
			
		}	
	}
	
	
}

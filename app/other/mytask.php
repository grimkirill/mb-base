<?php

class mytask extends View_Page_Controller {
	public $title = 'Мои задачи';
	public $category = 'other';

	
	function index()
	{
		$this->roll();
	}
	
	function roll()
	{
		global $db, $auth_user;	
		$table = new View_Table(array('caption' => View_Element::Button('Добавить', CLASSPATH.'add'), 
											'edit_link' => CLASSPATH.'edit/'));
		
		$table->AddHead('num', '№', View_Table::column_type_autonum, '30px' );
		$table->AddHead('text', 'Содежание');
		$table->AddHead('id', '', View_Table::column_type_edit);
		
		if ($res = $db->Query('SELECT * FROM  `other_task` WHERE user = '.$auth_user->id)->get_rows_array())
		{
			$table->AddData($res);
		}
		$this->AddContent($table);
	}
	
	function add()
	{
		global $db, $auth_user;
			$form = new View_Form(array('caption' => 'Добавить', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => '', 'items' => array(
							new view_form_select_user('user', $auth_user->id),
							new View_Form_WYSIWYG('text','')
							
							))
			) ));
		if ($form->get_values())
		{
			if ($db->InsertData('other_task', $form->return_values))
			{
				if ($db->GetAffectedRows() > 0)
				{
					$this->AddContent(View_Element::PanelSuccess('Добавлено.') );
				}
			}
			$this->roll();
		}
		else
		{
			$this->AddContent($form);
		}
	}
	
	function edit($id)
	{
		global $db, $auth_user;
		if ($data = $db->Query('SELECT * FROM  `other_task` WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$form = new View_Form(array('caption' => 'Добавить', 'method' => 'post', 'border' => FALSE, 'cancel' => CLASSPATH.'roll', 
			'rows' => array( array('caption' => '', 'items' => array(
							new view_form_select_user('user', $data->user),
							new View_Form_WYSIWYG('text', $data->text),
							new View_Form_Checkbox('delete','', array('caption' => 'Удалить'))
							
							))
			) ));
			
			if ($form->get_values())
			{
				if ($form->return_values['delete'])
				{
					$db->DeleteData('other_task', array('id' => $id));
				}
				else
				{
					unset($form->return_values['delete']);
					if ($db->UpdateData('other_task', $form->return_values, array('id' => $id)))
					{
						if ($db->GetAffectedRows() > 0)
						{
							$this->AddContent(View_Element::PanelSuccess('Изменения сохранены.') );
						}
					}
				}
				$this->roll();
			}
			else
			{
				$this->AddContent($form);
			}
		}
	}
	
	
}
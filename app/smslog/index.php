<?php

class index extends View_Page_Controller {
	public $title = 'Статистика СМС';
	public $category = 'smslog';
	public $first_call = FALSE;

	function index()
	{
		if ($this->first_call == FALSE)
		{
			$this->first_call = TRUE;
			//if (!ACL::has_perm(ACL::acl_network_device_roll)) { $this->AddContent(ACL::deny()); return ; }
			global $db;

			$form = new View_Form(array('method' => 'get'));
                	$form->add('Поиск по дате', array(
                        	new View_Form_Date('date_start', date('Y-m-d')), ' ПО ',
                        	new View_Form_Date('date_stop', date('Y-m-d')),View_Element::Space(2),
                        ));

                        $this->AddContent('<h3>Статистика отправки СМС пользователям</h3>');
			$this->AddContent($form);
			$this->AddContent("<hr>");

                        $date_start = date("Y-m-d");
                        $date_stop = date("Y-m-d");
			if ($form->get_values()) {
				$date_start = $form->return_values['date_start'];
				$date_stop = $form->return_values['date_stop'];
			} 

			$sql_clause = "(`dt` >= '$date_start' AND `dt` < '$date_stop') OR `dt` LIKE  '$date_stop %'";
			if($date_start == $date_stop) {
				$sql_clause = "dt LIKE '$date_start %'";
			}

			$sql = "SELECT * FROM `logsms` WHERE $sql_clause ORDER BY dt";

			$sms_list = $db->Query($sql)->get_rows_array();

			$table = new View_Table(array());
			$table->AddHead('num', '', View_Table::column_type_autonum );
			$table->AddHead('uid', 'UID', FALSE, '50px' );
			$table->AddHead('phone', 'Телефон', FALSE, '100px');
			$table->AddHead('sms', 'Текст СМС', FALSE, '350px');
			$table->AddHead('dt', 'Время' );
//$this->AddContent($sql);
			if(is_array($sms_list)) {
				$table->AddData($sms_list);
				$this->AddContent("<h4>Всего СМС за период: ".count($sms_list)."</h4>");
			}
			$this->AddContent($table);
		}

	}
}

<?php

include_once dirname(dirname(__FILE__)).'/system/init.php';

$db_nibs = new DB_MySql_Driver(array(
	'hostname' => get_default_value::get('option.nibs.hostname'),
	'username' => get_default_value::get('option.nibs.username'),
	'password' => get_default_value::get('option.nibs.password'),
	'database' => get_default_value::get('option.nibs.database'),
	'char_set' => get_default_value::get('option.nibs.char_set'),
	'dbcollat' => get_default_value::get('option.nibs.dbcollat')

));

#$sql = "SELECT users.uid, actions.in_bytes, actions.out_bytes  FROM actions, users WHERE terminate_cause = 'Online' AND actions.user = users.user AND actions.in_bytes IS NOT NULL ";
#ld
$sql = "SELECT uid, acctinputoctets AS in_bytes, acctoutputoctets AS out_bytes  FROM radacct WHERE  acctterminatecause = 'Online' acctinputoctets !=0 ";

$rrd_sub_name = dirname(dirname(__FILE__)).'/rrd/abonent_';

$res = $db_nibs->Query($sql);

while ($item = $res->get_next_row_object())
{
	$call = 'rrdtool update "'.$rrd_sub_name.$item->uid.'.rrd" N:'.$item->in_bytes.':'.$item->out_bytes; 
	echo exec($call);
}


#$sql = "SELECT SUM(in_bytes) as in_bytes, SUM(out_bytes) as out_bytes, gid  FROM actions WHERE `start_time` > '".date('Y-m')."-01' GROUP BY gid";

#ld
$sql = "SELECT SUM(acctinputoctets) as in_bytes, SUM(acctoutputoctets) as out_bytes, gid  FROM radacct WHERE `acctstarttime` > '".date('Y-m')."-01' GROUP BY gid";

$rrd_sub_name = dirname(dirname(__FILE__)).'/rrd/tarif_';

$res = $db_nibs->Query($sql);

while ($item = $res->get_next_row_object())
{
	$call = 'rrdtool update "'.$rrd_sub_name.$item->gid.'.rrd" N:'.$item->in_bytes.':'.$item->out_bytes; 
	echo exec($call);
}
<?php

include_once dirname(dirname(__FILE__)).'/system/init.php';

$packet_cost = nibs_abonent::get_cost_packets();
foreach (nibs_abonent::get_list_packets() as $id => $packet)
{
	if ($db->Query('SELECT * FROM `billing_packets` WHERE id ='.$id)->get_rows_count() > 0)
	{
		$db->UpdateData('billing_packets', array('packet' => $packet, 'amount' => $packet_cost[$id]), array('id' => $id));
	}
	else
	{
		$db->InsertData('billing_packets', array('id' => $id, 'packet' => $packet, 'amount' => $packet_cost[$id]));
	}
	
	create_tarif_rrd($id);
}

//Load users from nibs
foreach (nibs_abonent::get_list_users() as $id => $user)
{
	if ($data = $db->Query('SELECT * FROM `billing_users` WHERE id ='.$id)->get_next_row_object())
	{
		if ($data->gid != $user['gid'])
		{
			$db->InsertData('billing_user_gid', array('uid' => $id, 'gid' => $user['gid'], 'fgid' => $data->gid));
		}
		if ($data->blocked != $user['blocked'])
		{
			$db->InsertData('billing_user_blocked', array('uid' => $id, 'blocked' => $user['blocked']));
		}
		$db->UpdateData('billing_users', array('gid' => $user['gid'], 'blocked' => $user['blocked'], 'user' => $user['user']), array('id' => $id));
	}
	else
	{
		$db->InsertData('billing_users', array('id' => $id, 'gid' => $user['gid'], 'blocked' => $user['blocked'], 'user' => $user['user']));
		//create_abonent_rrd($id);
	}
	
	$abon = new nibs_abonent($id);
	if ($mac = $abon->get_mac())
	{
		$db->UpdateData('billing_users', array('mac' => $mac), array('id' => $id));
	}
}


function create_tarif_rrd($id)
{
	$rrd_sub_name = dirname(dirname(__FILE__)).'/rrd/tarif_';
	if (!file_exists($rrd_sub_name.$id.'.rrd'))
	{
		$cmd = '/usr/bin/rrdtool create '.$rrd_sub_name.$id.'.rrd -s 300 DS:input:COUNTER:600:0:10000000000 DS:output:COUNTER:600:0:10000000000 RRA:AVERAGE:0.5:1:600 RRA:AVERAGE:0.5:6:700 RRA:AVERAGE:0.5:24:775 RRA:AVERAGE:0.5:288:797 RRA:MAX:0.5:1:600 RRA:MAX:0.5:6:700 RRA:MAX:0.5:24:775 RRA:MAX:0.5:288:797';
		$msg = exec($cmd);
		exec('chmod 666 '.$rrd_sub_name.$id.'.rrd');
	}
}

function create_abonent_rrd($id)
{
	$rrd_sub_name = dirname(dirname(__FILE__)).'/rrd/abonent_';
	$cmd = '/usr/bin/rrdtool create '.$rrd_sub_name.$id.'.rrd -s 300 DS:input:COUNTER:600:0:10000000000 DS:output:COUNTER:600:0:10000000000 RRA:AVERAGE:0.5:1:600 RRA:AVERAGE:0.5:6:700 RRA:AVERAGE:0.5:24:775 RRA:AVERAGE:0.5:288:797 RRA:MAX:0.5:1:600 RRA:MAX:0.5:6:700 RRA:MAX:0.5:24:775 RRA:MAX:0.5:288:797';
	$msg = exec($cmd);
	exec('chmod 666 '.$rrd_sub_name.$id.'.rrd');
}


//Block OR Unlook users port
$abonents_device = $db->Query('SELECT devices_abonents.*, billing_users.blocked FROM `devices_abonents`, billing_users WHERE devices_abonents.uid != 0 AND devices_abonents.port != 0 AND devices_abonents.uid = billing_users.id');
while ($abonent = $abonents_device->get_next_row_object())
{
	if ($port = $db->Query('SELECT *  FROM `devices_ports` WHERE `gid` = '.$abonent->gid.' AND `port` = '.$abonent->port)->get_next_row_object() )
	{
		if ($port->admin == $abonent->blocked)
		{
			$dev = new device($port->gid);
			if (($dev->get_port_admin()) AND ($dev->is_ping()))
			{
				$set_value = FALSE;
				if ($abonent->blocked)
				{
					$set_value = TRUE;
				}
				print_r($abonent);
				echo $dev->id.' port('.$port->port.') abonent['.$abonent->id.'] = '.$set_value."\n"; 
				if ($dev->driver->block_port($port->port, $set_value) )
				{
					//$dev->_set_port_admin($port->port, $set_value);
					$new_admin = 1;
					if ($port->admin) { $new_admin = 0; }
					$db->UpdateData('devices_ports', array('admin' => $new_admin), array('id' => $port->id));
					$db->InsertData('devices_port_admin', array('gid'=> $port->id, 'state' => $new_admin));
				}
			}
		}
	}
}

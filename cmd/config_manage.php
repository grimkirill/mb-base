<?php
include_once dirname(dirname(__FILE__)).'/system/init.php';

//echo get_default_value::get('default.snmp.write');
device_types::init();
global $device_type_list;
$read_list = array();
foreach ($device_type_list as $key => $item)
{
	if ($item['snmp_dlink_vlan'])
	{
		$read_list[] = $key;
	}
}

$devices_list = $db->Query('SELECT `id`, `ip`, `access_snmp_read`, type FROM `devices` WHERE `ping` = 1 AND type IN ( '.implode(', ',$read_list).' )');

while ($device = $devices_list->get_next_row_object() ){
	
	$dev = new device($device->id);
	
	if (method_exists($dev->driver, 'get_config'))
	{
		$config = $dev->driver->get_config();
		echo $config;
		//TODO:  do update config
		$db->UpdateData('devices', array('config' => $config), array('id' => $device->id));
	}
	
	
}


<?php
include_once dirname(dirname(__FILE__)).'/system/init.php';

$read_list = array(20, 21, 22);

$devices_list = $db->Query('SELECT `id`, `ip`, `access_snmp_read` FROM `devices` WHERE `ping` = 1 AND type IN ( '.implode(', ',$read_list).' )');

$multi = new MultiTask;
$multi->maxThread = get_default_value::get('option.thread.eten');
$command = 'php '.dirname(__FILE__).'/eten_read_device.php ';

$count_line = (int)($devices_list->get_rows_count() / get_default_value::get('option.thread.eten'));

while ($device = $devices_list->get_next_row_object() ){
	$id_list = $device->id;
	for ($i = 1; $i <= $count_line; $i++)
	{
		if ($device = $devices_list->get_next_row_object())
		{
			$id_list .= ' '.$device->id;
		}
		else
		{
			break;
		}
	}
	$multi->addcommand($command.$id_list );	
}

$multi->execute();

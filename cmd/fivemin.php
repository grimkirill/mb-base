<?php


include_once dirname(dirname(__FILE__)).'/system/init.php';

$path = dirname(dirname(__FILE__)).'/cmd/';

$multi = new MultiTask;
$multi->maxThread = 7;

//$multi->addcommand('php ' . $path . 'snmp_graph_In_out.php');
//$multi->addcommand('php ' . $path . 'graph_counter.php');
//$multi->addcommand('php ' . $path . 'billing_graph_in_out.php');
$multi->addcommand('php ' . $path . 'snmp_reader.php');
$multi->addcommand('php ' . $path . 'snmp_traff.php');
//$multi->addcommand('php '.$path.'cron_plan.php ');
//$multi->addcommand('php '.$path.'snmp_broadcast.php');

if (date('h') == 1  && (date('i') < 5))
{
	$multi->addcommand('php '.$path.'one_day.php ');
}

$multi->execute();


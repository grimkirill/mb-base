<?php

include_once dirname(dirname(__FILE__)).'/system/init.php';


$devices_list = $db->Query('SELECT *  FROM `graph_counter` WHERE `use` = 1');

$rrd_sub_name = dirname(dirname(__FILE__)).'/rrd/graph_counter_';
$sript_path = dirname(dirname(__FILE__)).'/script/';

while ($device = $devices_list->get_next_row_object() ){
	
	$cmd = str_replace('{path}', $sript_path, $device->cmd);
	$value = floatval(exec($cmd));
	if ($device->trigger) {
	    if (($device->value < $device->value_max) AND ($value >= $device->value_max)) {
	        new trigger($device->trigger, 'Alert ' . $device->name . ' ' . $value);
	    }
	    if (($device->value > $device->value_min) AND ($value <= $device->value_min)) {
	        new trigger($device->trigger, 'Alert ' . $device->name . ' ' . $value);
	    }
	}
	$db->UpdateData('graph_counter', array('value' => $value), array('id' => $device->id)); 
	$call = 'rrdtool update "'.$rrd_sub_name.$device->id.'.rrd" N:' . $value;
	echo exec($call);
	//echo 	$call."\n";
	//$multi->addcommand($call, $device->id );
}

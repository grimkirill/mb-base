<?php

include_once dirname(dirname(__FILE__)).'/system/init.php';

check_update();

billing_termination();

billing_withdrawal();









function billing_withdrawal()
{
	global $db;
	
	if (get_default_value::get('billing.withdrawal.day') != 0 && get_default_value::get('billing.withdrawal.tarif') != 0)
	{
		$paket_list = implode(', ', $db->Query('SELECT `id` FROM `billing_packets` WHERE `type` = 0')->get_rows_array_id('id', 'id'));
		
		$users = $db->Query('SELECT *  FROM `billing_users` WHERE `gid` IN ('.$paket_list.') AND blocked = 1');
		while ($user_data = $users->get_next_row_object())
		{
			$nibs_abon = new nibs_abonent($user_data->id);
			
			if ($nibs_abon->get_inet_use())  //были выходы в интернет вообще значит подключен
			{
				$count = $nibs_abon->get_day_off(get_default_value::get('billing.withdrawal.day'));
				if ($count == 0)
				{
					$nibs_abon->make_withdrawal();
					echo $nibs_abon->uid." withdrawal\n";
				}
			}
			
		}
	}
}




function billing_termination()
{
	global $db;
	
	if (get_default_value::get('billing.termination.day') != 0 && get_default_value::get('billing.termination.tarif') != 0)
	{
		$paket_list = implode(', ', $db->Query('SELECT `id` FROM `billing_packets` WHERE `type` = 0')->get_rows_array_id('id', 'id')).' ,'.get_default_value::get('billing.withdrawal.tarif');
		
		$users = $db->Query('SELECT *  FROM `billing_users` WHERE `gid` IN ('.$paket_list.') AND blocked = 1');
		while ($user_data = $users->get_next_row_object())
		{
			$nibs_abon = new nibs_abonent($user_data->id);
			
			$count = $nibs_abon->get_day_off(get_default_value::get('billing.termination.day'));
			if ($count == 0)
			{
				$nibs_abon->make_termination();
				echo $nibs_abon->uid." termination\n";
			} 
		}
	}
}



function check_update()
{
	global $db;
	$host_url = 'http://support.noadmin.ru/';
	$server_ver = (int)file_get_contents($host_url.'ver.txt');
	if ($server_ver > VERSION)
	{

		if (!file_put_contents(BASEPATH.'base.tar', file_get_contents($host_url.'base.tar')))
		{
			echo 'Не удалось скачать обновление';
			return;
		}
		else {
			chmod(BASEPATH.'base.tar', 0666);
		}
		$sql = file_get_contents($host_url.'get_sql.php?ver='.VERSION);
		$sql_array = explode("\n",$sql);
		foreach ($sql_array as $item)
		{
			if ($item)
			{
				$db->SimpleQuery($item);
			}
		}
		
		exec("tar -xzf '".BASEPATH.'base.tar'."' -C '".BASEPATH."'");
		unlink(BASEPATH.'base.tar');
	}
}
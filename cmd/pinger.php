<?php

include_once dirname(dirname(__FILE__)).'/system/init.php';



function ip_in_network($ip, $net_addr, $net_mask){ 
    if($net_mask <= 0){ return false; } 
        $ip_binary_string = sprintf("%032b",ip2long($ip)); 
        $net_binary_string = sprintf("%032b",ip2long($net_addr)); 
        return (substr_compare($ip_binary_string,$net_binary_string,0,$net_mask) === 0); 
}

$networks = $db->Query('SELECT `interface`, `ip` FROM `devices_interface`')->get_rows_array_id('interface', 'ip'); 

function getEther($ip){
	global $networks;

	if (is_array($networks)){ 
		foreach ($networks as $key => $item){
			$subnet = explode('/',$item);
			if (ip_in_network($ip, $subnet[0], $subnet[1])){
				return $key;
			}
		}
	}	
	return false;
}


$devices_list = $db->Query('SELECT `id`, `ip`, `ping` FROM `devices` WHERE `ip` != \'\'');

$multi = new MultiTask;
$multi->maxThread = get_default_value::get('option.thread.ping');

$pingRes = ''; //данные по базе
$ping_ip = '';

while ($device = $devices_list->get_next_row_object() ){
	$multi->addcommand('fping -r 6 '.$device->ip, $device->id );
	$pingRes[$device->id] = ($device->ping == 1);
	$ping_ip[$device->id] = $device->ip;
}

$multi->execute();

$ping_results = array(); // полученные значения

foreach ($multi->results as $val) {
	
	$ping_results[$val['id']] = ($val['value'] == 0);
	 
}

$multi_arp = new MultiTask;
$multi_arp->maxThread = get_default_value::get('option.thread.ping');

if (is_array($ping_results))
{
	foreach ($ping_results as $dev_id => $dev_ping)
	{
		if (!$dev_ping )
		{
			if ($ether = getEther($ping_ip[$dev_id]))
			{
				echo $ether.' ip = '.$ping_ip[$dev_id]."\n";
				$multi_arp->addcommand('arping -f -c 6 -I '.$ether.' '.$ping_ip[$dev_id], $dev_id );
			}
		}
	}
}

$multi_arp->execute();
foreach ($multi_arp->results as $val) {
	
	$ping_results[$val['id']] = ($val['value'] == 0);
	 
}

$trigger_devices = array();

$db->Query('UPDATE `devices` SET `availability_total` = `availability_total` + 1');

if (is_array($ping_results))
{
	foreach ($ping_results as $dev_id => $dev_ping)
	{
		if ( $dev_ping)
		{
			$db->Query('UPDATE `devices` SET `availability_active` = `availability_active` + 1 WHERE id = '.$dev_id);
		}
		if ($dev_ping != $pingRes[$dev_id])
		{
			$trigger_devices[$dev_id] = $dev_ping;
			$db->UpdateData('devices', array('ping' => $dev_ping), array('id' => $dev_id ));
			$db->InsertData('devices_ping',array('gid' => $dev_id, 'state' => $dev_ping));
		}
	}
}

if (is_array($trigger_devices))
{
	foreach ($trigger_devices as $dev_id => $ping)
	{
		$triggers = $db->Query('SELECT *  FROM `trigger_devices` WHERE `gid` = '.$dev_id);
		while ($tg_data = $triggers->get_next_row_object())
		{
			$msg = 'Down ';
			if ($ping)
			{
				$msg = 'Up ';
			}
			$tg = new trigger($tg_data->action, $msg.$tg_data->text);
			echo $tg->msg;
			if ($tg_data->single)
			{
				$db->DeleteData('trigger_devices', array('id' => $tg_data->id));
			}
			
		}
	}
}


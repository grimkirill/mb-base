<?php

/*
 * OID 1.3.6.1.2.1.31.1.1.1.3  broadcast
 * 1.3.6.1.2.1.2.2.1.14        errors
 */

$time_start = microtime(true);

include_once dirname(dirname(__FILE__)).'/system/init.php';

device_types::init();
global $device_type_list;
$read_list = array();
foreach ($device_type_list as $key => $item)
{
	if ($item['use_snmp'])
	{
		$read_list[] = $key;
	}
}


$sql = 'SELECT devices_ports.id, devices_ports.port, devices.ip, devices.access_snmp_read  FROM `devices_ports`, devices WHERE devices_ports.gid IN (SELECT `id` FROM `devices` WHERE `ping` = 1 AND type IN ( '.implode(', ',$read_list).' )) AND devices.id = devices_ports.gid';

$devices_list = $db->Query($sql);

$multi = new MultiTask;
$multi->maxThread = get_default_value::get('option.thread.snmp');
$total = 0;
while ($device = $devices_list->get_next_row_object() ){
	$multi->addcommand('snmpwalk -v2c -m \'\' -Ovq  -c'.$device->access_snmp_read.' '.$device->ip.' 1.3.6.1.2.1.31.1.1.1.3.'.$device->port, $device->id );
	$total ++;  
}

$result_data = array();
$multi->execute();

$port_data = $db->Query('SELECT port, val FROM devices_port_broadcast')->get_rows_array_id('port', 'val');
$sql_data = array();
foreach ($multi->results as $val) {
	if ($val['value'] == 0)
	{
		$diff = 0;
		if (isset($port_data[$val['id']]))
		{
			$diff = (int)$val['content'] - $port_data[$val['id']];
			if ($diff < 0)
			{
				$diff = 0;
			}
		}
		$sql_data[] = "('".$val['id']."', '".(int)$val['content']."', '".$diff."')";
	}
}

$db->Query('TRUNCATE TABLE devices_port_broadcast'); 

$db->Query("INSERT INTO `devices_port_broadcast` (port, val, diff) VALUES ".implode(', ', $sql_data));

echo $total."\n";
echo count($port_data)."\n";
echo count($sql_data)."\n";

echo 'total time: '.(microtime(true) - $time_start)."\n";
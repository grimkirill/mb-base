<?php
include_once dirname(dirname(__FILE__)).'/system/init.php';

//echo get_default_value::get('default.snmp.write');
device_types::init();
global $device_type_list;
$read_list = array();
foreach ($device_type_list as $key => $item)
{
	if ($item['snmp_dlink_vlan'])
	{
		$read_list[] = $key;
	}
}

global $vlan_list;
$vlan_list = $db->Query('SELECT id FROM  `devices_vlan_list`')->get_rows_array_id('id', 'id');


$devices_list = $db->Query('SELECT `id`, `ip`, `access_snmp_read`, type FROM `devices` WHERE `ping` = 1 AND type IN ( '.implode(', ',$read_list).' )');

while ($device = $devices_list->get_next_row_object() ){
	check_create_rrd_cpu($device->id);
	$data = read_vlans($device->ip, $device->access_snmp_read);
	//print_r($data);
	if (is_array($data))
	{
		foreach ($data as $vlan => $ports)
		{
			update_vlan($vlan);
			$db_ports = $db->Query('SELECT `port`, `untagged` FROM `devices_vlans` WHERE `gid` = '.$device->id.' AND `vlan` = '.$vlan)->get_rows_array_id('port', 'untagged');
			foreach ($ports as $port_num => $port_untagged)
			{
				if (isset($db_ports[$port_num]))
				{
					if ($db_ports[$port_num] != $port_untagged)
					{
						$db->UpdateData('devices_vlans', array('untagged' => $port_untagged), array('gid' => $device->id, 'vlan' => $vlan, 'port' => $port_num));
						$db->InsertData('devices_vlan_history', array('gid' => $device->id, 'vlan' => $vlan, 'port' => $port_num, 'untagged' => $port_untagged, 'action' => 'update'));
					}
					
					unset($db_ports[$port_num]);
				}
				else
				{
					$db->InsertData('devices_vlans', array('gid' => $device->id, 'vlan' => $vlan, 'port' => $port_num, 'untagged' => $port_untagged));
					$db->InsertData('devices_vlan_history', array('gid' => $device->id, 'vlan' => $vlan, 'port' => $port_num, 'untagged' => $port_untagged, 'action' => 'new'));
				}
			}
			if (is_array($db_ports) > 0)
			{
				foreach ($db_ports as $port_num => $port_untagged)
				{
					$db->DeleteData('devices_vlans', array('gid' => $device->id, 'vlan' => $vlan, 'port' => $port_num));
					$db->InsertData('devices_vlan_history', array('gid' => $device->id, 'vlan' => $vlan, 'port' => $port_num, 'untagged' => $port_untagged, 'action' => 'DELETE'));
				}
			}
		}
	}
	
	
	if ($type = get_type($device->ip, $device->access_snmp_read))
	{
		//echo "type $type\n";
		if ($device->type != $type)
		{
			$db->UpdateData('devices', array('type' => $type), array('id' => $device->id));
			$dev = new device($device->id);
			$dev->update();
			//echo "Wrong type!!!";
		}
	}
	
}


function check_create_rrd_cpu($id)
{
	$rrd_name = dirname(dirname(__FILE__)).'/rrd/dlink_cpu_'.$id.'.rrd';
	if (!file_exists($rrd_name))
	{
		$cmd = '/usr/bin/rrdtool create '.$rrd_name.' -s 300 DS:value:GAUGE:600:0:U RRA:AVERAGE:0.5:1:17280 RRA:MIN:0.5:1440:1 RRA:MAX:0.5:1440:1';
		exec($cmd);
		exec('chmod 666 '.$rrd_name);
	}
	
}

function get_type($ip, $snmp_read)
{
	if ($name = @snmpget($ip, $snmp_read, "SNMPv2-MIB::sysDescr.0"))
	{
		if ($name == 'STRING: DES-3526 Fast-Ethernet Switch') {return 15;}
		if ($name == 'STRING: D-Link DES-3028 Fast Ethernet Switch') {return 16;}
		if ($name == 'STRING: D-Link DES-3200-28 Fast Ethernet Switch') {return 17;}
	}
	return FALSE;
}


function read_vlans($ip, $snmp_read)
{
	if ($a = @snmpwalkoid($ip, $snmp_read, "1.3.6.1.2.1.17.7.1.4.3.1") )
	{ 
		$vlan_array = array();
	
		foreach ($a as $key => $value) {
			
			if (preg_match('/17.7.1.4.3.1.4.(\d+)$/', $key, $vals)) //untagged
			{
				$vlan = $vals[1];
				$hex = str_ireplace('Hex-STRING:', '', $value);
				$hex = str_replace(' ', '', $hex);
				$ports = get_dlink_vlan_portlist($hex);
				$vlan_array[$vlan][1] = $ports;	
			}
			
			if (preg_match('/17.7.1.4.3.1.2.(\d+)$/', $key, $vals)) //tagged
			{
				$vlan = $vals[1];
				$hex = str_ireplace('Hex-STRING:', '', $value);
				$hex = str_replace(' ', '', $hex);
				$ports = get_dlink_vlan_portlist($hex);
				$vlan_array[$vlan][0] = $ports;
			}
		}
		
		foreach ($vlan_array as $vlan => $ports)
		{
			$port_list_untag = array();
			foreach ($ports[1] as $port)
			{
				$port_list_untag[$port] = TRUE;
			}
			foreach ($ports[0] as $port)
			{
				if (!isset($port_list_untag[$port]))
				{
					$port_list_untag[$port] = FALSE;
				}
			}
			
			$vlan_array[$vlan] = $port_list_untag;
		}
		
		return $vlan_array;
	}
}


function get_dlink_vlan_portlist($hex)
{
	$ports = array();
	for ($i = 0; $i < strlen($hex); $i++)
	{
		$hex_data = hexdec($hex[$i]);
		if ($hex_data & 8) { $ports[] = $i*4 + 1; }	//1 порт
		if ($hex_data & 4) { $ports[] = $i*4 + 2; }	//2 порт
		if ($hex_data & 2) { $ports[] = $i*4 + 3; }	//3 порт	
		if ($hex_data & 1) { $ports[] = $i*4 + 4; } //4 порт
	}
	return $ports;
}

function update_vlan($vid)
{
	global $db;
	global $vlan_list;
	if (!isset($vlan_list[$vid]))
	{
		$vlan_list[$vid] = $vid;
		$db->InsertData('devices_vlan_list', array('id' => $vid));
	}
}
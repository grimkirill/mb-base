<?php

/*
 * snmpwalk -v2c -Ovq  -c private 172.30.1.100 IF-MIB::ifHCInOctets.1
 * snmpwalk -v2c -Ovq  -c private 172.30.1.100 IF-MIB::ifHCOutOctets.1
 * 
 * 1 find alive device
 * 2 find active port
 * 
 */
$time_start = microtime(true);

include_once dirname(dirname(__FILE__)).'/system/init.php';

device_types::init();
global $device_type_list;
$read_list = array();
foreach ($device_type_list as $key => $item)
{
	if ($item['use_snmp'])
	{
		$read_list[] = $key;
	}
}


$sql = 'SELECT devices_ports.id, devices_ports.port, devices.ip, devices.access_snmp_read  FROM `devices_ports`, devices WHERE devices_ports.gid IN (SELECT `id` FROM `devices` WHERE `ping` = 1 AND type IN ( '.implode(', ',$read_list).' )) AND devices_ports.active = 1 AND devices.id = devices_ports.gid';

$devices_list = $db->Query($sql);

$multi = new MultiTask;
$multi->maxThread = get_default_value::get('option.thread.snmp');

while ($device = $devices_list->get_next_row_object() ){
	$multi->addcommand('snmpwalk -v2c -m \'\' -Ovq  -c'.$device->access_snmp_read.' '.$device->ip.' IF-MIB::ifHCInOctets.'.$device->port, 'i'.$device->id );
	$multi->addcommand('snmpwalk -v2c -m \'\' -Ovq  -c'.$device->access_snmp_read.' '.$device->ip.' IF-MIB::ifHCOutOctets.'.$device->port, 'o'.$device->id );  
}

$result_data = array();
$multi->execute();
foreach ($multi->results as $val) {
	if ($val['value'] == 0)
	{
		$dev_id = (int)substr($val['id'], 1, strlen($val['id'])-1);
		
		$value = (int)$val['content'];
		if (substr($val['id'], 0, 1) == 'i')
		{
			$result_data[$dev_id][0] = $value;
		}
		else
		{
			$result_data[$dev_id][1] = $value;
		}
	}
}

$rrd_sub_name = dirname(dirname(__FILE__)).'/rrd/ports/port_';

foreach ($result_data as $id => $data)
{
	if ((isset($data[0])) AND (isset($data[1])))
	{
		$call = 'rrdtool update "'.$rrd_sub_name.$id.'.rrd" N:'.$data[0].':'.$data[1];
		//echo $call."\n"; 
		echo exec($call);
	}
}



//print_r($result_data);
echo 'total time: '.(microtime(true) - $time_start)."\n";
echo count($result_data);

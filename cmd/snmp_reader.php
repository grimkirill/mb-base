<?php
include_once dirname(dirname(__FILE__)).'/system/init.php';


device_types::init();
global $device_type_list;
$read_list = array();
foreach ($device_type_list as $key => $item)
{
	if ($item['use_snmp'])
	{
		$read_list[] = $key;
	}
}



$devices_list = $db->Query('SELECT `id`, `ip`, `access_snmp_read`, `type` FROM `devices` WHERE `ping` = 1 AND type IN ( '.implode(', ',$read_list).' )');

$multi = new MultiTask;
$multi->maxThread = get_default_value::get('option.thread.snmp');

$pingRes = '';
while ($device = $devices_list->get_next_row_object() ){
	$oid_admin_status = '.1.3.6.1.2.1.2.2.1.7';
	$oid_oper_status = '.1.3.6.1.2.1.2.2.1.8';

	// !!!!!!!!!!!! FOR SNR_S2940 !!!!!!!!!!!!!!!!!!!
	if($device->type == '6' || $device->type == '5') {
		$oid_admin_status = '.1.3.6.1.4.1.40418.7.100.3.2.1.12';
	}

	$multi->addcommand('snmpwalk -v1 -m \'\' -c '.$device->access_snmp_read.' '.$device->ip.' '.$oid_admin_status, 'a'.$device->id );  // admin status
	$multi->addcommand('snmpwalk -v1 -m \'\' -c '.$device->access_snmp_read.' '.$device->ip.' '.$oid_oper_status, 'o'.$device->id );  // oper status
	
//	$pingRes[$device->id] = $device->ping;
}

$multi->execute();

foreach ($multi->results as $val) {
	$dev_id = substr($val['id'], 1, strlen($val['id'])-1);
	$dev_oper = substr($val['id'], 0, 1);
	$dev_device = new device($dev_id);
	//echo $val['content'];
	$rows = explode("\n", $val['content']);
	if (count($rows) >= $dev_device->get_port_count()){
		$states = array();
		for ($i = 0; $i < $dev_device->get_port_count(); $i++){
			if ( preg_match('/INTEGER: .*(\d{1})/', $rows[$i], $results))
			{
				$states[$i+1] = ((int)$results[1] == 1) ? 1 : 0;
			}
		}
		
		if ($dev_oper == 'a')
		{
			//admin state
			$port_list = $db->Query('SELECT port, admin FROM devices_ports WHERE gid = '.$dev_id)->get_rows_array_id('port', 'admin');
			foreach ($port_list as $key => $value)
			{
				if (isset($states[$key]) &&  $states[$key] != $value)
				{
					$db->UpdateData('devices_ports', array('admin' => $states[$key]), array('gid' => $dev_id, 'port' => $key));
					$db->Query('INSERT INTO `devices_port_admin` ( `gid` , `state` ) VALUES ( (SELECT `id` FROM  `devices_ports` WHERE  `gid` ='.$dev_id.' AND  `port` ='.$key.'), '.$states[$key].')');
				}
			}
		}
		
		if ($dev_oper == 'o')
		{
			//active state
			$port_list = $db->Query('SELECT port, active FROM devices_ports WHERE gid = '.$dev_id)->get_rows_array_id('port', 'active');
			foreach ($port_list as $key => $value)
			{
				if (isset($states[$key]) && $states[$key] != $value)
				{
					$db->UpdateData('devices_ports', array('active' => $states[$key]), array('gid' => $dev_id, 'port' => $key));
					$db->Query('INSERT INTO `devices_port_active` ( `gid` , `state` ) VALUES ( (SELECT `id` FROM  `devices_ports` WHERE  `gid` ='.$dev_id.' AND  `port` ='.$key.'), '.$states[$key].')');
				}
			}
		}
		
	}
	
}

/*
 * Create list of Dlink
 */

$read_list = '';
foreach ($device_type_list as $key => $item)
{
	if (isset($item['snmp_dlink_vlan']))
	{
		if ($key != 23)
		$read_list[] = $key;
	}
}


/*
 * Load MAC addr from dev 
 */

$devices_list = '';
$devices_list = $db->Query('SELECT `id`, `ip`, `access_snmp_read` FROM `devices` WHERE `ping` = 1 AND type IN ( '.implode(', ',$read_list).' )');
$multi = '';
$multi = new MultiTask;
$multi->maxThread = get_default_value::get('option.thread.snmp');

while ($device = $devices_list->get_next_row_object() ){
	$multi->addcommand('snmpwalk -v1 -m \'\' -c '.$device->access_snmp_read.' '.$device->ip.' 1.3.6.1.2.1.17.7.1.2.2.1.2', $device->id ); 
}

$multi->execute();

foreach ($multi->results as $val) {
	if ($val['value'] == 0) 
	{
	//	echo $val['id']."\n\n";
		$device_data = $db->Query('SELECT `id`, `port_in` FROM `devices` WHERE id = '.$val['id'])->get_next_row_object();
	//	echo $val['content'];
		$rows = explode("\n", $val['content']);
		if (is_array($rows)) foreach ($rows as $item)
		{
			if (preg_match('/17.7.1.2.2.1.2.\d+.(\d{1,3}).(\d{1,3}).(\d{1,3}).(\d{1,3}).(\d{1,3}).(\d{1,3}) = INTEGER: (\d{1,2})/', $item, $res_array))
			{
				//print_r($res_array);
				$port = (int)$res_array[7];
				$mac = '';
				for ($i = 1; $i <= 6; $i++)
				{
					$res = dechex($res_array[$i]);
					if (strlen($res) == 1)
					{
						$mac .= '0';
					}
					$mac .= $res;
				}
				$mac = strtoupper($mac);
				//if($device_data->id == '91' && $port == '22') {
				//	continue;
				//}
				
				if (($port != 0) && ($port != $device_data->port_in))
				{
					//echo  $mac.' [ '.$port.' ]'."\n";
					if($device_data->id == '98' && $port == '4') {
						;
					} else if($port->port == '26' && $port->gid == '172')  {
						;
					} else {
						$sql = "INSERT INTO `devices_port_mac` (`gid`, `mac`) VALUES ( (SELECT id  FROM `devices_ports` WHERE `gid` = $device_data->id AND `port` = $port), '$mac');";
						$db->SimpleQuery($sql);
					}
				}
			}
		}
	}
	else
	{
		echo 'ERROR: '.$val['id']."\n";
	}
}


/*
 * Load Cpu from Dev
 * CPU LOAD snmpwalk -v2c -Ovq  -c private 172.30.1.100 1.3.6.1.4.1.171.12.1.1.6.3.0
 */
$read_list[] = 23;
$devices_list = '';
$devices_list = $db->Query('SELECT `id`, `ip`, `access_snmp_read` FROM `devices` WHERE `ping` = 1 AND type IN ( '.implode(', ',$read_list).' )');
$multi = '';
$multi = new MultiTask;
$multi->maxThread = get_default_value::get('option.thread.snmp');

while ($device = $devices_list->get_next_row_object() ){
	$multi->addcommand('snmpwalk -Ovq -m \'\' -v1 -c '.$device->access_snmp_read.' '.$device->ip.' 1.3.6.1.4.1.171.12.1.1.6.3.0', $device->id ); 
}
$multi->execute();


$rrd_sub_name = dirname(dirname(__FILE__)).'/rrd/dlink_cpu_';
foreach ($multi->results as $val) {
	if ($val['value'] == 0) 
	{
		$value = (int)$val['content'];
		$dev_id = (int)$val['id'];
		exec('rrdtool update "'.$rrd_sub_name.$dev_id.'.rrd" N:'.$value.'');
		
	}
}

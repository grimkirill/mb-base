<?php
/**
 * Created by JetBrains PhpStorm.
 * User: k.Skatov
 * Date: 03.07.12
 * Time: 16:47
 * To change this template use File | Settings | File Templates.
 */

include_once dirname(dirname(__FILE__)).'/system/init.php';


$rrd_name = dirname(dirname(__FILE__)).'/rrd/traff_all.rrd';
//if (!file_exists($rrd_name))
//{
//    $cmd = '/usr/bin/rrdtool create ' . $rrd_name . ' -s 300 DS:input:COUNTER:600:0:1000000000 DS:output:COUNTER:600:0:1000000000 RRA:AVERAGE:0.5:1:600 RRA:AVERAGE:0.5:6:700 RRA:AVERAGE:0.5:24:775 RRA:AVERAGE:0.5:288:797 RRA:MAX:0.5:1:600 RRA:MAX:0.5:6:700 RRA:MAX:0.5:24:775 RRA:MAX:0.5:288:797';
//    exec($cmd);
//    exec('chmod 666 '.$rrd_name);
//}

$devices_list = $db->Query('SELECT `ip`, `port`, `snmp_read`  FROM `graph_traff` WHERE `block` = 0');
$total_in = 0;
$total_out = 0;
while ($device = $devices_list->get_next_row_object() ) {
    $str = snmp2_get($device->ip, $device->snmp_read, 'IF-MIB::ifHCInOctets.' . $device->port, 3000, 2);
    if (preg_match('/: (\d*)$/', $str, $match)) {
        $total_in += $match[1];
    }

    $str = snmp2_get($device->ip, $device->snmp_read, 'IF-MIB::ifHCOutOctets.' . $device->port, 3000, 2);
    if (preg_match('/: (\d*)$/', $str, $match)) {
        $total_out += $match[1];
    }
}

$call = 'rrdtool update "' . $rrd_name . '" N:' . $total_in . ':' . $total_out;
exec($call);
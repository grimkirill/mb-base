#!/usr/bin/php -q
<?php

$stdin = fopen('php://stdin', 'r');

$trap_arr = array();

$i=0;
while ( $trap_line = trim(fgets($stdin,4096)) ) {

    $trap_key   = ereg_replace("([0-9\.]*)\ (.*)","\\1", $trap_line);
    $trap_value = ereg_replace("([0-9\.]*)\ (.*)","\\2", $trap_line);

    $trap_arr[$trap_key] = $trap_value;
    //syslog(LOG_INFO, "$i ==> $trap_key == $trap_value");

    $i++;
    if ($i == 10) {
        // prevent loop forever
        die();
    }
}

include_once dirname(dirname(__FILE__)).'/system/init.php';

$dev_ip = $trap_arr[key($trap_arr)];
unset($trap_arr[key($trap_arr)]);
unset($trap_arr[key($trap_arr)]);
foreach ($trap_arr as $item)
{
	if (preg_match('/01 ([0-9A-F]{2} [0-9A-F]{2} [0-9A-F]{2} [0-9A-F]{2} [0-9A-F]{2} [0-9A-F]{2}) [0-9A-F]{2} ([0-9A-F]{2}) [0-9A-F]{2} /', $item, $values))
	{
		// mac notification add 
		$port = hexdec($values[2]);
		$mac = strtoupper(str_replace(' ', '',$values[1]));
		$db->SimpleQuery("INSERT INTO `devices_port_mac` (`gid`, `mac`) VALUES ( (SELECT id  FROM `devices_ports` WHERE `gid` = (SELECT `id` FROM `devices` WHERE `ip` = '$dev_ip') AND `port` = $port), '$mac');");
		//$db->InsertData('log',array('text' => $dev_ip.' ['.$port.'] '.$mac ));
		return;
	}
	elseif (preg_match('/02 ([0-9A-F]{2} [0-9A-F]{2} [0-9A-F]{2} [0-9A-F]{2} [0-9A-F]{2} [0-9A-F]{2}) [0-9A-F]{2} ([0-9A-F]{2}) [0-9A-F]{2} /', $item, $values))
	{
		// mac notification DELETE 
		$port = hexdec($values[2]);
		$mac = strtoupper(str_replace(' ', '',$values[1]));
		//$db->InsertData('log',array('text' => $dev_ip.' ['.$port.'] '.$mac ));
		return;
	}
	elseif ($item == 'SNMPv2-MIB::snmpTrapOIDIF-MIB::linkUp')
	{
		// port UP 
		
		return;
	}
	elseif ($item == 'SNMPv2-MIB::snmpTrapOIDIF-MIB::linkDown')
	{
		// port DOWN 
		
		return;
	}
		
}

file_put_contents('/home/kirill/snmp.log', print_r($trap_arr, true), FILE_APPEND);


$db->InsertData('log',array('text' => $dev_ip.' = '.print_r($trap_arr, true) ));
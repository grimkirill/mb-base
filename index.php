<?php
//$time_start = microtime(true);
//ini_set('session.gc_maxlifetime', 90*60);
include_once 'system/init.php';

$webpath = str_replace("//", "/", dirname($_SERVER['SCRIPT_NAME']).'/');
 
$controller = substr($_SERVER['REQUEST_URI'], strlen($webpath));
if (isset($_SERVER['REDIRECT_URL']))
{
	$controller = substr($_SERVER['REDIRECT_URL'], strlen($webpath));
}

define('WEBPATH', $webpath);

include_once BASEPATH.'system/user.php';

$auth_user = new User_Auth();
if (!$auth_user->isLogin())
{
	$auth_user->Show_Login_Page();
	exit(0);
}

if ($controller == 'logout')
{
	$auth_user->logout();
	header('Location: '.WEBPATH);
	exit(0);
}

if ($controller == '')
{
	$controller = $conf_global['default_route'];
}

$request_data = '';
if (count($_REQUEST)) {
    if (count($_REQUEST)) {
        foreach ($_REQUEST AS $key => $val) {
            $request_data .= '<p>' . $key . '=' . $val .'</p>';
        }
    }
}

$db->InsertData('access_log', array(
    'user_id' => $auth_user->id,
    'uri'     => $controller,
    'request' => $request_data
));
include_once BASEPATH.'system/controller.php';

//echo  microtime(true) - $time_start;

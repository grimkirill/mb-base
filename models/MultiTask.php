<?php

class MultiTask{
	private $streams=array();
    private $handles=array();
	private $all_pipes = array();
	public $commandlist = array();
	public $commandlistId = array();
	public $results = array();
	public $timeout = 60;
	public $maxThread = 200;
	private $position = 0;
	
	function addcommand($command, $id = ''){
		$this->commandlist[] = $command;
		$this->commandlistId[] = $id;
	}
	
	private function executeone($command){
        $descriptorspec=array(
        	0 => array("pipe", "r"),
            1 => array("pipe", "w"),
            2 => array("file", "/tmp/error.txt", "w")
        );
        $this->handles[$this->position]=proc_open($command, $descriptorspec, $pipes);
        $this->streams[$this->position]=$pipes[1];
        $this->all_pipes[$this->position]=$pipes;
        $this->position++;
	}
	
	
	private function executeNext(){
		if ($this->position < sizeof($this->commandlist)){
			$this->executeone($this->commandlist[$this->position]);
		}
	}
	
	function execute(){
		
		for ($i = 0; $i < min(array($this->maxThread, sizeof($this->commandlist))); $i++ ){
			$this->executeNext();
		}
				
		while (count($this->streams)) { 
              $read=$this->streams; 
              stream_select($read, $w=null, $e=null, $this->timeout); 
              foreach ($read as $r) { 
                  $id=array_search($r, $this->streams); 
				  $resconetent = stream_get_contents($this->all_pipes[$id][1]);
				  $return_value = '';
                  if (feof($r)) {
                      fclose($this->all_pipes[$id][0]);
                      fclose($this->all_pipes[$id][1]);
                      $return_value=proc_close($this->handles[$id]);
                      unset($this->streams[$id]); 
                      $this->executeNext();
                  }				  
				  $this->results[$id] = array('value'=>$return_value, 'content'=>$resconetent, 'id'=> $this->commandlistId[$id]);
              } 
        }
	}
	
}

?>
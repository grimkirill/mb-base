<?php

class device {
	public $data;
	public $id;
	/**
	 * @var device_driver
	 */
	public $driver;
	
	function __construct($id){
		global $db;
		if ($this->data = $db->Query('SELECT *, (availability_active / availability_total) as availability FROM devices WHERE id = '.$db->EscapeValue($id))->get_next_row_object())
		{
			$this->id = $this->data->id;
			device_types::init();
			
			$driver_name = device_types::get_driver($this->data->type);
			$this->driver = new $driver_name($this->data->ip, $this->data->access_username, $this->data->access_password,  $this->data->access_snmp_read,  $this->data->access_snmp_write, $this->data->id);
		}
		else
		{
			exit;
		}
	}
	
	function get_availability()
	{
		return $this->data->availability;
	}
	
	function list_function_device()
	{
		return $this->driver->list_function_device();
	}
	
	function get_port_count()
	{
		return device_types::get_port_count($this->data->type);
	}
	
	function get_use_snmp()
	{
		return device_types::get_use_snmp($this->data->type);
	}
	
	function get_dev_driver()
	{
		return device_types::get_driver($this->data->type);
	}
	
	
	function get_dev_dlink()
	{
		return device_types::get_dlink($this->data->type);
	}
	
	function get_port_admin()
	{
		return device_types::get_port_admin($this->data->type);
	}
	
	function get_port_active()
	{
		return device_types::get_port_active($this->data->type);
	}
	
	function get_address()
	{
		return view_form_select_home::get_home_print($this->data->address_home).', п '.$this->data->address_dorway;
	}
	
	function get_address_link($port = '')
	{
		return View_Element::Link($this->get_address(), 'network/looking/show/'.$this->id.'/'.$port);
	}
	
	function get_diff_address($home_compare)
	{
		return view_form_select_home::get_diff_home_print($this->data->address_home, $home_compare).', п '.$this->data->address_dorway;
	}
	
	function is_ping()
	{
		return $this->data->ping;
	}
	
	function get_ip_link()
	{
		if ($this->data->ip)
		{
			return '<a href="http://'.$this->data->ip.'" target="blank">'.$this->data->ip.'</a>';
		}
		else
		{
			return '';
		}
	}
	
	function get_address_disposition()
	{
		return $this->data->address_disposition;
	}
	
	function get_type_name()
	{
		return device_types::get_type_name($this->data->type);
	}	
	
	function update()
	{
		global $db;
		$db->Query('UPDATE `devices` SET `config` = NULL WHERE `id` = '.$this->id);
		
		//необходимо обновить все значения количества портов 
		$port_list = $db->Query('SELECT * FROM devices_ports WHERE gid = '.$this->id);
		if ($port_list->get_rows_count() != $this->get_port_count())
		{
			if ($port_list->get_rows_count() > $this->get_port_count())
			{
				$db->DeleteData('devices_ports','gid = '.$this->id.' AND port > '.$this->get_port_count());
			}
			else
			{
				for ($i = ($port_list->get_rows_count() + 1) ; $i <= $this->get_port_count(); $i++)
				{
					$db->InsertData('devices_ports', array('gid' => $this->id, 'port' => $i));
					create_ports_rrd($db->GetInsertId());
				}
			}			
		}
		
	}
	
	
	function _set_port_admin($port, $admin)
	{
		global $db;
		$db->UpdateData('devices_ports', array('admin' => $admin), array('gid' => $this->id, 'port' => $port));
		if ($db->GetAffectedRows() > 0)
		{
			$db->Query('INSERT INTO `devices_port_admin` ( `gid` , `state` ) VALUES ( (SELECT `id` FROM  `devices_ports` WHERE  `gid` ='.$this->id.' AND  `port` ='.$port.'), '.$admin.')');
		}
	}
	
	
	function _set_port_active($port, $active)
	{
		global $db;
		
		$db->UpdateData('devices_ports', array('active' => $active), array('gid' => $this->id, 'port' => $port));
		if ($db->GetAffectedRows() > 0)
		{
			$db->Query('INSERT INTO `devices_port_active` ( `gid` , `state` ) VALUES ( (SELECT `id` FROM  `devices_ports` WHERE  `gid` ='.$this->id.' AND  `port` ='.$port.'), '.$active.')');
		}
	}
	
	function read_port_data()
	{
		global $db;
		
		if ($this->get_port_admin())
		{
			if ($this->get_use_snmp() )
			{	
				$oid_admin_status = '.1.3.6.1.2.1.2.2.1.7';
				$oid_oper_status = '.1.3.6.1.2.1.2.2.1.8';
				// !!!!!!!!!!!!!!!!! FOR SNR_S2940 !!!!!!!!!!!!!!!!!!!!!!
				if ($this->get_dev_driver() == 'device_driver_snr_s2940_8g' ) {
					$oid_admin_status = '.1.3.6.1.4.1.40418.7.100.3.2.1.12';
				}
				$data_admin = shell_exec('snmpwalk -v1 -Ovq -c '.$this->data->access_snmp_read.' '.$this->data->ip.' '.$oid_admin_status);
				$data_admin = str_replace('up', 1, $data_admin);
				$data_admin = str_replace('down', 2, $data_admin);
				$array_admin = explode("\n", $data_admin);
				
				$data_oper  = shell_exec('snmpwalk -v1 -Ovq -c '.$this->data->access_snmp_read.' '.$this->data->ip.' '.$oid_oper_status);
				$data_oper = str_replace('up', 1, $data_oper);
				$data_oper = str_replace('down', 2, $data_oper);
				$array_oper  = explode("\n", $data_oper);
				
				$port_list = $db->Query('SELECT port, admin, active FROM devices_ports WHERE gid = '.$this->id);
				if (is_array($array_admin) && is_array($array_oper) )
				while ($port = $port_list->get_next_row_object())
				{
					$this->_set_port_admin($port->port, $array_admin[$port->port-1] == 1? 1: 0);
					$this->_set_port_active($port->port, $array_oper[$port->port-1] == 1? 1: 0);
				}
			} // END SNMP USE

			if ($this->get_dev_driver() == 'device_driver_eten' )
			{
				shell_exec('php '.BASEPATH.'cmd/eten_read_device.php '.$this->id);
			}
		}
		
	}
	
	function resetcounter_ping()
	{
		global $db;
		$db->Query('UPDATE devices SET availability_active = 1, availability_total = 1, ping = 0 WHERE id = '.$this->id);
		$db->DeleteData('devices_ping', array('gid' => $this->id));
	}
	
	function resetcounter_ports()
	{
		global $db;
		$res = $db->Query('SELECT *  FROM `devices_ports` WHERE `gid` = '.$this->id);
		while ($port = $res->get_next_row_object())
		{
			$this->resetcounter_port($port->id);
			//create_ports_rrd($port->id);
		}
	}
	
	function resetcounter_port($port)
	{
		global $db;

		$db->UpdateData('devices_ports', array('admin' => 1, 'active' => 0), array('id' => $port));
		$db->DeleteData('devices_port_active', array('gid' => $port));
		$db->DeleteData('devices_port_admin', array('gid' => $port));
		$db->DeleteData('devices_port_mac', array('gid' => $port));
		
	}
	
	function resetmac_port($port)
	{
		global $db;
		$db->DeleteData('devices_port_mac', array('gid' => $port));
	}
	
	
	function get_list_commands()
	{
		global $db;
		return $db->Query('SELECT *  FROM `devices_commands` WHERE `gid` = '.$this->data->type )->get_rows_array_id('id', 'name'); 
	}
	
	
	function set_config($command)
	{
		$ret = '';
		$cmd_list = explode("\n", $command);
		$out = array();
		foreach ($cmd_list as $cmd)
		{
			$ip_list = explode('.', $this->data->ip);
			$cmd = str_replace('{ip1}', $ip_list[0], $cmd);
			$cmd = str_replace('{ip2}', $ip_list[1], $cmd);
			$cmd = str_replace('{ip3}', $ip_list[2], $cmd);
			$cmd = str_replace('{ip4}', $ip_list[3], $cmd);
			
			if ($this->data->port_in != 0)
			{
				$cmd = str_replace('{port_in}', $this->data->port_in, $cmd);
			}
			
			if (strpos($cmd, '{port_not_in}'))
			{
				if ($this->data->port_in != 0)
				{
					for ($i = 1; $i <= $this->get_port_count() ; $i++)
					{
						if ($i != $this->data->port_in)
						{
							$out[] = str_replace('{port_not_in}', $i, $cmd);
						}
					}
				}
			}
			else
			{
				$out[] = $cmd;
			}
		}
		
		if (is_array($out))
		{
			foreach ($out as $cmd)
			{
				$ret .= $this->driver->set_config($cmd)."\n---------------------------------\n";
			}
		}
		return $ret;
	}
	
	function call_command($id)
	{
		global $db;
		if ($data = $db->Query('SELECT *  FROM `devices_commands` WHERE `gid` = '.$this->data->type.' AND id = '.$id )->get_next_row_object())
		{
			return $this->set_config($data->command);
		}
	}
	
}


function create_ports_rrd($id)
{
	$rrd_sub_name = dirname(dirname(__FILE__)).'/rrd/ports/port_';
	$cmd = '/usr/bin/rrdtool create '.$rrd_sub_name.$id.'.rrd -s 300 DS:input:COUNTER:600:0:1000000000 DS:output:COUNTER:600:0:1000000000 RRA:AVERAGE:0.5:1:600 RRA:AVERAGE:0.5:6:700 RRA:AVERAGE:0.5:24:775 RRA:AVERAGE:0.5:288:797 RRA:MAX:0.5:1:600 RRA:MAX:0.5:6:700 RRA:MAX:0.5:24:775 RRA:MAX:0.5:288:797';
	exec($cmd);
	chmod($rrd_sub_name.$id.'.rrd', 0666);
}

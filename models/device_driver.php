<?php

class device_driver {
	
	public $ip;
	public $username;
	public $password;
	public $snmp_read;
	public $snmp_write;
	
	
	function block_port($port, $block = FALSE)
	{
		return FALSE;
	}
	
	function __construct($ip, $username, $password, $snmp_read, $snmp_write, $id_dev=0)
	{
		$this->ip = $ip;
		$this->username = $username;
		$this->password = $password;
		$this->snmp_read = $snmp_read;
		$this->snmp_write = $snmp_write;
		$this->id = $id_dev;
	}
	
	/**
	 * assoc array of available function of device
	 * @return multitype:
	 */
	function list_function_device()
	{
		return array();
	}
	
	/**
	 * assoc array of available function of port
	 * 
	 * @return multitype:
	 */
	function list_function_port()
	{
		return array();
	}
	
}

<?php
 class device_driver_dlink_3028 extends device_driver {
 	
 	/**
 	 * @var Telnet
 	 */
 	public $telnet = FALSE;
 	
 	
 	function save_config() {
 		if ($this->init_telnet())
 		{
 			return $this->telnet->exec('save config');
 		}
 	}
 	
 	function show_firmware()
 	{
 		try {
	 		if ($this->init_telnet())
	 		{
	 			return $this->telnet->exec('show switch');
	 		}
 		} catch (Exception $e) {
 			
 		}
 		
 	}
 	
 	
 	function get_mac()
 	{
 		try {
	 		if ($this->init_telnet())
	 		{
	 			$data = $this->telnet->exec('show switch');
	 			$data_array = explode("\n", $data);
	 			//print_r($data_array);
	 			foreach ($data_array as $item)
	 			{
	 				if (preg_match("/MAC Address .* (.{2})-(.{2})-(.{2})-(.{2})-(.{2})-(.{2})/", $item, $out))
	 				{
	 					$mac = strtoupper($out[1].$out[2].$out[3].$out[4].$out[5].$out[6]);
	 					return $mac;
	 				}
	 			}
	 		}
 		} catch (Exception $e) {
 			
 		}
 	}
 	
 	function set_config($cmd)
 	{
 		if ($this->init_telnet())
 		{
 			$this->telnet->exec('dis cli');
 			return $this->telnet->exec($cmd);
 		}
 	}
 	
 	function get_config()
 	{
 		if ($this->init_telnet())
 		{
 			$this->telnet->exec('dis cli');
 			$this->telnet->setPrompt('3028:4#');
 			return $this->telnet->exec('show config current_config');
 		}
 	}
 	
 	function create_snmp()
 	{
 		if ($this->init_telnet())
 		{
 			$ret = $this->telnet->exec('create snmp host 172.30.20.254 v2c private')."\n";
 			$ret .= $this->telnet->exec('enable mac_notification')."\n";
 			$ret .= $this->telnet->exec('config mac_notification ports 1-24 enable');
 			return $ret; 
 		}
 		 
 	}
 	
 	function reboot()
	{
		@snmp2_set ( $this->ip , $this->snmp_write , '1.3.6.1.4.1.171.12.1.2.3.0', 'i' , 3);
	}
 	
	function show_error_port($port)
	{
		if ($this->init_telnet())
 		{
 			return $this->telnet->exec('show ports '.$port);
 		}
	}

 	function block_port($port, $block = FALSE)
	{
		if ($block)
		{
			$block = 2;
		}
		else
		{
			$block = 1;
		}
		return snmp2_set ( $this->ip , $this->snmp_write , '.1.3.6.1.2.1.2.2.1.7.'.$port , 'i' , $block);
	}
 	
	function port_mac_list($port)
	{
		if ($this->init_telnet())
 		{
                        if($this->id == '536' && $port == '23') {
$fake = <<<EOD
show fdb port 23

Command: show fdb port 23


 VID  VLAN Name                        MAC Address       Port  Type    Status

 ---- -------------------------------- ----------------- ----- ------- -------


Total Entries: 0
EOD;
                                return $fake;
                        }

 			return $this->telnet->exec('show fdb port '.$port);
 		}
	}
	
	function port_vlan($port)
	{
		if ($this->init_telnet())
 		{
 			return $this->telnet->exec('show vlan ports '.$port);
 		}
	}
	
	function port_diag($port)
	{
		if ($this->init_telnet())
 		{
			if($this->id == '536' && $port == '23') {
$fake = <<<EOD
cable_diag ports 29

Command: cable_diag ports 29


Perform Cable Diagnostics ...


Port      Type      Link Status    Test Result                 Cable Length (M)

------  ----------  -------------  -------------------------  -----------------

23      100BASE-T   Link Down      Pair 2 Open      at  28M          -

                                   Pair 3 Open      at  29M
EOD;
				return $fake;
			}
 			return $this->telnet->exec('cable_diag ports '.$port);
 		}
	}
 	
    function port_security($port)
	{
		if ($this->init_telnet())
 		{
 			return $this->telnet->exec('clear port_security_entry port '.$port);
 		}
	}
	
 	function init_telnet()
 	{
 		if (!is_object($this->telnet))
 		{
	 		$this->telnet = new Telnet($this->ip);
			$this->telnet->connect();
			$this->telnet->login($this->username, $this->password);
			//$this->telnet->exec('');
			$this->telnet->exec('dis cli');
			return TRUE;
 		}
 		else
 		{
 			return TRUE;
 		}
		
 	} 
 	
 	
 	function set_new_password($password)
 	{
 		if ($this->init_telnet())
 		{
 			$this->telnet->setPrompt(':');
 			$this->telnet->exec('config account '.$this->username);
 			$this->telnet->exec($this->password);
 			$this->telnet->exec($password);
 			$this->telnet->exec($password);
 			$this->telnet->setPrompt();
 			return TRUE;
 		}
 	}
 	
 	function show_ip()
 	{
 		if ($this->init_telnet())
 		{
 			return $this->telnet->exec('show ipif');
 		}
 	}
 	
 	function list_function_device()
	{
		return array( 'save_config' => 'Сохранить настройки', 'reboot' => 'Перезагрузить', 'show_firmware' => 'Версия прошивки');
	}
	
 	function list_function_port()
	{
		return array(
			'port_diag' => 'Диагностика', 
			'port_vlan' => 'Vlan', 
			'port_mac_list' => 'Список MAC адресов',
		    'port_security' => 'Cброс MAC',
			//'show_error_port' => 'Ошибки на порту',
		);
	}
 	
 }

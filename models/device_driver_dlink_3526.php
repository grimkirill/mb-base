<?php
 class device_driver_dlink_3526 extends device_driver {
 	
 	/**
 	 * @var Telnet
 	 */
 	public $telnet = FALSE;
 	
 	
 	function save_config() {
 		if ($this->init_telnet())
 		{
 			return $this->telnet->exec('save');
 		}
 	}
 	
 	function show_firmware()
 	{
 		if ($this->init_telnet())
 		{
 			return $this->telnet->exec('show firmware information');
 		}
 	}
 	
  	function set_config($cmd)
 	{
 		if ($this->init_telnet())
 		{
 			$this->telnet->exec('dis cli');
 			return $this->telnet->exec($cmd);
 		}
 	}
 	
 	function get_config()
 	{
 		if ($this->init_telnet())
 		{
 			$this->telnet->exec('dis cli');
 			$this->telnet->setPrompt(':admin#');
 			return $this->telnet->exec('show config current_config');
 		}
 	}
 	
 	function create_snmp()
 	{
 		if ($this->init_telnet())
 		{
 			$ret = $this->telnet->exec('create snmp host 172.30.1.254 v2c private')."\n";
 			$ret .= $this->telnet->exec('enable mac_notification')."\n";
 			$ret .= $this->telnet->exec('config mac_notification ports 1-24 enable');
 			return $ret; 
 		}	 
 	}
 	
 	function block_port($port, $block = FALSE)
	{
		if ($block)
		{
			$block = 2;
		}
		else
		{
			$block = 1;
		}
		return snmp2_set ( $this->ip , $this->snmp_write , '.1.3.6.1.2.1.2.2.1.7.'.$port , 'i' , $block);
	}
 	
	function reboot()
	{
		@snmp2_set ( $this->ip , $this->snmp_write , '1.3.6.1.4.1.171.12.1.2.3.0', 'i' , 3);
	}
	
	function port_mac_list($port)
	{
		if ($this->init_telnet())
 		{
 			return $this->telnet->exec('show fdb port '.$port);
 		}
	}
	
	function port_vlan($port)
	{
		if ($this->init_telnet())
 		{
 			
 			return $this->telnet->exec('show vlan ports '.$port);
 			
 		}
	}
	
	function port_diag($port)
	{
		if ($this->init_telnet())
 		{
 			return $this->telnet->exec('cable_diag ports '.$port);
 		}
	}
 	
 	function init_telnet()
 	{
 		if (!is_object($this->telnet))
 		{
	 		$this->telnet = new Telnet($this->ip);
			$this->telnet->connect();
			$this->telnet->login($this->username, $this->password);
			//$this->telnet->exec('');
			$this->telnet->exec('dis cli');
			return TRUE;
 		}
 		else
 		{
 			return TRUE;
 		}
		
 	} 
 	
 function set_new_password($password)
 	{
 		if ($this->init_telnet())
 		{
 			$this->telnet->setPrompt(':');
 			$this->telnet->exec('config account '.$this->username);
 			$this->telnet->exec($this->password);
 			$this->telnet->exec($password);
 			$this->telnet->exec($password);
 			$this->telnet->setPrompt();
 			return TRUE;
 		}
 	}
 	
 	function show_ip()
 	{
 		if ($this->init_telnet())
 		{
 			return $this->telnet->exec('show ipif');
 		}
 	}
 	
 	function list_function_device()
	{
		return array( 'save_config' => 'Сохранить настройки', 'reboot' => 'Перезагрузить', 'show_firmware' => 'Версия прошивки');
	}
	
 	function list_function_port()
	{
		return array('port_diag' => 'Диагностика', 'port_vlan' => 'Vlan', 'port_mac_list' => 'Список MAC адресов');
	}
 	
 }

<?php

class device_driver_eten extends device_driver
{
		private $link;
		private $active = false;
		public $error;
		private $coocie = '';
		private $portcount;
		
		public $portsstate;
		public $portactive;
		
		function geturl($url){
			if ($this->isonline()){
				curl_setopt($this->link, CURLOPT_URL, $this->ip.$url);
       		 	curl_setopt($this->link, CURLOPT_POST, 0);
       	 		curl_setopt($this->link, CURLOPT_COOKIE, "Taifatech=".$this->coocie);
       	 		curl_setopt($this->link, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($this->link, CURLOPT_TIMEOUT, 10);
       			$output = curl_exec($this->link);
       			if (curl_errno($this->link) != 0){
					$this->error = curl_error($this->link);
					return false;
				} else {
					return $output;
				}
			} else {
				return false;
			}
		}
		
		function init(){
			if(!extension_loaded('curl')){
				$this->error = "no loaded curl extension";
				return false;
			}
			
			//  login2.htm
			$this->link = curl_init("http://".$this->ip."");
			curl_setopt($this->link, CURLOPT_HEADER, 1);
        	curl_setopt($this->link, CURLOPT_POST, 0);
        	curl_setopt($this->link, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($this->link, CURLOPT_TIMEOUT, 10);
       		$output = curl_exec($this->link);
       		
       		curl_setopt($this->link, CURLOPT_URL, $this->ip."/login2.htm");
			curl_setopt($this->link, CURLOPT_HEADER, 1);
        	curl_setopt($this->link, CURLOPT_POST, 0);
        	curl_setopt($this->link, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($this->link, CURLOPT_TIMEOUT, 10);
       		$output = curl_exec($this->link);
       		
			curl_setopt($this->link, CURLOPT_URL, $this->ip."/tgi/login.tgi?Username=".$this->username."&Password=".$this->password."&=++++++OK++++++&Challenge=M4iEH...&Response=");
			curl_setopt($this->link, CURLOPT_HEADER, 1);
        	curl_setopt($this->link, CURLOPT_POST, 0);
        	curl_setopt($this->link, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($this->link, CURLOPT_TIMEOUT, 10);
       		$output = curl_exec($this->link);
			if (curl_errno($this->link) != 0){
				$this->error = curl_error($this->link);
				return false;
			} else {
				if (preg_match("/Set-Cookie: Taifatech=(.+);/",$output,$resvar)){
					$this->coocie = $resvar[1];
					$this->active = true;
					if ($res = $this->geturl('/Status.htm')){
						if (preg_match("/<b>Number of Ports<\/b><\/td>\n.*<td><b>(\d{1,2})<\/b>/",$res,$resvar)){
							$this->portcount = (int)$resvar[1];
						} else {
							$this->error .= 'wrong switch to decode port count';

							return false;
						}			
					} else {
						return false;
					}					
				} else {
					$this->error = "wrong swicth to decode cookie";
					return false;
				}
			}
       		return true;
		}
		
				
		function readportsstate(){
			if ($this->isonline()) {
				$this->decodeportsstate($this->geturl('/PortSet.htm'));
			}
		}
		
		function decodeportsstate($text){
			for ($i=1;$i <= $this->portcount();$i++){
				if (preg_match('/<td ALIGN="center" NOWRAP>'.$i.'<\/td>\n{1,2}<td ALIGN="center" NOWRAP><img src=\/stop.gif><\/td>/', $text, $resvar))
				{
					$this->portsstate[$i] = 0;
				}
				elseif (preg_match('/<td ALIGN="center" NOWRAP>'.$i.'<\/td>\n{1,2}<td ALIGN="center" NOWRAP>/', $text, $resvar))
				{
					$this->portsstate[$i] = 1;
				}
				else
				{
					$this->error .= 'eroor while port analusus';
				}
				if (preg_match('/<td ALIGN="center" NOWRAP>'.$i.'<\/td>\n{1,2}<td ALIGN="center" NOWRAP><img src=\/green.gif><\/td>/', $text, $resvar))						
				{
					$this->portactive[$i] = 1;
				}
				else
				{
					$this->portactive[$i] = 0;
				}
				
			}
		}
		
		function isonline(){
			if ($this->error)
			{
				return FALSE;
			}
			else
			{
				if ($this->active)
				{
					return TRUE;
				}
				else
				{
					$this->init();
					return $this->active;
				}
			}
			
		}
		
		function portcount(){
			return $this->portcount;
		}
		
		function error(){
			return $this->error;
		}
		
		
		function enableport($portnum,$enable = true) {
			if ($this->isonline()) {
				if (((int)$portnum > 1) and ((int)$portnum <= $this->portcount())){
				
					if ($this->portcount() == 8){
						if ($enable){
							$this->decodeportsstate($this->geturl('/tgi/PortSet.tgi?PortNO=0'.$portnum.'&NWAY=AUTOON&DUPLEX=DUPFULL&Forced_Off=FEnable&Apply=Update'));
						} else {
							$this->decodeportsstate($this->geturl('/tgi/PortSet.tgi?PortNO=0'.$portnum.'&NWAY=AUTOON&DUPLEX=DUPFULL&Forced_Off=FDisable&Apply=Update'));
						}
					
					} elseif ( ($this->portcount() == 16) or ($this->portcount() == 24) ) {
						if ((int)$portnum > 9){
							$portswitch = $portnum;
						} else {
							$portswitch = '0'.$portnum;
						}
						if ($enable){
							$this->decodeportsstate($this->geturl('/tgi/PortSet.tgi?PortNO='.$portswitch.'&NWAY=FOFF1&SPEED=AUTOON&DUPLEX=DUPFULL&PAUSE=PAUSEON&Back=BPON&Apply=Update'));
						} else {
							$this->decodeportsstate($this->geturl('/tgi/PortSet.tgi?PortNO='.$portswitch.'&NWAY=FOFF0&SPEED=AUTOON&DUPLEX=DUPFULL&PAUSE=PAUSEON&Back=BPON&Apply=Update'));
						}
					}
				}
				return true;
			} else {
				return false;
			}
		}


	function set_new_password($password)
	{
		if ($this->isonline()) {
			return $this->geturl('/tgi/setting_pass.tgi?UN='.$this->username.'&PW='.$password.'&PW='.$password.'&Modify=Update');
		}
	}	
		
	function block_port($port, $block = FALSE)
	{
		return $this->enableport($port, !$block);
	}
	
	function port_config()
	{
		$this->readportsstate();
		print_r($this->portsstate);
		print_r($this->portactive);
	}	
		
 	
	
}
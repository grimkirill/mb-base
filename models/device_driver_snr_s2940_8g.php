<?php
    class device_driver_snr_s2940_8g extends device_driver {
        public $telnet = FALSE;
        
        
        function port_diag($port) {
	    if ($this->init_telnet()) {
 		return $this->telnet->exec('virtual-cable-test interface ethernet1/'.$port);
 	    }
        }
        
        
        function port_info($port) {
	    if ($this->init_telnet()) {
 		return $this->telnet->exec('show interface ethernet1/'.$port);
 	    }
        }
        
        
        function clear_mac_address_table($port) {
	    if ($this->init_telnet()) {
 		return $this->telnet->exec('clear mac-address-table dynamic interface ethernet1/'.$port);
 	    }
        }
        
        
        function list_function_device() {
		return array( 'save_config' => 'Сохранить настройки',
                             'reboot' => 'Перезагрузить',
                             'show_firmware' => 'Версия прошивки');
	}
	
        
 	function list_function_port() {
		return array('port_diag' => 'Диагностика',
                             'port_vlan' => 'Vlan',
                             'port_mac_list' => 'Список MAC адресов',
                             'clear_mac_address_table' => 'Очистить табилицу mac-адресов на порту',
                             'port_info' => 'Статистика порта');
	}
        
        
        function block_port($port, $block = FALSE) {
	    if ($block) {
		$block = 2;
	    }
	    else {
		$block = 1;
	    }
	    return snmp2_set ( $this->ip , $this->snmp_write , '.1.3.6.1.2.1.2.2.1.7.'.$port , 'i' , $block);
	}
        

        function port_mac_list($port) {
	    if ($this->init_telnet()) {
 		return $this->telnet->exec('show mac-address-table interface ethernet1/'.$port);
 	    }
	}
        
        
        function port_vlan($port) {
	    if ($this->init_telnet()) {
 			
 		return $this->telnet->exec('show vlan');
 			
 	    }
	}
        
        function init_telnet() {
 	    if (!is_object($this->telnet)) {
	 	$this->telnet = new Telnet($this->ip);
		$this->telnet->connect();
		$this->telnet->login($this->username, $this->password);
		//$this->telnet->exec('');
		$this->telnet->exec('terminal length 0');
		return TRUE;
 	    } else {
 		return TRUE;
 	    }	
 	}
    }

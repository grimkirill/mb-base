<?php
global $db;

global $device_tag_lists;
$device_tag_lists = $db->Query('SELECT * FROM devices_tag_list')->get_rows_array(); 

class device_tags {

	/**
	 * get a list for options
	 * @return array
	 */
	static function get_list()
	{
		global $device_tag_lists;
		$res = '';
		if (is_array($device_tag_lists))
		{
			foreach ($device_tag_lists as $item)
			{
				$res[$item['id']] = $item['name'];
			}
		}
		return $res;
	}
	
	static function get_list_null()
	{
		global $device_tag_lists;
		$res[0] = '--=--';
		if (is_array($device_tag_lists))
		{
			foreach ($device_tag_lists as $item)
			{
				$res[$item['id']] = $item['name'];
			}
		}
		return $res;
	}
	
	static function get_tag_print($id)
	{
		global $device_tag_lists;
		if (is_array($device_tag_lists))
		{
			foreach ($device_tag_lists as $item)
			{
				if ($item['id'] == $id)
				{
					return '<span style="color: '.$item['color'].';">'.$item['name'].'<span>'; 
				}
			}
		}
		return FALSE;
	} 
	
	static function genarate_options($value)
	{
		$out = '';
		if (is_array($value))
		{
			foreach ($value as $key => $name)
			{
				$out .= '<option value="'.$key.'">'.$name.'</option>';
			}
		}
		return $out;
	}
	
	/**
	 * @param int $id device
	 */
	static function get_device_info($id)
	{
		global $db;
		$select_list = self::get_list();		
		$list = $db->Query('SELECT * FROM devices_tags WHERE gid = '.$id);
		$out = '';
		while ($res = $list->get_next_row_object())
		{
			unset($select_list[$res->tag]);
			$out .= self::get_tag_print($res->tag);
			$out .= View_Element::SpanClick('X', '$(this).hide(); device_delete_tag('.$id.','.$res->tag.')').View_Element::Space(2);
		}
		if (count($select_list))
		{
			$out .= '<p id="select_addtag" class="hide">'.'<select id="select_tag"';
			$out .= self::genarate_options($select_list);
			$out .= '</select>';
			$out .= View_Element::Space(1);
			$out .= View_Element::SpanClick('добавить', '$(this).hide(); device_add_tag('.$id.', $(\'#select_tag\').val() ) ').'</p>';
			$out .= View_Element::SpanClick('добавить тэг', '$(this).hide();  $(\'#select_addtag\').toggleClass(\'hide\');');
		}
		
		return $out;
	}
	
}
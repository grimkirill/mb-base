<?php

global $device_type_list;
$device_type_list = array();
global $db;
$rows = $db->Query('SELECT * FROM devices_config');
while ($row = $rows->get_next_row_array())
{
    $device_type_list[$row['type']] = $row;
}

/*
$device_type_list[1] = array('name' => 'Простое устройство', 'driver' => 'device_driver_unmanage', 'port_count' => 1, 'port_admin' => FALSE, 'port_active' => FALSE, 'use_snmp' => FALSE);
$device_type_list[2] = array('name' => 'Неуправляемый 5 портов', 'driver' => 'device_driver_unmanage', 'port_count' => 5, 'port_admin' => FALSE, 'port_active' => FALSE, 'use_snmp' => FALSE);
$device_type_list[3] = array('name' => 'Неуправляемый 8 портов', 'driver' => 'device_driver_unmanage', 'port_count' => 8, 'port_admin' => FALSE, 'port_active' => FALSE, 'use_snmp' => FALSE);
$device_type_list[4] = array('name' => 'Неуправляемый 16 портов', 'driver' => 'device_driver_unmanage', 'port_count' => 16, 'port_admin' => FALSE, 'port_active' => FALSE, 'use_snmp' => FALSE);
$device_type_list[5] = array('name' => 'Неуправляемый 24 портов', 'driver' => 'device_driver_unmanage', 'port_count' => 24, 'port_admin' => FALSE, 'port_active' => FALSE, 'use_snmp' => FALSE);
$device_type_list[6] = array('name' => 'Неуправляемый 48 портов', 'driver' => 'device_driver_unmanage', 'port_count' => 48, 'port_admin' => FALSE, 'port_active' => FALSE, 'use_snmp' => FALSE);

$device_type_list[7] = array('name' => 'SNMP 8 портов', 'driver' => 'device_driver_SNMP', 'port_count' => 8, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE);
$device_type_list[8] = array('name' => 'SNMP 16 портов', 'driver' => 'device_driver_SNMP', 'port_count' => 16, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE);
$device_type_list[9] = array('name' => 'SNMP 20 портов', 'driver' => 'device_driver_SNMP', 'port_count' => 20, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE);
$device_type_list[10] = array('name' => 'SNMP 24 портов', 'driver' => 'device_driver_SNMP', 'port_count' => 24, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE);
$device_type_list[11] = array('name' => 'SNMP 26 портов', 'driver' => 'device_driver_SNMP', 'port_count' => 26, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE);
$device_type_list[12] = array('name' => 'SNMP 28 портов', 'driver' => 'device_driver_SNMP', 'port_count' => 28, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE);
$device_type_list[13] = array('name' => 'SNMP 48 портов', 'driver' => 'device_driver_SNMP', 'port_count' => 48, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE);
$device_type_list[14] = array('name' => 'SNMP 52 портов', 'driver' => 'device_driver_SNMP', 'port_count' => 52, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE);

$device_type_list[15] = array('name' => 'Dlink DES-3526', 'driver' => 'device_driver_dlink_3526', 'port_count' => 26, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE, 'snmp_dlink_vlan' => TRUE);
$device_type_list[16] = array('name' => 'Dlink DES-3028', 'driver' => 'device_driver_dlink_3028', 'port_count' => 28, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE, 'snmp_dlink_vlan' => TRUE);
$device_type_list[17] = array('name' => 'Dlink DES-3200-28', 'driver' => 'device_driver_dlink_3028', 'port_count' => 28, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE, 'snmp_dlink_vlan' => TRUE);
$device_type_list[18] = array('name' => 'Dlink DES-1228ME', 'driver' => 'device_driver_dlink_3028', 'port_count' => 28, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE, 'snmp_dlink_vlan' => TRUE);
$device_type_list[19] = array('name' => 'Dlink DGS-3100-24TG', 'driver' => 'device_driver_dlink_3028', 'port_count' => 24, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE, 'snmp_dlink_vlan' => TRUE);

$device_type_list[20] = array('name' => 'ETEN 8 портов', 'driver' => 'device_driver_eten', 'port_count' => 8, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => FALSE);
$device_type_list[21] = array('name' => 'ETEN 16 портов', 'driver' => 'device_driver_eten', 'port_count' => 16, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => FALSE);
$device_type_list[22] = array('name' => 'ETEN 24 портов', 'driver' => 'device_driver_eten', 'port_count' => 24, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => FALSE);


$device_type_list[23] = array('name' => 'Dlink DGS-3627', 'driver' => 'device_driver_dlink_3028', 'port_count' => 27, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE, 'snmp_dlink_vlan' => TRUE);
$device_type_list[24] = array('name' => 'Dlink DGS-3200-16', 'driver' => 'device_driver_dlink_3028', 'port_count' => 16, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE, 'snmp_dlink_vlan' => TRUE);
$device_type_list[25] = array('name' => 'Dlink DES-3200-18', 'driver' => 'device_driver_dlink_3028', 'port_count' => 18, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE, 'snmp_dlink_vlan' => TRUE);
$device_type_list[26] = array('name' => 'Dlink DES-3200-10', 'driver' => 'device_driver_dlink_3028', 'port_count' => 10, 'port_admin' => TRUE, 'port_active' => TRUE, 'use_snmp' => TRUE, 'snmp_dlink_vlan' => TRUE);
*/


class device_types {
	
	/**
	 * @return array
	 */
	static function get_list()
	{
		global $device_type_list;
		$res = array();
		foreach ($device_type_list as $key => $item)
		{
			$res[$key] = $item['name'];	
		}
		return $res;
	}
	
	static function init()
	{
		//load this file and class
	}
	
	static function get_driver($id)
	{
		global $device_type_list;
		return $device_type_list[$id]['driver'];
	}
	
	static function get_dlink($id)
	{
		global $device_type_list;
		if (isset($device_type_list[$id]['snmp_dlink_vlan']))
		{
			return $device_type_list[$id]['snmp_dlink_vlan'];
		}
		else
		{
			return FALSE;
		}
	}
	
	
	static function get_port_count($id)
	{
		global $device_type_list;
		return $device_type_list[$id]['port_count'];
	}

	static function get_port_admin($id)
	{
		global $device_type_list;
		return $device_type_list[$id]['port_admin'];
	} 
	
	static function get_port_active($id)
	{
		global $device_type_list;
		return $device_type_list[$id]['port_active'];
	} 
	
	static function get_type_name($id)
	{
		global $device_type_list;
		return $device_type_list[$id]['name'];
	}
	
	static function get_use_snmp($id)
	{
		global $device_type_list;
		return $device_type_list[$id]['use_snmp'];
	}
	
}

<?php
global $default_value_array;
global $db;
$default_value_array = $db->Query('SELECT value, name FROM default_values')->get_rows_array_id('name', 'value');

class get_default_value {
	
	static function get($name)
	{
		global $default_value_array;
		return $default_value_array[$name];
	}
	
}
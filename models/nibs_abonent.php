<?php

global $db_nibs;

$db_nibs = new DB_MySql_Driver(array(
	'hostname' => get_default_value::get('option.nibs.hostname'),
	'username' => get_default_value::get('option.nibs.username'),
	'password' => get_default_value::get('option.nibs.password'),
	'database' => get_default_value::get('option.nibs.database'),
	'char_set' => get_default_value::get('option.nibs.char_set'),
	'dbcollat' => get_default_value::get('option.nibs.dbcollat')

));

class nibs_abonent {
	const STATUS_USER_ACTIVE = '0';
	const STATUS_USER_BLOCKED = '1';
	const STATUS_USER_DELETED = '2';
	const STATUS_USER_FROZEN = '3';
	
	public $uid = FALSE;
	public $data;
	static private $users_tables = array("users"=>'0', "usersblok"=>'1', "usersdel"=>'2', "usersfreeze"=>'3');
	
	/**
	 * @param int $uid
	 */
	function __construct($uid)
	{
		global $db_nibs;
		foreach(self::$users_tables as $ut=>$status) {
			if ($this->data = $db_nibs->Query("SELECT * FROM `".$ut."` WHERE `uid` =".$uid)->get_next_row_object())
			{
				$this->uid = $this->data->uid;
				$sql = "SELECT u.uid uid, l.lane line, lh.house build, u.app appartment".
					" FROM `".$ut."` AS u INNER JOIN `lanes_houses` AS lh ON u.houseid = lh.houseid".
					" INNER JOIN `lanes` AS l ON lh.laneid = l.laneid".
					" WHERE u.uid = '$uid' LIMIT 1";
				$address = $db_nibs->Query($sql)->get_next_row_object();
				$this->data->address = "$address->line-$address->build-$address->appartment";
				$this->data->status = $status;
				break;
			}
		}
	}
	
	
	function get_inet_use()
#ld done
	{
		global $db_nibs;	
		return $db_nibs->Query('SELECT COUNT(*) AS count  FROM `inetonlinenew` WHERE `username` = \''.$this->data->user.'\'')->get_next_row_object()->count;
	}
	
	function get_day_off($day_count)
	{
		global $db_nibs;
#		return $db_nibs->Query('SELECT COUNT(*) AS count  FROM `actions` WHERE `user` = \''.$this->data->user.'\' AND `start_time` > \''.date('Y-m-d', mktime() - $day_count * 60*60*24).' 00:00:00\'')->get_next_row_object()->count;
		return $db_nibs->Query('SELECT COUNT(*) AS count  FROM `radacct` WHERE `uid` = \''.$this->data->uid.'\' AND `acctstarttime` > \''.date('Y-m-d', mktime() - $day_count * 60*60*24).' 00:00:00\'')->get_next_row_object()->count;
		//echo 'SELECT COUNT(*) AS count  FROM `actions` WHERE `user` = \''.$this->data->user.'\' AND `start_time` > \''.date('Y-m-d', mktime() - $day_count * 60*60*24).' 00:00:00\'';
	}
	
	
	function get_mac($del_colon=True)
#ld done
	{
		global $db_nibs;
		if ($data = $db_nibs->Query('SELECT callingstationid as call_from  FROM `radacct` WHERE `uid` = \''.$this->data->uid.'\' ORDER BY acctstarttime DESC LIMIT 1')->get_next_row_object()) 
		{
			$mac = $data->call_from;
			if ($del_colon) {
				$mac = str_replace(':', '', $mac);
			}
			return $mac;
		}
		else 
		{
			return FALSE;
		}
	}
	
	function make_termination()
#ld done
	{
		//global $db_nibs;
		//if (get_default_value::get('billing.termination.tarif'))
		//{
		//	$db_nibs->UpdateData('usersall', array('framed_ip' => '', 'gid' => get_default_value::get('billing.termination.tarif')), array('uid' => $this->uid));
		//}
		echo "123";
	}

	
	function make_withdrawal()
#ld done
	{
		//global $db_nibs;
		//if (get_default_value::get('billing.withdrawal.tarif'))
		//{
		//	$db_nibs->UpdateData('usersall', array( 'gid' => get_default_value::get('billing.withdrawal.tarif')), array('uid' => $this->uid));
		//}
		echo "123";
	}

	function get_device_info($uid) {
		global $db_nibs;
		$out = array();

		$sql = "SELECT devid, uid FROM dev_user WHERE uid='".$this->uid."' LIMIT 1";
		$data_id = $db_nibs->Query($sql)->get_next_row_array();

		if(count($data_id) <> 0) {
			$sql = "SELECT `devid`, `key`, `value` FROM `dev_fields` WHERE devid='".$data_id["devid"]."'";
			$data_dev = $db_nibs->Query($sql);
			while($dd = $data_dev->get_next_row_object()) {
				if($dd->key == "devtypeid") {
					$sql = "SELECT devtypeid, devtypename FROM dev_types WHERE devtypeid='".$dd->value."' LIMIT 1";
					$res = $db_nibs->Query($sql)->get_next_row_array();
					if(count($res) <> 0) {
						$out['devtypename'] = $res['devtypename'];
					}
				}
				$out[$dd->key] = $dd->value;
			}
			return $out;
		}
		return NULL;
	}

	function get_b_services() {
		global $db_nibs;
		$sql = "SELECT sp.gid, sp.serviceid, s.serviceid, s.servicename AS servicename, s.description AS description, s.amount AS amount FROM `services_packets_pairs` sp INNER JOIN `services` s ON sp.serviceid=s.serviceid WHERE sp.gid='".$this->data->gid."'";
                return $db_nibs->Query($sql)->get_rows_array();
	}
	
	function get_bserv_total() {
               	global $db_nibs;
               	$sql = "SELECT SUM(s.amount) AS total FROM `services_packets_pairs` sp INNER JOIN `services` s ON sp.serviceid=s.serviceid WHERE sp.gid='".$this->data->gid."'";
               	$res = $db_nibs->Query($sql)->get_next_row_array();
	       	//var_dump(res);
		return $res['total'];
        }


        function get_i_services() {
                global $db_nibs;
                $sql = "SELECT su.uid, su.serviceid, s.serviceid, s.servicename AS servicename, s.description AS description, s.amount AS amount FROM `services_users_pairs` su INNER JOIN `services` s ON su.serviceid=s.serviceid WHERE su.uid='".$this->data->uid."'";
                return $db_nibs->Query($sql)->get_rows_array();
        }

        function get_iserv_total() {
                global $db_nibs;
                $sql = "SELECT SUM(s.amount) AS total FROM `services_users_pairs` su INNER JOIN `services` s ON su.serviceid=s.serviceid WHERE su.uid='".$this->data->uid."'";
                $res = $db_nibs->Query($sql)->get_next_row_array();
                //var_dump(res);
                return $res['total'];
        }



	function get_print_array()
#ld done
	{
		global $db_nibs;
		$out = array();
		
		$users_tbl = array_search($this->data->status, self::$users_tables);
		//echo $users_tbl;
		
		$sql = "SELECT value as passportdate ".
			" FROM `".$users_tbl."` AS u INNER JOIN `users_custom_fields` ".
			" AS ucf ON u.uid=ucf.uid WHERE ucf.key='ext_passport_date' AND u.uid ='$this->uid'";
		$pd_res = $db_nibs->Query($sql)->get_next_row_array();
		
		$data_users = $db_nibs->Query('SELECT *,password as passwordprint," " as passportdate,passportpropiska as passportdislocate,passportgdevidan as passportbody,passportserie as passportnum,numdogovor as contractnum FROM '.$users_tbl.' WHERE `uid` ='.$this->uid)->get_next_row_array();

#		$data_users2 = $db_nibs->Query('SELECT laneid, neighborhoodid, settlementid FROM lanes_houses NATURAL JOIN lanes WHERE houseid = '.$data_users[houseid])->get_next_row_array();
		$data_users2 = $db_nibs->Query('SELECT house, lane, settlementname FROM lanes_houses NATURAL JOIN lanes LEFT JOIN lanes_settlements ON lanes.`settlementid` = `lanes_settlements`.`settlementid` WHERE `houseid` =  '.$data_users['houseid'])->get_next_row_array();
		$address=$data_users2['lane'].'-'.$data_users2['house'].'-'.$data_users['app'];
		if ($data_users2['settlementname']== NULL) {
		    unset($data_users2['settlementname']);
		}
		$data_users['address']=$address;
		$data_users['passportdate'] = $pd_res['passportdate'];
#		var_dump($data_users['address']);
		$data_packets = $db_nibs->Query('SELECT * FROM packets WHERE gid = '.$this->data->gid)->get_next_row_array();
		foreach ($data_users as $key => $value)
		{
			$out['u_'.$key] = mb_convert_encoding($value, "utf-8", get_default_value::get('option.nibs.encoding'));
		}
		$out['u_status'] = $this->data->status;
		foreach ($data_packets as $key => $value)
		{
			$out['p_'.$key] = mb_convert_encoding($value, "utf-8", get_default_value::get('option.nibs.encoding'));
		}
		return $out;
	}
	
	function get_address_link()
	{
		return View_Element::Link($this->get_address(), 'billing/looking/show/'.$this->uid);
	}
	
	function get_address()
	{
		if ($this->uid)
		{
			return mb_convert_encoding( $this->data->address, "utf-8", get_default_value::get('option.nibs.encoding'));
			//$sql = "SELECT u.uid uid, u.fio fio, u.gid gid, l.lane line, lh.house build, u.app appartment".
                	//	" FROM `$table` AS u INNER JOIN `lanes_houses` AS lh ON u.houseid = lh.houseid".
                	//	" INNER JOIN `lanes` AS l ON lh.laneid = l.laneid".
                	//	" WHERE u.uid = '$uid' LIMIT 1";
			
		}
	}
	
	function get_deposit()
	{
		if ($this->uid)
		{
			return round($this->data->deposit,2);
		}
	}
	
	function get_block()
	{
		if ($this->uid)
		{
			return $this->data->blocked;
		}
	}
	
        function get_status()
        {
                if ($this->uid)
                {
                      return $this->data->status;
                }
        }

	
	function set_block($block = 0)
#ld done
	{
		global $db_nibs;
		$db_nibs->UpdateData('users', array( 'blocked' => (int)$block) ,array('uid' => $this->uid) );
		
	}
	
	function get_packet()
	{
#ld done
		global $db_nibs;
		if ($this->uid)
		{
			if ($pack = $db_nibs->Query('SELECT `packet` FROM `packets` WHERE `gid` = '.$this->data->gid)->get_next_row_object())
			{
				return mb_convert_encoding( $pack->packet, "utf-8", get_default_value::get('option.nibs.encoding'));
			}
		}
	}
	
	function get_fio()
	{
		if ($this->uid)
		{
			return mb_convert_encoding( $this->data->fio, "utf-8", get_default_value::get('option.nibs.encoding'));
		}
	}
	
	function is_online()
	{
#ld done
		global $db_nibs;
		if ($res = $db_nibs->Query('SELECT COUNT(*) as count FROM `radacct` WHERE `acctterminatecause` = \'Online\' AND username = '.$db_nibs->EscapeValue($this->data->user))->get_next_row_object())
		{
			return $res->count; 
		}
		
	}
	
	function get_use_list($date_start = FALSE, $date_stop = FALSE)
#ld done 
	{
		global $db_nibs;
		if (!$date_start)
		{
			$date_start = date('Y-m-d', mktime(0,0,0,date('m')-1,date('d'),date('Y')));
		}
		
#		$sql = "SELECT `start_time`, `stop_time`, `in_bytes`, `out_bytes`, `ip`, `server`, `call_from`   FROM `actions` WHERE `user` = '".$this->data->user."' AND `start_time` > '".$date_start." 00:00:00' AND `comment` IS NULL ORDER BY  `start_time` DESC ";
		$sql = "SELECT acctstarttime as start_time, acctstoptime as stop_time, acctinputoctets as in_bytes, acctoutputoctets as out_bytes, framedipaddress as ip, nasipaddress as server, callingstationid as call_from   FROM `radacct` WHERE `uid` = '".$this->data->uid."' AND `acctstarttime` > '".$date_start." 00:00:00' ORDER BY  `acctstarttime` DESC ";
		
		return $db_nibs->Query($sql)->get_rows_array();
		
	}
	
	function get_pay_list($date_start = FALSE, $date_stop = FALSE)
#ld done 
	{
		global $db_nibs;
		if (!$date_start)
		{
			$date_start = date('Y-m-d', mktime(0,0,0,date('m')-3,date('d'),date('Y')));
		}
		
#		$sql = "SELECT `start_time`, `call_to`, `call_from`, `comment`, `before_billing`, `billing_minus`  FROM `actions` WHERE `user` = '".$this->data->user."' AND `start_time` > '".$date_start." 00:00:00' AND `comment` IS NOT NULL ORDER BY  `start_time` DESC ";
		$sql = "SELECT date as start_time, who as call_to, typename as call_from, comment, `before_billing`, summa as billing_minus  FROM `bugh_plategi_stat` A , `bugh_plategi_type` B  WHERE A.bughtypeid=B.bughtypeid AND `uid` = '".$this->data->uid."' AND `date` > '".$date_start." 00:00:00' ORDER BY  `date` DESC ";
		
		$data = $db_nibs->Query($sql)->get_rows_array(); 
		
		if (is_array($data))
		{
			foreach ($data as $key => $item)
			{
				$data[$key]['call_to'] = mb_convert_encoding( $data[$key]['call_to'], "utf-8", get_default_value::get('option.nibs.encoding'));
				$data[$key]['call_from'] = mb_convert_encoding( $data[$key]['call_from'], "utf-8", get_default_value::get('option.nibs.encoding'));
				$data[$key]['comment'] = mb_convert_encoding( $data[$key]['comment'], "utf-8", get_default_value::get('option.nibs.encoding'));
				$before_billing = round ($data[$key]['before_billing'] ,2 );
                                if($before_billing < 0) {
                                        $before_billing = "<font color=red>".$before_billing."</font>";
                                }
				$data[$key]['before_billing'] = $before_billing;
				//$data[$key]['before_billing'] = round ($data[$key]['before_billing'] ,2 );
				$billing_minus = round ($data[$key]['billing_minus'] ,2 );
				if($billing_minus < 0) {
					$billing_minus = "<font color=red>".$billing_minus."</font>";
				}
				//$data[$key]['billing_minus'] = round ($data[$key]['billing_minus'] ,2 );
				$data[$key]['billing_minus'] = $billing_minus;
			}
		}
		
		return $data; 
		
	}
	
	
	static function find_uid($address)
#ld done
	{
		global $db_nibs;
		$address = mb_convert_encoding( $address, get_default_value::get('option.nibs.encoding'), "utf-8");
		
                $tmp1=explode('-',$address);
                if(count($tmp1) > 3) {
                	$line = $tmp1[0]."-".$tmp1[1];
                        $house = $tmp1[2];
                        $appartment = $tmp1[3];
                } else {
                	$line = $tmp1[0];
                        $house = $tmp1[1];
                        $appartment = $tmp1[2];
                }

		$sql = "SELECT u.uid uid, l.lane line, lh.house build, u.app appartment".
                	" FROM `usersall` AS u INNER JOIN `lanes_houses` AS lh ON u.houseid = lh.houseid".
                        " INNER JOIN `lanes` AS l ON lh.laneid = l.laneid".
                        " WHERE l.lane = '$line' AND lh.house = '$house' AND u.app = '$appartment' ORDER BY u.uid DESC";

		if ($res = $db_nibs->Query($sql)->get_next_row_object())
		{
			return $res->uid; 
		}
		else
		{
			return 0;
		}
	}
	
	
	static function get_cost_packets()
#ld done
	{
		global $db_nibs;
		$packets = $db_nibs->Query('SELECT  gid, fixed_cost FROM `packets`');
		$packet_list = array();
		while ($packet = $packets->get_next_row_object())
		{
			$packet_list[$packet->gid] = $packet->fixed_cost;
		}
		return $packet_list;
	}
	
	static function get_list_packets()
	{
		global $db_nibs;
		$packets = $db_nibs->Query('SELECT `packet`, `gid` FROM `packets`');
		$packet_list = array();
		while ($packet = $packets->get_next_row_object())
		{
			$packet_list[$packet->gid] = mb_convert_encoding( $packet->packet, "utf-8", get_default_value::get('option.nibs.encoding'));
		}
		return $packet_list;
	}
	
	static function get_list_users($sql='SELECT `user`, `uid`, `gid`, `blocked` FROM `usersall` WHERE `gid` IN (SELECT `gid` FROM `packets`)')
	{
		global $db_nibs;

		$users = $db_nibs->Query($sql);
		$user_list = array();
		while ($user = $users->get_next_row_object())
		{
			$user_list[$user->uid] = array('gid' => $user->gid, 'blocked' => $user->blocked, 'user' => $user->user);
		}
		return $user_list;
	}

	static function get_list_users_db_result($sql) {
		global $db_nibs;
		return $db_nibs->Query($sql);
	}

        static function is_legal($uid)
        {
                global $db_nibs;
                
                $sql = "SELECT `uid`, `key`, `value` FROM `users_custom_fields` WHERE ".
                    " `uid`='$uid' AND `key`='ext_legal_person' AND `value`='1'";
                $users = $db_nibs->Query($sql);
                if($users->get_rows_count()) {
                    return true;
                }
                return false;
        }

	static function get_total_balance($uid) {
		global $db_nibs;

		$sql = "SELECT deposit+credit AS total_balance FROM users WHERE uid='$uid' LIMIT 1";
		$balance = $db_nibs->Query($sql);
		return (float) $balance->get_next_row_object()->total_balance;
	}

	
	static function get_list_found($address, $uid, $ip, $tarif)
	{
		global $db_nibs;
		$address = mb_convert_encoding( $address, get_default_value::get('option.nibs.encoding'), "utf-8");
		$sql = '';
		$list = '';
		if ($address != '')
		{
#			var_dump($address);
			$tmp1=explode('-',$address);
			if(count($tmp1) > 3) {
				$line = $tmp1[0]."-".$tmp1[1];
				//var_dump($line);
				$house = $tmp1[2];
				$appartment = $tmp1[3];
			} else {
				$line = $tmp1[0];
				$house = $tmp1[1];
                                $appartment = $tmp1[2];
			}
#		var_dump($tmp1);

		$list[] = ' houseid = (SELECT `houseid` FROM `lanes_houses` WHERE `laneid` = (SELECT `laneid` FROM `lanes` WHERE `lane` = \''.$line.'\' ) AND `house` = \''.$house.'\' ) ';
#		var_dump($tmp3);

#		$list[] = ' address LIKE '.$db_nibs->EscapeValue($address);
		if (isset($appartment)) {
			$list[] = ' app LIKE '.$db_nibs->EscapeValue($appartment);
#			var_dump($list);
		}

		}
		if ($uid)
		{
			$list[] = ' uid = '.$db_nibs->EscapeValue($uid);	
		}
		
		if ($ip)
		{
			$list[] = ' framed_ip LIKE '.$db_nibs->EscapeValue($ip);	
		}
		
		if ($tarif)
		{
			$list[] = ' gid = '.$db_nibs->EscapeValue($tarif);	
		}
		
		if (is_array($list))
		{
			$users_list = array();
			foreach(self::$users_tables as $ut=>$status) {
				$sql = "SELECT uid FROM `".$ut."` WHERE ".implode(" AND ", $list);
				$res = $db_nibs->Query($sql)->get_rows_array_id('uid', 'uid');
				if($res) {
					$users_list += $res;
				}
			}
			return $users_list;
		}
		else
		{	
			return FALSE;
		}
		
	}
	
	
}

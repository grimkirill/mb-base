<?php

/**
 * class of request, contain all of fields.
 * @author kirill
 *
 */
class request {
	public $id;
	public $data = array();
	
	function __construct($id)
	{
		global $db;
		$this->id = $id;
		$this->data = $db->Query("SELECT * FROM `requests` WHERE id = ".$id)->get_next_row_object();
	}
	
	function get_time_print()
	{
		$str = substr($this->data->time_start,0,5).' - '.
			date('H:i', strtotime(date( 'Y-m-d').' '.$this->data->time_start)+$this->data->time_count);
		if ($this->data->time_start == '00:00:00')
		{
			$str = 'весь день';
		}	
		return $str;
	}
	
	function get_done_print()
	{	
		$color = 'red';
		if ($this->data->done == 1)
		{
			$color = 'green';
		}
		if ($this->data->done == 2)
		{
			$color = '#4363e6';
		}
		return '<span title="'.$this->data->dt_done.View_Element::Space(1).User_Auth::get_loginid($this->data->user_done).'" ondblclick="$(this).parent().load(WEBPATH+\'ajax/request_load/set_done/'.$this->id.'\');" style="cursor: pointer;"> <span style="background-color: '.$color.';" >&nbsp;&nbsp;&nbsp;&nbsp;</span></span>';
	}
	
	function get_type()
	{
		global $db;
		return $db->Query('SELECT `name` FROM `request_type` WHERE `id` = '.$this->data->type)->get_next_row_object()->name;
	}
	
	function get_serviser()
	{
		global $db;
		return $db->Query('SELECT `name` FROM `request_servicer` WHERE `id` = '.$this->data->servicer)->get_next_row_object()->name;
		
	}
	
	function get_gid_link()
	{
		global $db;
		if ($this->data->gid)
		{
			$abon = new nibs_abonent($this->data->gid);
			return View_Element::Link($abon->get_address(), 'billing/looking/show/'.$this->data->gid);
		} else {
			return $this->data->gid_data; 
		}
	}
	
	function set_next_done()
	{
		global $db,  $auth_user;
		$new_done = 0;
		if ($this->data->done == 0) { $new_done = 1; }
		if ($this->data->done == 1) { $new_done = 2; }
		
		$db->UpdateData('requests', array('done' => $new_done, 'dt_done' => 'NOW()', 'user_done' => $auth_user->id), array('id' => $this->id));
		$this->data->done = $new_done;
		
	}
	
	function get_data_array()
	{
		global $db;
		
		$ret_data['id'] = $this->id;
		$ret_data['servicer'] = $this->get_serviser();
		$ret_data['type'] = $this->get_type();
		$ret_data['time'] = $this->get_time_print();
		$ret_data['comment'] = $this->data->comment;
		$ret_data['phone'] = $this->data->phone;
		$ret_data['date'] = $this->data->date;
		
		$ret_data['gid'] = '';
		$ret_data['gid_tarif'] = '';
		$ret_data['gid_balance'] = '';
		$ret_data['gid_fio'] = '';
		
		$ret_data['gid_link'] = '';
		
		$ret_data['other'] = '';
		$ret_data['other_br'] = '';
		
		if ($this->data->gid)
		{
			$abon = new nibs_abonent($this->data->gid);
			$ret_data['gid'] = $abon->get_address();
			$ret_data['gid_tarif'] = $abon->get_packet();
			$ret_data['gid_balance'] = $abon->get_deposit();
			$ret_data['gid_fio'] = $abon->get_fio();
			
			$ret_data['gid_link'] =  View_Element::Link($ret_data['gid'], 'billing/looking/show/'.$this->data->gid);
			
			$sql = 'SELECT *  FROM `devices_abonents` WHERE `uid` = '.$this->data->gid;
			if ($results = $db->Query($sql)->get_next_row_object() )
			{
				$dev = new device($results->gid);
				$ret_data['other'] = $dev->get_address().' [ '.$results->port.' ] '.$dev->data->ip.' '.$dev->get_address_disposition();
				$ret_data['other_br'] = '<br>';
			}
		} else {
			$ret_data['gid'] = $this->data->gid_data;
			$ret_data['gid_link'] = $this->data->gid_data; 
		}
		
		
		
		$ret_data['done'] = '';
		if ($this->data->done == 1)
		{
			$ret_data['done'] = 'Выполнено';
		}
		if ($this->data->done == 2)
		{
			$ret_data['done'] = 'Отмена';
		}
		$ret_data['done_print'] = $this->get_done_print();
		
		return $ret_data;
	}
	
	function do_trigger()
	{
		global $db;
		$servicer = $db->Query('SELECT * FROM  `request_servicer` WHERE id = '.$this->data->servicer)->get_next_row_object();
		if ($servicer->trigger)
		{
			$row_out = $servicer->trigger_text;
			$row_array = $this->get_data_array();
			foreach ($row_array as $key => $item)
			{
				$row_out = str_replace('{'.$key.'}', $item, $row_out);
			}
					
			$tg = new trigger($servicer->trigger, $row_out);
		}
	}
	
}
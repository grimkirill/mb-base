<?php

/**
 * @author kirill
 *
 */

class request_data {
	
	static function timeList()
	{
		return array(
			'00:00:00' => 'весь день',
		 	'08:00:00' => '8:00',
			'09:00:00' => '9:00',
			'10:00:00' => '10:00',
			'11:00:00' => '11:00',
			'12:00:00' => '12:00',
			'13:00:00' => '13:00',
			'14:00:00' => '14:00',
			'15:00:00' => '15:00',
			'16:00:00' => '16:00',
			'17:00:00' => '17:00',
			'18:00:00' => '18:00'
		);
	}

	static function getPrintTime($time_start, $time_count)
	{
		$str = substr($time_start,0,5).' - '.
			date('H:i', strtotime(date( 'Y-m-d').' '.$time_start)+$time_count);
		if ($time_start == '00:00:00')
		{
			$str = 'весь день';
		}	
		return $str;
	}
	
	static function reguestDone($id, $done)
	{
		$color = 'red';
		if ($done)
		{
			$color = 'green';
		}
		return '<span ondblclick="$(this).load(WEBPATH+\'ajax/request_load/set_done/'.$id.'\');" style="cursor: pointer;"> <span style="background-color: '.$color.';" >&nbsp;&nbsp;&nbsp;&nbsp;</span></span>';
	}
	
	static function timeCount()
	{
		return array( 
			1800 => '0:30',
			3600 => '1:00',
			5400 => '1:30',
			7200 => '2:00',
			9000 => '2:30',
			10800 => '3:00',
			14400 => '4:00',
			18000 => '5:00',
			21600 => '6:00',
			25200 => '7:00',
			28800 => '8:00'
		);
	}
}
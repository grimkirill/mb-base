<?php

class view_form_select_address_area extends View_Form_Select
{
	
	function __construct($name, $value = FALSE, $params = FALSE)
	{
		global $db;
		parent::__construct($name, $value, $params);
		$this->options = $db->Query('SELECT * FROM address_area')->get_rows_array_id('id', 'area');
	}
}
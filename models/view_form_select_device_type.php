<?php

class view_form_select_device_type extends View_Form_Select {
	
	public $as_filter = FALSE; 
	
	function __construct($name, $value = FALSE, $params = FALSE)
	{
		parent::__construct($name, $value, $params);
		if ($this->as_filter)
		{
			$this->options = array(0 => '--=--') + device_types::get_list();
		}
		else
		{
			$this->options = device_types::get_list();
		}
	}
}
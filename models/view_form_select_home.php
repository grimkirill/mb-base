<?php

class view_form_select_home extends View_Form_Select {
	
	public $as_filter = FALSE;
	
	function __construct($name, $value = FALSE, $params = FALSE)
	{
		global $db;
		parent::__construct($name, $value, $params);
		//$this->options = $db->Query('SELECT * FROM users_groups')->get_rows_array_id('id', 'name');
	}
	
	function get_value($type = 'post')
	{
		global $db;
		$result = FALSE;
		if ($type == 'post')
		{
			if (isset($_POST[$this->name]))
			{
				$result = $_POST[$this->name];
			}
			else
			{
				$this->error = TRUE;
			}
		}
		else
		{
			if (isset($_GET[$this->name]))
			{
				$result = $_GET[$this->name];
			}
			else
			{
				$this->error = TRUE;
			}
		}
		$this->value = $result;
		
		if ($this->as_filter)
		{
			if ($this->value != 0)
			{
				//определенный дом
				return array($this->value);
			}
			else
			{
				if ($type == 'post')
				{
				// POST POST POST POST
					if ((isset($_POST[$this->name.'_street'])) && ($_POST[$this->name.'_street'] != 0))
					{
						// принадлежат определенной улице
						$homes = $db->Query('SELECT id FROM  `address_home` WHERE gid = '.(int)$_POST[$this->name.'_street'])->get_rows_array_id('id', 'id');
						return $homes;
					}
					else
					{
						if (isset($_POST[$this->name.'_city']))
						{
							if ($_POST[$this->name.'_city'] != 0)
							{
								// принадлежит городу
								$homes = $db->Query('SELECT id FROM  `address_home` WHERE gid IN ( SELECT id FROM `address_street` WHERE gid ='.(int)$_POST[$this->name.'_city'].')')->get_rows_array_id('id', 'id');
								return $homes;
							}
							else
							{
								// все возможные
								$homes = $db->Query('SELECT id FROM  `address_home` ')->get_rows_array_id('id', 'id');
								return $homes;
							}
						}
					}
				// END POST     END POST     END POST     END POST     END POST     END POST     
				}
				else
				{
					// GET GET GET GET GET GET GET GET GET GET GET GET GET GET GET GET 

					if ((isset($_GET[$this->name.'_street'])) && ($_GET[$this->name.'_street'] != 0))
						{
							// принадлежат определенной улице
							$homes = $db->Query('SELECT id FROM  `address_home` WHERE gid = '.(int)$_GET[$this->name.'_street'])->get_rows_array_id('id', 'id');
							return $homes;
						}
						else
						{
							if (isset($_GET[$this->name.'_city']))
							{
								if ($_GET[$this->name.'_city'] != 0)
								{
									// принадлежит городу
									$homes = $db->Query('SELECT id FROM  `address_home` WHERE gid IN ( SELECT id FROM `address_street` WHERE gid ='.(int)$_GET[$this->name.'_city'].')')->get_rows_array_id('id', 'id');
									return $homes;
								}
								else
								{
									// все возможные
									$homes = $db->Query('SELECT id FROM  `address_home` ')->get_rows_array_id('id', 'id');
									return $homes;
								}
							}
						}
					
					// END GET   END GET   END GET   END GET   END GET   END GET   END GET   
				}
			}
			
		}
		
		return $result;
	}
	
	function out($type = 'post')
	{
		global $db, $conf_global;
		
		$city_gid = 0;
		$street_gid = 0;
		
		if ($this->value) {
			$data = $db->Query('SELECT * FROM  `address_home` WHERE id = '.$this->value)->get_next_row_object();
			$street_data = $db->Query('SELECT * FROM  `address_street` WHERE id = '.$db->EscapeValue($data->gid))->get_next_row_object();
			$city_gid = $street_data->gid; 
			$street_gid = $data->gid;
		}
		
		if ($this->as_filter)
		{
			if ($type == 'post')
			{
				if (isset($_POST[$this->name.'_city']))
				{
					$city_gid = $_POST[$this->name.'_city'];
					mySession_SetValue($conf_global['salt'].'session_city_gid', $city_gid);
				}
				else
				{
					if (mySession_HasKey($conf_global['salt'].'session_city_gid'))
					{
						$city_gid = (int)mySession_GetValue($conf_global['salt'].'session_city_gid');
					}
				}
			}
			else
			{
				if (isset($_GET[$this->name.'_city']))
				{
					$city_gid = $_GET[$this->name.'_city'];
					mySession_SetValue($conf_global['salt'].'session_city_gid', $city_gid);
				}
				else
				{
					if (mySession_HasKey($conf_global['salt'].'session_city_gid'))
					{
						$city_gid = (int)mySession_GetValue($conf_global['salt'].'session_city_gid');
					}
				}
				
			}
			
			if ($type == 'post')
			{
				if (isset($_POST[$this->name.'_street']))
				{
					$street_gid = $_POST[$this->name.'_street'];
					
				}
			}
			else
			{
				if (isset($_GET[$this->name.'_street']))
				{
					$street_gid = $_GET[$this->name.'_street'];
					
				}
			}
			
		}
		
		
		$str = 'Город: <select name="'.$this->name.'_city" id="'.$this->name.'_city">';
				
		foreach (self::get_citys($this->as_filter) as $key => $item)
		{
			if ((!$this->as_filter) && ($city_gid == 0)) { $city_gid = $key; }
			
			$str .= '<option value="'.$key.'" ';
			if ($key == $city_gid)
			{
				$str .= 'selected="selected"';
			}
			$str .= '>'.$item.'</option>';
		}
		$str .= '</select>';
		
		
		$str .= ' Улица: <select name="'.$this->name.'_street" id="'.$this->name.'_street">';
		
		foreach (self::get_streets($this->as_filter, $city_gid) as $key => $item)
		{
			
			if ((!$this->as_filter) && ($street_gid == 0)) { $street_gid = $key; }
			
			$str .= '<option value="'.$key.'" ';
			if ($key == $street_gid)
			{
				$str .= 'selected="selected"';
			}
			$str .= '>'.$item.'</option>';
		}
		$str .= '</select>';
		
		
		$str .= ' Дом: <select name="'.$this->name.'" title="'.$this->caption.'" id="'.$this->name.'">';
		
		foreach (self::get_homes($this->as_filter, $street_gid) as $key => $item)
		{
			$str .= '<option value="'.$key.'" ';
			if ($key == $this->value)
			{
				$str .= 'selected="selected"';
			}
			$str .= '>'.$item.'</option>';
		}
		
		
		$str .= '</select>';
		if (!$this->as_filter)
		{
			$str .= "<script type=\"text/javascript\">
					$('#".$this->name."_city').change(function() {
							$('#".$this->name."_street').load('".WEBPATH."ajax/address_loader/street_list/'+$('#".$this->name."_city').val(), 
							function() {
								$('#".$this->name."').load('".WEBPATH."ajax/address_loader/home_list/'+$('#".$this->name."_street').val());
							} );
					});
					$('#".$this->name."_street').change(function() {
							$('#".$this->name."').load('".WEBPATH."ajax/address_loader/home_list/'+$('#".$this->name."_street').val());
					});
					</script>";
		
		}
		else
		{
			$str .= "<script type=\"text/javascript\">
					$('#".$this->name."_city').change(function() {
							$('#".$this->name."_street').load('".WEBPATH."ajax/address_loader/street_list_filter/'+$('#".$this->name."_city').val(), 
							function() {
								$('#".$this->name."').load('".WEBPATH."ajax/address_loader/home_list_filter/'+$('#".$this->name."_street').val());
							} );
					});
					$('#".$this->name."_street').change(function() {
							$('#".$this->name."').load('".WEBPATH."ajax/address_loader/home_list_filter/'+$('#".$this->name."_street').val());
					});
					</script>";
		}
		return $str;
	}
	
	
	static function get_citys($as_filter)
	{
		global $db;
		$res = $db->Query('SELECT * FROM address_city')->get_rows_array_id('id', 'city');
		if ($as_filter)
		{
			$res = array(0 => '--=--') + $res;
		}
		return $res;
	}
	
	static function get_streets($as_filter, $gid)
	{
		global $db;
		$res = array();
		if ($gid != 0)
		{
			$res = $db->Query('SELECT * FROM address_street WHERE gid = '.$db->EscapeValue($gid).' ORDER BY `street` ASC')->get_rows_array_id('id', 'street');
		}
		if ($as_filter)
		{
			$res = array(0 => '--=--') + $res;
		}
		return $res;
	}
	
	static function get_homes($as_filter, $gid)
	{
		global $db;
		$res = array();
		if ($gid != 0)
		{
			$res = $db->Query('SELECT * FROM address_home WHERE gid = '.$db->EscapeValue($gid).' ORDER BY home+0')->get_rows_array_id('id', 'home');
		}
		if ($as_filter)
		{
			$res = array(0 => '--=--') + $res;
		}
		return $res;
	}
	
	static function get_home_print($home)
	{
		global $db;
		$data = $db->Query('SELECT address_home.home, address_street.street, address_city.city FROM `address_home`, address_street, address_city WHERE address_street.id = address_home.gid AND address_city.id = address_street.gid AND address_home.id = '.$home)->get_next_row_object();
		$str = $data->city.', '.$data->street.', '.$data->home;
		return $str;
	}
	
	static function get_diff_home_print($home, $home_compare)
	{
		global $db;
		$data = $db->Query('SELECT address_home.home, address_street.street, address_street.id as street_id, address_city.city, address_city.id as city_id FROM `address_home`, address_street, address_city WHERE address_street.id = address_home.gid AND address_city.id = address_street.gid AND address_home.id = '.$home)->get_next_row_object();
		$data_compare = $db->Query('SELECT address_home.home, address_street.street, address_street.id as street_id, address_city.city, address_city.id as city_id FROM `address_home`, address_street, address_city WHERE address_street.id = address_home.gid AND address_city.id = address_street.gid AND address_home.id = '.$home_compare)->get_next_row_object();
		$str = '';
		if ($data->city_id != $data_compare->city_id)
		{
			$str .= $data->city.', ';
		}
		
		if ($data->street_id != $data_compare->street_id)
		{
			$str .= $data->street.', ';
		}
		
		$str .= $data->home;
		return $str;
	}
}
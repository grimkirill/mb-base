<?php

class view_form_select_pdf_font extends View_Form_Select
{
	public $options = array (
		1 => 'dejavusans',
		2 => 'dejavuserif',
		3 => 'freemono',
		4 => 'freesans',
		5 => 'freeserif'
	);
	
	static function get_option_list()
	{
		return array (
			1 => 'dejavusans',
			2 => 'dejavuserif',
			3 => 'freemono',
			4 => 'freesans',
			5 => 'freeserif',
			6 => 'times'
		);
	}
}
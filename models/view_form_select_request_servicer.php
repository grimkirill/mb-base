<?php

class view_form_select_request_servicer extends View_Form_Select
{
	public $as_filter = FALSE; 
	
	public static function get_list()
	{
		global $db;
		return $db->Query('SELECT * FROM request_servicer WHERE `use` = 1')->get_rows_array_id('id', 'name');
	}
	
	function __construct($name, $value = FALSE, $params = FALSE)
	{
		global $db;
		parent::__construct($name, $value, $params);
		
		$list = $db->Query('SELECT * FROM request_servicer WHERE `use` = 1')->get_rows_array_id('id', 'name');
		if ($this->as_filter)
		{
			$list = array_merge([0 => '--=--'], $list);
		}
		$this->options = $list;
	}
}
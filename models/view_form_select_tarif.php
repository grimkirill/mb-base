<?php

class view_form_select_tarif extends View_Form_Select
{
	public $as_filter = FALSE; 
	
	function __construct($name, $value = FALSE, $params = FALSE)
	{
		global $db;
		parent::__construct($name, $value, $params);
		$list = $db->Query('SELECT * FROM billing_packets')->get_rows_array_id('id', 'packet');
		if ($this->as_filter)
		{
			$list = array_merge([0 => '--=--'], $list);
		}
		$this->options = $list;
	}
}
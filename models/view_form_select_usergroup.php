<?php

class view_form_select_usergroup extends View_Form_Select
{
	
	function __construct($name, $value = FALSE, $params = FALSE)
	{
		global $db;
		parent::__construct($name, $value, $params);
		$this->options = $db->Query('SELECT * FROM users_groups')->get_rows_array_id('id', 'name');
	}
}
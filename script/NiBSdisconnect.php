<?php

include_once dirname(dirname(__FILE__)).'/system/init.php';


$db_nibs = new DB_MySql_Driver(array(
	'hostname' => get_default_value::get('option.nibs.hostname'),
	'username' => get_default_value::get('option.nibs.username'),
	'password' => get_default_value::get('option.nibs.password'),
	'database' => get_default_value::get('option.nibs.database'),
	'char_set' => get_default_value::get('option.nibs.char_set'),
	'dbcollat' => get_default_value::get('option.nibs.dbcollat')

));

$sql = 'SELECT count(*) as count FROM radacct WHERE acctterminatecause=\'Lost-Carrier\' AND `acctstoptime` > DATE_SUB( NOW( ) , INTERVAL 5 MINUTE ) ';

if ($_SERVER['argc'] == 2)
{
	$nas = $_SERVER['argv'][1];
	$sql .= " AND `nasipaddress` = '$nas' ";
}

echo $db_nibs->Query($sql)->get_next_row_object()->count;

#!/usr/bin/env python

import httplib
import sys

def sendSms(phoneNumber, textSms, smsPortal="www.smsdirect.ru", 
			href="/submit_message", login="garry", 
			password="ymoQ0TxxE", fromName="TELEWEST"):
	smsLine = href+"?login="+login+"&pass="+password+"&to="+phoneNumber+"&from="+fromName+"&text="+textSms
	connection = httplib.HTTPConnection(smsPortal)
	connection.request("GET", smsLine)
	connection.close()

def usage():
	print 'sendsms.py "Text Of SMS Message" phone_number1 [phone_number2... phone_numberN]'

if __name__ == '__main__':
	if len(sys.argv) < 3:
		usage()
		sys.exit(1)
	textSms = sys.argv[1]
	for phn in sys.argv[2:]:
		sendSms(phn, textSms)

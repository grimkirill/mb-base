#!/usr/bin/python
# -*- coding: utf-8 -*-

import telnetlib
import sys

VOIP_GW = "172.16.128.229"
LOGIN = "root"
PASSWORD = "router"

COMMAND_SMS = "mobile 0 1 sms message send "


if len(sys.argv) == 3:
    sms_recipient = sys.argv[1]
    sms_message = sys.argv[2]

    if sms_recipient[0] == '7':
        sms_recipient = '+' + sms_recipient
    print sms_recipient
    
    tn = telnetlib.Telnet(VOIP_GW+"\n")

    tn.read_until("Login: ")
    tn.write(LOGIN+"\n")
    tn.read_until("Password: ")
    tn.write(PASSWORD+"\n")

    tn.write("enable\n")
    tn.write(COMMAND_SMS+" "+sms_recipient+" "+" "+sms_message+"\n")
    tn.write("exit\n")
    tn.write("exit\n")
    print COMMAND_SMS+" "+sms_recipient+" "+" "+sms_message+"\n"

    sys.exit(0)
else:
    print "Invalid arguments!\n"
    sys.exit(1)


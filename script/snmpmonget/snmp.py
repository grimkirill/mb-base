# -*- coding: utf-8 -*-

import sys
import netsnmp

# snmp.py - snmp interface for netwokr devices management
# by protocol SNMP, version 1.0.

class Snmp:
    def __init__(self, desthost='127.0.0.1', version=2):
        self.value = None
        self.result = None
        self.objectname = None
        self.desthost = desthost
        self.version = version

    @staticmethod
    def exception_info(message):
        '''Print exception information.'''
        print message
        print 'Exception information:', sys.exc_info()

    def call_snmpf(self, object_name, community, snmp_function):
        '''This method calls the netsnmp functions library.
        Returns the result of these functions working or None if exception occurs.
        Don\'t call it!'''
        if snmp_function.__name__ not in ('snmpgetnext', 'snmpset'):
            self.objectname = object_name
            self.oid = netsnmp.Varbind(object_name)
        self.community = community
        try:
            return snmp_function(self.oid,
                            Version=self.version,
                            DestHost=self.desthost,
                            Community=self.community)
        except:
            print 'Exception in: %s' % (snmp_function.__name__, )
            return None

    def walk(self, object_name='sysDescr', community='public'):
        '''It walks along the system subtree.'''
        self.value = self.call_snmpf(object_name, community, netsnmp.snmpwalk)

    def get(self, object_name='sysDescr.0', community='public'):
        '''This method gets alone value.'''
        self.value = self.call_snmpf(object_name, community, netsnmp.snmpget)[0]

    def getnext(self):
        '''Get next value. Call it after calling Snmp.get().
        Don\'t pass any arguments!'''
        self.value = self.call_snmpf(self.objectname, self.community, netsnmp.snmpgetnext)[0]
        self.objectname = self.oid.tag + '.' + self.oid.iid

    def sety(self, new_value, object_name, community='private'):
        '''Assigment new value to MIB object.
        Returns True or False (or 1, 0).'''
        if object_name is None:
            raise SnmpException
        self.oid = netsnmp.Varbind(object_name)
        self.oid.val = self.value = new_value
        self.objectname = object_name
        self.result = self.call_snmpf(object_name, community, netsnmp.snmpset)


class SnmpException(Exception):
    pass

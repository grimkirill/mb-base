#!/usr/bin/python

import sys
import snmp
import subprocess
import shlex
import os

def usage():
    print 'Usage: snmpmonget id'
    print 'id: number of configuration file (conf1.py, conf2.py etc)'

def help_and_exit(exit_code):
    usage()
    sys.exit(exit_code)

def run_command(cmd):
    args = shlex.split(cmd)
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT);


if __name__ == '__main__':
    if len(sys.argv) < 2 or not sys.argv[1].isdigit():
        help_and_exit(1)
     
    config_file = 'conf'+sys.argv[1]
    config_module = __import__(config_file)
    
    snmp_object = snmp.Snmp(config_module.ip, config_module.snmpver)
    try:
        snmp_object.get(config_module.oid, config_module.community)
    except:
        sys.exit(1)
    
    if snmp_object.value != config_module.correct_result:
	cmd = config_module.command
        run_command(cmd) 

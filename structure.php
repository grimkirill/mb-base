<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('VERSION', 65);

$structure_root_menu = array(
	'billing' => 'Биллинг',
	'network' => 'Сеть',
	'option' => 'Настройки',
	'admin' => 'Администрирование',
	'other' => 'Другое',
	//,'test/simple/12' => 'test'
	'smslog' => 'SMSLOG'
);

$structure_left_menu['billing'] = array(
	'billing/find' => 'Поиск абонента',
	'billing/packets' => 'Тарифы',
	'billing/requests' => 'Заявки',
	'billing/requeststat' => 'Заявки статистика',
	'billing/changeplan' => 'Переходы',
	'billing/autotickets' => 'Автоматическое создание тикетов'
);


$structure_left_menu['other'] = array(
	'other/mytask' => 'Мои задачи',
	'other/myplan' => 'Мои планы'
);

$structure_left_menu['test'] = array(
	'main' => 'Test1',
	'test' => 'Test2',
	'admin' => 'Test3',
	'test/simple/12' => 'test'
);

$structure_left_menu['network'] = array(
	'network/find' => 'Поиск',
//	'network/newdevices' => 'Добавить',
//	'network/review' => 'Обзор',
//	'network/report_block/show' => 'Блокировки',
//	'network/pinger_viewer' => 'История ping',
	'network/map' => 'Карта',
	'network/vlanlist' => 'VLan',
	'network/macaddr' => 'MAC адреса',
	'network/netping' => 'NetPing',
	'network/setconfig' => 'Configuring',
	'network/graphlist' => 'Графики',
	'network/report' => 'Отчеты',
    'network/traff' => 'Траффик',
);

$structure_left_menu['option'] = array(
	'option/default_values' => 'Конфигурация',
	'option/addr' => 'Адресный классификатор',
	'option/area' => 'Участки',
	'option/interfaces' => 'Интерфейсы',
	'option/taglists' => 'Тэги устройств',
	'option/request_type' => 'Заявки Типы',
	'option/request_servicer' => 'Заявки Исполнители',
	'option/triggeractions' => 'Триггеры',
	'option/billprint' => 'Печатные формы',
	'option/graphcounter' => 'Графики',
	'option/deviceconfig' => 'Оборудование',
    'option/traff' => 'Траффик',
);

$structure_left_menu['admin'] = array(
	'admin/users' => 'Пользователи',
	'admin/groups' => 'Группы',
	'admin/update' => 'Обновление'
);

class ACL{
	
	const acl_admin_user_roll	= 'acl_admin_user_roll';
	const acl_admin_user_add	= 'acl_admin_user_add';
	const acl_admin_user_edit	= 'acl_admin_user_edit';
	
	const acl_admin_group_roll	= 'acl_admin_group_roll';
	const acl_admin_group_add	= 'acl_admin_group_add';
	const acl_admin_group_edit	= 'acl_admin_group_edit';
	
	const acl_admin_update	= 'acl_admin_update';
	
	const acl_netping_add	= 'acl_netping_add';
	const acl_netping_edit	= 'acl_netping_edit';
	const acl_netping_roll	= 'acl_netping_roll';
	const acl_netping_actions	= 'acl_netping_actions';
	
	const acl_option_request_type_roll	= 'acl_option_request_type_roll';
	const acl_option_request_type_add	= 'acl_option_request_type_add';
	const acl_option_request_type_edit	= 'acl_option_request_type_edit';
	
	const acl_option_request_servicer_roll	= 'acl_option_request_servicer_roll';
	const acl_option_request_servicer_add	= 'acl_option_request_servicer_add';
	const acl_option_request_servicer_edit	= 'acl_option_request_servicer_edit';
	
	const acl_option_devices_tag_roll	= 'acl_option_devices_tag_roll';
	const acl_option_devices_tag_add	= 'acl_option_devices_tag_add';
	const acl_option_devices_tag_edit	= 'acl_option_devices_tag_edit';

	
	const acl_option_devices_conf_roll	= 'acl_option_devices_conf_roll';
	const acl_option_devices_conf_add	= 'acl_option_devices_conf_add';
	const acl_option_devices_conf_edit	= 'acl_option_devices_conf_edit';
	
	const acl_option_config_roll	= 'acl_option_config_roll';
	const acl_option_config_edit	= 'acl_option_config_edit';
	
	const acl_option_address_roll	= 'acl_option_address_roll';
	const acl_option_address_add	= 'acl_option_address_add';
	const acl_option_address_edit	= 'acl_option_address_edit';
	
	const acl_option_address_area_roll	= 'acl_option_address_area_roll';
	const acl_option_address_area_add	= 'acl_option_address_area_add';
	const acl_option_address_area_edit	= 'acl_option_address_area_edit';
	
	const acl_option_interfaces_roll	= 'acl_option_interfaces_roll';
	const acl_option_interfaces_add		= 'acl_option_interfaces_add';
	const acl_option_interfaces_edit	= 'acl_option_interfaces_edit';
	
	const acl_option_billprint_roll	= 'acl_option_billprint_roll';
	const acl_option_billprint_add		= 'acl_option_billprint_add';
	const acl_option_billprint_edit	= 'acl_option_billprint_edit';
	
	const acl_option_triggers_roll	= 'acl_option_triggers_roll';
	const acl_option_triggers_add		= 'acl_option_triggers_add';
	const acl_option_triggers_edit	= 'acl_option_triggers_edit';
	
	const acl_network_vlan_roll	= 'acl_network_vlan_roll';
	const acl_network_vlan_edit	= 'acl_network_vlan_edit';
	
	const acl_network_device_add	= 'acl_network_device_add';
	const acl_network_device_edit	= 'acl_network_device_edit';
	const acl_network_device_roll	= 'acl_network_device_roll';
	const acl_network_device_map	= 'acl_network_device_map';
	const acl_network_device_portadmin	= 'acl_network_device_portadmin';
	const acl_network_device_useradmin	= 'acl_network_device_useradmin';
	const acl_network_device_configuring	= 'acl_network_device_configuring';
	
	const acl_billing_request_roll	= 'acl_billing_request_roll';
	const acl_billing_request_add	= 'acl_billing_request_add';
	const acl_billing_request_edit	= 'acl_billing_request_edit';
	
	
	static function get_list()
	{
		$items = array(
			'Управление пользователями' => array(
				self::acl_admin_user_roll => 'Просмотр',
				self::acl_admin_user_add => 'Добавление',
				self::acl_admin_user_edit => 'Редактирование'
			),
			'Управление группами пользователей' => array(
				self::acl_admin_group_roll => 'Просмотр',
				self::acl_admin_group_add => 'Добавление',
				self::acl_admin_group_edit => 'Редактирование'
			),
			'Обновление системы' => array(
				self::acl_admin_update => 'Выполнять обновление системы'
			),
			'Настройки Типы заявок' => array(
				self::acl_option_request_type_roll => 'Просмотр',
				self::acl_option_request_type_add => 'Добавление',
				self::acl_option_request_type_edit => 'Редактирование'
			),
			'Настройки Исполнители заявок' => array(
				self::acl_option_request_servicer_roll => 'Просмотр',
				self::acl_option_request_servicer_add => 'Добавление',
				self::acl_option_request_servicer_edit => 'Редактирование'
			),
			'Настройки Тэги устройств' => array(
				self::acl_option_devices_tag_roll => 'Просмотр',
				self::acl_option_devices_tag_add => 'Добавление',
				self::acl_option_devices_tag_edit => 'Редактирование'
			),
			'Настройки Адресного классификатора' => array(
				self::acl_option_address_roll => 'Просмотр',
				self::acl_option_address_add => 'Добавление',
				self::acl_option_address_edit => 'Редактирование'
			),
			'Настройки Адреса участки' => array(
				self::acl_option_address_area_roll => 'Просмотр',
				self::acl_option_address_area_add => 'Добавление',
				self::acl_option_address_area_edit => 'Редактирование'
			),
			'Настройки Триггеры' => array(
				self::acl_option_triggers_roll => 'Просмотр',
				self::acl_option_triggers_add => 'Добавление',
				self::acl_option_triggers_edit => 'Редактирование'
			),
			'Настройки Интерфейсы' => array(
				self::acl_option_interfaces_roll => 'Просмотр',
				self::acl_option_interfaces_add => 'Добавление',
				self::acl_option_interfaces_edit => 'Редактирование'
			),
			'Настройки Оборудование' => array(
				self::acl_option_devices_conf_roll => 'Просмотр',
				self::acl_option_devices_conf_add => 'Добавление',
				self::acl_option_devices_conf_edit => 'Редактирование'
			),
			'Настройки Печатные формы' => array(
				self::acl_option_billprint_roll => 'Просмотр',
				self::acl_option_billprint_add => 'Добавление',
				self::acl_option_billprint_edit => 'Редактирование'
			),
			'Настройки Конфигурация' => array(
				self::acl_option_config_roll => 'Просмотр',
				self::acl_option_config_edit => 'Редактирование'
			),
			'Управление NetPing' => array(
				self::acl_netping_roll => 'Просмотр',
				self::acl_netping_add => 'Добавление',
				self::acl_netping_edit => 'Редактирование',
				self::acl_netping_actions => 'Управление'
			),
			'Управление VLan' => array(
				self::acl_network_vlan_roll => 'Просмотр',
				self::acl_network_vlan_edit => 'Редактирование'
			),
			'Управление Оборудованием' => array(
				self::acl_network_device_add	=> 'Добавление',
				self::acl_network_device_edit	=> 'Редактирование',
				self::acl_network_device_roll	=> 'Просмотр',
				self::acl_network_device_map	=> 'Карта',
				self::acl_network_device_portadmin	=> 'Управление Портами',
				self::acl_network_device_useradmin	=> 'Управление абонентами',
				self::acl_network_device_configuring => 'Конфигурирование'
			),
			'Управление Заявками' => array(
				self::acl_billing_request_roll	=> 'Просмотр',
				self::acl_billing_request_add	=> 'Добавление',
				self::acl_billing_request_edit	=> 'Редактирование'
			)
			
			
			
		);
		return $items;
	}
	
	static function has_perm($acl)
	{
		global $auth_user;
		if (isset($auth_user->acl_list[$acl]))
		{
			return $auth_user->acl_list[$acl];
		}
		else
		{
			return FALSE;
		}
	} 
	
	static function deny()
	{
		return View_Element::PanelError('<h2>Доступ запрещен!</h2>');
	}
}

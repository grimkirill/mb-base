<?php
/**
 * @author kirill
 * загрузка контроллера из $controller
 * 
 */

$uri_list = explode('/', $controller);
$controller_dir = BASEPATH.'app/';

$controller_path	= '';
$controller_name	= FALSE;
$controller_metod	= FALSE;
$controller_params	= array();

foreach ($uri_list as $item)
{
	if (!$controller_name ) 
	{
		if (file_exists($controller_dir.$controller_path.$item.'.php')){
			$controller_name = $item;
		}
		elseif (file_exists($controller_dir.$controller_path.$item.'/'))
		{
			$controller_path .= $item.'/';
		}
		else
		{
			//нет запрашиваемого контроллера
			break;
		}
	}
	else 
	{
		if (!$controller_metod)
		{
			$controller_metod = $item;
		}
		else 
		{
			$controller_params[] = $item;
		}
	}
}

if (!$controller_name)
{
	$controller_name = 'index';
}

if (!$controller_metod)
{
	$controller_metod = 'index';
}

define('CONTROLLER_PATH', $controller_path);
define('CONTROLLER_NAME', $controller_name);
define('CLASSPATH', $controller_path.$controller_name.'/');

if (file_exists($controller_dir.$controller_path.$controller_name.'.php'))
{
	include_once $controller_dir.$controller_path.$controller_name.'.php';
	if (class_exists($controller_name))
	{
		$load_controller = new $controller_name;
		if (method_exists($load_controller, $controller_metod))
		{
			call_user_func_array(array($load_controller, $controller_metod), $controller_params);
		}
		else
		{
			echo '404 Page not found!!!';
		}
	}
}


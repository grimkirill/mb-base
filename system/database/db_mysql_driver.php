<?php

/**
 * MySQLi database sample driver
 * @author kirill
 *
 */
class DB_MySql_Driver {
	
	private $username;
	private $password;
	private $hostname;
	private $database;
	private $char_set		= 'utf8';
	private $dbcollat		= 'utf8_general_ci';
	/**
	 * mysqli link
	 * @var mysqli
	 */
	public $conn_id;
	private $autoinit		= TRUE;
	
	/**
	 * Construct driver with params 
	 * @param array $params
	 */
	function __construct($params)
	{
		if (is_array($params))
		{
			foreach ($params as $key => $val)
			{
				$this->$key = $val;
			}
		}
	}
	
	
	/**
	 * Initialise connection and set params
	 * @return boolean
	 */
	function Initialize()
	{
		global $debug_log;
		if (is_resource($this->conn_id) OR is_object($this->conn_id))
		{
			return TRUE;
		}
		
		$this->conn_id = @new mysqli($this->hostname, $this->username, $this->password, $this->database);
		
		if (mysqli_connect_errno()) {
			//TODO: make debug
   	 		//printf("Connect failed: %s\n", mysqli_connect_error());
   	 		$debug_log[] = array('type' => 'Error', 'value' => mysqli_connect_error());
   	 		return FALSE;
		}
		if ($this->char_set)
		{
			if ($res = $this->conn_id->query("SET NAMES '".$this->char_set."' COLLATE '".$this->dbcollat."'") )
			{
				return TRUE;
			}
			else
			{
				$debug_log[] = array('type' => 'Error', 'value' => $sql.'<br>'.$this->conn_id->error);
			}
		}
		
	}
	
	
	/**
	 * @param string $sql
	 * @return MySQLi_Result
	 */
	function SimpleQuery($sql)
	{
		global $debug_log;
		if ($this->Initialize())
		{
			if ($result = $this->conn_id->query($sql))
			{
				if ($this->conn_id->warning_count)
				{
					$message = $sql;
					$e = $this->conn_id->get_warnings(); 
					   do { 
					       $message .= "<br>$e->errno: $e->message"; 
					   } while ($e->next()); 
					$debug_log[] = array('type' => 'Warning', 'value' => $message);
				}
				return $result;
			}
			else
			{
				$debug_log[] = array('type' => 'Error', 'value' => $sql.'<br>'.$this->conn_id->error);
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}	
	}
	
	/**
	 * @param string $sql
	 * @return Db_Mysql_Result
	 */
	function Query($sql)
	{
		return new Db_Mysql_Result($this->SimpleQuery($sql));
	}
	
	
	function GetInsertId()
	{
		return $this->conn_id->insert_id;
	}
	
	function GetAffectedRows()
	{
		if ($this->Initialize())
		{
			return $this->conn_id->affected_rows;
		}
	}
	
	function RealEscapeString($string)
	{
		if ($this->Initialize())
		{
			return $this->conn_id->real_escape_string($string);
		}
	}
	
	/**
	 * Escape value
	 * 
	 * @param unknown_type $value
	 * @return Ambigous <string, number, unknown>
	 */
	function EscapeValue($value)
	{
		$str = '';
		if (in_array($value, array( 'NOW()')))
		{
			$str = $value;
		}
		elseif (is_string($value))
		{
			$str = "'".$this->RealEscapeString($value)."'";
		}
		elseif (is_bool($value))
		{
			$str = ($value === FALSE) ? 0 : 1;
		}
		elseif (is_null($value))
		{
			$str = 'NULL';
		}
		elseif (is_numeric($value))
		{
			$str = $value;
		}
		return $str;
	}
	
	/**
	 * Generate an insert string
	 *
	 * @access	public
	 * @param	string	the table upon which the query will be performed
	 * @param	array	an associative array data of key/values
	 * @return	bool		
	 */	
	function InsertData($table, $data)
	{
		$fields = array();
		$values = array();
		
		foreach($data as $key => $val)
		{
			$fields[] = $key;
			$values[] = $this->EscapeValue($val);
		}
				
		$sql = "INSERT INTO ".$table." (".implode(', ', $fields).") VALUES (".implode(', ', $values).")";
		return $this->SimpleQuery($sql);
	}
	
	/**
	 * Generate an update string
	 *
	 * @access	public
	 * @param	string	the table upon which the query will be performed
	 * @param	array	an associative array data of key/values
	 * @param	array	an associative array data of key/values
	 * @return	bool		
	 */	
	function UpdateData($table, $data, $where)
	{
		if (!is_array($data))
		{
			return FALSE;
		}
		
		$valstr = array();
		foreach($data as $key => $val)
		{
			$valstr[] = '`'.$key."` = ".$this->EscapeValue($val);
		}
		
		$wherestr = array();
		if (is_array($where))
		{
			foreach($where as $key => $val)
			{
				$wherestr[] = "`".$key."` = ".$this->EscapeValue($val);
			}
		} 	 
		else 
		{
			$wherestr[] = $where;
		}
		
		$sql = "UPDATE ".$table." SET ".implode(', ', $valstr)." WHERE ".implode(' AND ', $wherestr);
		return $this->SimpleQuery($sql);
	}
	
	/**
	 * @param string $table
	 * @param array $where
	 * @return bool
	 */
	function DeleteData($table, $where)
	{
		$wherestr = array();
		if (is_array($where))
		{
			foreach($where as $key => $val)
			{
				$wherestr[] = $key." = ".$this->EscapeValue($val);
			}
		} 	 
		else 
		{
			$wherestr[] = $where;
		}	 
		
		$sql = "DELETE FROM  ".$table." WHERE ".implode(' AND ', $wherestr);
		return $this->SimpleQuery($sql);
	}
	
}


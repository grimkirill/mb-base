<?php


class Db_Mysql_Result {
	
	/**
	 * @var MySQLi_Result
	 */
	public $result;
	public $last_row;
	
	function __construct($res)
	{
		
		$this->result = $res;
	}
	
	function __destruct()
	{
		if (is_object($this->result))
		{
			$this->result->free();
		}
	}
	
	/**
	 * @return array 
	 */
	function get_next_row_array()
	{
		if (is_object($this->result))
		{
			$this->last_row = $this->result->fetch_assoc();
			return $this->last_row;
		}
		else
		{
			return FALSE;
		}
	}
	
	/**
	 * @return object 
	 */
	function get_next_row_object()
	{
		if (is_object($this->result))
		{
			$this->last_row = $this->result->fetch_object();
			return $this->last_row;
		}
		else
		{
			return FALSE;
		}
	}
	
	/**
	 * @return int 
	 */
	function get_rows_count()
	{
		if (is_object($this->result))
		{
			return $this->result->num_rows;
		}
		else
		{
			return FALSE;
		}
	}
	
	function get_fields_count()
	{
		if (is_object($this->result))
		{
			return $this->result->field_count;
		}
		else
		{
			return FALSE;
		}
	}
	
	function set_row_seek($offset = 0)
	{
		if (is_object($this->result))
		{
			return $this->result->data_seek($offset);
		}
		else
		{
			return FALSE;
		}
	}
	
	/**
	 * Return all data as assoc array
	 * 
	 * @return array
	 */
	function get_rows_array(){
		if ($this->set_row_seek(0))
		{
			$res = array();
			while($row = $this->result->fetch_assoc())
			{
				$res[] = $row;
			}
			return $res;
		}
		else 
		{
			return FALSE;
		}
	}
	
	
	function get_rows_array_id($key_name = 'id', $value_name)
	{
		if ($this->set_row_seek(0))
		{
			$res = array();
			while($row = $this->result->fetch_assoc() )
			{
				$res[$row[$key_name]] = $row[$value_name];
			}
			return $res;
		}
		else 
		{
			return FALSE;
		}	
	}	
	
	
}
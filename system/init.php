<?php

$system_folder = realpath(dirname(dirname(__FILE__))).'/';
$system_folder = str_replace("\\", "/", $system_folder); 
define('BASEPATH', $system_folder);
$debug_log = array();

include_once BASEPATH.'conf.php';
include_once BASEPATH.'structure.php';
include_once BASEPATH.'system/loader.php';

$db = new DB_MySql_Driver(array(
	'hostname' => $conf_global['db_host'], 
	'username' => $conf_global['db_username'], 
	'password' => $conf_global['db_password'], 
	'database' => $conf_global['db_database']
));
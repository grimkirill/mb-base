<?php

$library_array['DB_MySql_Driver'] = 'database/db_mysql_driver.php';
$library_array['Db_Mysql_Result'] = 'database/db_mysql_result.php';

$library_array['View'] = 'view/view.php'; 
$library_array['View_Page'] = 'view/view_page.php';
$library_array['View_Page_Controller'] = 'view/view_page_controller.php';
$library_array['View_Table'] = 'view/view_table.php';
$library_array['View_Properties'] = 'view/view_properties.php';
$library_array['View_Element'] = 'view/view_element.php';
$library_array['View_Form'] = 'view/view_form.php';
$library_array['View_Form_Element'] = 'view/view_form_element.php';
$library_array['View_Form_Text'] = 'view/form/view_form_text.php';
$library_array['View_Form_Select'] = 'view/form/view_form_select.php';
$library_array['View_Form_Checkbox'] = 'view/form/view_form_checkbox.php';
$library_array['View_Form_Date'] = 'view/form/view_form_date.php';
$library_array['View_Form_Password'] = 'view/form/view_form_password.php';
$library_array['View_Form_Textarea'] = 'view/form/view_form_textarea.php';
$library_array['View_Form_Color'] = 'view/form/view_form_color.php';
$library_array['View_Form_Time'] = 'view/form/view_form_time.php';
$library_array['View_Form_WYSIWYG'] = 'view/form/view_form_WYSIWYG.php';

$library_array['TCPDF'] = 'lib/tcpdf/tcpdf.php';


function __autoload($class_name)
{
	global $library_array;
	if (isset($library_array[$class_name]))
	{
		include_once BASEPATH.'system/'.$library_array[$class_name];
	}
	else 
	{
		if (file_exists(BASEPATH.'models/'.$class_name.'.php'))
		{
			include_once BASEPATH.'models/'.$class_name.'.php';
		}
	}
}
<?php

//auto start session
mySession_Start();

function mySession_IsStarted()
{
	return isset($_SESSION);
}

function mySession_Start()
{
	if (!mySession_IsStarted())
	{
		session_start();
	}
}

function mySession_Stop()
{
	if (mySession_IsStarted())
	{
		session_destroy();
	}
}

function mySession_SetValue($key, $value)
{
	if (mySession_IsStarted())
	{
		if ($key != '')
		{
			$_SESSION[$key] = $value;
			return TRUE;
		}
		else
		{
			return NULL;
		}
	}
	else
	{
		return NULL;
	}
}

function mySession_GetValue($key)
{
	if (!mySession_IsStarted())
	{
		mySession_Start();
	}
	
	if ($key != '')
	{
		return isset($_SESSION[$key]) ? $_SESSION[$key] : '';
	}
	else
	{
		return NULL;
	}
}

function mySession_HasKey($key)
{
	if (mySession_IsStarted())
	{
		return isset($_SESSION[$key]);
	}
	else
	{
		return NULL;
	}
}

function mySession_UnsetValue($key)
{
	if (mySession_IsStarted())
	{
		unset($_SESSION[$key]);
	}
}



class User_Auth {
	public $id;
	public $username;
	//data from database
	private $user;
	private $session_username;
	private $session_password;
	private $session_host;
	
	public $acl_list = array();
	
	
	function __construct()
	{
		global $conf_global;
		$this->session_username = $conf_global['salt'].'session_username';
		$this->session_password = $conf_global['salt'].'session_password';
		$this->session_host = $conf_global['salt'].'session_host';
		
		if (isset($_POST[$conf_global['salt'].'_do_login']))
		{
			$this->login($_POST['username'], $_POST['password']);
		}
		else
		{
			$this->resume();
		}
	}
	
	static function get_loginid($id)
	{
		global $db;
		if ($id)
		{
			if ($user = $db->Query('SELECT username FROM users WHERE id = '.$id)->get_next_row_object()->username)
			{
				return $user;
			}
		}
		else
		{
			return '';
		}
	} 
	
	function auth()
	{
		mySession_SetValue($this->session_username, $this->user->username);
		mySession_SetValue($this->session_password, $this->user->password);
		mySession_SetValue($this->session_host, $_SERVER['REMOTE_ADDR']);
	}
	
	function isLogin()
	{
		if ($this->id)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	function logout()
	{
		if ($this->isLogin())
		{
			mySession_UnsetValue($this->session_username);
			mySession_UnsetValue($this->session_password);
			mySession_UnsetValue($this->session_host);
			$this->id = FALSE;
		}
	}
	
	function check_other()
	{
		if (!is_object($this->user))
		{
			return FALSE;
		}
		
		if ($this->user->block == 1)
		{
			return FALSE;
		}
		
		if (!preg_match($this->user->allowip, $_SERVER['REMOTE_ADDR']))
		{
			return FALSE;
		}
		
		return TRUE;
	}
	
	
	
	
	function resume()
	{
		global $db;
		if (mySession_GetValue($this->session_username))
		{
			
			$username = mySession_GetValue($this->session_username);
			if ($this->user = $db->Query('SELECT * FROM users WHERE username = '.$db->EscapeValue($username))->get_next_row_object())
			{
				if ( (mySession_GetValue($this->session_password) == $this->user->password) AND
					 (mySession_GetValue($this->session_host) == $_SERVER['REMOTE_ADDR']) AND 
					 ($this->check_other())
					)
				{
					$this->init();
				}
			}
		}
	}
	
	
	function login($username, $password)
	{
		global $db;
		if ($username != '')
		{
			if ($this->user = $db->Query('SELECT * FROM users WHERE username = '.$db->EscapeValue($username))->get_next_row_object())
			{
				if ((md5($password) == $this->user->password) AND ($this->check_other()))
				{
					//$db->insert_string('user_loging', array('user'=> $this->user->id, 'dt' => 'NOW()', 'ip' => $_SERVER['REMOTE_ADDR']));
					//$db->update_string('user',array('dt'=> 'NOW()'), array('id' => $this->user->id));
					$this->auth();
					$this->init();
					$db->UpdateData('users', array('date' => 'NOW()'), array('id' => $this->id));
					$db->InsertData('users_logon', array('user' => $this->id, 'ip' => $_SERVER['REMOTE_ADDR']));
					return TRUE;
				}
			}
		}
		return FALSE;
	}
	
	function init()
	{
		global $db;
		$this->id = $this->user->id;
		$this->username = $this->user->username;
		$this->acl_list = $db->Query('SELECT acl, value FROM users_acls WHERE gid = '.$this->user->gid)->get_rows_array_id('acl', 'value'); 
		//parent::USER_group($this->user->gid);
	}
	
	
	function Show_Login_Page()
	{
		global $conf_global;
		echo '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Авторизация</title>
<script type="text/javascript">

function focusIt()
{
  var mytext = document.getElementById("login");
  mytext.focus();
}

onload = focusIt;

</script>

</head>
<body>

<center>
<h2>Авторизация</h2>
<div style="width: 20em; border: 2px solid rgb(177, 177, 177); -moz-border-radius: 0px 12px 0px 12px; -webkit-border-radius: 0px 12px 0px 12px;  padding: 1em 1em 1em 1em;" align="left">

	<form action="" method="post" >
	<p>Cистема операционного управления и контроля сетевой инфраструктуры.</p>
	<p style="border-bottom: 1px dotted rgb(177, 177, 177); padding-bottom: 4px;">
		<label style="display: inline-block; width: 6em; ">Логин:</label>
		<input type="text" name="username" id="login">
	</p>
	
	<p style="border-bottom: 1px dotted rgb(177, 177, 177); padding-bottom: 4px;">
		<label style="display: inline-block; width: 6em; ">Пароль:</label>
		<input type="password" name="password">
	</p>
	
	<p align="right">
		<input type="submit" value="Войти" name="'.$conf_global['salt'].'_do_login">
	</p>
	</form>
	
</div>
</center>

</body>
</html>';
	}
	
}

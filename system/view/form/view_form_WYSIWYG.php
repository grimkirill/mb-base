<?php

class View_Form_WYSIWYG extends View_Form_Element {
	
	public $min = FALSE;
	
	function out()
	{
		$str = '<textarea rows="1" cols="50" class="text" id="'.$this->name.'" name="'.$this->name.'" >'.$this->value.'</textarea>
		 <script type="text/javascript">    $(document).ready(function() { 
		  $("#'.$this->name.'").cleditor({     ';

		if ($this->min)
		{
		$str .= 'width:        500, 
         		 height:       135,
         		 controls:     
                        "bold italic underline strikethrough subscript superscript | font size " +
                        "style | color highlight removeformat | undo redo | " +
                        "link unlink | cut copy paste pastetext "';
		}
		else
		{
			$str .= 'width:        780, 
         			 height:       500';
		}
		
        $str .= '});  });</script>';
		
		return $str;
	}
	
}
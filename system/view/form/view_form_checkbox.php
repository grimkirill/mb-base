<?php

class View_Form_Checkbox extends View_Form_Element {
	
	function out()
	{
		$str = '<label>	<input type="checkbox" name="'.$this->name.'" value="on"';
		if ($this->value)
		{
			$str .= ' checked="checked" ';
		}
		$str .= ' > '.$this->caption.' </label>';
			
		return $str;
	}

	function get_value($type = 'post')
	{
		$result = 0;
		if ($type == 'post')
		{
			if (isset($_POST[$this->name]))
			{
				$result = ($_POST[$this->name] == 'on');
			}
		}
		else
		{
			if (isset($_GET[$this->name]))
			{
				$result = ($_GET[$this->name] == 'on');
			}
		}
		$this->value = $result;
		return $result;
	}
}

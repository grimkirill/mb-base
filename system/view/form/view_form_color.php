<?php

class View_Form_Color extends View_Form_Element {
	
	
	public $width = FALSE;
	public $other = '';
	
	function out()
	{
		$str = '<input class="text colors" name="'.$this->name.'" type="text" value="'.$this->value.'" size="7" title="'.$this->caption.'" '.$this->other.' style="';
		if ($this->width)
		{
			$str .= 'width: '.$this->width.'; ';
		}
		$str .= '">';
		$str .= '<script type="text/javascript"> $(document).ready(function(){ $(".colors").miniColors() } ); </script>';
		return $str;
	}
	

}
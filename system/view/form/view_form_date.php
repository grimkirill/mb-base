<?php

class View_Form_Date extends View_Form_Element {
	
	function out()
	{
		$result = '';
		if ($this->value)
		{
			if (preg_match('/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/',trim($this->value), $vals)){
				$result = $vals[3].'-'.$vals[2].'-'.$vals[1];
			}
		}
				
		$str = '<input class="text date" name="'.$this->name.'" type="text" value="'.$result.'" style="width: 6em; cursor: pointer;"  title="'.$this->caption.'">';
		$str .= '<script type="text/javascript"> $(document).ready(function(){ $(\'.date\').jdPicker(); } ); </script>';
		return $str;
	}
	
	
	function get_value($type = 'post')
	{
		$result = FALSE;
		if ($type == 'post')
		{
			if (isset($_POST[$this->name]))
			{
				$result = $_POST[$this->name];
			}
			else
			{
				$this->error = TRUE;
			}
		}
		else
		{
			if (isset($_GET[$this->name]))
			{
				$result = $_GET[$this->name];
			}
			else
			{
				$this->error = TRUE;
			}
		}
		if ($result)
		{
			if (preg_match('/^([0-9]{2})-([0-9]{2})-([0-9]{4})$/',trim($result), $vals)){
				$result = $vals[3].'-'.$vals[2].'-'.$vals[1];
			}
		}
		
		$this->value = $result;
		return $result;
	}
}
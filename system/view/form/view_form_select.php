<?php

class View_Form_Select extends View_Form_Element {
	public $options = array();
	public $other = ''; 
	
	function out()
	{
		$str = '<select name="'.$this->name.'"  title="'.$this->caption.'" '.$this->other.'>';
		if (count($this->options) > 0)
		{
			foreach ($this->options as $key => $item)
			{
				$str .= '<option value="'.$key.'" ';
				if ($key == $this->value)
				{
					$str .= 'selected="selected"';
				}
				$str .= '>'.$item.'</option>';
			}
		}
		
		$str .= '</select>';
		return $str;
	}
	
}


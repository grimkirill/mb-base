<?php

class View_Form_Text extends View_Form_Element {
	public $width = FALSE;
	public $other = '';
	public $mask = '';
	function out()
	{
		$str = '<input class="text" id="'.$this->id.'" name="'.$this->name.'" type="text" value="'.$this->value.'" title="'.$this->caption.'" '.$this->other.' style="';
		if ($this->width)
		{
			$str .= 'width: '.$this->width.'; ';
		}
		$str .= '">';
		if ($this->mask)
		{
			$str .= '<script type="text/javascript"> $(document).ready(function(){ $(\'#'.$this->id.'\').mask(\''.$this->mask.'\'); } ); </script>';
		}
		
		return $str;
	}
}
<?php

class View_Form_Textarea extends View_Form_Element
{
	public $width = FALSE;
	
	function out()
	{
		$str = '<textarea rows="1" cols="50" class="text" name="'.$this->name.'" style="';
		if ($this->width)
		{
			$str .= 'width: '.$this->width.'; ';
		}
		$str .= '" >'.$this->value.'</textarea>';
		return $str;
	}
}
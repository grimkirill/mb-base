<?php

class View_Form_Time extends View_Form_Element {
	public $width = FALSE;
	public $other = '';
	function out()
	{
		$result = '00:00';
		if ($this->value)
		{
			$result = substr($this->value,0 , 5);
		}
		
		$str = '<input class="text timeselect" name="'.$this->name.'" type="text" value="'.$result.'" title="'.$this->caption.'" '.$this->other.' size="5" style="';
		if ($this->width)
		{
			$str .= 'width: '.$this->width.'; ';
		}
		$str .= '">';
		$str .= '<script type="text/javascript"> $(document).ready(function(){ $(".timeselect").timeEntry({spinnerImage: \'\', show24Hours: true}); } ); </script>';
		return $str;
	}
	
	
	function get_value($type = 'post')
	{
		$result = FALSE;
		if ($type == 'post')
		{
			if (isset($_POST[$this->name]))
			{
				$result = $_POST[$this->name];
			}
			else
			{
				$this->error = TRUE;
			}
		}
		else
		{
			if (isset($_GET[$this->name]))
			{
				$result = $_GET[$this->name];
			}
			else
			{
				$this->error = TRUE;
			}
		}
		
		if ($result)
		{
			$result = $result.':00';
		}
		else
		{
			$result = '00:00:00';
		}
		
		$this->value = $result;
		return $result;
	}
}
<?php

function generate_view_id($length = 8){
  $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
  $numChars = strlen($chars);
  $string = '';
  for ($i = 0; $i < $length; $i++) {
    $string .= substr($chars, rand(1, $numChars) - 1, 1);
  }
  return $string;
}

/**
 * abstract class of all view class
 * @author kirill
 *
 */
abstract  class View {
	private $contents;
	
	function out()
	{
		return implode('', $this->contents);
	}
	
	function __construct($params = FALSE)
	{
		if (is_array($params))
		{
			foreach ($params as $key => $val)
			{
				$this->$key = $val;
			}
		}
	}
	
	function AddContent()
	{
		$args = func_get_args();
		
		foreach ($args as $items)
		{
			if (is_array($items))
			{
				foreach ($items as $item)
				{
					$this->contents[] = $item;
				}
			}
			else 
			{ 
				$this->contents[] = $items;
			}
		}
	}
	
	function __toString()
	{
		return $this->out();
	}
	
}
<?php

class View_Element {
	
	static function Link($caption, $href, $new_page = FALSE, $bolder = FALSE)
	{
		$new = '';
		if ($new_page)
		{
			$new .= ' target="_blank" ';
		}
		if ($bolder)
		{
		    $new .= ' class="bold_link" ';
		}
		return '<a '.$new.' href="'.WEBPATH.$href.'" >'.$caption.'</a>';
	}
	
	
	static function Button($caption, $href)
	{
		return '<button style="cursor: pointer;" onclick="location.href=\''.WEBPATH.$href.'\';">'.$caption.'</button>';
	}
	
	static function SpanEdit($value, $url, $mask ='')
	{
		$out_value = $value;
		$color = '';
		if ($value == '')
		{
			$out_value = 'Не установлено';
			$color = 'color: #db9200;';
		}
		$str = '<span style="cursor: pointer; '.$color.'" 
		onclick="make_edit(this, \''.$value.'\', \''.$url.'\', \''.$mask.'\');">'.$out_value.'</span>';
		return $str;
	}

	static function ButtonLoad($caption, $url, $href = '')
	{
		if ($href)
		{
			return '<button style="cursor: pointer;" onclick="
			$.get(\''.WEBPATH.$url.'\', function(data) {
			location.href=\''.WEBPATH.$href.'\';
			}); ">'.$caption.'</button>';
		}
		else 
		{
			return '<button style="cursor: pointer;" onclick="
			$.get(\''.WEBPATH.$url.'\', function(data) {
			window.location.reload();
			});">'.$caption.'</button>';
		}
	}
	
	
	static function boxyLoad($caption, $url)
	{
		return self::SpanClick($caption, 'Boxy.load(\''.WEBPATH.$url.'\', {title: \'  \', modal: true})');
	}
	
	static function submit_link($caption, $title, $url)
	{
		return self::SpanClick($caption, '
    Boxy.ask(\''.$title.'\', [ \'OK\', \'Отмена\'], function(val) {
    	if (val == \'OK\')
    	{
    		$(\'.container\').html(\'<h3>Загрузка...</h3>\');
    		$.get(\''.WEBPATH.$url.'\', function(data) {
			window.location.reload(); });
      	}       
    }, {title: \'Подтверждение\'});
    return false;   
		');
	}
	
	static function submit_action($caption, $title, $action)
	{
		return self::SpanClick($caption, '
    Boxy.ask(\''.$title.'\', [ \'OK\', \'Отмена\'], function(val) {
    	if (val == \'OK\')
    	{
    		'.$action.'
      	}       
    }, {title: \'Подтверждение\'});
    return false;   
		');
	}
	
	static function boxyask($caption)
	{
		return self::SpanClick($caption, '
		
    Boxy.ask(\'How are you feeling?\', [\'Great\', \'OK\', \'Not so good\'], function(val) {
      alert(\'You chose: \' + val);       
    }, {title: \'This is a question...\'});
    return false;   
		');
	}
	
	static function ColorPanel($color)
	{
		return '<span style="background-color: '.$color.';">&nbsp;&nbsp;&nbsp;&nbsp;</span>';
	}
	
	static function Space($count = 1)
	{
		return implode('', array_fill(0, $count, '&nbsp;'));
	}
	
	static function SpanClick($caption, $action)
	{
		return '<span style="cursor: pointer; color: #09f; border-bottom: 1px dotted #09f;" onclick="'.$action.'">'.$caption.'</span>';
	}
	
	static function PanelError($content)
	{
		return '<div class="error">'.$content.'</div>';
	}
	
	static function PanelNotice($content)
	{
		return '<div class="notice">'.$content.'</div>';
	}
	
	static function PanelInfo($content)
	{
		return '<div class="info">'.$content.'</div>';
	}
	
	static function PanelSuccess($content)
	{
		return '<div class="success">'.$content.'</div>';
	}
	
	static function PanelStandart($content)
	{
		return '<div style="padding:0.8em;margin-bottom:1em;border:2px solid #ccc;">'.$content.'</div>';
	}
	
	static function PanelClear($content)
	{
		return '<div style="padding-left:1.5em;">'.$content.'</div>';
	}
	
	static function PanelCollapse($caption, $content, $collapse = TRUE)
	{
		$hide = '';
		$id = generate_view_id();
		if ($collapse)
		{
			$hide = 'hide';
		}
		$str = '<div style="border: 2px solid #ccc;">
			<div style="background-color: #e5ecf9; padding:4px 10px 4px 5px;">
				<span onclick="$(\'#'.$id.'\').toggleClass(\'hide\');" style="cursor: pointer; display: block;">
				<small>▼ </small>'.$caption.'</span>
			</div>
			<div id="'.$id.'" style="border-top: 1px solid #ccc; padding:4px 4px 4px 5px;" class="'.$hide.'">
				'.$content.'
			</div>
		</div>';
		
		return $str;
	}
	
	static function Tabs($data)
	{
		$selected = 'selected';
		
		if (count($data) > 0)
		{
			$str = '<div class="idTabs"> <ul>'; 
			foreach ($data as $key => $value)
			{
				if ($value)
				{
					$str .= '<li><a class="'.$selected.'" href="#tab'.$key.'">'.$key.'</a></li>'; 
					if ($selected)
					{
						$selected = '';
					}
				}
			}
			
			$str .= '</ul><hr>';
			
			foreach ($data as $key => $value)
			{
				if ($value)
				{
					$str .= ' <div id="tab'.$key.'">'.$value.'</div>';
				} 
			}
			$str .= '</div> <script type="text/javascript">  $(".idTabs ul").idTabs(); </script>';
			return $str;
		}
		
	}
	
	static function image_stop()
	{
		return '<img alt="" src="'.WEBPATH.'web/ico/stop.png" style="border: none; margin-bottom: -3px;" >';
	}
	
	static function image_active()
	{
		return '<img alt="" src="'.WEBPATH.'web/ico/shape_square.png" style="border: none; margin-bottom: -3px;" >';
	}
	
	static function image_connect()
	{
		return '<img alt="" src="'.WEBPATH.'web/ico/connect.png" style="border: none; margin-bottom: -3px;" >';
	}
	
	static function image_disconnect()
	{
		return '<img alt="" src="'.WEBPATH.'web/ico/disconnect.png" style="border: none; margin-bottom: -3px;" >';
	}
	
	static function image_online()
	{
		return '<img alt="" src="'.WEBPATH.'web/ico/world.png" style="border: none; margin-bottom: -3px;" >';
	}
	
	static function image_cross()
	{
		return '<img alt="" src="'.WEBPATH.'web/ico/cross.png" style="border: none; margin-bottom: -4px;" >';
	}
	
	static function image_lock()
	{
		return '<img alt="" src="'.WEBPATH.'web/ico/lock.png" style="border: none; margin-bottom: -4px;" >';
	}
	
	static function image_arrow_left()
	{
		return '<img alt="" src="'.WEBPATH.'web/ico/arrow_left.png" style="border: none; margin-bottom: -3px;" >';
	}
	
	static function image_arrow_right()
	{
		return '<img alt="" src="'.WEBPATH.'web/ico/arrow_right.png" style="border: none; margin-bottom: -3px;" >';
	}
	
	static function image_exclamation()
	{
		return '<img alt="" src="'.WEBPATH.'web/ico/exclamation.png" style="border: none; margin-bottom: -3px;" >';
	}
	
	static function image_marker()
	{
		return '<img alt="" src="'.WEBPATH.'web/ico/marker.png" style="border: none; margin-bottom: -3px;" >';
	}
	
	static function image_add()
	{
		return '<img alt="" src="'.WEBPATH.'web/ico/add.png" style="border: none; margin-bottom: -3px;" >';
	}
	
	static function image_refresh()
	{
		return '<img alt="" src="'.WEBPATH.'web/ico/arrow_refresh.png" style="border: none; margin-bottom: -3px;" >';
	}
}
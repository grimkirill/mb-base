<?php

class View_Form extends View {
	
	public $caption = FALSE;
	public $method  = 'post';
	public $action  = FALSE;
	public $border  = FALSE;
	public $cancel  = FALSE;
	public $submit  = 'Принять';
	public $rows = array();
	public $return_values;
	public $validators = array();
	public $oneline = FALSE;  // вывод формы в одну строку
	
	function add($caption = '', $items)
	{
		$this->rows[] = array('caption' => $caption, 'items' => $items);
	}
	
	function get_values()
	{
		$read = FALSE;
		if ($this->method == 'post')
		{
			if ( isset($_POST['form_submit']) and ($_POST['form_submit'] == $this->submit) )
			{
				$read = TRUE;
			}
		}
		else
		{
			if ( isset($_GET['form_submit']) and ($_GET['form_submit'] == $this->submit))
			{
				$read = TRUE;
			}
		}
		if (($read) AND (count($this->rows) > 0))
		{
			foreach ($this->rows as $row)
			{	
				foreach ($row['items'] as $item)
				{
					if (is_object($item))
					{
						$this->return_values[$item->name] = $item->get_value($this->method);
						if ($item->error)
						{
							$this->return_values = '';
							return FALSE;
						}
						if ($valid = $item->Validate())
						{
							$this->validators = array_merge($this->validators, $valid);
						}
					}
				}
			}
		}
		
		if (count($this->validators) == 0)
		{
			return $read;
		}
		else
		{
			return FALSE;
		}
	}
	
	
	function out()
	{
		$str = '';
		
		if (count($this->validators) > 0)
		{
			$str .= View_Element::PanelNotice(implode('<br>', $this->validators));
		}
		$str .= '<div class="form_panel" style="padding:0.8em;';
		if ($this->border) 
		{
			 $str .= 'border:2px solid #ddd; ';
		}
		$str .= 'margin-bottom:1em;" >';
		
		if ($this->caption)
		{
			$str .= '<h3 onclick="$(this).parent().children(\'form\').toggleClass(\'hide\');">'.$this->caption.'</h3>';
		}
		
		$str .= '<form action="'.$this->action.'" method="'.$this->method.'">';
		
		if (count($this->rows) > 0)
		{
			foreach ($this->rows as $row)
			{
				if (!$this->oneline)
				{
					$str .= '<p>';
				}
				if ($row['caption'])
				{
					$str .= '<b>'.$row['caption'].'</b><br>';
				}			
				foreach ($row['items'] as $item)
				{
					if (is_object($item))
					{
						$str .= $item->out($this->method);
					}
					else
					{
						$str .= $item;
					}
				}
				
				if (!$this->oneline)
				{
					$str .= '</p>';
				}
			}
		}
		
		if ($this->oneline)
		{
			$str .= View_Element::Space(2);
		}
		$str .= '<input type="submit" name="form_submit" class="button" value="'.$this->submit.'">';
		if ($this->cancel)
		{
			$str .= ' '.View_Element::Link('Отмена', $this->cancel);
 		}
		$str .= '</form>';
		$str .= '</div>';
		return $str;
	}
	
}
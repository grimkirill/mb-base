<?php

class View_Form_Element {
	public $name;
	public $caption;
	public $value;
	public $error = FALSE;
	public $validators = array();
	public $id;
	const validator_require = 'require';
	const validator_min_length = 'min_length';
	
	function __construct($name, $value = FALSE, $params = FALSE)
	{
		$this->name = $name;
		$this->value = $value;
		$this->id = uniqid();
		if (is_array($params))
		{
			foreach ($params as $key => $val)
			{
				$this->$key = $val;
			}
		}
		
	}
	
	function Validate()
	{
		$errors = FALSE;
		if (count($this->validators) > 0)
		{
			foreach ($this->validators as $type => $item)
			{
				if ($type == self::validator_require)
				{
					if (!$this->value)
					{
						if (is_array($item))
						{
							$errors[] = 'Поле '.$this->caption.' обязательно для заполнения.';
						}
						else
						{
							$errors[] = 'Поле '.$this->caption.' обязательно для заполнения.';
						}
					}
					
				}
			}
		}
		return $errors;
	}
	
	function get_value($type = 'post')
	{
		$result = FALSE;
		if ($type == 'post')
		{
			if (isset($_POST[$this->name]))
			{
				$result = $_POST[$this->name];
			}
			else
			{
				$this->error = TRUE;
			}
		}
		else
		{
			if (isset($_GET[$this->name]))
			{
				$result = $_GET[$this->name];
			}
			else
			{
				$this->error = TRUE;
			}
		}
		$this->value = $result;
		return $result;
	}
	
	function out()
	{
		return '';
	}
	
	function __toString()
	{
		return $this->out();
	} 
	
}

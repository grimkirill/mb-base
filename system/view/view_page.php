<?php

class View_Page extends View{
	public $title;
	public $rootMenu;
	public $leftMenu;
	public $out_left_menu = TRUE;
	public $category = FALSE;
	public $content = '';
	private $Header = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
	"http://www.w3.org/TR/html4/strict.dtd">

	<html lang="en">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>%title%</title>
	<!-- Framework CSS -->
	<link rel="stylesheet" href="BASEPATHweb/css/screen.css" type="text/css" media="screen, projection">
	<link rel="stylesheet" href="BASEPATHweb/css/print.css" type="text/css" media="print">
	<!--[if lt IE 8]><link rel="stylesheet" href="BASEPATHweb/css/ie.css" type="text/css" media="screen, projection"><![endif]-->
	
	<link rel="stylesheet" href="BASEPATHweb/css/jdpicker.css" type="text/css">
	<link rel="stylesheet" href="BASEPATHweb/css/jquery.cleditor.css" type="text/css">
	<link rel="stylesheet" href="BASEPATHweb/css/jquery.miniColors.css" type="text/css">
	<link rel="stylesheet" href="BASEPATHweb/css/boxy.css" type="text/css">
	<link rel="stylesheet" href="BASEPATHweb/css/google-buttons.css" type="text/css"  media="screen" />
	
	
	<script type="text/javascript" src="BASEPATHweb/js/jquery.js"></script>
	<script type="text/javascript" src="BASEPATHweb/js/jquery.idTabs.js"></script>
	<script type="text/javascript" src="BASEPATHweb/js/jquery.jdpicker.js"></script>
	<script type="text/javascript" src="BASEPATHweb/js/jquery.cleditor.min.js"></script>
	<script type="text/javascript" src="BASEPATHweb/js/jquery.cleditor.table.min.js"></script>
	<script type="text/javascript" src="BASEPATHweb/js/functions.js"></script>
	<script type="text/javascript" src="BASEPATHweb/js/sorttable.js"></script>
	<script type="text/javascript" src="BASEPATHweb/js/jquery.miniColors.js"></script>
	<script type="text/javascript" src="BASEPATHweb/js/jquery.timeentry.min.js"></script>
	<script type="text/javascript" src="BASEPATHweb/js/jquery.maskedinput-1.3.js"></script>
	<script type="text/javascript" src="BASEPATHweb/js/jquery.boxy.js"></script>
	<script type="text/javascript" src="BASEPATHweb/js/bootstrap-dropdown.js"></script>
	</head>
	<body style="background: none repeat scroll 0 0 #F1F1F1;">
	<script type="text/javascript">
		$(document).ready(function() 
    		{ 
        		//$(".sortable").tablesorter();
    		} 
		); 
    
		var WEBPATH = \'BASEPATH\';
	</script>  
	<div class="container" style="background: none repeat scroll 0 0 #FFFFFF; height: 100%">';
	
	private $Footer = '</div> 
	<div style="span-24 last" class="dispaly_print_none"><br><center>© <a href="http://noadmin.ru" target="new">Скатов К.Д. 2011</a></center> </div>
	<div style="position: fixed; height:100%; width:100%; left: 0; top: 40px;" id="box_show_info" class="hide">
	<div style="display:table; margin:0 auto; border: 2px solid #ddd; padding: 10px; background-color: #eee; width: 900px; height: 700px; overflow: scroll;" >
	<center><a href="#" onclick="$(\'#box_show_info\').hide()">Закрыть</a></center><hr>
		<pre style="width: 880px; height: 650px; overflow: scroll; position:relative;" id="box_show_info_content"> </pre> 
	</div>
	</div>

	</body> </html>	';
	
	private $Boxs = array();
	
	function AddBox($content, $size = 1, $last = FALSE, $other = FALSE)
	{
		$str = '<div class="span-'.$size;
		if ($last)
		{
			$str .= ' last';
		}
		if ($other)
		{
			$str .= ' '.$other;
		}
		$str .= '">';
		if (is_object($content))
		{
			$str .= $content->out();
		}
		elseif (is_array($content))
		{
			$str .= implode('', $content);
		}
		else 
		{
			$str .= $content;
		}
		$str .= '</div>';
		$this->Boxs[] = $str;
	}
	
	function _PrepareRootMenu()
	{
		global $structure_root_menu, $auth_user;
		$this->rootMenu = '<div class="span-19" id="root_nav_div_menu" > ';
		
		foreach ($structure_root_menu as $link => $caption)
		{
			$this->rootMenu .= View_Element::Link($caption, $link, FALSE, ($link == $this->category)).View_Element::Space(4);
		}
		$ver_str = (string)VERSION;
		if (VERSION < 1000) {
			$ver_str = '0'.$ver_str;
		}
		
		$this->rootMenu .= View_Element::Space(2).date('d-m-Y H:i');
		//$this->rootMenu .= View_Element::Space(12).'<a href="http://noadmin.ru" target="new"><img alt="" src="'.WEBPATH.'web/css/logo.png" style="border: none; margin-bottom: -4px;" > </a>';
		$this->rootMenu .= '</div>  <div class="span-5 last" align="right" id="root_nav_div_user">'.
		'<img alt="" src="'.WEBPATH.'web/ico/printer.png" style="border: none; margin-bottom: -4px; cursor: pointer;" onclick="
			if ($(\'#left_nav_div\').hasClass(\'hide\'))
			{
				$(\'#left_nav_div\').removeClass(\'hide\');  $(\'form\').show();
				$(\'#main_contetnt_div\').removeClass(\'span-24\').addClass(\'span-20\');
			}
			else
			{
				$(\'#left_nav_div\').addClass(\'hide\');  $(\'form\').hide();
				$(\'#main_contetnt_div\').removeClass(\'span-20\').addClass(\'span-24\');
				//window.print();
			}" >'.View_Element::Space(2)
		.'ver '.substr($ver_str, 0, 1).'.'.substr($ver_str, 1, 1).'.'.substr($ver_str, 2, 1).' ['.$auth_user->username.'] '.View_Element::Link('exit','logout').'</div>
		 <hr style="border-top: 2px solid #888; padding-top: 2px; margin-top: 4px;">';
	}
	
	function _PrepareLeftMenu()
	{
		global $structure_left_menu;
		$this->leftMenu = '<div id="left_nav_div" class="span-4 border">';
		if (!$this->category)
		{
			$keys = array_keys($structure_left_menu);
			$this->category = $keys[0];
		}
		$this->leftMenu .= '<dl> <dt>Навигация </dt> <dl> ';
		
		foreach ($structure_left_menu[$this->category] as $link => $caption)
		{
			$this->leftMenu .= '<dd style="border-bottom: 1px solid #ddd; margin-bottom: 4px;">'.View_Element::Link($caption, $link).'</dd>';
		}
		
		$this->leftMenu .= '</dl>';
		$this->leftMenu .= '</div>';
	}
	
	function GetDebugLog()
	{
		global $debug_log;
		if (count($debug_log) > 0)
		{
			$table = new View_Table(array('caption' => 'Лог ошибок'));
			$table->AddHead('type', 'Тип', FALSE, '50px');
			$table->AddHead('value', 'Значение');
			$table->AddData($debug_log);
			return $table->out();
		}
		else
		{
			return '';
		}
	}
	
	function AddContent($aContent)
	{
		$this->content .= $aContent;
	}
	
	function out()
	{
		$this->Header = str_replace('BASEPATH', WEBPATH, $this->Header);
		$this->Header = str_replace('%title%', $this->title, $this->Header);
		$this->_PrepareRootMenu();
		$this->_PrepareLeftMenu();
		$left_menu = '';
		$content_start = '<div class="span-20 last" id="main_contetnt_div">';
		$content_stop  = '</div>';
		if ($this->out_left_menu)
		{
			$left_menu = $this->leftMenu;
		}
		else
		{
			$content_start = '<div class="span-24 last">';
			$content_stop  = '</div>';
		}
		
		
		return $this->Header.$this->rootMenu.$left_menu.
				$content_start.$this->content.
				implode('', $this->Boxs).
				$content_stop.
				$this->GetDebugLog().
				$this->Footer;
	}
}

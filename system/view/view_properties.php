<?php

class View_Properties extends View {
	
	private $data;
	
	function AddTitle ($caption)
	{
		$this->data[] = array('type' => 0, 'caption' => $caption, 'value' => '');
	} 
	
	function AddProperty($caption, $value)
	{
		$this->data[] = array('type' => 1, 'caption' => $caption, 'value' => $value);
	}
	
	
	function out()
	{
		$str = '<table border="0" cellpadding="0" cellspacing="0" class="table_properties"> <tbody>';
		if (is_array($this->data))
		{
			foreach ($this->data as $item)
			{
				if ($item['type'] == 0)
				{
					//create header
					$str .= '<tr><td class="properties_title" colspan="2">'.$item['caption'].'</td></tr>';
				}
				else
				{
					//create property values
					$str .= '<tr> <td class="properties_label"><span>'.$item['caption'].'</span></td>
							 <td class="properties_value">'.$item['value'].'</td> </tr>';
				}
			}
		}
		$str .= '</tbody></table>';
		return $str;
	}
}
<?php

class View_Table extends View {
	
	private $headers;
	private $footers;
	private $rows;
	private $row_position = 0;
	private $template;
	public $caption = FALSE;
	private $row_counter = 0;
	public $edit_link = '';
	
	const column_type_default = 0;
	const column_type_number = 1;  
	const column_type_bool = 2;  
	const column_type_bool_inverse = 3;
	const column_type_autonum = 4;
	const column_type_edit = 5;
	const column_type_no_zero = 6;
	 
	function AddHead($aName, $aTitle, $aType = 0, $aWidth = FALSE)
	{
		$this->headers[] = array(
			'name' => $aName,
			'title' => $aTitle,
			'type' => $aType,
			'width' => $aWidth
		);
	}
	
	function addFoot($content)
	{
		$this->footers[] = $content;
	}
	
	function SetCaption($aCaption)
	{
		$this->caption = $aCaption;
	}
	
	function AddCell($aName, $aContent)
	{
		$this->rows[$this->row_position][$aName] = $aContent; 
	}
	
	function AddRow($aValues)
	{
		if (is_array($aValues))
		{
			foreach ($aValues as $key => $item)
			{
				$this->AddCell($key, $item);
			}
			$this->NextRow();
		}
	}
	
	
	function NextRow()
	{
		$this->row_position++;
	}
	
	function AddData($aValues)
	{
		if (is_array($aValues))
		{
			foreach ($aValues as $item)
			{
				$this->AddRow($item);
			}
		}
	}
	
	function _PrintStyle($pContent, $pType = 0)
	{
		if ($pType == self::column_type_default)
		{
			if ($pContent != '')
			{
				return $pContent;
			}
			else
			{
				return '&nbsp;';
			}
		}
		elseif ($pType == self::column_type_number)
		{
			$color = 'black';
			if ($pContent < 0)
			{
				$color = 'red';
			}
			elseif ($pContent > 0)
			{
				$color = 'blue';
			}
			return '<span style="color: '.$color.'">'.$pContent.'</span>';
		}
		elseif ($pType == self::column_type_no_zero)
		{
			if ($pContent != 0)
			{
				return $pContent;
			}
			else
			{
				return '&nbsp;';
			}
		}
		elseif ($pType == self::column_type_bool)
		{
			$color = 'red';
			if ($pContent)
			{
				$color = 'green';
			}
			return '<span style="background-color: '.$color.';">&nbsp;&nbsp;&nbsp;&nbsp;</span>';
		}
		elseif ($pType == self::column_type_bool_inverse)
		{
			$color = 'green';
			if ($pContent)
			{
				$color = 'red';
			}
			return '<span style="background-color: '.$color.';">&nbsp;&nbsp;&nbsp;&nbsp;</span>';
		}
		elseif ($pType == self::column_type_autonum)
		{
			$this->row_counter++;
			
			return $this->row_counter;
		}
		elseif ($pType == self::column_type_edit)
		{
			
			return '<a class="edit_link" href="'.WEBPATH.$this->edit_link.$pContent.'" >edit</a>'; 
		}
		
	}
	
	function out()
	{
		$columns_count = 0;
		
		$str = '<table border="0" cellpadding="0" cellspacing="0" class="sortable">';
		
		if ($this->caption)
		{
			$str .= '<caption>'.$this->caption.'</caption>';
		}
		
		// generating headers
		if (is_array( $this->headers))
		{
			$columns_count = count($this->headers);
			$str .= '<thead><tr>';
			foreach ($this->headers as $header)
			{
				$str .= '<th ';
				if ($header['width'])
				{
					$str .= 'style="width: '.$header['width'].';"';
				}
				$str .= '>'.$header['title'].'</th>';
			}
			
			$str .= '</tr></thead>';
		}
		//generate body
		if (is_array($this->rows))
		{
			$str .= '<tbody>';
			foreach ($this->rows as $row)
			{
				$str .= '<tr onClick="$(this).toggleClass(\'select\'); ">';
				if (is_array( $this->headers))
				{
					foreach ($this->headers as $header)
					{
						if ($header['type'] != self::column_type_autonum)
						{
							$str .= '<td>'.$this->_PrintStyle(@$row[$header['name']], $header['type']).'</td>';
						}
						else
						{
							$str .= '<td>'.$this->_PrintStyle(0, $header['type']).'</td>';
						}
					}
				}
				else
				{
					foreach ($row as $cell)
					{
						$str .= '<td>'.$cell.'</td>';
					}
				}
				
				$str .= '</tr>';
			}
			$str .= '</tbody>';
		}
		
		if (is_array( $this->footers))
		{

			$str .= '<tfoot><tr>';
			foreach ($this->footers as $header)
			{
				$str .= '<td';
				$str .= '>'.$header.'</td>';
			}
			
			$str .= '</tr></tfoot>';
		}
		
		
		$str .= '</table>';
		return $str;
	}
	
	
}

var selected = 0 ;

$(document).keydown(function(e) {
	//$('.container').text(e.keyCode);
	if (e.keyCode == 113)
	{
		window.location.href = WEBPATH+'billing/find';		
	}
	
	if (e.keyCode == 115)
	{
		window.location.href = WEBPATH+'network/find';		
	}
});


$(document).ready(function(){
	$(':input:first').not('.date').focus();
});

function select_port(port, WEBPATH)
{
	if (selected != port)
	{
		$('#info_container').text('Загрузка...');
		$('#port_'+selected).removeClass('highlight');
		$('#port_'+port).addClass('highlight');
		$('#info_container').load(WEBPATH+'ajax/port_info/info/'+port);
		selected = port;
	}
}


function guidGenerator() {
    var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}


function do_editble_edit(item, url)
{
	$(item).parent().load(WEBPATH+url+'/'+encodeURI($(item).val()));
}

function make_edit(item, value, url, maskedit)
{
	var id = guidGenerator();
	
	$(item).parent().html('<input id="'+id+'" name="'+id+'" class="text" value="'+value+'" style="margin-top: -3px;" onchange="do_editble_edit(this, \''+url+'\')" onblur="do_editble_edit(this, \''+url+'\')">');
	
	if (maskedit)
	{
		$('#'+id).mask(maskedit);
	}
	$('#'+id).focus();
}


function make_new_flat(gid, port)
{
	var parentCell = $('#port_flat_'+port).parent();
	$(parentCell).html('<input style="padding: 0; border: none; width: 100%" id="input_flat_'+port+'">');
	$('#input_flat_'+port).focus();
	$('#input_flat_'+port).change(function() {
		//$('#input_flat_'+port).focusout();
	});
	$('#input_flat_'+port).focusout(function() {
		if ($(this).val() != ''){
			
			$('#info_container').load(WEBPATH+'ajax/port_info/addflat/'+gid+'/'+port+'/'+encodeURI($(this).val()));
			//$(this).parent().parent().load(WEBPATH+'ajax/port_info/addflat/'+gid+'/'+port+'/'+$(this).val());
			//$(this).parent().load('WEBPATHdevices.addNewFlat/add/'+$(this).parent().parent().attr("id")+'/'+$(this).val() );
			$(this).parent().html($(this).val());
		} else {
			$(this).parent().html('<span style="display: block; width: 100%; height: 20px;" ondblclick="make_new_flat('+gid+', '+port+')" id="port_flat_'+port+'"> </span>'); 
		}
		$(this).parent().parent().click();
	});
}

function make_new_comment(port)
{
	var parentCell = $('#port_comment_'+port).parent();
	$(parentCell).html('<input style="padding: 0; border: none; width: 100%" id="input_comment_'+port+'" value="'+$(parentCell).text()+'">');
	$('#input_comment_'+port).focus();
	$('#input_comment_'+port).focusout(function() {
		$('#info_container').load(WEBPATH+'ajax/port_info/addcomment/'+port+'/'+encodeURI($(this).val()));
		$(this).parent().html('<span style="display: block; width: 100%; height: 20px;" ondblclick="make_new_comment('+port+')" id="port_comment_'+port+'">'+$(this).val()+'</span>'); 
	});	
}

function block_port(port)
{
	
	$('#port_block_'+port).text('...');
	$('#port_block_'+port).load(WEBPATH+'ajax/device_manage/block_port/'+port);
	$('#info_container').load(WEBPATH+'ajax/port_info/info/'+port);
		
}

function device_add_tag(id, tag)
{
	$.get(WEBPATH+'ajax/device_manage/add_tag/'+id+'/'+tag, function(data) {
		window.location.reload();
	});
	
}

function device_delete_tag(id, tag)
{
	$.get(WEBPATH+'ajax/device_manage/delete_tag/'+id+'/'+tag, function(data) {
		window.location.reload();
		});
	
}

function billing_set_block(id, block)
{
	$.get(WEBPATH+'billing/ajax_users/block/'+id+'/'+block, function(data) {
		window.location.reload();
		});
	
}

function unset_filter_padi(id, block, mac)
{
	$.get(WEBPATH+'billing/allowpadi/unset/'+id+'/'+block, function(data) {
                window.location.reload();
                });
}

function bind_request_list_load()
{
	$('select[name="servicer"]').change(function(){request_list_load();});
	$('input[name="date"]').change(function(){request_list_load();});
}

function request_list_load()
{
	var servicer = $('select[name="servicer"]').val();
	var date = $('input[name="date"]').val();
	$('#ajax_request_list').text('Загрузка...');
	$('#ajax_request_list').load(WEBPATH+'ajax/request_load/get_list/'+servicer+'/'+date);
}


function load_data(url)
{
	
	$('#box_show_info_content').text('Загрузка...');
	$('#box_show_info_content').load(WEBPATH+url);
	new Boxy($('#box_show_info_content'), {title: '  ', modal: true});
	
	//$('#box_show_info').show();
	//$('#box_show_info_content').text('Загрузка...');
	//$('#box_show_info_content').load(WEBPATH+url);
}

function device_exec(id, func)
{

	
	//$('#box_show_info').show();
	//$('#box_show_info_content').text('Загрузка...');
	//$('#box_show_info_content').load(WEBPATH+'ajax/device_manage/exec/'+id+'/'+func);
	load_data('ajax/device_manage/exec/'+id+'/'+func);
}

function device_exec_port(id, func, port)
{	
	load_data('ajax/device_manage/port_exec/'+id+'/'+func+'/'+port);
	//Boxy.load(WEBPATH+'ajax/device_manage/port_exec/'+id+'/'+func+'/'+port, {title: '  ', modal: true});
	//$('#box_show_info').show();
	//$('#box_show_info_content').text('Загрузка...');
	//$('#box_show_info_content').load(WEBPATH+'ajax/device_manage/port_exec/'+id+'/'+func+'/'+port);
	//new Boxy($('#box_show_info_content'), {title: '  ', modal: true});
}
